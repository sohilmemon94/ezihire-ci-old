<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Interviewschedule extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));

        $this->load->model("Common_model");
        $this->load->model("Interviewschedule_model");

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }
    }

    // _is_customer_login for admin
    // _is_customer_login for customer

  
    public function deleteimages(){

        $data_insert = array(
            "image"=>NULL
        );
        $id=$_POST['id'];
        $this->load->model("common_model");      
        $category=$this->common_model->data_update("tbl_customers", $data_insert, array("customer_id" => $id));
        if($category){
            echo 1;
        } else {
            echo 0;
        }
    }


    public function change_status() {
        $table = $this->input->post("table");
        $id = $this->input->post("id");
        $on_off = $this->input->post("on_off");
        $id_field = $this->input->post("id_field");
        $status = $this->input->post("status");

        $this->Common_model->data_update($table, array("$status" => $on_off), array("$id_field" => $id));
        echo $_POST['on_off'];
    }

    public function interviewschedule_list() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');

            $my_permission = get_my_permission();

            if(in_array('interviewschedule_list',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "Interview Schedule";
                $data['page'] = "Interview Schedule";
                $data['action'] = "List";

                $this->load->model("Technology_model");
                $technologies = $this->Technology_model->get_active_technology_list();
                $data['technologies'] = $technologies;

                $this->load->model("Vendors_model");
                $vendors = $this->Vendors_model->get_vendors_by_role();
                $data['vendors'] = $vendors;
                $data['customer_role_type'] = $customer_role_type;
                
                $this->load->view('admin/interviewschedule/list', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_interviewschedule_list(){
        if(_is_customer_login($this)) {
            
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('interviewschedule_list',$my_permission)) {

                $search_array = array();
                if(isset($_POST['extra_search']) && !empty($_POST['extra_search'])) {
                    foreach ($_POST['extra_search'] as $key => $value) {
                        $search_array[$value['name']] = $value['value'];
                    }
                }

                $this->load->model("interviewschedule_model");
                $fetch_data = $this->interviewschedule_model->get_interviewschedule_list_by_ajax($user_id,$customer_role_type,$search_array);
                    
                $data = array();  
                $no = 1;
                foreach($fetch_data as $row)  
                {  
                    $customer_id  = $row->id;
                    $date = date('d-M-Y',strtotime($row->post_date));

                    if(isset($row->interview_one_date) && !empty($row->interview_one_date)) {
                        $interview_one_date = date('d-M-Y',strtotime($row->interview_one_date));
                    } else {
                        $interview_one_date = "-";
                    }

                    if(isset($row->interview_two_date) && !empty($row->interview_two_date)) {
                        $interview_two_date = date('d-M-Y',strtotime($row->interview_two_date));
                    } else {
                        $interview_two_date = "-";
                    }
                    $username = $row->username;
                    $created_by_name = $row->created_by_name;
                    $candidate_name = $row->candidate_name;
                    $tname = $row->tname;
                    $budget = $row->budget;
                    $experience = $row->experience;
                    
                    $status = $row->status;
                       
                    
                    $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));

                    $action = '';

                    $edit_link = '';
                    if(in_array('interviewschedule_edit', $my_permission)) {
                        $edit_link.= ' <a href="'.site_url("interviewschedule/interviewschedule_edit/".$customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-edit"></i></a>';
                    }

                    $delete_link = '';
                    if(in_array('interviewschedule_delete', $my_permission)) {
                        $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                        $delete_link .= "confirm_delete('";
                        $delete_link .= site_url("interviewschedule/interviewschedule_delete/".$customer_id);
                        $delete_link .= "'";
                        $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                    }

                    $action  =  $edit_link.$delete_link;



                    $sub_array = array();  
                    $sub_array[] = $no;  
                    $sub_array[] = $interview_one_date;  
                    $sub_array[] = $interview_two_date;  
                    $sub_array[] = $created_by_name;  
                    $sub_array[] = $username;  
                    $sub_array[] = $candidate_name;  
                    $sub_array[] = $tname;  
                    $sub_array[] = $status;  
                    $sub_array[] = $action;  

                    $data[] = $sub_array;  
                    $no = $no+1;
                }
                $output = array(  
                    "draw"                    =>     intval($_POST["draw"]),  
                    "recordsTotal"          =>      $this->interviewschedule_model->get_all_interviewschedule_result($user_id,$customer_role_type,$search_array),  
                    "recordsFiltered"     =>     $this->interviewschedule_model->get_filtered_data_interviewschedule($user_id,$customer_role_type,$search_array),  
                    "data"                    =>     $data  
                ); 
                echo json_encode($output);  
           }
        } else {
            echo "Not access this function";
        }
    }

    public function interviewschedule_report_list() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');

            $my_permission = get_my_permission();

            if(in_array('interviewschedule_list',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Interview Schedule Reports";
                $data['page'] = "Interview Schedule Reports";
                $data['action'] = "List";

                $this->load->model("Technology_model");
                $technologies = $this->Technology_model->get_active_technology_list();
                $data['technologies'] = $technologies;

                $this->load->model("Vendors_model");
                $vendors = $this->Vendors_model->get_vendors_by_role_user();
                $data['vendors'] = $vendors;
                $data['customer_role_type'] = $customer_role_type;
                
                $this->load->view('admin/interviewschedule/reportlist', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_interviewschedule_report_list(){
        if(_is_customer_login($this)) {
            
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('interviewschedule_list',$my_permission)) {

                $search_array = array();
                if(isset($_POST['extra_search']) && !empty($_POST['extra_search'])) {
                    foreach ($_POST['extra_search'] as $key => $value) {
                        $search_array[$value['name']] = $value['value'];
                    }
                }

                $this->load->model("interviewschedule_model");
                $fetch_data = $this->interviewschedule_model->get_interviewschedule_list_by_ajax($user_id,$customer_role_type,$search_array);
                // echo $this->db->last_query();
                // exit;
                
                $data = array();  
                $no = 1;
                foreach($fetch_data as $row)  
                {  
                    $customer_id  = $row->id;
                    $date = date('d-M-Y',strtotime($row->post_date));

                    if(isset($row->interview_one_date) && !empty($row->interview_one_date)) {
                        $interview_one_date = date('d-M-Y',strtotime($row->interview_one_date));
                    } else {
                        $interview_one_date = "-";
                    }

                    if(isset($row->interview_two_date) && !empty($row->interview_two_date)) {
                        $interview_two_date = date('d-M-Y',strtotime($row->interview_two_date));
                    } else {
                        $interview_two_date = "-";
                    }
                    $username = $row->username;
                    $created_by_name = $row->created_by_name;
                    $candidate_name = $row->candidate_name;
                    $tname = $row->tname;
                    $budget = $row->budget;
                    $experience = $row->experience;
                    
                    $status = $row->status;
                       
                    
                    $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));

                    $action = '';

                    $edit_link = '';
                    if(in_array('interviewschedule_edit', $my_permission)) {
                        $edit_link.= ' <a href="'.site_url("interviewschedule/interviewschedule_edit/".$customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-edit"></i></a>';
                    }

                    $delete_link = '';
                    if(in_array('interviewschedule_delete', $my_permission)) {
                        $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                        $delete_link .= "confirm_delete('";
                        $delete_link .= site_url("interviewschedule/interviewschedule_delete/".$customer_id);
                        $delete_link .= "'";
                        $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                    }

                    $action  =  $edit_link.$delete_link;

                    $sub_array = array();  
                    $sub_array[] = $no;  
                    $sub_array[] = $interview_one_date;  
                    $sub_array[] = $interview_two_date;  
                    $sub_array[] = $created_by_name;  
                    $sub_array[] = $username;  
                    $sub_array[] = $candidate_name;  
                    $sub_array[] = $tname;  
                    $sub_array[] = $status;  

                    $data[] = $sub_array;  
                    $no = $no+1;
                }
                $output = array(  
                    "draw"                    =>     intval($_POST["draw"]),  
                    "recordsTotal"          =>      $this->interviewschedule_model->get_all_interviewschedule_result($user_id,$customer_role_type,$search_array),  
                    "recordsFiltered"     =>     $this->interviewschedule_model->get_filtered_data_interviewschedule($user_id,$customer_role_type,$search_array),  
                    "data"                    =>     $data  
                ); 
                echo json_encode($output);  
           }
        } else {
            echo "Not access this function";
        }
    }

    public function interviewschedule_add($postjob_id=NULL) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('interviewschedule_add',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Ourteam";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Interview Schedule";
                    $data['action'] = "Add";
                    $data['customer_role_type'] = $customer_role_type;
                    $data['postjob_id'] = $postjob_id;

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {

                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('postjob_id', 'Post Job', 'trim|required');
                        $this->form_validation->set_rules('interview_date', 'Interview Date', 'trim|required');
                        
                        $this->form_validation->set_rules('candidate_name', 'Candidate Name', 'trim|required');
                        
                        if ($this->form_validation->run() == FALSE) {
                            $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                        } else {
                            $date = date('Y-m-d');
                            $postjob_id = $this->input->post("postjob_id") ? $this->input->post("postjob_id") : '';
                            $candidate_name = $this->input->post("candidate_name") ? $this->input->post("candidate_name") : '';
                            $interview_date = $this->input->post("interview_date") ? $this->input->post("interview_date") : '';
                            $status = 'Proposed';
                            
                            // get employee permission records
                            $this->load->model("Postjob_model");
                            $postjob = $this->Postjob_model->get_postjob_list_by_id($postjob_id);

                            if(isset($postjob) && !empty($postjob)) {
                                $data = array();
                                $this->load->model("common_model");
                                $created_at = date('Y-m-d H:i:s');
                                $data_insert = array(
                                    "postjob_id"=>$postjob->id,
                                    "vendor_id"=>$postjob->vendor_id,
                                    "skill_id"=>$postjob->skill_id,
                                    "subskills"=>$postjob->subskills,
                                    "duration"=>$postjob->duration,
                                    "description"=>$postjob->description,
                                    "experience"=>$postjob->experience,
                                    "budget"=>$postjob->budget,
                                    "no_of_position"=>$postjob->no_of_position,
                                    "no_of_interview"=>$postjob->no_of_interview,
                                    "candidate_availability"=>$postjob->candidate_availability,
                                    "candidate_name"=>$candidate_name,
                                    "interview_one_date"=>date('Y-m-d',strtotime($interview_date)),
                                    "status"=>$status,
                                    "post_date"=>$postjob->date,
                                    "created_by"=>$admin_id,
                                    "created_at"=>$created_at,
                                    "updated_at"=>$created_at,
                                );

                                $last_id = $this->common_model->data_insert("tbl_interviewschedule", $data_insert, TRUE);

                                $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Interview Schedule created successfully</div>');
                                redirect("interviewschedule/interviewschedule_list");
                                exit;
                            }
                        }
                    }
                        
                    $this->load->model("Technology_model");
                    $technologies = $this->Technology_model->get_active_technology_list();
                    $data['technologies'] = $technologies;

                    $this->load->model("postjob_model");
                    $postjob = $this->postjob_model->get_total_postjob($admin_id,$customer_role_type,'active');
                    $data['postjobs'] = $postjob;
                    $data['admin_id'] = $admin_id;


                    $this->load->view('admin/interviewschedule/add', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                } 
            } else {
                redirect("admin"); exit;
            }    

        } else {
           redirect("admin"); exit;
        }
    }

    public function interviewschedule_edit($id) {
        if (_is_customer_login($this)) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $my_permission = get_my_permission();

                    if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('interviewschedule_edit',$my_permission)) {

                        $data = array();
                        $data["error"] = "";
                        $data["pageTitle"] = "Admin Our Team";
                        $data['admin'] = "Admin";
                        $data["title"] = PROJECT_NAME;
                        $data['page'] = "Interview Schedule";
                        $data['action'] = "Edit";
                        $data['customer_role_type'] = $customer_role_type;  

                    $interviewschedule = $this->Interviewschedule_model->get_interviewschedule_list_by_id($id);

                    if(isset($interviewschedule) && !empty($interviewschedule)) {

                        $exist_vendor_id = $interviewschedule->vendor_id;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }

                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {

                            $data["interviewschedule"] = $interviewschedule;

                            if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {
                            
                                $this->load->library('form_validation');
                                $this->form_validation->set_rules('comment', 'comment', 'trim|required');
                                $this->form_validation->set_rules('status', 'status', 'trim|required');
                               
                                if ($this->form_validation->run() == FALSE) {
                                    $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                                } else {

                                    $date = date('Y-m-d');
                                    $interview_two_date = $this->input->post("interview_two_date") ? $this->input->post("interview_two_date") : '';
                                    $comment = $this->input->post("comment") ? $this->input->post("comment") : '';
                                    $status = $this->input->post("status") ? $this->input->post("status") : '';
                                    $interview_date = $this->input->post("interview_date") ? $this->input->post("interview_date") : '';
                                    $data = array();
                                    $this->load->model("common_model");
                                    $created_at = date('Y-m-d H:i:s');
                                   
                                    $data_insert = array(
                                        "interview_two_date"=>date('Y-m-d',strtotime($interview_date)),
                                        "comment"=>$comment,
                                        "status"=>$status,
                                        "updated_at"=>$created_at,
                                    );

                                 
 
                                    $this->common_model->data_update("tbl_interviewschedule", $data_insert, array("id" => $id));

                                    $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Interview Schedule updated successfully</div>');
                                        redirect("interviewschedule/interviewschedule_list");
                                    exit;
                                }
                            }

                            $this->load->model("Technology_model");
                            $technologies = $this->Technology_model->get_active_technology_list();
                            $data['technologies'] = $technologies;

                            $this->load->model("postjob_model");
                            $postjob = $this->postjob_model->get_total_postjob($admin_id,$customer_role_type);
                            $data['postjobs'] = $postjob;

                            $data['admin_id'] = $admin_id;
                            // dd($data);

                            $this->load->view('admin/interviewschedule/edit', $data);
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                            redirect('interviewschedule/interviewschedule_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('interviewschedule/interviewschedule_list');
                        exit;
                    }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('admin/dashboard');
                        exit;
                    }  
                }  else {
                    redirect("admin"); exit;
                }   
            
        } else {
           redirect("admin"); exit;
        }
    } 

    public function interviewschedule_view($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('interviewschedule_view',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Our Team";
                $data['admin'] = "Admin";
                $data["title"] = PROJECT_NAME;
                $data['page'] = "Household";
                $data['action'] = "View";

                $admin_id = $this->session->userdata('admin_id');
                $role =  $this->session->userdata('customer_role_id');

                $this->load->model("Interviewschedule_model");
                $interviewschedule = $this->Interviewschedule_model->get_interviewschedule_list_by_id($id);

                $data["interviewschedule"] = $interviewschedule;

                $this->load->model("location_model");
                $locations = $this->location_model->all();
                $data['locations'] = $locations;

                $this->load->view('admin/interviewschedule/view', $data);
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }       
        } else {
           redirect("admin"); exit;
        }
    } 

    public function interviewschedule_delete($id) {

        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('interviewschedule_delete',$my_permission)) {

                    $interviewschedule = $this->Interviewschedule_model->get_interviewschedule_list_by_id($id);
                    if(isset($interviewschedule) && !empty($interviewschedule)) {
                        $data = array();
                        $data['id'] = $id;

                        $exist_vendor_id = $interviewschedule->vendor_id;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }

                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {

                            $this->load->model("common_model");
                            $this->common_model->data_remove('tbl_interviewschedule',$data);

                            $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Interview Schedule deleted successfully</div>');
                            redirect("interviewschedule/interviewschedule_list");
                            exit;
                        
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                            redirect('interviewschedule/interviewschedule_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('interviewschedule/interviewschedule_list');
                        exit;
                    }

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }  
            }  else {
                redirect("admin"); exit;
            }   
            
        } else {
           redirect("admin"); exit;
        }
    } 

    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }

    public function check_email_exist() {
        $email = $this->input->post("email");
        if(!empty($email)) {
            $this->load->model("interviewschedule_model");
            $check_email_exist = $this->interviewschedule_model->check_email_exist($email);

            if($check_email_exist>0){
                 echo "false";
                 exit;
            } else {
                 echo "true";
                 exit;
            }
        }
    }
}
?>