<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');

class Clients extends CI_Controller {

    public function __construct() {
        parent::__construct();
            // Your own constructor code
            $this->load->database();
            $this->load->helper('login_helper');
            $this->load->model("common_model");
            $this->load->library('javascript');
            $this->load->library('form_validation');
            $this->load->library('email');
            $this->load->library('session');
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $this->load->helper(array('form', 'url'));
            
            if ($_SERVER['HTTP_HOST'] == "localhost") {
                $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
            } else {
                $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
            }
    }

    public function clients_list() {
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $data["error"] = "";
            $data['page'] = "Clients";
            $data['action'] = "List";


            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();

                if(in_array('clients_list',$my_permission)) {

                        $this->load->model("clients_model");
                        $clients = $this->clients_model->get_all_clients_list();
                        $data['clients'] = $clients;
                        $this->load->view('admin/clients/list', $data);

                } else { 
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }
            } else {
                redirect("admin"); exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function add_clients(){
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            $data["error"] = "";
            $data['page'] = "Clients";
            $data['action'] = "List";


            if($role >= 1 && $admin_id >= 1) {
                
                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();
                if(in_array('add_clients',$my_permission)) {

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {
                       
                        $bname = $this->input->post("bname") ? $this->input->post("bname") : '';
                        $status = ($this->input->post("status") == "on") ? '1' : '0';
                        $ct = date('Y-m-d H:i:s');
                        $strtotime = strtotime($ct);
                        $this->load->model("common_model");

                        $ct = date('Y-m-d H:i:s');
                        $file_namewithpath='';
                        
                        if(isset($_FILES) && !empty($_FILES["clients_images"])) {
                            if ($_FILES["clients_images"]["size"] > 0) {
                                $config['upload_path'] = $this->config->item('clients_images_path');
                                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                                $this->upload->initialize($config);
                                $this->load->library('upload', $config);
                                $temp = explode(".", $_FILES["clients_images"]["name"]);
                               
                                $newfilename = time() . '.' . end($temp);

                                if (!$this->upload->do_upload('clients_images', $newfilename)) {
                                    $error = array('error' => $this->upload->display_errors());
                                } else {
                                    $img_data = $this->upload->data();
                                    $file_name = $img_data['file_name'];
                                    $error = array();
                                }
                                $file_namewithpath = $file_name;
                            }
                        } 

                        $data_insert_menu = array(
                            "bname" => $bname,
                            "bimage"=>$file_namewithpath,
                            "status"=>$status,
                            "created_at"=>$ct,
                            "updated_at"=>$ct,
                        );

                        $temp =  $this->common_model->data_insert("tbl_clients", $data_insert_menu, TRUE);
                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert">
                                            <i class="fa fa-check"></i>
                                          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                          <strong>Success ! </strong> Clients created successfully.</div>');
                        redirect("clients/clients_list");
                        exit;
                    }

                    $this->load->view('admin/clients/add', $data);

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }
            } else {
                redirect("admin"); exit;
            }
        } else {
            redirect("admin"); exit;
        }
    }

    public function edit_clients($id) {
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            $data["error"] = "";
            $data['page'] = "Clients";
            $data['action'] = "Edit";

            $this->load->model("clients_model");
            $menuDetails = $this->clients_model->get_clients_by_id($id);
            $data['clients'] = $menuDetails;
            $file_namewithpath = $menuDetails->bimage;

            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();

                if(in_array('edit_clients',$my_permission)) {
                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {
                        
                        $bname = $this->input->post("bname") ? $this->input->post("bname") : '';
                        $status = ($this->input->post("status") == "on") ? '1' : '0';
                        $ct = date('Y-m-d H:i:s');
                        
                        $this->load->model("common_model");
                        
                        if(isset($_FILES) && !empty($_FILES["clients_images"])) {
                            if ($_FILES["clients_images"]["size"] > 0) {
                                $config['upload_path'] = $this->config->item('clients_images_path');
                                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                               
                                $this->upload->initialize($config);
                                $this->load->library('upload', $config);
                                $temp = explode(".", $_FILES["clients_images"]["name"]);
                                $newfilename = time() . '.' . end($temp);

                                if (!$this->upload->do_upload('clients_images', $newfilename)) {
                                    $error = array('error' => $this->upload->display_errors());
                                } else {
                                    $img_data = $this->upload->data();
                                    $file_name = $img_data['file_name'];
                                    $error = array();
                                }
                                $file_namewithpath = $file_name;
                            }
                        } 
                    
                        $data_update_menu = array(
                            "bname" => $bname,
                            "bimage"=>$file_namewithpath,
                            "status"=>$status,
                            "updated_at"=>$ct,
                        );

                      
                        $this->common_model->data_update("tbl_clients", $data_update_menu, array("id" => $id));

                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert">
                                            <i class="fa fa-check"></i>
                                          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                          <strong>Success ! </strong> clients updated successfully.</div>');
                       
                         redirect("clients/clients_list");
                        exit;
                    }
                    $this->load->view('admin/clients/edit', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }

            } else {
                redirect("admin"); exit;
            }
        } else {
            redirect("admin"); exit;
        }
    }

    public function clients_delete($id){
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            $data["error"] = "";
            $data["pageTitle"] = "Event List";
            $data["title"] = "Party Locator";
            $data['admin'] = "Business";
            $data['page'] = "Future Events";
            $data['action'] = "Event Details";

            $this->load->model("clients_model");
            $menuDetails = $this->clients_model->get_clients_by_id($id);
            $clients_id = $menuDetails->id;
          

                if($role >= 1 && $admin_id >= 1) {

                    $customer_role_type =  $this->session->userdata('customer_role_type');
                    $my_permission = get_my_permission();
                    if(in_array('clients_delete',$my_permission)) {

                        $this->db->query("DELETE FROM `tbl_clients` WHERE `id` = '" . $clients_id . "' ");
                        $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <strong>Success ! </strong> clients deleted successfully
                                      </div>');
                        redirect("clients/clients_list");
                        exit;
                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('admin/dashboard');
                        exit;
                    }
                    
               } else {
                redirect("admin"); exit;
                exit;
            } 
        }
    }

    public function get_random_string($length = 10) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }
}
