<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');

class Login extends CI_Controller {

    public function __construct() {

        header("Cache-Control: no cache");
        session_cache_limiter("private_no_expire");

        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');

        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));
        
        if ($_SERVER['HTTP_HOST'] == "localhost") {
            $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/";
        } else {
            $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/";
        }

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }  
    }


     public function index() {
        if (_is_customer_login($this)) {
            redirect(_get_customer_redirect($this));
        } else {
            $data = array();
            if (isset($_POST)) {
                $this->load->library('form_validation');

                $this->form_validation->set_rules('email', 'Email', 'trim|required');
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    if ($this->form_validation->error_string() != "") {
                        $data["error"] = '<div class="alert alert-warning alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning!</strong> ' . $this->form_validation->error_string() . '</div>';
                    }
                } else {    
                   
                    if ($_SERVER['HTTP_HOST'] != "localhost") {

                        $captcha_response = trim($this->input->post('g-recaptcha-response'));
                        if($captcha_response != '' && $_SERVER['HTTP_HOST'] != "localhost"){
                            $keySecret = $this->config->item('google_secret');
                            $check = array(
                                'secret'        =>  $keySecret,
                                'response'      =>  $this->input->post('g-recaptcha-response')
                            );

                            $startProcess = curl_init();
                            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
                            curl_setopt($startProcess, CURLOPT_POST, true);
                            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
                            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
                            $receiveData = curl_exec($startProcess);
                            $finalResponse = json_decode($receiveData, true);

                            if($finalResponse['success']){
                                $email = $this->input->post("email");
                                $password = md5($this->input->post("password"));
                                $where = array("email" => $email, "password" => $password);
                                $login = "SELECT * FROM  `tbl_customers` WHERE `username`='$email' OR `email`='$email' AND `password`='$password'";
                                $login_infos = $this->Basic->select_custom($login);
                                if (count($login_infos) > 0) {
                                    $info = $login_infos[0];
                                        if ($info['status'] == 0) {
                                            $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Currently your account is deactived,Please contact to Admin for active your account. </div>';
                                        } else {
                                            $newdata = array(
                                                'customer_user_name' => $info['username'],
                                                'customer_name' => $info['username'],
                                                'customer_email' => $info['email'],
                                                'customer_logged_in' => TRUE,
                                                'customer_id' => $info['customer_id'],
                                                'customer_image' => $info['image'],
                                                'customer_password' => $info['password'],
                                                'customer_type_id' => $info['type_id'],
                                                'customer_role_id' => $info['role_id'],
                                                'customer_role_type' => $info['role_type'],
                                            );
                                            $this->session->set_userdata($newdata);
                                            setcookie("customer_login", true, time() + (60 * 2));
                                            redirect("admin/dashboard");
                                            exit;
                                        }
                                } else {
                                    $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Invalid User and password. </div>';
                                }
                            } else {
                                $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Google Recaptcha is not verified...</div>';
                            }
                        } else {
                            $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Google Recaptcha is not verified...</div>';
                        }
                    } else {
                        $email = $this->input->post("email");
                        $password = md5($this->input->post("password"));
                        $where = array("email" => $email, "password" => $password);
                        $login = "SELECT * FROM  `tbl_customers` WHERE `username`='$email' OR `email`='$email' AND `password`='$password'";
                        $login_infos = $this->Basic->select_custom($login);
                        if (count($login_infos) > 0) {
                            $info = $login_infos[0];
                                if ($info['status'] == 0) {
                                    $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Currently your account is deactived,Please contact to Admin for active your account. </div>';
                                } else {
                                    $newdata = array(
                                        'customer_user_name' => $info['username'],
                                        'customer_name' => $info['username'],
                                        'customer_email' => $info['email'],
                                        'customer_logged_in' => TRUE,
                                        'customer_id' => $info['customer_id'],
                                        'customer_image' => $info['image'],
                                        'customer_password' => $info['password'],
                                        'customer_type_id' => $info['type_id'],
                                        'customer_role_id' => $info['role_id'],
                                        'customer_role_type' => $info['role_type'],
                                    );
                                    $this->session->set_userdata($newdata);
                                    setcookie("customer_login", true, time() + (60 * 2));
                                    redirect("admin/dashboard");
                                    exit;
                                }
                        } else {
                            $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Invalid User and password. </div>';
                        }
                    }
                }
            }
            $data["active"] = "Login";
            $data["title"] = PROJECT_NAME;
            $this->load->model("Admin_model");
            $data['adminmaster'] = $this->Admin_model->get_detail_by_id(1);
        
            $this->load->view("admin/login", $data);
        }
    }

    public function signout() {

        $customer_id = $this->session->userdata('customer_id');
        if(isset($customer_id)) {
            if($customer_id >= 1) {
                session_destroy();
                foreach($_SESSION as $k=>$v){
                    unset($_SESSION[$v]);
                }
                $this->session->sess_destroy();
                $this->session->unset_userdata($_SESSION);

                $this->session->sess_destroy();

                if (isset($_COOKIE['customer_login'])) {
                    unset($_COOKIE['customer_login']); 
                    setcookie('customer_login', null, -1, '/'); 
                } 


                $data["error"] = '<div class="alert alert-success alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success !</strong> You have logout successfully </div>';
                $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Invalid User and password. </div>';

                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong>  You have logout successfully.</div>');
                redirect("admin/login");
                exit;
            } else {
                redirect("admin/login");
                exit;
            }
        } else {
            redirect("admin/login");
            exit;
        }
    }
}
