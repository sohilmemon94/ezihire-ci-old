<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');

class Industry extends CI_Controller { 

    public function __construct() {
        parent::__construct();
       
            // Your own constructor code
            $this->load->database();
            $this->load->helper('login_helper');
            $this->load->model("common_model");
            $this->load->library('javascript');
            $this->load->library('form_validation');
            $this->load->library('email');
            $this->load->library('session');
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $this->load->helper(array('form', 'url'));
            
            if ($_SERVER['HTTP_HOST'] == "localhost") {
                $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
            } else {
                $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
            }  

            $customer_status = get_user_status();
            if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
            } else {
                auto_signout();
            }       
    } 

    public function industry_list() {
        if (_is_customer_login($this)) {

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $data["error"] = "";
            $data['page'] = "Industry";
            $data['action'] = "List";
            
            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();
              
                if(in_array('industry_list',$my_permission)) {
                    $this->load->model("industry_model");
                    $industrys = $this->industry_model->get_all_industry_list();
                    $data['industrys'] = $industrys;
                    $this->load->view('admin/industry/list', $data);
                } else { 
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }
            } else {
               redirect("admin"); exit;
            }
        } else {
          redirect("admin"); exit;
        }
    }

    public function deleteimages(){

        $data_insert = array(
            "iimage"=>NULL
        );
        $id=$_POST['id'];
        $this->load->model("common_model");      
        $category=$this->common_model->data_update("tbl_industry", $data_insert, array("id" => $id));
        if($category){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function add_industry(){
        if (_is_customer_login($this)) {
            $data["error"] = "";
            $data['page'] = "Industry";
            $data['action'] = "Add";

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            
            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();
                if(in_array('add_industry',$my_permission)) {

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {
                      
                        $vname = $this->input->post("vname") ? $this->input->post("vname") : '';
                        $category_id = $this->input->post("category_id") ? $this->input->post("category_id") : NULL;
                        $status = ($this->input->post("status") == "on") ? '1' : '0';
                        $ct = date('Y-m-d H:i:s');
                        
                        $this->load->model("common_model");

                        $ct = date('Y-m-d H:i:s');
                        $file_namewithpath=NULL;


                        if(isset($_FILES) && !empty($_FILES["industry_images"])) {
                            if ($_FILES["industry_images"]["size"] > 0) {
                                $temp = explode(".", $_FILES["industry_images"]["name"]);
                                $newfilename = time() . '.' . end($temp);
                                $uploadpath = $this->config->item('industry_images_path');
                                $returnpath = $this->config->item('industry_images_uploaded_path');
                                $file_name = $this->file_upload($_FILES["industry_images"], $uploadpath, $returnpath);
                                $file_namewithpath = $file_name;
                            } else {
                                $file_namewithpath = "";
                            }   
                        }

                        $data_insert_menu = array(
                            "iname" => $vname,
                            "iimage"=>$file_namewithpath,
                            "status"=>$status,
                            "created_at"=>$ct,
                            "updated_at"=>$ct,
                        );

                        $temp =  $this->common_model->data_insert("tbl_industry", $data_insert_menu, TRUE);
                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> industry created successfully.</div>');
                        redirect("industry/industry_list");
                        exit;
                    }

                    $this->load->view('admin/industry/add', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }

            } else {
               redirect("admin"); exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function edit_industry($id) {
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $data["error"] = "";
            $data['page'] = "Industry";
            $data['action'] = "Edit";
            
            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();

                if(in_array('edit_industry',$my_permission)) {

                    $this->load->model("industry_model");
                    $editDetails = $this->industry_model->get_industry_by_id($id);
                    $data['industry_edit'] = $editDetails;
                    $file_namewithpath = $editDetails->iimage;
                    
                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {

                        $vname = $this->input->post("vname") ? $this->input->post("vname") : '';
                        $status = ($this->input->post("status") == "on") ? '1' : '0';
                        $ct = date('Y-m-d H:i:s');
                        
                        $this->load->model("common_model");

                        $ct = date('Y-m-d H:i:s');
                        $file_namewithpath=NULL;


                        if(isset($_FILES) && !empty($_FILES["industry_images"])) {
                            if ($_FILES["industry_images"]["size"] > 0) {
                                $temp = explode(".", $_FILES["industry_images"]["name"]);
                                $newfilename = time() . '.' . end($temp);
                                $uploadpath = $this->config->item('industry_images_path');
                                $returnpath = $this->config->item('industry_images_uploaded_path');
                                $file_name = $this->file_upload($_FILES["industry_images"], $uploadpath, $returnpath);
                                $file_namewithpath = $file_name;
                            } else {
                                $file_namewithpath = $editDetails->iimage;
                            }   
                        }

                        $data_update_menu = array(
                            "iname" => $vname,
                            "iimage" => $file_namewithpath,
                            "status"=>$status,
                            "updated_at"=>$ct,
                        );

                        $this->common_model->data_update("tbl_industry", $data_update_menu, array("id" => $id));

                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> industry updated successfully.</div>');
                       
                         redirect("industry/industry_list");
                        exit;
                    }

                    $this->load->view('admin/industry/edit', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }

            } else {
               redirect("admin"); exit;
            }
        } else {
           redirect("admin"); exit;
        }
    } 

    public function industry_delete($id){
        if (_is_customer_login($this)) {
           
            $this->load->model("industry_model");
            $menuDetails = $this->industry_model->get_industry_by_id($id);
            $industry_id = $menuDetails->id;
          
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            
            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();
                if(in_array('industry_delete',$my_permission)) {

                    $this->db->query("DELETE FROM `tbl_industry` WHERE `id` = '" . $industry_id . "' ");
                    $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <strong>Success ! </strong> industry deleted successfully
                                  </div>');
                    redirect("industry/industry_list");
                    exit;
                } else { 
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }
            } else {
               redirect("admin"); exit;
                exit;
            } 
        }
    } 

    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }
    
    public function get_random_string($length = 10) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }
}
