<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));

        $this->load->model("Common_model");
        $this->load->model("Customers_model");

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }
    }

    // _is_customer_login for admin
    // _is_customer_login for customer

    public function change_status() {
        $table = $this->input->post("table");
        $id = $this->input->post("id");
        $on_off = $this->input->post("on_off");
        $id_field = $this->input->post("id_field");
        $status = $this->input->post("status");

        $this->Common_model->data_update($table, array("$status" => $on_off), array("$id_field" => $id));
        echo $_POST['on_off'];
    }

    public function deleteimages(){

        $data_insert = array(
            "image"=>NULL
        );
        $id=$_POST['id'];
        $this->load->model("common_model");      
        $category=$this->common_model->data_update("tbl_customers", $data_insert, array("customer_id" => $id));
        if($category){
            echo 1;
        } else {
            echo 0;
        }
    }


    public function approve_customers_list() {

        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');


            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');


            $my_permission = get_my_permission();

            if(in_array('approve_customers_list',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "Approve Users";
                $data['page'] = "Approve Users";
                $data['action'] = "List";
                
                $this->load->view('admin/customers/list', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_approve_customers_list(){
        if(_is_customer_login($this)) {
            
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('approve_customers_list',$my_permission)) {

                $this->load->model("customers_model");
                $fetch_data = $this->customers_model->get_customers_list_by_ajax();
                $data = array();  
                $no = 1;
                foreach($fetch_data as $row)  
                {       
                        $customer_id = $row->customer_id;
                        $random_array = array("round-success","round-primary","round-danger","round-warning");
                        $random_keys=array_rand($random_array,1);
                        $backColor =  $random_array[$random_keys];
                        $image = $row->image;
                        $imagePath= '';
                        if ($image == NULL) { 
                            $imagePath .= '<span class="round ';
                            $imagePath .= $backColor;
                            $imagePath .='">';
                            $firstCharacter = substr($row->username , 0, 1);
                            $imagePath .= trim(strtoupper($firstCharacter));
                            $imagePath .='</span>';
                        } else {

                            $imagefullpath = base_url().$image;
                            $returnpath = $this->config->item('customers_images_uploaded_path');
                        
                            if(!file_exists($imagefullpath)){
                                $imagefullpath = $returnpath.$image;
                                $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpath.'"><img src="';
                                $imagePath .= $imagefullpath;
                                $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                </span>';
                            } else {
                                $imagePath .= '<span class="round ';
                                $imagePath .= $backColor;
                                $imagePath .='">';
                                $firstCharacter = substr($row->username , 0, 1);
                                $imagePath .= trim(strtoupper($firstCharacter));
                                $imagePath .='</span>';
                            }
                        } 

                        $username = $row->username;
                        $email = $row->email;
                        $org_password = $row->org_password;
                        $role_type = $row->role_type;
                        $roletype = $row->roletype;
                        $is_social = "";
                        $readonly="";
                        $disabled="";
                        

                        $status = '';
                         $readonly = "";
                        $disabled = "";
                        $status .=' <input type="checkbox" class="js-switch tgl_checkbox" data-color="#55ce63" data-table="tbl_customers" data-status="status" data-idfield="customer_id"  data-id="';
                        $status .=$customer_id;
                        $status .='" id="cb_'.$customer_id.'"';

                         if(!in_array('customers_edit', $my_permission)) {
                            $status .="readonly ";
                            $status .="disabled ";
                        }
                        if($row->status == '1') {
                            $status .='checked';
                        } 

                        // if(in_array('customers_edit', $my_permission)) {                            
                        // } else {
                        //     $status .='readonly="readonly"';
                        //     $status .='disabled="disabled"';
                        // }

                        $status .= ' />';

                        $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));

                        $action = '';

                       
                        $view_link = '';
                        $view_link.= '<a href="'.site_url("customers/customers_view/".$row->customer_id) .'" data-toggle="tooltip"  data-placement="left" title="View"  class="btn btn-warning"><i class="fa fa-eye"></i></a>';

                        $permission_link = '';
                        if($user_id == 1 || $user_id == "1") {

                            $permission_link.= '<a href="'.site_url("customers/permission_details/".$row->customer_id) .'" data-toggle="tooltip"  data-placement="left" data-toggle="tooltip"  data-placement="left" title="Permission"  class="btn btn-primary"><i class="fa fa-unlock"></i></a>';
                        }
                        
                        $edit_link = '';
                        if(in_array('customers_edit', $my_permission)) {
                            $edit_link.= ' <a href="'.site_url("customers/customers_edit/".$row->customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-edit"></i></a>';
                        }

                        $delete_link = '';
                        if(in_array('customers_delete', $my_permission)) {
                            $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                            $delete_link .= "confirm_delete('";
                            $delete_link .= site_url("customers/customers_delete/".$row->customer_id);
                            $delete_link .= "'";
                            $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                        }

                        $action  =  $permission_link.$edit_link.$delete_link;

                        $sub_array = array();  
                        $sub_array[] = $no;  
                        $sub_array[] = $imagePath;  
                        // $sub_array[] = $customer_name;  
                        $sub_array[] = $username;  
                        $sub_array[] = $email;  
                        $sub_array[] = $org_password;  
                        
                        $sub_array[] = $roletype;  
                        $sub_array[] = $status;  
                        
                        // $sub_array[] = $created_at;  
                        // $sub_array[] = $status;  
                        $sub_array[] = $action;  

                        $data[] = $sub_array;  
                        $no = $no+1;
                }
                $output = array(  
                    "draw"                    =>     intval($_POST["draw"]),  
                    "recordsTotal"          =>      $this->customers_model->get_all_customers_result(),  
                    "recordsFiltered"     =>     $this->customers_model->get_filtered_data_customers(),  
                    "data"                    =>     $data  
                ); 
                echo json_encode($output);  
           }
        } else {
            echo "Not access this function";
        }
    }


    public function permission_details($id) {
        if (_is_customer_login($this)) {
           
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $data["error"] = "";
            $data['page'] = "Permission";
            $data['action'] = "List";
            
            if($role >= 1 && $admin_id >= 1) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Ourteam";
                $data['admin'] = "Admin";
                $data["title"] = PROJECT_NAME;
                $data['page'] = "Permission";
                $data['action'] = "Add";

                if (isset($_POST)) {

                    if(count($_POST) > 0) {

                        // remove all existing permission
                        $removed = $this->Customers_model->remove_permissions($id);
                        unset($_POST['save_button']);
                        foreach ($_POST as $key => $value) {

                            // get permission id 
                            $details = $this->Customers_model->permission_details_by_name($key);
                            
                            $data_insert = array();
                            $data_insert['permission_id'] = $details->id;
                            $data_insert['customer_id'] = $id;
                            $data_insert['permission_name'] = $key;
                            $data_insert['status'] = $value;
                            $save = $this->Common_model->data_insert("tbl_customers_permissions", $data_insert, TRUE);
                        }
                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Permission set successfully</div>');
                            redirect("customers/approve_customers_list");
                        exit;
                    }
                }
                   
                $permission = $this->Customers_model->all_permissions();

                $permission_arr = array();
                $permission_arr = array_chunk($permission, 4);
                $data['permission'] = $permission_arr;
                
                $match_permission_arr = array();
                $match_permission = $this->Customers_model->get_customers_permission_list_by_id($id);

                if(count($match_permission)>0){
                    foreach($match_permission as $kk=>$vv){
                        $match_permission_arr[] = $vv->name;
                    }
                }
                $data['match_permission_arr'] = $match_permission_arr;
       
                $this->load->view('admin/customers/permission', $data);
            } else {
               redirect("admin"); exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function customers_add() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('customers_add',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Ourteam";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Customers";
                    $data['action'] = "Add";

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {
                
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('username', 'Name', 'trim|required');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required');
                        $this->form_validation->set_rules('password', 'Password', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                        } else {
                            $username = $this->input->post("username") ? $this->input->post("username") : '';
                            $email = $this->input->post("email") ? $this->input->post("email") : '';
                            $password = $this->input->post("password") ? $this->input->post("password") : '';
                            $role_id = $this->input->post("role_id") ? $this->input->post("role_id") : '3';
                            
                        
                            $is_approve = ($this->input->post("is_approve") == "on") ? '1' : '0';
                            $status = ($this->input->post("status") == "on") ? '1' : '0';

                            if ($_FILES["image"]["size"] > 0) {

                                $temp = explode(".", $_FILES["image"]["name"]);
                                $newfilename = time() . '.' . end($temp);
                                $uploadpath = $this->config->item('customers_images_path');
                                $returnpath = $this->config->item('customers_images_uploaded_path');
                                $file_name = $this->file_upload($_FILES["image"], $uploadpath, $returnpath);
                                $logo = $file_name;
                            } else {
                                $logo = "";
                            }

                            if($role_id == "3") {
                                $role_type = "U";
                                $roletype = "User";
                            } else if($role_id == "2") {
                                $role_type = "S";
                                $roletype = "Sub Admin";
                            } else {
                                $role_type = "A";
                                $roletype = "Admin";
                            }

                            $data = array();
                            $this->load->model("common_model");
                            $created_at = date('Y-m-d H:i:s');
                            $data_insert = array(
                                "username"=>$username,
                                "email"=>$email,
                                "image"=>$logo,
                                "password"=>md5($password),
                                "org_password"=>$password,
                                "type_id"=>1,
                                "role_id"=>$role_id,
                                "role_type"=>$role_type,
                                "roletype"=>$roletype,
                                "status"=>$status,
                                "created_at"=>$created_at,
                                "updated_at"=>$created_at,
                            );

                            $this->common_model->data_insert("tbl_customers", $data_insert, TRUE);
                            $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> User created successfully</div>');
                                redirect("customers/approve_customers_list");
                                exit;
                        }
                    }
                        
                    $this->load->model("role_model");
                    $data["role"] = $this->role_model->get_active_role_list();
                    $this->load->view('admin/customers/add', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('customers/dashboard');
                    exit;
                } 
            } else {
                redirect("admin"); exit;
            }    

        } else {
           redirect("admin"); exit;
        }
    }

    public function customers_edit($id) {
        if (_is_customer_login($this)) {

            if($id=="1" || $id =="1") {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
            } else {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $my_permission = get_my_permission();

                    if(in_array('customers_edit',$my_permission)) {

                        $data = array();
                        $data["error"] = "";
                        $data["pageTitle"] = "Admin Our Team";
                        $data['admin'] = "Admin";
                        $data["title"] = PROJECT_NAME;
                        $data['page'] = "Customers";
                        $data['action'] = "Edit";

                        $this->load->model("customers_model");
                        $customers = $this->customers_model->get_customers_list_by_id($id);
                       
                        $data["customers"] = $customers;
                        
                        if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {
                            
                            $this->load->library('form_validation');
                            $this->form_validation->set_rules('username', 'Name', 'trim|required');

                            if ($this->form_validation->run() == FALSE) {
                                $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                            } else {

                                $username = $this->input->post("username") ? $this->input->post("username") : '';
                                $email = $this->input->post("email") ? $this->input->post("email") : '';
                                $password = $this->input->post("password") ? $this->input->post("password") : '';
                                $role_id = $this->input->post("role_id") ? $this->input->post("role_id") : '3';
                            
                                $is_approve = ($this->input->post("is_approve") == "on") ? '1' : '0';
                                $status = ($this->input->post("status") == "on") ? '1' : '0';

                                if($role_id == "3") {
                                    $role_type = "U";
                                    $roletype = "User";
                                } else if($role_id == "2") {
                                    $role_type = "S";
                                    $roletype = "Sub Admin";
                                } else {
                                    $role_type = "A";
                                    $roletype = "Admin";
                                }

                                if ($_FILES["image"]["size"] > 0) {

                                    $temp = explode(".", $_FILES["image"]["image"]);
                                    $newfilename = time() . '.' . end($temp);
                                    $uploadpath = $this->config->item('customers_images_path');
                                    $returnpath = $this->config->item('customers_images_uploaded_path');
                                    $file_name = $this->file_upload($_FILES["image"], $uploadpath, $returnpath);
                                   
                                    $logo = $file_name;
                                } else {
                                    $logo = $customers->image;
                                }


                                $this->load->model("common_model");
                                $ct = date('Y-m-d H:i:s');

                                $data_insert = array(
                                    "username"=>$username,
                                    "image"=>$logo,
                                    "password"=>md5($password),
                                    "org_password"=>$password,
                                    "type_id"=>1,
                                    "role_id"=>$role_id,
                                    "role_type"=>$role_type,
                                    "roletype"=>$roletype,
                                    "status"=>$status,
                                    "created_at"=>$created_at,
                                    "updated_at"=>$created_at,
                                );

                               
                                $this->common_model->data_update("tbl_customers", $data_insert, array("customer_id" => $id));

                                $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> User updated successfully</div>');
                                    redirect("customers/approve_customers_list");
                                exit;
                            }
                        }

                        $this->load->model("role_model");
                        $data["role"] = $this->role_model->get_active_role_list();

                        $this->load->view('admin/customers/edit', $data);
                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('customers/dashboard');
                        exit;
                    }  
                }  else {
                    redirect("admin"); exit;
                }   
            }
        } else {
           redirect("admin"); exit;
        }
    } 

    public function customers_view($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('customers_view',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Our Team";
                $data['admin'] = "Admin";
                $data["title"] = PROJECT_NAME;
                $data['page'] = "Household";
                $data['action'] = "View";

                $admin_id = $this->session->userdata('admin_id');
                $role =  $this->session->userdata('customer_role_id');

                $this->load->model("customers_model");
                $customers = $this->customers_model->get_customers_list_by_id($id);
                $data["customers"] = $customers;
                $this->load->model("location_model");
                $locations = $this->location_model->all();
                $data['locations'] = $locations;

                $this->load->view('admin/customers/view', $data);
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('customers/dashboard');
                exit;
            }       
        } else {
           redirect("admin"); exit;
        }
    } 

    public function customers_delete($id) {
        if (_is_customer_login($this)) {

            if($id=="1" || $id =="1") {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
            } else {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $customer_role_type =  $this->session->userdata('customer_role_type');
                    $my_permission = get_my_permission();

                    if(in_array('customers_delete',$my_permission)) {
                        $data = array();
                        $data['customer_id'] = $id;
                        $this->load->model("customers_model");
                      
                        $category = $this->customers_model->get_customers_list_by_id($id);
                        if ($category) {

                            $this->load->model("common_model");
                            $this->common_model->data_remove('tbl_customers',$data);

                            $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> User deleted successfully</div>');
                            redirect("customers/approve_customers_list");
                            exit;
                        }
                    }  else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('customers/dashboard');
                        exit;
                    }  
                }
            }  
        } else {
           redirect("admin"); exit;
        }
    } 



    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }


    public function signout() {

        $customer_id = $this->session->userdata('customer_id');
        if(isset($customer_id)) {
            if($customer_id >= 1) {
                session_destroy();
                foreach($_SESSION as $k=>$v){
                    unset($_SESSION[$v]);
                }
                $this->session->sess_destroy();
                $this->session->unset_userdata($_SESSION);

                $this->session->sess_destroy();

                if (isset($_COOKIE['customer_login'])) {
                    unset($_COOKIE['customer_login']); 
                    setcookie('customer_login', null, -1, '/'); 
                } 


                $data["error"] = '<div class="alert alert-success alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success !</strong> You have logout successfully </div>';
                $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Invalid User and password. </div>';

                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong>  You have logout successfully.</div>');
                redirect("admin/login");
                exit;
            } else {
                redirect("admin/login");
                exit;
            }
        } else {
            redirect("admin/login");
            exit;
        }
    }
    
    public function check_email_exist() {
        $email = $this->input->post("email");
        if(!empty($email)) {
            $this->load->model("Customers_model");
            $check_email_exist = $this->Customers_model->check_email_exist($email);

            if($check_email_exist>0){
                 echo "false";
                 exit;
            } else {
                 echo "true";
                 exit;
            }
        }
    }
}
?>