<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Postjob extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));

        $this->load->model("Common_model");
        $this->load->model("Postjob_model");

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }
    }

    // _is_customer_login for admin
    // _is_customer_login for customer

  
    public function deleteimages(){

        $data_insert = array(
            "image"=>NULL
        );
        $id=$_POST['id'];
        $this->load->model("common_model");      
        $category=$this->common_model->data_update("tbl_customers", $data_insert, array("customer_id" => $id));
        if($category){
            echo 1;
        } else {
            echo 0;
        }
    }


    public function change_status() {
        $table = $this->input->post("table");
        $id = $this->input->post("id");
        $on_off = $this->input->post("on_off");
        $id_field = $this->input->post("id_field");
        $status = $this->input->post("status");

        $this->Common_model->data_update($table, array("$status" => $on_off), array("$id_field" => $id));
        echo $_POST['on_off'];
    }

    public function postjob_list() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');

            $my_permission = get_my_permission();

            if(in_array('postjob_list',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "Post Job";
                $data['page'] = "Post Job";
                $data['action'] = "List";

                $this->load->model("Technology_model");
                $technologies = $this->Technology_model->get_active_technology_list();
                $data['technologies'] = $technologies;

                $this->load->model("Vendors_model");
                $vendors = $this->Vendors_model->get_vendors_by_role();
                $data['vendors'] = $vendors;
                $data['customer_role_type'] = $customer_role_type;
                
                $this->load->view('admin/postjob/list', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_postjob_list(){
        if(_is_customer_login($this)) {
            
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('postjob_list',$my_permission)) {

                $search_array = array();
                if(isset($_POST['extra_search']) && !empty($_POST['extra_search'])) {
                    foreach ($_POST['extra_search'] as $key => $value) {
                        $search_array[$value['name']] = $value['value'];
                    }
                }

                $this->load->model("Postjob_model");
                $fetch_data = $this->Postjob_model->get_postjob_list_by_ajax($user_id,$customer_role_type,$search_array);
                

                $data = array();  
                $no = 1;
                foreach($fetch_data as $row)  
                {  
                    $customer_id  = $row->id;
                    $date = date('d-M-Y',strtotime($row->post_date));
                    $username = $row->username;
                    $created_by_name = $row->created_by_name;
                    $tname = $row->tname;
                    $budget = $row->budget;
                    $experience = $row->experience ."+" ;
                    $no_of_position = $row->no_of_position;
                    $created_by = $row->created_by;

                    $status = '';
                    $readonly = "";
                    $disabled = "";
                    $status .=' <input type="checkbox" class="js-switch tgl_checkbox" data-color="#55ce63" data-table="tbl_postjob" data-status="status" data-idfield="id"  data-id="';
                    $status .=$customer_id;
                    $status .='" id="cb_'.$customer_id.'"';

                    if($created_by == $user_id) {

                        if(!in_array('postjob_edit', $my_permission)) {
                            $status .="readonly ";
                            $status .="disabled ";
                        }
                    } else {
                        if($customer_role_type == "A"){

                        } else {
                            $status .="readonly ";
                            $status .="disabled ";
                        }
                    }

                    if($row->status == '1') {
                        $status .='checked';
                    } 

                    $status .='/>';

                    $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));

                    $action = '';


                    $view_link = '';


                    $pdf_link = '';
                    $pdf_link.= '<a href="'.site_url("postjob/postjob_pdf/".$row->id) .'" data-toggle="tooltip"  data-placement="left" title="Download"  class="btn btn-primary"><i class="fa fa-download"></i></a> ';

                    if(in_array('interviewschedule_list', $my_permission)) {
                        $view_link.= ' <a href="'.site_url("postjob/postjob_interviewschedule_details/".$customer_id) .'" data-toggle="tooltip"  data-placement="left" title="View" class="btn btn-warning"><i class="fa fa-eye"></i></a>';
                    }

                    $edit_link = '';
                    if(in_array('postjob_edit', $my_permission)) {
                        $edit_link.= ' <a href="'.site_url("postjob/postjob_edit/".$customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-edit"></i></a>';
                    }

                    $delete_link = '';
                    if(in_array('postjob_delete', $my_permission)) {
                        $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                        $delete_link .= "confirm_delete('";
                        $delete_link .= site_url("postjob/postjob_delete/".$customer_id);
                        $delete_link .= "'";
                        $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                    }

                    if($created_by == $user_id) {
                        $action  =  $pdf_link.$view_link.$edit_link.$delete_link;
                    } else {
                        if($customer_role_type == "A"){
                            $action  =  $pdf_link.$view_link.$edit_link.$delete_link;
                        } else {
                            $action  =  $pdf_link.$view_link;
                        }
                    }


                    $sub_array = array();  
                    $sub_array[] = $no;  
                    $sub_array[] = $date;  
                    $sub_array[] = $created_by_name;  
                    $sub_array[] = $username;  
                    $sub_array[] = $tname;  
                    $sub_array[] = $budget;  
                    $sub_array[] = $experience;  
                    $sub_array[] = $no_of_position;  
                    $sub_array[] = $status;  
                    $sub_array[] = $action;  

                    $data[] = $sub_array;  
                    $no = $no+1;
                }
                $output = array(  
                    "draw"                    =>     intval($_POST["draw"]),  
                    "recordsTotal"          =>      $this->Postjob_model->get_all_postjob_result($user_id,$customer_role_type,$search_array),  
                    "recordsFiltered"     =>     $this->Postjob_model->get_filtered_data_postjob($user_id,$customer_role_type,$search_array),  
                    "data"                    =>     $data  
                ); 
                echo json_encode($output);  
           }
        } else {
            echo "Not access this function";
        }
    }

    public function postjob_interviewschedule_details($id) {
        if (_is_customer_login($this)) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $user_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $my_permission = get_my_permission();

                    if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('postjob_edit',$my_permission) || in_array('postjob_add',$my_permission)) {

                        $data = array();
                        $data["error"] = "";
                        $data["pageTitle"] = "Admin Our Team";
                        $data['admin'] = "Admin";
                        $data["title"] = PROJECT_NAME;
                        $data['page'] = "Post Job";
                        $data['action'] = "Details";
                        $data['customer_role_type'] = $customer_role_type;

                    $postjob = $this->Postjob_model->get_postjob_list_by_id($id);
                    if(isset($postjob) && !empty($postjob)) {

                        $exist_vendor_id = $postjob->vendor_id;
                        $exist_created_by = $postjob->created_by;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }


                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {
                            if(($exist_created_by == $user_id) || ($customer_role_type == "A" || $customer_role_type == "U")) {

                                $this->load->model("vendors_model");
                                
                                $data['admin_id'] = $admin_id;
                                $postjob = $this->Postjob_model->get_postjob_lists_by_id($id);
                                $data['postjob'] = $postjob;
                                // get postjobs

                                $this->load->model('Interviewschedule_model');
                                $interviews = $this->Interviewschedule_model->get_interviewschedule_lists_by_id($id);
                                $data['interviews'] = $interviews;
                                $data['postjob_id'] = $id;
                                
                                $this->load->view('admin/postjob/interviewschedule_list', $data);
                            } else {
                                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                                redirect('postjob/postjob_list');
                                exit;
                            }
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                            redirect('postjob/postjob_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('postjob/postjob_list');
                        exit;
                    }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('admin/dashboard');
                        exit;
                    }  
                }  else {
                    redirect("admin"); exit;
                }   
            
        } else {
           redirect("admin"); exit;
        }
    }

    public function postjob_add() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('postjob_add',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Ourteam";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Post Job";
                    $data['action'] = "Add";
                    $data['customer_role_type'] = $customer_role_type;

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {

                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('vendor_id', 'Company Name', 'trim|required');
                        $this->form_validation->set_rules('skill_id', 'Skill Name', 'trim|required');
                        
                        $this->form_validation->set_rules('duration', 'duration', 'trim|required');
                        // $this->form_validation->set_rules('description', 'description', 'trim|required');
                        $this->form_validation->set_rules('experience', 'experience', 'trim|required');
                        $this->form_validation->set_rules('budget', 'budget', 'trim|required');
                        $this->form_validation->set_rules('no_of_position', 'no_of_position', 'trim|required');
                        $this->form_validation->set_rules('no_of_interview', 'no_of_interview', 'trim|required');
                        $this->form_validation->set_rules('candidate_availability', 'candidate_availability', 'trim|required');
                        
                        if ($this->form_validation->run() == FALSE) {
                            $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                        } else {
  
                            $date = date('Y-m-d');

                            $vendor_id = $this->input->post("vendor_id") ? $this->input->post("vendor_id") : '';
                            $skill_id = $this->input->post("skill_id") ? $this->input->post("skill_id") : '';
                            $subskills = $this->input->post("subskills") ? $this->input->post("subskills") : '';
                            $duration = $this->input->post("duration") ? $this->input->post("duration") : '';
                            $description = $this->input->post("description") ? $this->input->post("description") : '';
                            $experience = $this->input->post("experience") ? $this->input->post("experience") : '';
                            $budget = $this->input->post("budget") ? $this->input->post("budget") : '';
                            $no_of_position = $this->input->post("no_of_position") ? $this->input->post("no_of_position") : '';
                            $no_of_interview = $this->input->post("no_of_interview") ? $this->input->post("no_of_interview") : '';
                            $candidate_availability = $this->input->post("candidate_availability") ? $this->input->post("candidate_availability") : '';
                            
                            // get employee permission records
                           
                            $status = ($this->input->post("status") == "on") ? '1' : '1';

                           

                            $data = array();
                            $this->load->model("common_model");
                            $created_at = date('Y-m-d H:i:s');
                            $data_insert = array(
                                "vendor_id"=>$vendor_id,
                                "skill_id"=>$skill_id,
                                "subskills"=>$subskills,
                                "duration"=>$duration,
                                "description"=>$description,
                                "experience"=>$experience,
                                "budget"=>$budget,
                                "no_of_position"=>$no_of_position,
                                "no_of_interview"=>$no_of_interview,
                                "candidate_availability"=>$candidate_availability,
                                "status"=>$status,
                                "post_date"=>$date,
                                "created_by"=>$admin_id,
                                "created_at"=>$created_at,
                                "updated_at"=>$created_at,
                            );

                            
                            $last_id = $this->common_model->data_insert("tbl_postjob", $data_insert, TRUE);

                            $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Post Job created successfully</div>');
                                redirect("postjob/postjob_list");
                                exit;
                        }
                    }
                        
                    $this->load->model("Technology_model");
                    $technologies = $this->Technology_model->get_active_technology_list();
                    $data['technologies'] = $technologies;

                    $this->load->model("vendors_model");
                    $vendors = $this->vendors_model->get_vendors_by_role();
                    $data['vendors'] = $vendors;
                    $data['admin_id'] = $admin_id;
                    $this->load->view('admin/postjob/add', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                } 
            } else {
                redirect("admin"); exit;
            }    

        } else {
           redirect("admin"); exit;
        }
    }

    public function postjob_edit($id) {
        if (_is_customer_login($this)) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $my_permission = get_my_permission();

                    if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('postjob_edit',$my_permission)) {

                        $data = array();
                        $data["error"] = "";
                        $data["pageTitle"] = "Admin Our Team";
                        $data['admin'] = "Admin";
                        $data["title"] = PROJECT_NAME;
                        $data['page'] = "Post Job";
                        $data['action'] = "Edit";
                        $data['customer_role_type'] = $customer_role_type;

                    $postjob = $this->Postjob_model->get_postjob_list_by_id($id);
                    if(isset($postjob) && !empty($postjob)) {

                        $exist_vendor_id = $postjob->vendor_id;
                        $exist_created_by = $postjob->created_by;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }

                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {
                           
                             if(($exist_created_by == $admin_id) || ($customer_role_type == "A" || $customer_role_type == "U")) {

                                $data["postjob"] = $postjob;

                                if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {
                             
                                    $this->load->library('form_validation');
                                    $this->form_validation->set_rules('vendor_id', 'Company Name', 'trim|required');
                                    $this->form_validation->set_rules('skill_id', 'Skill Name', 'trim|required');
                                    
                                    $this->form_validation->set_rules('duration', 'duration', 'trim|required');
                                    // $this->form_validation->set_rules('description', 'description', 'trim|required');
                                    $this->form_validation->set_rules('experience', 'experience', 'trim|required');
                                    $this->form_validation->set_rules('budget', 'budget', 'trim|required');
                                    $this->form_validation->set_rules('no_of_position', 'no_of_position', 'trim|required');
                                    $this->form_validation->set_rules('no_of_interview', 'no_of_interview', 'trim|required');
                                    $this->form_validation->set_rules('candidate_availability', 'candidate_availability', 'trim|required');
                            

                                    if ($this->form_validation->run() == FALSE) {
                                        $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                                    } else {

                                     
                                        $vendor_id = $this->input->post("vendor_id") ? $this->input->post("vendor_id") : '';
                                        $skill_id = $this->input->post("skill_id") ? $this->input->post("skill_id") : '';
                                        $subskills = $this->input->post("subskills") ? $this->input->post("subskills") : '';
                                        $duration = $this->input->post("duration") ? $this->input->post("duration") : '';
                                        $description = $this->input->post("description") ? $this->input->post("description") : '';
                                        $experience = $this->input->post("experience") ? $this->input->post("experience") : '';
                                        $budget = $this->input->post("budget") ? $this->input->post("budget") : '';
                                        $no_of_position = $this->input->post("no_of_position") ? $this->input->post("no_of_position") : '';
                                        $no_of_interview = $this->input->post("no_of_interview") ? $this->input->post("no_of_interview") : '';
                                        $candidate_availability = $this->input->post("candidate_availability") ? $this->input->post("candidate_availability") : '';
                                        
                                        // get employee permission records
                                       
                                        $status = ($this->input->post("status") == "on") ? '1' : '1';

                                        $data = array();
                                        $this->load->model("common_model");
                                        $created_at = date('Y-m-d H:i:s');
                                        $data_insert = array(
                                            "skill_id"=>$skill_id,
                                            "subskills"=>$subskills,
                                            "duration"=>$duration,
                                            "description"=>$description,
                                            "experience"=>$experience,
                                            "budget"=>$budget,
                                            "no_of_position"=>$no_of_position,
                                            "no_of_interview"=>$no_of_interview,
                                            "candidate_availability"=>$candidate_availability,
                                            "updated_at"=>$created_at,
                                        );
     
                                        $this->common_model->data_update("tbl_postjob", $data_insert, array("id" => $id));

                                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Post Job updated successfully</div>');
                                            redirect("postjob/postjob_list");
                                        exit;
                                    }
                                }

                                $this->load->model("Technology_model");
                                $technologies = $this->Technology_model->get_active_technology_list();
                                $data['technologies'] = $technologies;


                                $this->load->model("vendors_model");
                                $vendors = $this->vendors_model->get_vendors_by_role();
                                $data['vendors'] = $vendors;
                                $data['admin_id'] = $admin_id;

                                $this->load->view('admin/postjob/edit', $data);

                            } else {
                                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit other record.</div>');
                                redirect('postjob/postjob_list');
                                exit;
                            }

                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit other record.</div>');
                            redirect('postjob/postjob_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('postjob/postjob_list');
                        exit;
                    }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('admin/dashboard');
                        exit;
                    }  
                }  else {
                    redirect("admin"); exit;
                }   
        } else {
           redirect("admin"); exit;
        }
    } 

    public function postjob_pdf($id) {

        if (_is_customer_login($this)) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $my_permission = get_my_permission();

                    if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('postjob_list',$my_permission)) {

                        $data = array();
                       

                        $postjob = $this->Postjob_model->get_postjob_lists_by_id_pdf($id);
                        if(isset($postjob) && !empty($postjob)) {

                            $exist_vendor_id = $postjob->vendor_id;
                            $exist_created_by = $postjob->created_by;
                            $is_access = "No";
                            if($customer_role_type == "V") {
                                if($admin_id == $exist_vendor_id) {
                                    $is_access = "Yes";
                                }
                            } else if($customer_role_type == "A" || $customer_role_type == "U") {
                                $is_access = "Yes";
                            } else {
                                $is_access = "No";
                            }

                            if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {
                               
                                if(($exist_created_by == $admin_id) || ($customer_role_type == "A" || $customer_role_type == "U")) {

                                    $random_no = rand(1111111111,9999999999).$id;
                                    $invoice_name = substr($random_no,-10);

                                    $data['postjob'] = $postjob;
                                    $html = $this->load->view('admin/postjob/postjob', $data, true);
                                    
                                  
                                    $pdfFilePath = "infiraise-postjob-".$random_no.".pdf";
                                   
                                    $this->load->library('Pdf');
                                    $this->pdf->pdf->AddPage('P');
                                    $this->pdf->pdf->WriteHTML($html);
                                    $this->load->model("common_model");

                                    $this->pdf->pdf->Output($pdfFilePath, "D");
                                    exit;


                                } else {
                                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit other record.</div>');
                                    redirect('postjob/postjob_list');
                                    exit;
                                }

                            } else {
                                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit other record.</div>');
                                redirect('postjob/postjob_list');
                                exit;
                            }

                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                            redirect('postjob/postjob_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('admin/dashboard');
                        exit;
                    }  
                }  else {
                    redirect("admin"); exit;
                }   
        } else {
           redirect("admin"); exit;
        }
    }

    public function postjob_view($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('postjob_view',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Our Team";
                $data['admin'] = "Admin";
                $data["title"] = PROJECT_NAME;
                $data['page'] = "Household";
                $data['action'] = "View";

                $admin_id = $this->session->userdata('admin_id');
                $role =  $this->session->userdata('customer_role_id');

                $this->load->model("postjob_model");
                $postjob = $this->postjob_model->get_postjob_list_by_id($id);

                $data["postjob"] = $postjob;

                $this->load->model("location_model");
                $locations = $this->location_model->all();
                $data['locations'] = $locations;

                $this->load->view('admin/postjob/view', $data);
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }       
        } else {
           redirect("admin"); exit;
        }
    } 

    public function postjob_delete($id) {

        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('postjob_delete',$my_permission)) {

                    $postjob = $this->Postjob_model->get_postjob_list_by_id($id);
                    if(isset($postjob) && !empty($postjob)) {
                        $data = array();
                        $data['id'] = $id;

                        $exist_vendor_id = $postjob->vendor_id;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }

                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {

                            $this->load->model("common_model");
                            $this->common_model->data_remove('tbl_postjob',$data);

                            $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Post Job deleted successfully</div>');
                            redirect("postjob/postjob_list");
                            exit;
                        
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                            redirect('postjob/postjob_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('postjob/postjob_list');
                        exit;
                    }

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }  
            }  else {
                redirect("admin"); exit;
            }   
            
        } else {
           redirect("admin"); exit;
        }
    } 

    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }

    public function check_email_exist() {
        $email = $this->input->post("email");
        if(!empty($email)) {
            $this->load->model("postjob_model");
            $check_email_exist = $this->postjob_model->check_email_exist($email);

            if($check_email_exist>0){
                 echo "false";
                 exit;
            } else {
                 echo "true";
                 exit;
            }
        }
    }
}
?>