<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Vendors extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));

        $this->load->model("Common_model");
        $this->load->model("Vendors_model");

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }
    }

    // _is_customer_login for admin
    // _is_customer_login for customer

  
    public function deleteimages(){

        $data_insert = array(
            "image"=>NULL
        );
        $id=$_POST['id'];
        $this->load->model("common_model");      
        $category=$this->common_model->data_update("tbl_customers", $data_insert, array("customer_id" => $id));
        if($category){
            echo 1;
        } else {
            echo 0;
        }
    }


    public function change_status() {
        $table = $this->input->post("table");
        $id = $this->input->post("id");
        $on_off = $this->input->post("on_off");
        $id_field = $this->input->post("id_field");
        $status = $this->input->post("status");

        $this->Common_model->data_update($table, array("$status" => $on_off), array("$id_field" => $id));
        echo $_POST['on_off'];
    }

    public function change_new_status() {
        $table = $this->input->post("table");
        $id = $this->input->post("id");
        $on_off = $this->input->post("on_off");
        $id_field = $this->input->post("id_field");
        $status = $this->input->post("status");

        $this->Common_model->data_update($table, array("is_front"=>"2","is_approve" =>  $on_off,"$status" => $on_off,"$status" => $on_off), array("$id_field" => $id));
        echo $_POST['on_off'];
    }

    public function approve_vendors_list() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');

            $my_permission = get_my_permission();

            if(in_array('approve_vendors_list',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "Vendors";
                $data['page'] = "Vendors";
                $data['action'] = "List";
                
                $this->load->view('admin/vendors/list', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_approve_vendors_list(){
        if(_is_customer_login($this)) {
            
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('approve_vendors_list',$my_permission)) {
                $is_front = '2';
                $this->load->model("Vendors_model");
                $fetch_data = $this->Vendors_model->get_vendors_list_by_ajax($is_front);
                $data = array();  
              
                $no = 1;
                foreach($fetch_data as $row)  
                {  
                        
                        $customer_id = $row->customer_id;
                        $random_array = array("round-success","round-primary","round-danger","round-warning");
                        $random_keys=array_rand($random_array,1);
                        $backColor =  $random_array[$random_keys];
                        $image = $row->image;
                        $imagePath= '';
                        if ($image == NULL) { 
                            $imagePath .= '<span class="round ';
                            $imagePath .= $backColor;
                            $imagePath .='">';
                            $firstCharacter = substr($row->username , 0, 1);
                            $imagePath .= trim(strtoupper($firstCharacter));
                            $imagePath .='</span>';
                        } else {

                            $imagefullpath = base_url().$image;
                            $returnpath = $this->config->item('customers_images_uploaded_path');
                        
                            if(!file_exists($imagefullpath)){
                                $imagefullpath = $returnpath.$image;
                                $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpath.'"><img src="';
                                $imagePath .= $imagefullpath;
                                $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                </span>';
                            } else {
                                $imagePath .= '<span class="round ';
                                $imagePath .= $backColor;
                                $imagePath .='">';
                                $firstCharacter = substr($row->username , 0, 1);
                                $imagePath .= trim(strtoupper($firstCharacter));
                                $imagePath .='</span>';
                            }
                        } 

                        $username = $row->username;
                        $ownername = $row->ownername;
                        $code = $row->code;
                        $org_password = $row->org_password;
                        $mobileno = "";
                        $alternateno = $row->alternateno;
                        $linkedin = $row->linkedin;
                        $linkedin = $row->linkedin;
                        $skypeid = $row->skypeid;
                        $is_social = "";
                        $email = "";
                        if(isset($row->email)){
                            $email .= '<a href="mailto:'.$row->email.'" target="_blank">'.$row->email.'</a>';
                        }

                        if(isset($row->mobileno)){
                            $mobileno .= '<a href="tel:'.$row->mobileno.'" target="_blank">+91'.$row->mobileno.'</a>';
                        }

                        // if(isset($row->alternateno)){
                        //     $mobileno .=" - "; 
                        //     $mobileno .= '<a href="tel:'.$row->alternateno.'" target="_blank">+91'.$row->alternateno.'</a>';
                        // }

                        if(isset($linkedin)){
                            $is_social .= '<a href="'.$linkedin.'" target="_blank" class="btn btn-circle btn-success"><i class="fa fa-linkedin"></i></a>';
                        }

                        if(isset($skypeid)){
                            $is_social .= '<a href="skype:'.$skypeid.'?userinfo" target="_blank" class="btn btn-circle btn-warning"><i class="fa fa-skype"></i></a>';
                        }
                       
                        $readonly = "";
                        $disabled = "";
                        $status = '';
                        $status .=' <input type="checkbox" class="js-switch tgl_checkbox" data-color="#55ce63" data-table="tbl_customers" data-status="status" data-idfield="customer_id"  data-id="';
                        $status .=$customer_id;
                        $status .='" id="cb_'.$customer_id.'"';

                        if(!in_array('vendors_edit', $my_permission)) {
                            $status .="readonly ";
                            $status .="disabled ";
                        }

                        if($row->status == '1') {
                         $status .='checked ';
                        } 
                        $status .='/>';

                        $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));

                        $action = '';

                        $action = '<div class="btn-group"><button class="btn btn-success"><i class="fa fa-unlock"></i></button><button class="btn btn-success dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button><ul class="dropdown-menu">';

                        $view_link = '';

                        $view_link .= ' <a href="javascript:void(0);"  onclick="';
                        $view_link .= "confirm_delete('";
                        $view_link .= site_url("vendors/vendors_delete/".$row->customer_id);
                        $view_link .= "'";
                        $view_link .= ')"; data-toggle="tooltip" data-placement="left" title="View"  style="text-align: center; margin: 10px;padding: 10px;">View</a>';

                        // $action .= '<li>';
                        // $action .= $view_link; 
                        // $action .='</li>';

                        $permission_link = '';
                        if($user_id == 1 || $user_id == "1") {
                            $permission_link.= '<a href="'.site_url("vendors/permission_details/".$row->customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Permission"  style="text-align: center; margin: 10px;padding: 10px;">Role Permission</a>';
                            $action .= '<li>';
                            $action .= $permission_link; 
                            $action .='</li>';
                        }

                        $agreement_link = '';
                        if(in_array('vendors_agreement', $my_permission)) {
                            $agreement_link.= ' <a href="'.site_url("vendors/vendors_agreement/".$row->customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Agreement" style="text-align: center; margin: 10px;padding: 10px;">Agreement</a>&nbsp;';
                            $action .= '<li>';
                            $action .= $agreement_link; 
                            $action .='</li>';
                        }

                        $employeee_link = '';
                        if(in_array('vendors_employees_details', $my_permission) || in_array('vendors_employees_details_view', $my_permission)) {
                            $employeee_link.= ' <a href="'.site_url("vendors/vendors_employees_details/".$row->customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Skills Matrix"  style="text-align: center; margin: 10px;padding: 10px;">Skills Matrix</a>&nbsp;';
                            $action .= '<li>';
                            $action .= $employeee_link; 
                            $action .='</li>';
                        }

                        $edit_link = '';
                        if(in_array('vendors_edit', $my_permission)) {
                            $edit_link.= ' <a href="'.site_url("vendors/vendors_edit/".$row->customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit"  style="text-align: center; margin: 10px;padding: 10px;">Edit</a>';
                            $action .= '<li>';
                            $action .= $edit_link; 
                            $action .='</li>';
                        }

                        $delete_link = '';
                        if(in_array('vendors_delete', $my_permission)) {
                            $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                            $delete_link .= "confirm_delete('";
                            $delete_link .= site_url("vendors/vendors_delete/".$row->customer_id);
                            $delete_link .= "'";
                            $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete"  style="text-align: center; margin: 10px;padding: 10px;">Delete</a>';
                        }

                        // $action  =  $view_link.$agreement_link.$permission_link.$edit_link;
                        $action .='</ul></div>';

                        $sub_array = array();  
                        $sub_array[] = $no;  
                        $sub_array[] = $imagePath;  
                        $sub_array[] = $code;  
                        $sub_array[] = $username;  
                        $sub_array[] = $ownername;  
                        $sub_array[] = $email;  
                        $sub_array[] = $mobileno;  
                        $sub_array[] = $is_social;  
                        $sub_array[] = $status;  
                        $sub_array[] = $action;  

                        $data[] = $sub_array;  
                        $no = $no+1;
                }
                $output = array(  
                    "draw"                    =>     intval($_POST["draw"]),  
                    "recordsTotal"          =>      $this->Vendors_model->get_all_vendors_result($is_front),  
                    "recordsFiltered"     =>     $this->Vendors_model->get_filtered_data_vendors($is_front),  
                    "data"                    =>     $data  
                ); 
                echo json_encode($output);  
           }
        } else {
            echo "Not access this function";
        }
    }

    public function unapprove_vendors_list() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');

            $my_permission = get_my_permission();

            if(in_array('approve_vendors_list',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "Vendors";
                $data['page'] = "New Vendors";
                $data['action'] = "List";
                
                $this->load->view('admin/vendors/unapprovelist', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_unapprove_vendors_list(){
        if(_is_customer_login($this)) {
            
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('approve_vendors_list',$my_permission)) {

                $is_front = '1';
                $this->load->model("Vendors_model");
                $fetch_data = $this->Vendors_model->get_vendors_list_by_ajax($is_front);
                $data = array();  
                // echo $this->db->last_query();
                // exit;

                $no = 1;
                foreach($fetch_data as $row)  
                {  
                        
                        $customer_id = $row->customer_id;
                        $random_array = array("round-success","round-primary","round-danger","round-warning");
                        $random_keys=array_rand($random_array,1);
                        $backColor =  $random_array[$random_keys];
                        $image = $row->image;
                        $imagePath= '';
                        if ($image == NULL) { 
                            $imagePath .= '<span class="round ';
                            $imagePath .= $backColor;
                            $imagePath .='">';
                            $firstCharacter = substr($row->username , 0, 1);
                            $imagePath .= trim(strtoupper($firstCharacter));
                            $imagePath .='</span>';
                        } else {

                            $imagefullpath = base_url().$image;
                            $returnpath = $this->config->item('customers_images_uploaded_path');
                        
                            if(!file_exists($imagefullpath)){
                                $imagefullpath = $returnpath.$image;
                                $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpath.'"><img src="';
                                $imagePath .= $imagefullpath;
                                $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                </span>';
                            } else {
                                $imagePath .= '<span class="round ';
                                $imagePath .= $backColor;
                                $imagePath .='">';
                                $firstCharacter = substr($row->username , 0, 1);
                                $imagePath .= trim(strtoupper($firstCharacter));
                                $imagePath .='</span>';
                            }
                        } 

                        $username = $row->username;
                        $ownername = $row->ownername;
                        $code = $row->code;
                        $org_password = $row->org_password;
                        $mobileno = "";
                        $alternateno = $row->alternateno;
                        $linkedin = $row->linkedin;
                        $linkedin = $row->linkedin;
                        $skypeid = $row->skypeid;
                        $is_social = "";
                        $email = "";
                        if(isset($row->email)){
                            $email .= '<a href="mailto:'.$row->email.'" target="_blank">'.$row->email.'</a>';
                        }

                        if(isset($row->mobileno)){
                            $mobileno .= '<a href="tel:'.$row->mobileno.'" target="_blank">+91'.$row->mobileno.'</a>';
                        }

                        // if(isset($row->alternateno)){
                        //     $mobileno .=" - "; 
                        //     $mobileno .= '<a href="tel:'.$row->alternateno.'" target="_blank">+91'.$row->alternateno.'</a>';
                        // }

                        if(isset($linkedin)){
                            $is_social .= '<a href="'.$linkedin.'" target="_blank" class="btn btn-circle btn-success"><i class="fa fa-linkedin"></i></a>';
                        }

                        if(isset($skypeid)){
                            $is_social .= '<a href="skype:'.$skypeid.'?userinfo" target="_blank" class="btn btn-circle btn-warning"><i class="fa fa-skype"></i></a>';
                        }
                       
                        $readonly = "";
                        $disabled = "";
                        $status = '';
                        $status .=' <input type="checkbox" class="js-switch tgl_checkbox" data-color="#55ce63" data-table="tbl_customers" data-status="status" data-idfield="customer_id"  data-id="';
                        $status .=$customer_id;
                        $status .='" id="cb_'.$customer_id.'"';

                        if(!in_array('vendors_edit', $my_permission)) {
                            $status .="readonly ";
                            $status .="disabled ";
                        }

                        if($row->status == '1') {
                         $status .='checked ';
                        } 
                        $status .='/>';

                        $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));

                        $action = '';

                        $action = '<div class="btn-group"><button class="btn btn-success"><i class="fa fa-unlock"></i></button><button class="btn btn-success dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button><ul class="dropdown-menu">';

                        $edit_link = '';
                        if(in_array('vendors_edit', $my_permission)) {
                            $edit_link.= ' <a href="'.site_url("vendors/vendors_edit/".$row->customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit"  style="text-align: center; margin: 10px;padding: 10px;">Edit</a>';
                            $action .= '<li>';
                            $action .= $edit_link; 
                            $action .='</li>';
                        }

                       
                        // $action  =  $view_link.$agreement_link.$permission_link.$edit_link;
                        $action .='</ul></div>';

                        $sub_array = array();  
                        $sub_array[] = $no;  
                        $sub_array[] = $imagePath;  
                        $sub_array[] = $code;  
                        $sub_array[] = $username;  
                        $sub_array[] = $ownername;  
                        $sub_array[] = $email;  
                        $sub_array[] = $mobileno;  
                        $sub_array[] = $is_social;  
                        $sub_array[] = $status;  
                        $sub_array[] = $action;  

                        $data[] = $sub_array;  
                        $no = $no+1;
                }
                $output = array(  
                    "draw"                    =>     intval($_POST["draw"]),  
                    "recordsTotal"          =>      $this->Vendors_model->get_all_vendors_result($is_front),  
                    "recordsFiltered"     =>     $this->Vendors_model->get_filtered_data_vendors($is_front),  
                    "data"                    =>     $data  
                ); 
                echo json_encode($output);  
           }
        } else {
            echo "Not access this function";
        }
    }


    public function vendors_agreement($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('approve_vendors_list',$my_permission) || $customer_role_type == "V") {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Our Team";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Employee";
                    $data['action'] = "View";
                    
                    $this->load->model("Vendors_model");
                    $vendors = $this->Vendors_model->get_vendors_list_by_id($id);

                    if(isset($vendors) && !empty($vendors)) {
                            
                            $data['vendors'] = $vendors;
                            $this->load->model("Industry_model");
                            $data['industry'] = $this->Industry_model->get_industry_by_id($vendors->industry_id);
                            $random_no = rand(1111111111,9999999999).$id;
                            $invoice_name = substr($random_no,-10);
                            $html = $this->load->view('admin/vendors/invoice', $data, true);
                            $pdfFilePath = "vendor-aggrements-".$invoice_name.".pdf";
                            
                            $this->load->library('Pdf');
                            $this->pdf->pdf->AddPage('P');
                            $this->pdf->pdf->WriteHTML($html);
                            $this->load->model("common_model");

                            $this->pdf->pdf->Output($pdfFilePath, "D");
                            exit;

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('employees/employees_list');
                        exit;
                    }

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }  
            }  else {
                redirect("admin"); exit;
            }   
        } else {
           redirect("admin"); exit;
        }
    }

      public function vendors_skills_matrix($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('vendors_employees_details_view',$my_permission) || $customer_role_type == "V") {

                    $data = array();
                    $this->load->model("Vendors_model");
                    $vendors = $this->Vendors_model->get_vendors_list_by_id($id);

                    if(isset($vendors) && !empty($vendors)) {

                        $data['vendors'] = $vendors;
                        $this->load->model("Technology_model");
                        $technologies = $this->Technology_model->get_active_technology_list();
                        $emp_technologies = array();
                        if(count($technologies)>0){
                            foreach($technologies as $k=>$v) {
                                $tech_id = $v->id;
                                $emp_technologies = $this->Technology_model->get_all_technlogywise_emp_list($id,$tech_id);
                                if(isset($emp_technologies) && !empty($emp_technologies)) {
                                    $technologies[$k]->exp_3_year = $emp_technologies->exp_3_year;
                                    $technologies[$k]->exp_3_year_cost = $emp_technologies->exp_3_year_cost;
                                    $technologies[$k]->exp_5_year = $emp_technologies->exp_5_year;
                                    $technologies[$k]->exp_5_year_cost = $emp_technologies->exp_5_year_cost;
                                    $technologies[$k]->exp_7_year = $emp_technologies->exp_7_year;
                                    $technologies[$k]->exp_7_year_cost = $emp_technologies->exp_7_year_cost;
                                    $technologies[$k]->total_emp = $emp_technologies->total_emp;
                                } else {
                                    $technologies[$k]->exp_3_year = "N/A";
                                    $technologies[$k]->exp_3_year_cost = "N/A";
                                    $technologies[$k]->exp_5_year = "N/A";
                                    $technologies[$k]->exp_5_year_cost = "N/A";
                                    $technologies[$k]->exp_7_year = "N/A";
                                    $technologies[$k]->exp_7_year_cost = "N/A";
                                    $technologies[$k]->total_emp = 0;
                                }
                            }
                        }
                       
                        $data['technologies'] = $technologies;

                        $random_no = rand(1111111111,9999999999).$id;
                        $vendor_code = $vendors->code; 

                        $html = $this->load->view('admin/vendors/vendors_skills_matrix', $data, true);

                
                        $pdfFilePath = "vendor-skills-matrix-".$vendor_code.".pdf";
                        
                        $this->load->library('Pdf');
                        $this->pdf->pdf->AddPage('P');
                        $this->pdf->pdf->WriteHTML($html);
                        $this->load->model("common_model");

                        $this->pdf->pdf->Output($pdfFilePath, "D");

                        echo "<pre>";
                        print_r($html);
                        exit;

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('employees/employees_list');
                        exit;
                    }

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }  
            }  else {
                redirect("admin"); exit;
            }   
        } else {
           redirect("admin"); exit;
        }
    }

    public function permission_details($id) {
        if (_is_customer_login($this)) {
           
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $data["error"] = "";
            $data['page'] = "Permission";
            $data['action'] = "List";
            
            if($role >= 1 && $admin_id >= 1) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Ourteam";
                $data['admin'] = "Admin";
                $data["title"] = PROJECT_NAME;
                $data['page'] = "Permission";
                $data['action'] = "Add";
                    
               if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {
                   
                    if(count($_POST) > 0) {

                        // remove all existing permission
                        $removed = $this->Vendors_model->remove_permissions($id);

                        unset($_POST['save_button']);

                        foreach ($_POST as $key => $value) {

                            // get permission id 
                            $details = $this->Vendors_model->permission_details_by_name($key);
                            
                            $data_insert = array();
                            $data_insert['permission_id'] = $details->id;
                            $data_insert['customer_id'] = $id;
                            $data_insert['permission_name'] = $key;
                            $data_insert['status'] = $value;
                            $save = $this->Common_model->data_insert("tbl_customers_permissions", $data_insert, TRUE);
                        }
                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Vendor Permission set successfully</div>');
                            redirect("vendors/approve_vendors_list");
                        exit;
                    } else {
                         // remove all existing permission
                        $removed = $this->Vendors_model->remove_permissions($id);
                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Vendor Permission set successfully</div>');
                            redirect("vendors/approve_vendors_list");
                        exit;
                    }
                }
                   
                $permission = $this->Vendors_model->all_permissions();

                $permission_arr = array();
                $permission_arr = array_chunk($permission, 4);
                $data['permission'] = $permission_arr;
                
                $match_permission_arr = array();
                $match_permission = $this->Vendors_model->get_vendors_permission_list_by_id($id);

                if(count($match_permission)>0){
                    foreach($match_permission as $kk=>$vv){
                        $match_permission_arr[] = $vv->name;
                    }
                }
                $data['match_permission_arr'] = $match_permission_arr;
       
                $this->load->view('admin/vendors/permission', $data);
            } else {
               redirect("admin"); exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function vendors_add() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('vendors_add',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Ourteam";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Vendors";
                    $data['action'] = "Add";

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {

                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('username', 'Company Name', 'trim|required');
                        $this->form_validation->set_rules('ownername', 'Name', 'trim|required');
                        
                        $this->form_validation->set_rules('industry_id', 'Industry Type', 'trim|required');
                        $this->form_validation->set_rules('address', 'Address', 'trim|required');
                        $this->form_validation->set_rules('mobileno', 'Mobile No', 'trim|required');
                        $this->form_validation->set_rules('alternateno', 'Alternate No', 'trim|required');
                        $this->form_validation->set_rules('linkedin', 'linkedin', 'trim|required');
                        $this->form_validation->set_rules('skypeid', 'skypeid', 'trim|required');
                        $this->form_validation->set_rules('email', 'Email', 'trim|required');
                        $this->form_validation->set_rules('password', 'Password', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                        } else {

                            $username = $this->input->post("username") ? $this->input->post("username") : '';
                            $ownername = $this->input->post("ownername") ? $this->input->post("ownername") : '';
                            $industry_id = $this->input->post("industry_id") ? $this->input->post("industry_id") : '';
                            $address = $this->input->post("address") ? $this->input->post("address") : '';
                            $mobileno = $this->input->post("mobileno") ? $this->input->post("mobileno") : '';
                            $alternateno = $this->input->post("alternateno") ? $this->input->post("alternateno") : '';
                            $linkedin = $this->input->post("linkedin") ? $this->input->post("linkedin") : '';
                            $skypeid = $this->input->post("skypeid") ? $this->input->post("skypeid") : '';
                            $email = $this->input->post("email") ? $this->input->post("email") : '';
                            $password = $this->input->post("password") ? $this->input->post("password") : '';
                            $agreement_date = $this->input->post("agreement_date") ? $this->input->post("agreement_date") : '';
                            $cin = $this->input->post("cin") ? $this->input->post("cin") : '';
                            $gstin = $this->input->post("gstin") ? $this->input->post("gstin") : '';
                            $registered_office = $this->input->post("registered_office") ? $this->input->post("registered_office") : '';
                            $role_id = '4';
                        
                            // get employee permission records
                            
                            $is_approve = ($this->input->post("is_approve") == "on") ? '1' : '1';
                            $status = ($this->input->post("status") == "on") ? '1' : '0';
                            
                            if ($_FILES["vendor_image"]["size"] > 0) {
                                $temp = explode(".", $_FILES["vendor_image"]["name"]);
                                $newfilename = time() . '.' . end($temp);
                                $uploadpath = $this->config->item('customers_images_path');
                                $returnpath = $this->config->item('customers_images_uploaded_path');
                                $file_name = $this->file_upload($_FILES["vendor_image"], $uploadpath, $returnpath);
                                $logo = $file_name;
                            } else {
                                $logo = NULL;
                            }

                            if($role_id == "4") {
                                $role_type = "V";
                                $roletype = "Vendor";
                            } elseif($role_id == "3") {
                                $role_type = "U";
                                $roletype = "User";
                            } else if($role_id == "2") {
                                $role_type = "S";
                                $roletype = "Sub Admin";
                            } else {
                                $role_type = "A";
                                $roletype = "Admin";
                            }

                            $code = "";
                            $code .= "EHVL_";
                            $code .= $this->get_random_number(5);

                            $data = array();
                            $this->load->model("common_model");
                            $created_at = date('Y-m-d H:i:s');
                            $data_insert = array(
                                "code"=>$code,
                                "image"=>$logo,
                                "username"=>$username,
                                "code"=>$code,
                                "ownername"=>$ownername,
                                "address"=>$address,
                                "email"=>$email,
                                "password"=>md5($password),
                                "org_password"=>$password,
                                "mobileno"=>$mobileno,
                                "alternateno"=>$alternateno,
                                "industry_id"=>$industry_id,
                                "linkedin"=>$linkedin,
                                "skypeid"=>$skypeid,
                                "agreement_date"=>date('Y-m-d',strtotime($agreement_date)),
                                "cin"=>$cin,
                                "gstin"=>$gstin,
                                "registered_office"=>$registered_office,
                                "type_id"=>1,
                                "role_id"=>$role_id,
                                "role_type"=>$role_type,
                                "roletype"=>$roletype,
                                "is_approve"=>$is_approve,
                                "status"=>$status,
                                "created_at"=>$created_at,
                                "updated_at"=>$created_at,
                            );

                            $last_id = $this->common_model->data_insert("tbl_customers", $data_insert, TRUE);

                            // set default employee permission
                            // $last_id = "55";
                            $records = $this->Vendors_model->set_default_employee_permission();
                            if(count($records)>0){
                                foreach ($records as $key => $value) {
                                    $data_insert = array();
                                    $data_insert['permission_id'] = $value->id;
                                    $data_insert['customer_id'] = $last_id;
                                    $data_insert['permission_name'] = $value->name;
                                    $data_insert['status'] = '1';
                                    
                                    $save = $this->Common_model->data_insert("tbl_customers_permissions", $data_insert, TRUE);
                                }
                            }
                          

                            $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Vendor created successfully</div>');
                                if($is_approve == "1") {
                                    redirect("vendors/approve_vendors_list");
                                } else {
                                    redirect("vendors/unapprove_vendors_list");
                                }
                                exit;
                        }
                    }
                        
                    $this->load->model("industry_model");
                    $industrys = $this->industry_model->get_active_industry_list();
                    $data['industrys'] = $industrys;

                    $this->load->view('admin/vendors/add', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('vendors/dashboard');
                    exit;
                } 
            } else {
                redirect("admin"); exit;
            }    

        } else {
           redirect("admin"); exit;
        }
    }

    public function vendors_edit($id) {
        if (_is_customer_login($this)) {

            if($id=="1" || $id =="1") {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
            } else {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $my_permission = get_my_permission();

                    if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('vendors_edit',$my_permission)) {

                        $data = array();
                        $data["error"] = "";
                        $data["pageTitle"] = "Admin Our Team";
                        $data['admin'] = "Admin";
                        $data["title"] = PROJECT_NAME;
                        $data['page'] = "Vendors";
                        $data['action'] = "Edit";

                        $this->load->model("Vendors_model");
                        $vendors = $this->Vendors_model->get_vendors_list_by_id($id);
                        $data["vendors"] = $vendors;

                        if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {
                      
                            $this->form_validation->set_rules('username', 'Company Name', 'trim|required');
                            $this->form_validation->set_rules('ownername', 'Name', 'trim|required');
                            $this->form_validation->set_rules('industry_id', 'Industry Type', 'trim|required');
                            $this->form_validation->set_rules('address', 'Address', 'trim|required');
                            $this->form_validation->set_rules('mobileno', 'Mobile No', 'trim|required');
                            $this->form_validation->set_rules('alternateno', 'Alternate No', 'trim|required');
                            $this->form_validation->set_rules('linkedin', 'linkedin', 'trim|required');
                            $this->form_validation->set_rules('skypeid', 'skypeid', 'trim|required');
                            $this->form_validation->set_rules('email', 'Email', 'trim|required');
                            $this->form_validation->set_rules('password', 'Password', 'trim|required');

                            if ($this->form_validation->run() == FALSE) {
                                $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                            } else {

                                $username = $this->input->post("username") ? $this->input->post("username") : '';
                                $ownername = $this->input->post("ownername") ? $this->input->post("ownername") : '';
                                $industry_id = $this->input->post("industry_id") ? $this->input->post("industry_id") : '';
                                $address = $this->input->post("address") ? $this->input->post("address") : '';
                                $mobileno = $this->input->post("mobileno") ? $this->input->post("mobileno") : '';
                                $alternateno = $this->input->post("alternateno") ? $this->input->post("alternateno") : '';
                                $linkedin = $this->input->post("linkedin") ? $this->input->post("linkedin") : '';
                                $skypeid = $this->input->post("skypeid") ? $this->input->post("skypeid") : '';
                                $email = $this->input->post("email") ? $this->input->post("email") : '';
                                $password = $this->input->post("password") ? $this->input->post("password") : '';
                                $role_id = '4';
                            
                                $cin = $this->input->post("cin") ? $this->input->post("cin") : '';
                                $agreement_date = $this->input->post("agreement_date") ? $this->input->post("agreement_date") : '';
                                $gstin = $this->input->post("gstin") ? $this->input->post("gstin") : '';
                                $registered_office = $this->input->post("registered_office") ? $this->input->post("registered_office") : '';
                                
                                $is_approve = ($this->input->post("is_approve") == "on") ? '1' : '1';
                                $status = ($this->input->post("status") == "on") ? '1' : '0';
                                
                               
                                if ($_FILES["vendor_image"]["size"] > 0) {

                                    $temp = explode(".", $_FILES["vendor_image"]["name"]);
                                    $newfilename = time() . '.' . end($temp);
                                    $uploadpath = $this->config->item('customers_images_path');
                                    $returnpath = $this->config->item('customers_images_uploaded_path');
                                    $file_name = $this->file_upload($_FILES["vendor_image"], $uploadpath, $returnpath);
                                    $logo = $file_name;
                                } else {
                                    $logo = NULL;
                                }

                                if($role_id == "4") {
                                    $role_type = "V";
                                    $roletype = "Vendor";
                                } elseif($role_id == "3") {
                                    $role_type = "U";
                                    $roletype = "User";
                                } else if($role_id == "2") {
                                    $role_type = "S";
                                    $roletype = "Sub Admin";
                                } else {
                                    $role_type = "A";
                                    $roletype = "Admin";
                                }

                                $data = array();
                                $this->load->model("common_model");
                                $created_at = date('Y-m-d H:i:s');
                                $data_insert = array(
                                    "image"=>$logo,
                                    "username"=>$username,
                                    "ownername"=>$ownername,
                                    "address"=>$address,
                                    "email"=>$email,
                                    "password"=>md5($password),
                                    "org_password"=>$password,
                                    "mobileno"=>$mobileno,
                                    "alternateno"=>$alternateno,
                                    "industry_id"=>$industry_id,
                                    "linkedin"=>$linkedin,
                                    "skypeid"=>$skypeid,
                                    "agreement_date"=>date('Y-m-d',strtotime($agreement_date)),
                                    "cin"=>$cin,
                                    "gstin"=>$gstin,
                                    "registered_office"=>$registered_office,
                                    "type_id"=>1,
                                    "role_id"=>$role_id,
                                    "role_type"=>$role_type,
                                    "roletype"=>$roletype,
                                    "is_approve"=>$is_approve,
                                    "status"=>$status,
                                    "updated_at"=>$created_at,
                                );
                               
                                $this->common_model->data_update("tbl_customers", $data_insert, array("customer_id" => $id));

                                $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Vendor updated successfully</div>');
                                    redirect("vendors/approve_vendors_list");
                                exit;
                            }
                        }

                        $this->load->model("industry_model");
                        $industrys = $this->industry_model->get_active_industry_list();
                        $data['industrys'] = $industrys;

                        $this->load->view('admin/vendors/edit', $data);
                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('vendors/dashboard');
                        exit;
                    }  
                }  else {
                    redirect("admin"); exit;
                }   
            }
        } else {
           redirect("admin"); exit;
        }
    } 

    public function vendors_view($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(($customer_role_type == "A" || $customer_role_type == "V"  || $customer_role_type == "U") && in_array('vendors_view',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Our Team";
                $data['admin'] = "Admin";
                $data["title"] = PROJECT_NAME;
                $data['page'] = "Household";
                $data['action'] = "View";

                $admin_id = $this->session->userdata('admin_id');
                $role =  $this->session->userdata('customer_role_id');

                $this->load->model("Vendors_model");
                $vendors = $this->Vendors_model->get_vendors_list_by_id($id);

                $data["vendors"] = $vendors;

                $this->load->model("location_model");
                $locations = $this->location_model->all();
                $data['locations'] = $locations;

                $this->load->view('admin/vendors/view', $data);
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('vendors/dashboard');
                exit;
            }       
        } else {
           redirect("admin"); exit;
        }
    } 

    public function vendors_delete($id) {
        if (_is_customer_login($this)) {

            if($id=="1" || $id =="1") {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
            } else {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $customer_role_type =  $this->session->userdata('customer_role_type');
                    $my_permission = get_my_permission();

                    if(($customer_role_type == "A" || $customer_role_type == "S") && in_array('vendors_delete',$my_permission)) {
                        $data = array();
                        $data['customer_id'] = $id;
                        $this->load->model("Vendors_model");
                      
                        $category = $this->Vendors_model->get_vendors_list_by_id($id);
                        if ($category) {

                            $this->load->model("common_model");
                            $this->common_model->data_remove('tbl_customers',$data);

                            $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> User deleted successfully</div>');
                            redirect("vendors/approve_vendors_list");
                            exit;
                        }
                    }  else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('vendors/dashboard');
                        exit;
                    }  
                }
            }  
        } else {
           redirect("admin"); exit;
        }
    } 

    public function vendors_download_employees_details($id) {
        if (_is_customer_login($this)) {
        
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {
                $my_permission = get_my_permission();
                        
                $delimiter = ",";
                $newline = "\r\n";
                $filename = "employee_details.csv";

                $this->load->model("Technology_model");
                $technologies = $this->Technology_model->get_active_technology_list();
                $emp_technologies = array();
                if(count($technologies)>0){
                    foreach($technologies as $k=>$v) {
                        
                        $tech_id = $v->id;
                        $emp_technologies = $this->Technology_model->get_all_technlogywise_emp_list($id,$tech_id);
                        

                        if(isset($emp_technologies) && !empty($emp_technologies)) {
                            $technologies[$k]->exp_3_year = $emp_technologies->exp_3_year;
                            $technologies[$k]->exp_3_year_cost = $emp_technologies->exp_3_year_cost;
                            $technologies[$k]->exp_5_year = $emp_technologies->exp_5_year;
                            $technologies[$k]->exp_5_year_cost = $emp_technologies->exp_5_year_cost;
                            $technologies[$k]->exp_7_year = $emp_technologies->exp_7_year;
                            $technologies[$k]->exp_7_year_cost = $emp_technologies->exp_7_year_cost;
                            $technologies[$k]->total_emp = $emp_technologies->total_emp;
                        } else {
                            $technologies[$k]->exp_3_year = NULL;
                            $technologies[$k]->exp_3_year_cost = NULL;
                            $technologies[$k]->exp_5_year = NULL;
                            $technologies[$k]->exp_5_year_cost = NULL;
                            $technologies[$k]->exp_7_year = NULL;
                            $technologies[$k]->exp_7_year_cost = NULL;
                            $technologies[$k]->total_emp = NULL;
                        }
                    }
                }
             
                if(count($technologies)>0){
                    foreach($technologies as $k=>$v) {
                        unset($technologies[$k]->id);
                        unset($technologies[$k]->timage);
                        unset($technologies[$k]->status);
                        unset($technologies[$k]->created_at);
                        unset($technologies[$k]->updated_at);
                        unset($technologies[$k]->total_emp);
                    }
                }

                $delim = ',';
                $newline = "\n";
                $enclosure = '"';

                $out = '';
                $k = 0;
                foreach ($technologies as $value) {
                    foreach ($value as $name => $kl) {
                        if ($k == 0) {
                            $out .= $enclosure . str_replace($enclosure, $enclosure . $enclosure, $name) . $enclosure . $delim;
                        }
                    }
                    $k++;
                }

                $out = substr($out, 0, -strlen($delim)) . $newline;
                foreach ($technologies as $value) {
                    $line = array();
                    foreach ($value as $name => $item) {
                        $line[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $item) . $enclosure;
                    }
                    $out .= implode($delim, $line) . $newline;
                }

                $data = $out;
                //$data = $this->dbutil->csv_from_result($thisQur, $delimiter, $newline);
                force_download($filename, $data);
            }  else {
                redirect("admin"); exit;
            }       
        }
    }

    public function vendors_employees_details($id) {
        if (_is_customer_login($this)) {
                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $my_permission = get_my_permission();
                    
                    if(($customer_role_type == "A" || $customer_role_type == "V" ||  $customer_role_type == "U" ) && in_array('vendors_employees_details',$my_permission) || in_array('vendors_employees_details_view',$my_permission)) {

                        $data = array();
                        $data["error"] = "";
                        $data["pageTitle"] = "Admin Our Team";
                        $data['admin'] = "Admin";
                        $data["title"] = PROJECT_NAME;
                        $data['page'] = "Vendors";
                        $data['action'] = "Employee Details";
                        $data['admin_id'] = $admin_id;

                
                        $this->load->model("Vendors_model");
                        $vendors = $this->Vendors_model->get_vendors_list_by_id($id);
                        if(isset($vendors) && !empty($vendors)) {

                            $exist_vendor_id = $vendors->customer_id;
                            $is_access = "No";
                            if($customer_role_type == "V") {
                                if($admin_id == $exist_vendor_id) {
                                    $is_access = "Yes";
                                }
                            } else if($customer_role_type == "A" || $customer_role_type == "U") {
                                $is_access = "Yes";
                            } else {
                                $is_access = "No";
                            }

                            if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {

                                $data["vendors"] = $vendors;

                                if(isset($_FILES) && !empty($_FILES)) {
                                    if($_FILES['customers_files']['size'] > 0) {
                                        $upload_csv_path_with_name = $_FILES['customers_files']['tmp_name'];
                                        $handle = fopen($upload_csv_path_with_name, "r+");

                                        $first_row = true;
                                        $final_ata = array();
                                        $headers = array();
                                        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                                            if($first_row) {
                                                $headers = $data;
                                                $first_row = false;
                                            } else {
                                                $final_ata[] = array_combine($headers, array_values($data));
                                            }
                                        }

                                        $insertCount = $updateCount = $rowCount = $notAddCount = 0;

                                        $csvData = array();
                                        $csvData = $final_ata;                    
                                        // Insert/update CSV data into database
                                        $update_count = 0;
                                        $insert_count = 0;
                                        $pancard_not_match = 0;

                                        if(!empty($csvData)){
                                            $final_data = array();

                                            // remove old records
                                            $datass = array();
                                            $datass['customers_id'] = $id;
                                            $this->load->model("common_model");
                                            $this->common_model->data_remove('tbl_vendors_employee_details',$datass);

                                            foreach($csvData as  $k=> $row){ 
                                                $tname = $row['tname'];
                                                // get technology details by name
                                                $this->load->model("technology_model");
                                                $editDetails = $this->technology_model->get_technology_by_name($tname);
                                                if(isset($editDetails) && !empty($editDetails)){
                                                    $technology_id = $editDetails->id;
                                            
                                                    $tmp_arr = array();
                                            
                                                    $created_at = date('Y-m-d H:i:s');

                                                    $exp_3_years = 0;
                                                    $exp_5_years = 0;
                                                    $exp_7_years = 0;

                                                    if(isset($row['exp_3_year'])  && !empty($row['exp_3_year'])) { 
                                                        $exp_3_years = $row['exp_3_year'];
                                                    } else {
                                                        $exp_3_years = NULL;
                                                    }

                                                    if(isset($row['exp_5_year']) && !empty($row['exp_5_year'])) { $exp_5_years = $row['exp_5_year'];
                                                    } else {
                                                        $exp_5_years = NULL;
                                                    }

                                                    if(isset($row['exp_7_year']) && !empty($row['exp_7_year'])) {$exp_7_years = $row['exp_7_year'];
                                                    } else {
                                                        $exp_7_years = NULL;
                                                    }   

                                                    $total_emp = 0;
                                                    $total_emp = $exp_3_years + $exp_7_years + $exp_5_years;

                                                    $data_insert = array(
                                                        "customers_id"=>$id,
                                                        "technology_id"=>$technology_id,
                                                        "exp_3_year"=>$row['exp_3_year'],
                                                        "exp_3_year_cost"=>$row['exp_3_year_cost'],
                                                        "exp_5_year"=>$row['exp_5_year'],
                                                        "exp_5_year_cost"=>$row['exp_5_year_cost'],
                                                        "exp_7_year"=>$row['exp_7_year'],
                                                        "exp_7_year_cost"=>$row['exp_7_year_cost'],
                                                        "total_emp" =>$total_emp,
                                                        "created_at"=>$created_at,
                                                        "updated_at"=>$created_at,
                                                    );

                                                    $this->common_model->data_insert("tbl_vendors_employee_details", $data_insert, TRUE);

                                                }
                                            }
                                        }

                                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Record imported successfully</div>');
                                            redirect("vendors/vendors_employees_details/$id");
                                        exit;
                                     
                                    }
                                }

                                if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {
                              
                                        $technologies_id = $this->input->post("id") ? $this->input->post("id") : '';
                                        $exp_3_year = $this->input->post("exp_3_year") ? $this->input->post("exp_3_year") : NULL;

                                        $exp_3_year_cost = $this->input->post("exp_3_year_cost") ? $this->input->post("exp_3_year_cost") : NULL;

                                        $exp_5_year = $this->input->post("exp_5_year") ? $this->input->post("exp_5_year") : NULL;
                                        $exp_5_year_cost = $this->input->post("exp_5_year_cost") ? $this->input->post("exp_5_year_cost") : NULL;

                                        $exp_7_year = $this->input->post("exp_7_year") ? $this->input->post("exp_7_year") : NULL;
                                        $exp_7_year_cost = $this->input->post("exp_7_year_cost") ? $this->input->post("exp_7_year_cost") : NULL;
                                       
                                        

                                        if(count($technologies_id)>0){

                                            // remove old record
                                            //
                                            $this->load->model("common_model");
                                            $data = array();
                                            $data['customers_id'] = $id;
                                            $this->common_model->data_remove('tbl_vendors_employee_details',$data);
                                            foreach ($technologies_id as $key => $value) {
                                                $tmp_arr = array();
                                                $this->load->model("common_model");
                                                $created_at = date('Y-m-d H:i:s');

                                                $exp_3_years = 0;
                                                $exp_5_years = 0;
                                                $exp_7_years = 0;

                                                if(isset($exp_3_year[$key])  && !empty($exp_3_year[$key])) { 
                                                    $exp_3_years = $exp_3_year[$key];
                                                } else {
                                                    $exp_3_years = 0;
                                                }

                                                if(isset($exp_5_year[$key]) && !empty($exp_5_year[$key])) { $exp_5_years = $exp_5_year[$key];
                                                } else {
                                                    $exp_5_years = 0;
                                                }

                                                if(isset($exp_7_year[$key]) && !empty($exp_7_year[$key])) {$exp_7_years = $exp_7_year[$key];
                                                } else {
                                                    $exp_7_years = 0;
                                                }   

                                                $total_emp = 0;
                                                $total_emp = $exp_3_years + $exp_7_years + $exp_5_years;
                                                $data_insert = array(
                                                    "customers_id"=>$id,
                                                    "technology_id"=>$value,
                                                    "exp_3_year"=>$exp_3_years,
                                                    "exp_3_year_cost"=>$exp_3_year_cost[$key],
                                                    "exp_5_year"=>$exp_5_years,
                                                    "exp_5_year_cost"=>$exp_5_year_cost[$key],
                                                    "exp_7_year"=>$exp_7_years,
                                                    "exp_7_year_cost"=>$exp_7_year_cost[$key],
                                                    "total_emp" =>$total_emp,
                                                    "created_at"=>$created_at,
                                                    "updated_at"=>$created_at,
                                                );

                                                if($total_emp > 0) {
                                                    $this->common_model->data_insert("tbl_vendors_employee_details", $data_insert, TRUE);

                                                }
                                            }
                                        }   
                                        
                                      
                                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Vendor employee details saved successfully</div>');
                                            redirect("vendors/vendors_employees_details/$id");
                                        exit;
                                }

                                $this->load->model("Technology_model");
                                $technologies = $this->Technology_model->get_active_technology_list();
                                $emp_technologies = array();
                                if(count($technologies)>0){
                                    foreach($technologies as $k=>$v) {
                                        $tech_id = $v->id;
                                        $emp_technologies = $this->Technology_model->get_all_technlogywise_emp_list($id,$tech_id);
                                        if(isset($emp_technologies) && !empty($emp_technologies)) {
                                            $technologies[$k]->exp_3_year = $emp_technologies->exp_3_year;
                                            $technologies[$k]->exp_3_year_cost = $emp_technologies->exp_3_year_cost;
                                            $technologies[$k]->exp_5_year = $emp_technologies->exp_5_year;
                                            $technologies[$k]->exp_5_year_cost = $emp_technologies->exp_5_year_cost;
                                            $technologies[$k]->exp_7_year = $emp_technologies->exp_7_year;
                                            $technologies[$k]->exp_7_year_cost = $emp_technologies->exp_7_year_cost;
                                            $technologies[$k]->total_emp = $emp_technologies->total_emp;
                                        } else {
                                            $technologies[$k]->exp_3_year = NULL;
                                            $technologies[$k]->exp_3_year_cost = NULL;
                                            $technologies[$k]->exp_5_year = NULL;
                                            $technologies[$k]->exp_5_year_cost = NULL;
                                            $technologies[$k]->exp_7_year = NULL;
                                            $technologies[$k]->exp_7_year_cost = NULL;
                                            $technologies[$k]->total_emp = 0;
                                        }
                                    }
                                }
                               
                                $data['technologies'] = $technologies;
                                $data['customer_id'] = $id;
                               

                                if($_SESSION['customer_role_type'] == "V") {
                                    $this->load->view('admin/vendors/vendor_employee_details', $data);
                                } else {
                                    $this->load->view('admin/vendors/employee_details', $data);
                                }
                            } else {
                                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                                redirect('employees/employees_list');
                                exit;
                            }

                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                            redirect('employees/employees_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('admin/dashboard');
                        exit;
                    }  
                }  else {
                    redirect("admin"); exit;
                } 
        } else {
           redirect("admin"); exit;
        }
    }

    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }

    public function check_email_exist() {
        $email = $this->input->post("email");
        if(!empty($email)) {
            $this->load->model("Vendors_model");
            $check_email_exist = $this->Vendors_model->check_email_exist($email);

            if($check_email_exist>0){
                 echo "false";
                 exit;
            } else {
                 echo "true";
                 exit;
            }
        }
    }
}
?>