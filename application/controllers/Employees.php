<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

require_once $_SERVER['DOCUMENT_ROOT'].'/ezihire/vendor/HtmlToDoc.class.php';

class Employees extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));
        $this->load->library('pagination');
        $this->load->database();

        $this->load->model("Common_model");
        $this->load->model("Employees_model");
        $this->load->model("Technology_model");

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }   
    }

    public function change_status() {
        $table = $this->input->post("table");
        $id = $this->input->post("id");
        $on_off = $this->input->post("on_off");
        $id_field = $this->input->post("id_field");
        $status = $this->input->post("status");

        $this->Common_model->data_update($table, array("$status" => $on_off), array("$id_field" => $id));
        echo $_POST['on_off'];
    }

    public function employees_list() {  

        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('employees_list',$my_permission)) {
                
                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "Employee";
                $data['page'] = "Employee";
                $data['action'] = "List";
                $this->load->model("Technology_model");
                $technologies = $this->Technology_model->get_active_technology_list();
                $data['technologies'] = $technologies;

                $this->load->model("Vendors_model");
                $vendors = $this->Vendors_model->get_vendors_by_role();
                $data['vendors'] = $vendors;
                $data['customer_role_type'] = $customer_role_type;

                $this->load->view('admin/employees/list', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_employees_list(){
        if(_is_customer_login($this)) {
            
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();
            
            if(in_array('employees_list',$my_permission)) {

                $fetch_data = $this->Employees_model->get_employees_list_by_ajax($user_id,$customer_role_type);
                $data = array();  

                $no = 1;
                foreach($fetch_data as $row)  
                {       
                        $customer_id = $row->id;
                        $random_array = array("round-success","round-primary","round-danger","round-warning");
                        $random_keys=array_rand($random_array,1);
                        $backColor =  $random_array[$random_keys];
                        $image = $row->image;
                       
                        $imagePath= '';
                        if ($image == NULL) { 
                                if($row->gender == "Male") {
                                    $imagefullpaths = base_url()."img/boy.png";
                                } else if($row->gender == "Female"){
                                    $imagefullpaths = base_url()."img/girl.png";
                                }
                                $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpaths.'"><img src="';
                                $imagePath .= $imagefullpaths;
                                $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                </span>';
                        } else {

					
						$fullpath = $this->config->item('customers_images_path');
						$returnpath = $this->config->item('customers_images_uploaded_path');
						$imagefullpath = $fullpath.$image;
						$imagefullpaths = $returnpath.$image;
						
                            if(file_exists($imagefullpath)){
                            	
                                $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpaths.'"><img src="';
                                $imagePath .= $imagefullpaths;
                                $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                </span>';
                            }  else {
                            	
                                if($row->gender == "Male") {
                                    $imagefullpaths = base_url()."img/boy.png";
                                } else if($row->gender == "Female"){
                                    $imagefullpaths = base_url()."img/girl.png";
                                }
                                $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpaths.'"><img src="';
                                $imagePath .= $imagefullpaths;
                                $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                </span>';
                            }
                        } 

                        $name = $row->name;
                        $company_name = $row->username;
                        $code = $row->code;
                        
                    	$linkedin = $row->linkedin;
                
                        $email = "";
                        if(isset($row->email) && !empty($row->email)){
                            $email .= '<a href="mailto:'.$row->email.'" target="_blank">'.$row->email.'</a>';
                        } else {
                            $email .="N.A";
                        }

                        $mobileno = "";
                        if(isset($row->mobileno) && !empty($row->mobileno)){
                            $mobileno .= '<a href="tel:'.$row->mobileno.'" target="_blank">+91'.$row->mobileno.'</a>';
                        } else {
                            $mobileno .="N.A";
                        }

                        // if(isset($row->alternateno)){
                        //     $mobileno .=" - "; 
                        //     $mobileno .= '<a href="tel:'.$row->alternateno.'" target="_blank">+91'.$row->alternateno.'</a>';
                        // }
                        $is_social = "";
                        if(isset($row->linkedin) && !empty($row->linkedin)) {
                            $is_social = '<a href="'.$linkedin.'" target="_blank" class="btn btn-circle btn-success"><i class="fa fa-linkedin"></i></a>';
                        } else {
                        	$is_social = "N.A.";
                        }

                        $status = '';
                        $readonly = "";
                        $disabled = "";

                        $status .=' <input type="checkbox" class="js-switch tgl_checkbox" data-color="#55ce63" data-table="tbl_employees" data-status="status" data-idfield="id"  data-id="';
                        $status .=$customer_id;
                        $status .='" id="cb_'.$customer_id.'"';

                        if(!in_array('employees_edit', $my_permission)) {
                            $status .="readonly ";
                            $status .="disabled ";
                        }

                        if($row->status == '1') {
                            $status .='checked ';
                        } 
                       
                       
                        $status .=' />';

                        $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));

                        $action = '';

                        // $htd->createDoc($html,'htmldoc',true);

                        $doc_link = '';
                        $doc_link.= '<a href="'.site_url("employees/employees_doc/".$row->id) .'" data-toggle="tooltip"  data-placement="left" title="Docx"  class="btn bg-themess"><font color="#fff"><i class="fa fa-file"></i></font></a> ';

                    $pdf_link = '';
                        $pdf_link.= '<a href="'.site_url("employees/employees_pdf/".$row->id) .'" data-toggle="tooltip"  data-placement="left" title="PDF"  class="btn btn-primary"><i class="fa fa-download"></i></a> ';

                        $view_link = '';
                        $view_link.= '<a href="'.site_url("employees/employees_view/".$row->id) .'" data-toggle="tooltip"  data-placement="left" title="View"  class="btn btn-warning"><i class="fa fa-eye"></i></a>';

                        $permission_link = '';
                        if($user_id == 1 || $user_id == "1") {
                            $permission_link.= '<a href="'.site_url("employees/permission_details/".$customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Permission"  class="btn btn-primary"><i class="fa fa-unlock"></i></a>';
                        }
                        
                        $edit_link = '';
                        if(in_array('employees_edit', $my_permission)) {
                            $edit_link.= ' <a href="'.site_url("employees/employees_edit/".$customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-edit"></i></a>';
                        }

                        $delete_link = '';
                        if(in_array('employees_delete', $my_permission)) {
                            $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                            $delete_link .= "confirm_delete('";
                            $delete_link .= site_url("employees/employees_delete/".$customer_id);
                            $delete_link .= "'";
                            $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                        }

                        $action  =  $doc_link.$pdf_link.$view_link.$edit_link.$delete_link;

                        $sub_array = array();  
                        $sub_array[] = $no;  
                        $sub_array[] = $imagePath;  
                        // $sub_array[] = $code;  
                        $sub_array[] = $company_name;  
                        $sub_array[] = $name;  
                        $sub_array[] = $email;  
                        $sub_array[] = $mobileno;  
                        // $sub_array[] = $is_social;  
                        $sub_array[] = $status;  
                        $sub_array[] = $action;  

                        $data[] = $sub_array;  
                        $no = $no+1;
                }
                $output = array(  
                    "draw"                    =>     intval($_POST["draw"]),  
                    "recordsTotal"          =>      $this->Employees_model->get_all_employees_result($user_id,$customer_role_type),  
                    "recordsFiltered"     =>     $this->Employees_model->get_filtered_data_employees($user_id,$customer_role_type),  
                    "data"                    =>     $data  
                ); 
                echo json_encode($output);  
           }
        } else {
            echo "Not access this function";
        }
    }

    public function employees_listview() {

        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('employees_list',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "Employee";
                $data['page'] = "Employee";
                $data['action'] = "List";
                $this->load->model("Technology_model");
                $technologies = $this->Technology_model->get_active_technology_list();
                $data['technologies'] = $technologies;

                $this->load->model("Vendors_model");
                $vendors = $this->Vendors_model->get_vendors_by_role();
                $data['vendors'] = $vendors;



                $skills = $this->input->post("skills") ? $this->input->post("skills") : '0';
                $vendor_id = $this->input->post("vendor_id") ? $this->input->post("vendor_id") : '0';
                $search_type = $this->input->post("search_type") ? $this->input->post("search_type") : '0';
              

                $data['skills'] = $skills;
                $data['customer_id'] = $vendor_id;
                $data['search_type'] = $search_type;
                $data['customer_role_type'] = $customer_role_type;
                $this->load->view('admin/employees/grid_list', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_employees_listview($rowno=0){

        if (_is_customer_login($this)) {

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            
            $skillsid = $_GET['skillsid'];
            $vendor_id = $_GET['vendor_id'];
            $search_type = $_GET['search_type'];
            
            $rowperpage = 4;
            if($rowno != 0){
              $rowno = ($rowno-1) * $rowperpage;
            }

            if(isset($skillsid) && !empty($skillsid) && $skillsid >=1 || isset($vendor_id) && !empty($vendor_id) && $vendor_id >=1) {
                $all_records = $this->Employees_model->get_employees_skills_records_all($user_id,$customer_role_type,$skillsid,$vendor_id,$search_type);
                $allcount = count($all_records);

                $users_record = $this->Employees_model->get_employees_skills_records($rowperpage, $rowno, $user_id,$customer_role_type,$skillsid,$vendor_id,$search_type);
            } else {
                $allcount = $this->db->count_all('tbl_employees');
                $this->db->limit($rowperpage, $rowno);
                    if(isset($user_id) && !empty($user_id) && isset($customer_role_type) && !empty($customer_role_type) && $customer_role_type == "V") {
                        $this->db->where('customers_id',$user_id);
                    }  
                    $this->db->order_by("id", "desc");
                    $this->db->group_by("id", "desc");
                $users_record = $this->db->get('tbl_employees')->result_array();
            }
            
            $tmp_users_record = array();

          

            if(count($users_record)>0){
                foreach ($users_record as $k => $value) {
                    $tmp_users_record[$k] = $value;
                    $id= $value['id'];
                    $primary_skills = $value['primary_skills'];
                    

                    // get all technologies name from employee id
                    $primary_skills_data = $this->Technology_model->get_technology_by_id($primary_skills);
                    if(isset($primary_skills_data) && !empty($primary_skills_data)){
                        $primary_skills_name = $primary_skills_data->tname;
                    } else {
                        $primary_skills_name="N.A.";
                    }

                    // get all technologies name from employee id
                    $skills = $this->Employees_model->get_skills_list_by_id($id);
                    
                    $skill_arr = array();
                    if(count($skills)>0){
                        foreach ($skills as $keys => $values) {
                           $skill_arr[] = $values['tname']; 
                        }
                    }
                    $skills_list = "";
                    $skills_list = implode(', ', $skill_arr);

                    $tmp_users_record[$k]['skills'] = $skills_list;

                    $random_array = array("round-success","round-primary","round-danger","round-warning");
                    $random_keys=array_rand($random_array,1);
                    $backColor =  $random_array[$random_keys];
                    $image = $value['image'];
                    $imagePath= '';
                    $imagefullpaths = "";
                    if (empty($image) && is_null($image)) { 
                        
                            if($value['gender'] == "Male") {
                                $imagefullpaths = base_url()."img/boy.png";
                            } else if($value['gender'] == "Female"){
                                $imagefullpaths = base_url()."img/girl.png";
                            }

                            $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpaths.'"><img src="';
                            $imagePath .= $imagefullpaths;
                            $imagePath .= '"style="height: 100px;width: 100px;border-radius: 50%;" border="1" class="img-circle img-responsive"></a>
                            </span>';
                    } else {

                        $fullpath = $this->config->item('customers_images_path');
						$returnpath = $this->config->item('customers_images_uploaded_path');
						$imagefullpath = $fullpath.$image;
						
						$imagefullpaths = $returnpath.$image;
						

                        if(file_exists($imagefullpath)){

                            $image = $value['image'];
                            $imagePath= '';
                            $imagePath .= '<img src="';
                            $imagePath .= $imagefullpaths;
                            $imagePath .= '" class="img-circle img-responsive">';
                        } else {
                            if($value['gender'] == "Male") {
                                $imagefullpaths = base_url()."img/boy.png";
                            } else if($value['gender'] == "Female"){
                                $imagefullpaths = base_url()."img/girl.png";
                            }
                            $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpaths.'"><img src="';
                            $imagePath .= $imagefullpaths;
                            $imagePath .= '"style="height: 100px;width: 100px;border-radius: 50%;" border="1" class="img-circle img-responsive""></a>
                            </span>';
                        }
                    } 
            
                    $email = "";
                    // $email = $value['email'];
                    if(isset($value['email']) && !empty($value['email'])){
                        $email .= '<a href="mailto:'.$value['email'].'" target="_blank">'.$value['email'].'</a>';
                    }

                    $mobileno = "";
                    if(isset($value['mobileno']) && !empty($value['mobileno'])){
                        $mobileno .= '<a href="tel:'.$value['mobileno'].'" target="_blank">+'.$value['mobileno'].'</a>';
                    }
                    $tmp_users_record[$k]['image'] = $imagePath;
                    $tmp_users_record[$k]['email'] = $email;
                    $tmp_users_record[$k]['mobileno'] = $mobileno;
                    $tmp_users_record[$k]['primary_skills_name'] = $primary_skills_name;
                }
            } else {
                $tmp_users_record = array();
            }
          

            $config['base_url'] = base_url().'employees/fetch_employees_listview';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $allcount;
            $config['per_page'] = $rowperpage;
     
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tag_close']  = '</span></li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tag_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tag_close']  = '</span></li>';
     
            $this->pagination->initialize($config);
     
            $data['pagination'] = $this->pagination->create_links();
            $data['result'] = $tmp_users_record;
            $data['row'] = $rowno;
            echo json_encode($data);
        }
    }

    public function employees_add() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('employees_add',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Ourteam";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Employee";
                    $data['action'] = "Add";
                    $data['admin_id'] = $admin_id;
                    $data['customer_role_type'] = $customer_role_type;

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {

                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('name', 'Name', 'trim|required');
                        $this->form_validation->set_rules('vendor_id', 'Vendor Name', 'trim|required');
                        // $this->form_validation->set_rules('address', 'Address', 'trim|required');
                       // $this->form_validation->set_rules('mobileno', 'Mobile No', 'trim|required');
                        $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
                        // $this->form_validation->set_rules('alternateno', 'Alternate No', 'trim|required');
                        $this->form_validation->set_rules('designation', 'Designation', 'trim|required');
                        $this->form_validation->set_rules('linkedin', 'linkedin', 'trim');
                       // $this->form_validation->set_rules('email', 'Email', 'trim|required');
                        if ($this->form_validation->run() == FALSE) {
                            $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                        } else {
                            $vendor_id = $this->input->post("vendor_id") ? $this->input->post("vendor_id") : '';
                            $name = $this->input->post("name") ? $this->input->post("name") : '';
                            $email = $this->input->post("email") ? $this->input->post("email") : '';
                            $address = $this->input->post("address") ? $this->input->post("address") : '';
                            $gender = $this->input->post("gender") ? $this->input->post("gender") : 'Male';
                            $mobileno = $this->input->post("mobileno") ? $this->input->post("mobileno") : '';
                            $alternateno = $this->input->post("alternateno") ? $this->input->post("alternateno") : '';
                            $about_me = $this->input->post("about_me") ? $this->input->post("about_me") : '';
                            $linkedin = $this->input->post("linkedin") ? $this->input->post("linkedin") : '';
                            $designation = $this->input->post("designation") ? $this->input->post("designation") : '';
                            $graduation = $this->input->post("graduation") ? $this->input->post("graduation") : '';
                            $passing_year = $this->input->post("passing_year") ? $this->input->post("passing_year") : '';
                            $board_university = $this->input->post("board_university") ? $this->input->post("board_university") : '';
                            $percentage = $this->input->post("percentage") ? $this->input->post("percentage") : '';


                            $company_name = $this->input->post("company_name") ? $this->input->post("company_name") : '';
                            $exprience_year = $this->input->post("exprience_year") ? $this->input->post("exprience_year") : '';
                            $job_position = $this->input->post("job_position") ? $this->input->post("job_position") : '';
                            $job_location = $this->input->post("job_location") ? $this->input->post("job_location") : '';
                            $job_role = $this->input->post("job_role") ? $this->input->post("job_role") : '';

                            $project_name = $this->input->post("project_name") ? $this->input->post("project_name") : '';
                            $project_details = $this->input->post("project_details") ? $this->input->post("project_details") : '';
                            $project_url = $this->input->post("project_url") ? $this->input->post("project_url") : '';
                            $skills_multiple = $this->input->post("skills_multiple") ? $this->input->post("skills_multiple") : '';

                            $skills = $this->input->post("skills") ? $this->input->post("skills") : '';
                            $primary_skills = $this->input->post("primary_skills") ? $this->input->post("primary_skills") : '';

                            $exp_year = $this->input->post("exp_year") ? $this->input->post("exp_year") : '0';
                            $exp_month = $this->input->post("exp_month") ? $this->input->post("exp_month") : '0';

                            
                            if ($_FILES["vendor_image"]["size"] > 0) {
                                $temp = explode(".", $_FILES["vendor_image"]["name"]);
                                $newfilename = time() . '.' . end($temp);
                                $uploadpath = $this->config->item('customers_images_path');
                                $returnpath = $this->config->item('customers_images_uploaded_path');
                                $file_name = $this->file_upload($_FILES["vendor_image"], $uploadpath, $returnpath);
                                $logo = $file_name;
                            } else {
                                $logo = NULL;
                            }

                            $code = "";
                            $code .= "EHEM_";
                            $code .= $this->get_random_number(5);

                            $status = "1";

                                // insert education
                                $education_array = array();
                                if(count($graduation)>0) {
                                    $total_graduation = count($graduation);
                                    for($i=0;$i<$total_graduation;$i++){
                                        $education_array[$i]['graduation'] = $_POST['graduation'][$i];
                                        $education_array[$i]['passing_year'] = $_POST['passing_year'][$i];
                                        $education_array[$i]['board_university'] = $_POST['board_university'][$i];
                                        $education_array[$i]['percentage'] = $_POST['percentage'][$i];
                                    }
                                }

                                // insert company
                                $company_array = array();
                                if(count($company_name)>0) {
                                    $total_company = count($company_name);
                                    for($j=0;$j<$total_company;$j++){
                                        $company_array[$j]['company_name'] = $_POST['company_name'][$j];
                                        $company_array[$j]['exprience_year'] = $_POST['exprience_year'][$j];
                                        $company_array[$j]['job_position'] = $_POST['job_position'][$j];
                                        $company_array[$j]['job_location'] = $_POST['job_location'][$j];
                                        $company_array[$j]['job_role'] = $_POST['job_role'][$j];
                                    }
                                }

                                // insert projects
                                $project_array = array();
                                if(count($project_name)>0) {
                                    $total_project_name = count($project_name);
                                    for($k=0;$k<$total_project_name;$k++){
                                        $project_array[$k]['project_name'] = $_POST['project_name'][$k];
                                        $project_array[$k]['project_details'] = $_POST['project_details'][$k];
                                        $project_array[$k]['project_url'] = $_POST['project_url'][$k];
                                        $project_array[$k]['project_url_android'] = $_POST['project_url_android'][$k];
                                        $project_array[$k]['project_url_ios'] = $_POST['project_url_ios'][$k];
                                        $project_array[$k]['project_url_ios'] = $_POST['project_url_ios'][$k];
                                        $project_array[$k]['skills_multiple'] = $_POST['skills_multiple'][$k];
                                    }
                                }

                                // insert skills
                                $skill_array = array();
                                if(count($skills)>0) {
                                    $total_skill_array = count($skills);
                                    for($s=0;$s<$total_skill_array;$s++){
                                        $skill_array[$s]['technology_id'] = $_POST['skills'][$s];
                                    }
                                }

                                $data = array();
                                $this->load->model("common_model");
                                $created_at = date('Y-m-d H:i:s');
                                $data_insert = array(
                                    "customers_id "=>$vendor_id,
                                    "code"=>$code,
                                    "image"=>$logo,
                                    "name"=>$name,
                                    "email"=>$email,
                                    "address"=>$address,
                                    "gender"=>$gender,
                                    "mobileno"=>$mobileno,
                                    "alternateno"=>$alternateno,
                                    "about_me"=>$about_me,
                                    "linkedin"=>$linkedin,
                                    "designation"=>$designation,
                                    "primary_skills"=>$primary_skills,
                                    "exp_year"=>$exp_year,
                                    "exp_month"=>$exp_month,
                                    "status"=>$status,
                                    "created_at"=>$created_at,
                                    "updated_at"=>$created_at,
                                );

                                $employee_id = $this->common_model->data_insert("tbl_employees", $data_insert, TRUE);

                                if(isset($employee_id) && !empty($employee_id)) {
                                    if(count($education_array)>0){
                                        foreach ($education_array as $key => $value) {
                                            if(isset($value['graduation']) && !empty($value['graduation'])) {

                                                $data_education_insert = array();
                                                $data_education_insert['customers_id'] = $vendor_id;
                                                $data_education_insert['employee_id'] = $employee_id;
                                                $data_education_insert['graduation'] = $value['graduation'];
                                                $data_education_insert['passing_year'] = $value['passing_year'];
                                                $data_education_insert['board_university'] = $value['board_university'];
                                                $data_education_insert['percentage'] = $value['percentage'];
                                                $data_education_insert['created_at'] = $created_at;
                                                $data_education_insert['updated_at'] = $created_at;

                                                $this->common_model->data_insert("tbl_employees_education", $data_education_insert, TRUE);
                                            }
                                        }
                                    }

                                    if(count($company_array)>0){
                                        foreach ($company_array as $key => $values) {
                                            if(isset($values['company_name']) && !empty($values['company_name'])) {
                                                $data_company_insert = array();
                                                $data_company_insert['customers_id'] = $vendor_id;
                                                $data_company_insert['employee_id'] = $employee_id;
                                                $data_company_insert['company_name'] = $values['company_name'];
                                                $data_company_insert['exprience_year'] = $values['exprience_year'];
                                                $data_company_insert['job_position'] = $values['job_position'];
                                                $data_company_insert['job_role'] = $values['job_role'];
                                                $data_company_insert['job_location'] = $values['job_location'];
                                                $data_company_insert['created_at'] = $created_at;
                                                $data_company_insert['updated_at'] = $created_at;
                                                $this->common_model->data_insert("tbl_employees_company", $data_company_insert, TRUE);
                                            }
                                        }
                                    }

                                    if(count($project_array)>0){
                                        foreach ($project_array as $key => $valuep) {
                                            if(isset($valuep['project_name']) && !empty($valuep['project_name'])) {
                                                $data_project_insert = array();
                                                $data_project_insert['customers_id'] = $vendor_id;
                                                $data_project_insert['employee_id'] = $employee_id;
                                                $data_project_insert['project_name'] = $valuep['project_name'];
                                                $data_project_insert['project_details'] = $valuep['project_details'];
                                                $data_project_insert['project_url'] = $valuep['project_url'];
                                                $data_project_insert['project_url_android'] = $valuep['project_url_android'];
                                                $data_project_insert['project_url_ios'] = $valuep['project_url_ios'];
                                                $data_project_insert['skills_multiple'] = $valuep['skills_multiple'];
                                                $data_project_insert['created_at'] = $created_at;
                                                $data_project_insert['updated_at'] = $created_at;
                                                $last_project_id = $this->common_model->data_insert("tbl_employees_projects", $data_project_insert, TRUE);

                                                // if(isset($last_project_id) && !empty($last_project_id)) {

                                                //     if(count($valuep['skills_multiple'])>0) {

                                                //         $dataWheres = array();
                                                //         $dataWheres['employee_id'] = $id;
                                                //         $dataWheres['project_id'] = $last_project_id;

                                                //         $this->common_model->data_remove('tbl_employees_project_skills',$dataWheres);

                                                //         foreach($valuep['skills_multiple'] as $p=>$pv) {
                                                           
                                                //             $this->load->model("technology_model");
                                                //             $Detailss = $this->technology_model->get_technology_by_id($pv);

                                                //             $tnames = "";
                                                //             $tnames = $Detailss->tname;
                                                //             $data_project_skill_insert = array();
                                                //             $data_project_skill_insert['customers_id'] = $vendor_id;
                                                //             $data_project_skill_insert['employee_id'] = $id;
                                                //             $data_project_skill_insert['project_id'] = $last_project_id;
                                                //             $data_project_skill_insert['technology_id'] = $pv;
                                                //             $data_project_skill_insert['tname'] = $tnames;
                                                //             $data_project_skill_insert['created_at'] = $created_at;
                                                //             $data_project_skill_insert['updated_at'] = $created_at;

                                                //             $this->common_model->data_insert("tbl_employees_project_skills", $data_project_skill_insert, TRUE);
                                                //         }
                                                //     }
                                                // }
                                            }
                                        }
                                    }

                                    if(count($skill_array)>0){
                                        foreach ($skill_array as $key => $valuess) {
                                            if(isset($valuess['technology_id']) && !empty($valuess['technology_id'])) {

                                                // get technology name
                                                 $this->load->model("technology_model");
                                                $Details = $this->technology_model->get_technology_by_id($valuess['technology_id']);
                                                $tname = "";
                                                $tname = $Details->tname;
                                                $data_skill_insert = array();
                                                $data_skill_insert['customers_id'] = $vendor_id;
                                                $data_skill_insert['employee_id'] = $employee_id;
                                                $data_skill_insert['technology_id'] = $valuess['technology_id'];
                                                $data_skill_insert['tname'] = $tname;
                                                $data_skill_insert['created_at'] = $created_at;
                                                $data_skill_insert['updated_at'] = $created_at;
                                                $this->common_model->data_insert("tbl_employees_skills", $data_skill_insert, TRUE);
                                            }
                                        }
                                    }
                                } 

                                $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Employee created successfully</div>');
                                    redirect("employees/employees_list");
                                    exit;
                        }
                    }
                        
                    $this->load->model("Vendors_model");
                    $vendors = $this->Vendors_model->get_vendors_by_role();
                    $data['vendors'] = $vendors;

                    $this->load->model("Technology_model");
                    $technologies = $this->Technology_model->get_active_technology_list();
                    $data['technologies'] = $technologies;


                    $this->load->view('admin/employees/add', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('employees/dashboard');
                    exit;
                } 
            } else {
                redirect("admin"); exit;
            }    

        } else {
           redirect("admin"); exit;
        }
    }

    public function employees_edit($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('employees_edit',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Our Team";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Employee";
                    $data['action'] = "Edit";
                    $data['admin_id'] = $admin_id;
                    $data['customer_role_type'] = $customer_role_type;
                        
                    $employees = $this->Employees_model->get_employees_list_by_id($id);
                    if(isset($employees) && !empty($employees)) {

                        $exist_vendor_id = $employees->customers_id;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }

                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {

                            $data["employees"] = $employees;

                            $employeesEducation = $this->Employees_model->get_employees_education_list_by_id($id);
                            $data["empEdu"] = $employeesEducation;

                            $employeesCompany = $this->Employees_model->get_employees_company_list_by_id($id);
                            $data["empCompany"] = $employeesCompany;

                            $employeesProject = $this->Employees_model->get_employees_project_list_by_id($id);
                            $data["empProject"] = $employeesProject;

                            $employeesSkills = $this->Employees_model->get_employees_skill_list_by_id($id);
                            $data["empSkill"] = $employeesSkills;

                            if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {
                                
                                $this->load->library('form_validation');
                                $this->form_validation->set_rules('name', 'Name', 'trim|required');
                                $this->form_validation->set_rules('vendor_id', 'Vendor Name', 'trim|required');
                                // $this->form_validation->set_rules('address', 'Address', 'trim|required');
                               // $this->form_validation->set_rules('mobileno', 'Mobile No', 'trim|required');
                                $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
                                $this->form_validation->set_rules('linkedin', 'linkedin', 'trim');
                                $this->form_validation->set_rules('designation', 'designation', 'trim|required');
                               // $this->form_validation->set_rules('email', 'Email', 'trim|required');
                                if ($this->form_validation->run() == FALSE) {
                                    $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                                } else {
                                   
                                    $vendor_id = $this->input->post("vendor_id") ? $this->input->post("vendor_id") : '';
                                    $name = $this->input->post("name") ? $this->input->post("name") : '';
                                    $email = $this->input->post("email") ? $this->input->post("email") : '';
                                    $address = $this->input->post("address") ? $this->input->post("address") : '';
                                    $gender = $this->input->post("gender") ? $this->input->post("gender") : 'Male';
                                    $mobileno = $this->input->post("mobileno") ? $this->input->post("mobileno") : '';
                                    $alternateno = $this->input->post("alternateno") ? $this->input->post("alternateno") : '';
                                    $about_me = $this->input->post("about_me") ? $this->input->post("about_me") : '';
                                    $linkedin = $this->input->post("linkedin") ? $this->input->post("linkedin") : '';
                                    $designation = $this->input->post("designation") ? $this->input->post("designation") : '';
                                    $graduation = $this->input->post("graduation") ? $this->input->post("graduation") : '';
                                    $passing_year = $this->input->post("passing_year") ? $this->input->post("passing_year") : '';
                                    $board_university = $this->input->post("board_university") ? $this->input->post("board_university") : '';
                                    $percentage = $this->input->post("percentage") ? $this->input->post("percentage") : '';

                                    $company_name = $this->input->post("company_name") ? $this->input->post("company_name") : '';
                                    $exprience_year = $this->input->post("exprience_year") ? $this->input->post("exprience_year") : '';
                                    $job_position = $this->input->post("job_position") ? $this->input->post("job_position") : '';
                                    $job_location = $this->input->post("job_location") ? $this->input->post("job_location") : '';
                                    $job_role = $this->input->post("job_role") ? $this->input->post("job_role") : '';

                                    $project_name = $this->input->post("project_name") ? $this->input->post("project_name") : '';
                                    $project_details = $this->input->post("project_details") ? $this->input->post("project_details") : '';
                                    $project_url = $this->input->post("project_url") ? $this->input->post("project_url") : '';
                                    $skills_multiple = $this->input->post("skills_multiple") ? $this->input->post("skills_multiple") : '';

                                    $skills = $this->input->post("skills") ? $this->input->post("skills") : '';
                                    $primary_skills = $this->input->post("primary_skills") ? $this->input->post("primary_skills") : '';
                                    $exp_year = $this->input->post("exp_year") ? $this->input->post("exp_year") : '0';
                                    $exp_month = $this->input->post("exp_month") ? $this->input->post("exp_month") : '0';
                                    
                                    if ($_FILES["vendor_image"]["size"] > 0) {
                                        $temp = explode(".", $_FILES["vendor_image"]["name"]);
                                        $newfilename = time() . '.' . end($temp);
                                        $uploadpath = $this->config->item('customers_images_path');
                                        $returnpath = $this->config->item('customers_images_uploaded_path');
                                        $file_name = $this->file_upload($_FILES["vendor_image"], $uploadpath, $returnpath);
                                        $logo = $file_name;
                                    } else {
                                        $logo = $employees->image;
                                    }

                                    $status = "1";

                                        // insert education
                                        $education_array = array();
                                        if(count($graduation)>0) {
                                            $total_graduation = count($graduation);
                                            for($i=0;$i<$total_graduation;$i++){
                                                $education_array[$i]['graduation'] = $_POST['graduation'][$i];
                                                $education_array[$i]['passing_year'] = $_POST['passing_year'][$i];
                                                $education_array[$i]['board_university'] = $_POST['board_university'][$i];
                                                $education_array[$i]['percentage'] = $_POST['percentage'][$i];
                                            }
                                        }

                                        // insert company
                                        $company_array = array();
                                        if(count($company_name)>0) {
                                            $total_company = count($company_name);
                                            for($j=0;$j<$total_company;$j++){
                                                $company_array[$j]['company_name'] = $_POST['company_name'][$j];
                                                // $company_array[$j]['exprience_year'] = $_POST['exprience_year'][$j];
                                                $company_array[$j]['job_position'] = $_POST['job_position'][$j];
                                                // $company_array[$j]['job_location'] = $_POST['job_location'][$j];
                                                $company_array[$j]['job_role'] = $_POST['job_role'][$j];
                                            }
                                        }

                                     
                                        // insert projects
                                        $project_array = array();
                                        if(count($project_name)>0) {
                                            $total_project_name = count($project_name);
                                            for($k=0;$k<$total_project_name;$k++){
                                                $project_array[$k]['project_name'] = $_POST['project_name'][$k];
                                                $project_array[$k]['project_details'] = $_POST['project_details'][$k];
                                                $project_array[$k]['project_url'] = $_POST['project_url'][$k];
                                                $project_array[$k]['project_url_android'] = $_POST['project_url_android'][$k];
                                                $project_array[$k]['project_url_ios'] = $_POST['project_url_ios'][$k];
                                                $project_array[$k]['project_url_ios'] = $_POST['project_url_ios'][$k];
                                                $project_array[$k]['skills_multiple'] = $_POST['skills_multiple'][$k];
                                            }
                                        }
                                       
                                      

                                      
                                        // insert skills
                                        $skill_array = array();
                                        if(count($skills)>0) {
                                            $total_skill_array = count($skills);
                                            for($s=0;$s<$total_skill_array;$s++){
                                                $skill_array[$s]['technology_id'] = $_POST['skills'][$s];
                                            }
                                        }

                                        $data = array();
                                        $this->load->model("common_model");
                                        $created_at = date('Y-m-d H:i:s');
                                        $data_insert = array(
                                            "customers_id "=>$vendor_id,
                                            "image"=>$logo,
                                            "name"=>$name,
                                            "gender"=>$gender,
                                            "email"=>$email,
                                            "address"=>$address,
                                            "mobileno"=>$mobileno,
                                            "alternateno"=>$alternateno,
                                            "about_me"=>$about_me,
                                            "linkedin"=>$linkedin,
                                            "designation"=>$designation,
                                            "primary_skills"=>$primary_skills,
                                            "exp_year"=>$exp_year,
                                            "exp_month"=>$exp_month,
                                            "status"=>$status,                                    
                                            "updated_at"=>$created_at,
                                        );

                                        $employee_id = $this->common_model->data_update("tbl_employees", $data_insert, array("id" => $id));

                                        $dataWhere = array();
                                        $dataWhere['employee_id'] = $id;

                                        if(isset($employee_id) && !empty($employee_id)) {
                                            if(count($education_array)>0){
                                                // remove all education then save it
                                                $this->common_model->data_remove('tbl_employees_education',$dataWhere);
                                                foreach ($education_array as $key => $value) {
                                                    if(isset($value['graduation']) && !empty($value['graduation'])) {

                                                        $data_education_insert = array();
                                                        $data_education_insert['customers_id'] = $vendor_id;
                                                        $data_education_insert['employee_id'] = $id;
                                                        $data_education_insert['graduation'] = $value['graduation'];
                                                        $data_education_insert['passing_year'] = $value['passing_year'];
                                                        $data_education_insert['board_university'] = $value['board_university'];
                                                        $data_education_insert['percentage'] = $value['percentage'];
                                                        $data_education_insert['created_at'] = $created_at;
                                                        $data_education_insert['updated_at'] = $created_at;

                                                        $this->common_model->data_insert("tbl_employees_education", $data_education_insert, TRUE);
                                                    }
                                                }
                                            }

                                            if(count($company_array)>0){
                                                $this->common_model->data_remove('tbl_employees_company',$dataWhere);
                                                foreach ($company_array as $key => $values) {
                                                    if(isset($values['company_name']) && !empty($values['company_name'])) {
                                                        $data_company_insert = array();
                                                        $data_company_insert['customers_id'] = $vendor_id;
                                                        $data_company_insert['employee_id'] = $id;
                                                        $data_company_insert['company_name'] = $values['company_name'];
                                                        // $data_company_insert['exprience_year'] = $values['exprience_year'];
                                                        $data_company_insert['job_position'] = $values['job_position'];
                                                        // $data_company_insert['job_location'] = $values['job_location'];
                                                        $data_company_insert['job_role'] = $values['job_role'];
                                                        $data_company_insert['created_at'] = $created_at;
                                                        $data_company_insert['updated_at'] = $created_at;
                                                        $this->common_model->data_insert("tbl_employees_company", $data_company_insert, TRUE);
                                                    }
                                                }
                                            }


                                            if(count($project_array)>0){
                                                $this->common_model->data_remove('tbl_employees_projects',$dataWhere);


                                                foreach ($project_array as $key => $valuep) {

                                                    if(isset($valuep['project_name']) && !empty($valuep['project_name'])) {

                                                        $data_project_insert = array();
                                                        $data_project_insert['customers_id'] = $vendor_id;
                                                        $data_project_insert['employee_id'] = $id;
                                                        $data_project_insert['project_name'] = $valuep['project_name'];
                                                        $data_project_insert['project_details'] = $valuep['project_details'];
                                                        $data_project_insert['project_url'] = $valuep['project_url'];
                                                        $data_project_insert['project_url_android'] = $valuep['project_url_android'];
                                                        $data_project_insert['project_url_ios'] = $valuep['project_url_ios'];
                                                        $data_project_insert['skills_multiple'] = $valuep['skills_multiple'];
                                                        $data_project_insert['created_at'] = $created_at;
                                                        $data_project_insert['updated_at'] = $created_at;

                                                        $last_project_id = $this->common_model->data_insert("tbl_employees_projects", $data_project_insert, TRUE);
                                                    }
                                                }
                                            }   

                                          
                                            if(count($skill_array)>0){
                                                $this->common_model->data_remove('tbl_employees_skills',$dataWhere);
                                                foreach ($skill_array as $key => $valuess) {
                                                    if(isset($valuess['technology_id']) && !empty($valuess['technology_id'])) {

                                                        // get technology name
                                                        $this->load->model("technology_model");
                                                        $Details = $this->technology_model->get_technology_by_id($valuess['technology_id']);
                                                        $tname = "";
                                                        $tname = $Details->tname;
                                                        $data_skill_insert = array();
                                                        $data_skill_insert['customers_id'] = $vendor_id;
                                                        $data_skill_insert['employee_id'] = $id;
                                                        $data_skill_insert['technology_id'] = $valuess['technology_id'];
                                                        $data_skill_insert['tname'] = $tname;
                                                        $data_skill_insert['created_at'] = $created_at;
                                                        $data_skill_insert['updated_at'] = $created_at;
                                                        $this->common_model->data_insert("tbl_employees_skills", $data_skill_insert, TRUE);
                                                    }
                                                }
                                            }
                                        } 

                                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Employee updated successfully</div>');
                                            redirect("employees/employees_list");
                                            exit;
                                }
                            }

                            $this->load->model("Vendors_model");
                            $vendors = $this->Vendors_model->get_vendors_by_role();
                            $data['vendors'] = $vendors;

                            $this->load->model("Technology_model");
                            $technologies = $this->Technology_model->get_active_technology_list();
                            $data['technologies'] = $technologies;


                            $this->load->view('admin/employees/edit', $data);
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                            redirect('employees/employees_list');
                            exit;
                        }
                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('employees/employees_list');
                        exit;
                    }

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('employees/dashboard');
                    exit;
                }  
            }  else {
                redirect("admin"); exit;
            }   
            
        } else {
           redirect("admin"); exit;
        }
    } 

    public function deleteimages(){

        $data_insert = array(
            "image"=>NULL
        );
        $id=$_POST['id'];
        $this->load->model("common_model");      
        $category=$this->common_model->data_update("tbl_employees", $data_insert, array("id" => $id));
        if($category){
            echo 1;
        } else {
            echo 0;
        }
    }

    public function employees_view($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('employees_list',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Our Team";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Employee";
                    $data['action'] = "View";

                    
                    $employees = $this->Employees_model->get_employees_list_by_id($id);

                    if(isset($employees) && !empty($employees)) {

                        $exist_vendor_id = $employees->customers_id;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }
                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {
                            $data["employees"] = $employees;

                            $employeesEducation = $this->Employees_model->get_employees_education_list_by_id($id);
                            $data["empEdu"] = $employeesEducation;

                            $employeesCompany = $this->Employees_model->get_employees_company_list_by_id($id);
                            $data["empCompany"] = $employeesCompany;

                            $employeesProject = $this->Employees_model->get_employees_project_list_by_id($id);
                            $data["empProject"] = $employeesProject;

                            $employeesSkills = $this->Employees_model->get_employees_skill_list_by_id($id);
                            $data["empSkill"] = $employeesSkills;

                            $this->load->model("Vendors_model");
                            $vendors = $this->Vendors_model->get_vendors_by_role();
                            $data['vendors'] = $vendors;

                            $this->load->model("Technology_model");
                            $technologies = $this->Technology_model->get_active_technology_list();
                            $data['technologies'] = $technologies;

                            $primary_skills_data = $this->Technology_model->get_technology_by_id($employees->primary_skills);

                            $data['primary_skills'] = $primary_skills_data->tname;


                            $this->load->view('admin/employees/view', $data);
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                            redirect('employees/employees_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('employees/employees_list');
                        exit;
                    }

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('employees/dashboard');
                    exit;
                }  
            }  else {
                redirect("admin"); exit;
            }   
        } else {
           redirect("admin"); exit;
        }
    } 

    public function employees_pdf($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('employees_list',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Our Team";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Employee";
                    $data['action'] = "View";

                    
                    $employees = $this->Employees_model->get_employees_list_by_id($id);

                    if(isset($employees) && !empty($employees)) {

                        $exist_vendor_id = $employees->customers_id;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }
                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {
                            $data["employees"] = $employees;

                            $employeesEducation = $this->Employees_model->get_employees_education_list_by_id($id);
                            $data["empEdu"] = $employeesEducation;

                            $employeesCompany = $this->Employees_model->get_employees_company_list_by_id($id);
                            $data["empCompany"] = $employeesCompany;

                            $employeesProject = $this->Employees_model->get_employees_project_list_by_id($id);
                            $data["empProject"] = $employeesProject;

                            $employeesSkills = $this->Employees_model->get_employees_skill_list_by_id($id);
                            $data["empSkill"] = $employeesSkills;
                           

                            $skill_arr = array();

                            if(count($employeesSkills)>0){
                                foreach ($employeesSkills as $keys => $values) {
                                   $skill_arr[] = $values->tname; 
                                }
                            }

                         
                            $data['skill_arr'] = $skill_arr;
                            $skills_list = "";
                            $skills_list = implode(', ', $skill_arr);
                            $data['skills_list'] = $skills_list;

                            $this->load->model("Vendors_model");
                            $vendors = $this->Vendors_model->get_vendors_by_role();
                            $data['vendors'] = $vendors;

                            $this->load->model("Technology_model");
                            $technologies = $this->Technology_model->get_active_technology_list();
                            $data['technologies'] = $technologies;
                           
                            $random_no = rand(1111111111,9999999999).$id;
                            $invoice_name = substr($random_no,-10);

                            $name = explode(" ",$employees->name);
                            $first_name = strtolower($name[0]);
                            $designation = str_replace(".","-",$employees->designation);
                            $designation = str_replace(" ","-",$designation);
                            $designation = strtolower($designation);
                            $html = $this->load->view('admin/employees/invoice', $data, true);
                           
                            // echo $html;
                            // exit;
                            $pdfFilePath = "infiraise-".$first_name."-".$designation.".pdf";
                           
                            $this->load->library('Pdf');
                            $this->pdf->pdf->AddPage('P');
                            $this->pdf->pdf->WriteHTML($html);
                            $this->load->model("common_model");

                            $this->pdf->pdf->Output($pdfFilePath, "D");
                            exit;

                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                            redirect('employees/employees_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('employees/employees_list');
                        exit;
                    }

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('employees/dashboard');
                    exit;
                }  
            }  else {
                redirect("admin"); exit;
            }   
        } else {
           redirect("admin"); exit;
        }
    }

    public function employees_doc($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('employees_list',$my_permission)) {

                    $employees = $this->Employees_model->get_employees_list_by_id($id);

                    if(isset($employees) && !empty($employees)) {

                        $exist_vendor_id = $employees->customers_id;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }
                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {
                            $data["employees"] = $employees;

                            $employeesEducation = $this->Employees_model->get_employees_education_list_by_id($id);
                            $data["empEdu"] = $employeesEducation;

                            $employeesCompany = $this->Employees_model->get_employees_company_list_by_id($id);
                            $data["empCompany"] = $employeesCompany;

                            $employeesProject = $this->Employees_model->get_employees_project_list_by_id($id);
                            $data["empProject"] = $employeesProject;

                            $employeesSkills = $this->Employees_model->get_employees_skill_list_by_id($id);
                            $data["empSkill"] = $employeesSkills;
                                $skill_arr = array();
                            if(count($employeesSkills)>0){
                                foreach ($employeesSkills as $keys => $values) {
                                   $skill_arr[] = $values->tname; 
                                }
                            }
                            $skills_list = "";
                            $skills_list = implode(', ', $skill_arr);
                            $data['skills_list'] = $skills_list;

                            $this->load->model("Vendors_model");
                            $vendors = $this->Vendors_model->get_vendors_by_role();
                            $data['vendors'] = $vendors;

                            $this->load->model("Technology_model");
                            $technologies = $this->Technology_model->get_active_technology_list();
                            $data['technologies'] = $technologies;
                           
                            $random_no = rand(1111111111,9999999999).$id;
                            $invoice_name = substr($random_no,-10);

                            $name = explode(" ",$employees->name);
                            $first_name = strtolower($name[0]);
                            $designation = str_replace(".","-",$employees->designation);
                            $designation = str_replace(" ","-",$designation);
                            $designation = strtolower($designation);
                            $html = $this->load->view('admin/employees/doc', $data, true);
                            $pdfFilePath = "infiraise-".$first_name."-".$designation;
                            $htd = new HTML_TO_DOC();
                            $docfile = $htd->createDoc($html,$pdfFilePath,true);
                            exit;
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                            redirect('employees/employees_list');
                            exit;
                        }

                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('employees/employees_list');
                        exit;
                    }

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('employees/dashboard');
                    exit;
                }  
            }  else {
                redirect("admin"); exit;
            }   
        } else {
           redirect("admin"); exit;
        }
    }

    public function employees_delete($id) {
        if (_is_customer_login($this)) {

            if($id=="1" || $id =="1") {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
            } else {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $customer_role_type =  $this->session->userdata('customer_role_type');
                    $my_permission = get_my_permission();

                    if(in_array('employees_delete',$my_permission)) {
                        $data = array();
                        $data['id'] = $id;

                        $category = $this->Employees_model->get_employees_list_by_id($id);
                        if ($category) {

                            $this->load->model("common_model");
                            $this->common_model->data_remove('tbl_employees',$data);

                            $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Emplyeee deleted successfully</div>');
                            redirect("employees/employees_list");
                            exit;
                        }
                    }  else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('employees/dashboard');
                        exit;
                    }  
                }
            }  
        } else {
           redirect("admin"); exit;
        }
    } 

    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }

    public function signout() {

        $customer_id = $this->session->userdata('customer_id');
        if(isset($customer_id)) {
            if($customer_id >= 1) {
                session_destroy();
                foreach($_SESSION as $k=>$v){
                    unset($_SESSION[$v]);
                }
                $this->session->sess_destroy();
                $this->session->unset_userdata($_SESSION);

                $this->session->sess_destroy();

                if (isset($_COOKIE['customer_login'])) {
                    unset($_COOKIE['customer_login']); 
                    setcookie('customer_login', null, -1, '/'); 
                } 


                $data["error"] = '<div class="alert alert-success alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success !</strong> You have logout successfully </div>';
                $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Invalid User and password. </div>';

                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong>  You have logout successfully.</div>');
                redirect("admin/login");
                exit;
            } else {
                redirect("admin/login");
                exit;
            }
        } else {
            redirect("admin/login");
            exit;
        }
    }
    
    public function check_email_exist() {
        $email = $this->input->post("email");
        if(!empty($email)) {
            
            $check_email_exist = $this->Employees_model->check_email_exist($email);

            if($check_email_exist>0){
                 echo "false";
                 exit;
            } else {
                 echo "true";
                 exit;
            }
        }
    }


    // upload documents

    public function my_employees_list() {  

        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('my_employees_list',$my_permission)) {
                
                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "My Employee";
                $data['page'] = "My Employee";
                $data['action'] = "List";
                $this->load->model("Technology_model");
                $technologies = $this->Technology_model->get_active_technology_list();
                $data['technologies'] = $technologies;

                $this->load->model("Vendors_model");
                $vendors = $this->Vendors_model->get_vendors_by_role();
                $data['vendors'] = $vendors;
                $data['customer_role_type'] = $customer_role_type;

                $this->load->view('admin/my_employees/list', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_my_employees_list(){
        if(_is_customer_login($this)) {
            
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();
            
            if(in_array('my_employees_list',$my_permission)) {

                $fetch_data = $this->Employees_model->get_my_employees_list_by_ajax($user_id,$customer_role_type);
                $data = array();  
                
                $no = 1;
                foreach($fetch_data as $row)  
                {       
                        $skill = $row->tname;
                        $customer_id = $row->id;
                        $image = $row->gender;
                       
                        $imagePath= '';
                        if($row->gender == "Male") {
                            $imagefullpaths = base_url()."img/boy.png";
                        } else if($row->gender == "Female"){
                            $imagefullpaths = base_url()."img/girl.png";
                        }
                        $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpaths.'"><img src="';
                        $imagePath .= $imagefullpaths;
                        $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                        </span>';
                                
                        $name = $row->name;
                        $company_name = $row->username;
                        $budget = $row->budget;
                        $candidate_availability = $row->candidate_availability;
                        $resume = $row->resume;

                        $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));

                        $action = '';
                        $fullpath = $this->config->item('customers_images_path');
                        $returnpath = $this->config->item('customers_images_uploaded_path');
                        $resumefullpath = $returnpath.$resume;
                        $pdf_link = '';
                        if(isset($resume) && !empty($resume)) {

                            $pdf_link.= '<a href="'.$resumefullpath.'" data-toggle="tooltip"  data-placement="left" title="PDF"  class="btn btn-primary"><i class="fa fa-download"></i></a> ';
                        }

                        $edit_link = '';
                        if(in_array('my_employees_edit', $my_permission)) {
                            $edit_link.= ' <a href="'.site_url("employees/my_employees_edit/".$customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-edit"></i></a>';
                        }

                        $delete_link = '';
                        if(in_array('my_employees_delete', $my_permission)) {
                            $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                            $delete_link .= "confirm_delete('";
                            $delete_link .= site_url("employees/my_employees_delete/".$customer_id);
                            $delete_link .= "'";
                            $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                        }

                        $action  =  $pdf_link.$edit_link.$delete_link;

                        $sub_array = array();  
                        $sub_array[] = $no;  
                        $sub_array[] = $imagePath;  
                        // $sub_array[] = $code;  
                        $sub_array[] = $company_name;  
                        $sub_array[] = $name;  
                        $sub_array[] = $skill;  
                        $sub_array[] = $budget;  
                        $sub_array[] = $candidate_availability;  
                        $sub_array[] = $action;  

                        $data[] = $sub_array;  
                        $no = $no+1;
                }
                $output = array(  
                    "draw"                    =>     intval($_POST["draw"]),  
                    "recordsTotal"          =>      $this->Employees_model->get_all_my_employees_result($user_id,$customer_role_type),  
                    "recordsFiltered"     =>     $this->Employees_model->get_filtered_data_my_employees($user_id,$customer_role_type),  
                    "data"                    =>     $data  
                ); 
                echo json_encode($output);  
           }
        } else {
            echo "Not access this function";
        }
    }

    public function my_employees_add() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('my_employees_add',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Ourteam";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "My Employee";
                    $data['action'] = "Add";
                    $data['admin_id'] = $admin_id;
                    $data['customer_role_type'] = $customer_role_type;

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {
                        
                        $this->load->library('form_validation');
                        $this->form_validation->set_rules('name', 'Name', 'trim|required');
                        $this->form_validation->set_rules('vendor_id', 'Vendor Name', 'trim|required');
                        
                        $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
                        // $this->form_validation->set_rules('alternateno', 'Alternate No', 'trim|required');
                        $this->form_validation->set_rules('budget', 'budget', 'trim|required');
                       
                        if ($this->form_validation->run() == FALSE) {
                            $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                        } else {

                            $vendor_id = $this->input->post("vendor_id") ? $this->input->post("vendor_id") : '';
                            $name = $this->input->post("name") ? $this->input->post("name") : '';
                            $gender = $this->input->post("gender") ? $this->input->post("gender") : 'Male';
                           
                            $skill_id = $this->input->post("skill_id") ? $this->input->post("skill_id") : '';
                            $budget = $this->input->post("budget") ? $this->input->post("budget") : '';

                            $candidate_availability = $this->input->post("candidate_availability") ? $this->input->post("candidate_availability") : '';
                           
                            
                            if ($_FILES["resume"]["size"] > 0) {
                                $temp = explode(".", $_FILES["resume"]["name"]);
                                $newfilename = time() . '.' . end($temp);
                                $uploadpath = $this->config->item('customers_images_path');
                                $returnpath = $this->config->item('customers_images_uploaded_path');
                                $file_name = $this->file_upload($_FILES["resume"], $uploadpath, $returnpath);
                                $logo = $file_name;
                            } else {
                                $logo = NULL;
                            }

                            $status = "1";


                                $data = array();
                                $this->load->model("common_model");
                                $created_at = date('Y-m-d H:i:s');
                                $data_insert = array(
                                    "customers_id "=>$vendor_id,
                                    "resume"=>$logo,
                                    "name"=>$name,
                                    "gender"=>$gender,
                                    "primary_skills"=>$skill_id,
                                    "budget"=>$budget,
                                    "candidate_availability"=>$candidate_availability,
                                    "status"=>$status,
                                    "created_at"=>$created_at,
                                    "updated_at"=>$created_at,
                                );

                                $employee_id = $this->common_model->data_insert("tbl_employees_data", $data_insert, TRUE);

                                $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Employee created successfully</div>');
                                    redirect("employees/my_employees_list");
                                    exit;
                        }
                    }
                        
                    $this->load->model("Vendors_model");
                    $vendors = $this->Vendors_model->get_vendors_by_role();
                    $data['vendors'] = $vendors;

                    $this->load->model("Technology_model");
                    $technologies = $this->Technology_model->get_active_technology_list();
                    $data['technologies'] = $technologies;


                    $this->load->view('admin/my_employees/add', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('employees/dashboard');
                    exit;
                } 
            } else {
                redirect("admin"); exit;
            }    

        } else {
           redirect("admin"); exit;
        }
    }

    public function my_employees_edit($id) {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {

                $my_permission = get_my_permission();

                if(in_array('employees_edit',$my_permission)) {

                    $data = array();
                    $data["error"] = "";
                    $data["pageTitle"] = "Admin Our Team";
                    $data['admin'] = "Admin";
                    $data["title"] = PROJECT_NAME;
                    $data['page'] = "Employee";
                    $data['action'] = "Edit";
                    $data['admin_id'] = $admin_id;
                    $data['customer_role_type'] = $customer_role_type;
                        
                    $employees = $this->Employees_model->get_my_employees_list_by_id($id);
                    if(isset($employees) && !empty($employees)) {

                        $exist_vendor_id = $employees->customers_id;
                        $is_access = "No";
                        if($customer_role_type == "V") {
                            if($admin_id == $exist_vendor_id) {
                                $is_access = "Yes";
                            }
                        } else if($customer_role_type == "A" || $customer_role_type == "U") {
                            $is_access = "Yes";
                        } else {
                            $is_access = "No";
                        }

                        if(isset($is_access) && !empty($is_access) && $is_access == "Yes") {

                            $data["employees"] = $employees;

                            if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {

                                $this->load->library('form_validation');
                                $this->form_validation->set_rules('name', 'Name', 'trim|required');
                                $this->form_validation->set_rules('vendor_id', 'Vendor Name', 'trim|required');

                                $this->form_validation->set_rules('gender', 'Gender', 'trim|required');
                                // $this->form_validation->set_rules('alternateno', 'Alternate No', 'trim|required');
                                $this->form_validation->set_rules('budget', 'budget', 'trim|required');
                    

                                if ($this->form_validation->run() == FALSE) {
                                    $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                                } else {
                                   
                                $vendor_id = $this->input->post("vendor_id") ? $this->input->post("vendor_id") : '';
                                $name = $this->input->post("name") ? $this->input->post("name") : '';
                                $gender = $this->input->post("gender") ? $this->input->post("gender") : 'Male';

                                $skill_id = $this->input->post("skill_id") ? $this->input->post("skill_id") : '';
                                $budget = $this->input->post("budget") ? $this->input->post("budget") : '';

                                $candidate_availability = $this->input->post("candidate_availability") ? $this->input->post("candidate_availability") : '';
                           
                                    
                                    if ($_FILES["resume"]["size"] > 0) {
                                        $temp = explode(".", $_FILES["resume"]["name"]);
                                        $newfilename = time() . '.' . end($temp);
                                        $uploadpath = $this->config->item('customers_images_path');
                                        $returnpath = $this->config->item('customers_images_uploaded_path');
                                        $file_name = $this->file_upload($_FILES["resume"], $uploadpath, $returnpath);
                                        $logo = $file_name;
                                    } else {
                                        $logo = $employees->resume;
                                    }

                                       $data = array();
                                $this->load->model("common_model");
                                $created_at = date('Y-m-d H:i:s');
                                $data_insert = array(
                                    "customers_id "=>$vendor_id,
                                    "resume"=>$logo,
                                    "name"=>$name,
                                    "gender"=>$gender,
                                    "primary_skills"=>$skill_id,
                                    "budget"=>$budget,
                                    "candidate_availability"=>$candidate_availability,
                                    "status"=>$status,
                                    "created_at"=>$created_at,
                                    "updated_at"=>$created_at,
                                );

                                 $this->common_model->data_update("tbl_employees_data", $data_insert, array("id" => $id));

                                $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Employee updated successfully</div>');
                                    redirect("employees/my_employees_list");
                                    exit;
                                }
                            }

                            $this->load->model("Vendors_model");
                            $vendors = $this->Vendors_model->get_vendors_by_role();
                            $data['vendors'] = $vendors;

                            $this->load->model("Technology_model");
                            $technologies = $this->Technology_model->get_active_technology_list();
                            $data['technologies'] = $technologies;


                            $this->load->view('admin/my_employees/edit', $data);
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to edit this record.</div>');
                            redirect('employees/my_employees_list');
                            exit;
                        }
                    } else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> Record not exist</div>');
                        redirect('employees/my_employees_list');
                        exit;
                    }

                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('employees/dashboard');
                    exit;
                }  
            }  else {
                redirect("admin"); exit;
            }   
            
        } else {
           redirect("admin"); exit;
        }
    }   

    public function my_employees_delete($id) {
        if (_is_customer_login($this)) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $admin_id = $this->session->userdata('customer_id');
                $role =  $this->session->userdata('customer_role_id');
              
                if($role >= 1 && $admin_id >= 1) {

                    $customer_role_type =  $this->session->userdata('customer_role_type');
                    $my_permission = get_my_permission();

                    if(in_array('my_employees_delete',$my_permission)) {
                        $data = array();
                        $data['id'] = $id;

                        $category = $this->Employees_model->get_my_employees_list_by_id($id);
                        if ($category) {

                            $this->load->model("common_model");
                            $this->common_model->data_remove('tbl_employees_data',$data);

                            $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Emplyeee deleted successfully</div>');
                            redirect("employees/my_employees_list");
                            exit;
                        }
                    }  else {
                        $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                        redirect('employees/dashboard');
                        exit;
                    }  
                }
             
        } else {
           redirect("admin"); exit;
        }
    } 


}
?>