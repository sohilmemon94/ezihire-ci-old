<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');

class job extends CI_Controller { 

    public function __construct() {
        parent::__construct();
       
            // Your own constructor code
            $this->load->database();
            $this->load->helper('login_helper');
            $this->load->model("common_model");
            $this->load->library('javascript');
            $this->load->library('form_validation');
            $this->load->library('email');
            $this->load->library('session');
            $this->load->dbutil();
            $this->load->helper('file');
            $this->load->helper('download');
            $this->load->helper(array('form', 'url'));
            
            if ($_SERVER['HTTP_HOST'] == "localhost") {
                $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
            } else {
                $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
            }   

            $customer_status = get_user_status();
            if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
            } else {
                auto_signout();
            }      
    } 

    public function job_list() {
        if (_is_customer_login($this)) {

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $data["error"] = "";
            $data['page'] = "Job";
            $data['action'] = "List";
            
            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();
              
                if(in_array('job_list',$my_permission)) {
                    $this->load->model("job_model");
                    $jobs = $this->job_model->get_all_job_list();
                    $data['jobs'] = $jobs;
                    $this->load->view('admin/job/list', $data);
                } else { 
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }
            } else {
               redirect("admin"); exit;
            }
        } else {
          redirect("admin"); exit;
        }
    }

    public function add_job(){
        if (_is_customer_login($this)) {
            $data["error"] = "";
            $data['page'] = "Job";
            $data['action'] = "Add";

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            
            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();
                if(in_array('add_job',$my_permission)) {

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Insert") {
                      
                        $vname = $this->input->post("vname") ? $this->input->post("vname") : '';
                       
                        $status = ($this->input->post("status") == "on") ? '1' : '0';
                        $ct = date('Y-m-d H:i:s');
                        
                        $this->load->model("common_model");

                        $ct = date('Y-m-d H:i:s');
                        $file_namewithpath=NULL;


                        $data_insert_menu = array(
                            "ename" => $vname,
                            "status"=>$status,
                            "created_at"=>$ct,
                            "updated_at"=>$ct,
                        );

                        $temp =  $this->common_model->data_insert("tbl_job", $data_insert_menu, TRUE);
                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Job created successfully.</div>');
                        redirect("job/job_list");
                        exit;
                    }

                    $this->load->view('admin/job/add', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }

            } else {
               redirect("admin"); exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function edit_job($id) {
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $data["error"] = "";
            $data['page'] = "Job";
            $data['action'] = "Edit";
            
            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();

                if(in_array('edit_job',$my_permission)) {

                    $this->load->model("job_model");
                    $editDetails = $this->job_model->get_job_by_id($id);
                    $data['job_edit'] = $editDetails;
                    
                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {

                        $vname = $this->input->post("vname") ? $this->input->post("vname") : '';
                        $status = ($this->input->post("status") == "on") ? '1' : '0';
                        $ct = date('Y-m-d H:i:s');
                        
                        $this->load->model("common_model");

                        $ct = date('Y-m-d H:i:s');
                        $file_namewithpath=NULL;

                        $data_update_menu = array(
                            "ename" => $vname,
                            "status"=>$status,
                            "updated_at"=>$ct,
                        );

                        $this->common_model->data_update("tbl_job", $data_update_menu, array("id" => $id));

                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Job updated successfully.</div>');
                       
                         redirect("job/job_list");
                        exit;
                    }

                    $this->load->view('admin/job/edit', $data);
                } else {
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }

            } else {
               redirect("admin"); exit;
            }
        } else {
           redirect("admin"); exit;
        }
    } 

    public function job_delete($id){
        if (_is_customer_login($this)) {
           
            $this->load->model("job_model");
            $menuDetails = $this->job_model->get_job_by_id($id);
            $job_id = $menuDetails->id;
          
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            
            if($role >= 1 && $admin_id >= 1) {

                $customer_role_type =  $this->session->userdata('customer_role_type');
                $my_permission = get_my_permission();
                if(in_array('job_delete',$my_permission)) {

                    $this->db->query("DELETE FROM `tbl_job` WHERE `id` = '" . $job_id . "' ");
                    $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <strong>Success ! </strong> Job deleted successfully
                                  </div>');
                    redirect("job/job_list");
                    exit;
                } else { 
                    $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                    redirect('admin/dashboard');
                    exit;
                }
            } else {
               redirect("admin"); exit;
                exit;
            } 
        }
    } 

    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }
    
    public function get_random_string($length = 10) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }
}
