<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));
        $this->load->library('pagination');
        $this->load->database();

        $this->load->model("Common_model");
        $this->load->model("Vendors_model");

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }  
    }

    // _is_customer_login for admin
    // _is_customer_login for customer

    public function vendors_skills_list() {

        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

            if(in_array('approve_vendors_list',$my_permission)) {

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "Vendors";
                $data['page'] = "Vendors";
                $data['action'] = "Skill List";
                $this->load->model("Technology_model");
                $technologies = $this->Technology_model->get_active_technology_list();
                $data['technologies'] = $technologies;
                $skills = $this->input->post("skills") ? $this->input->post("skills") : '0';
                $data['skills'] = $skills;

                $exp_year = $this->input->post("exp_year") ? $this->input->post("exp_year") : '0';
                $data['exp_year'] = $exp_year;
                $this->load->view('admin/vendors/grid_list', $data);
                
            } else {
                $this->session->set_flashdata("message", '<div class="alert alert-info alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning ! </strong> You have not access to module.</div>');
                redirect('admin/dashboard');
                exit;
            }
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_vendors_listview($rowno=0){

        if (_is_customer_login($this)) {

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $skillsid = $this->input->get("skillsid") ? $this->input->get("skillsid") : '';
            $exp_id = $this->input->get("exp_id") ? $this->input->get("exp_id") : '';

            
            $rowperpage = 4;
            if($rowno != 0){
              $rowno = ($rowno-1) * $rowperpage;
            }
           
            if(isset($skillsid) && !empty($skillsid) && $skillsid >=1) {
                $all_records = $this->Vendors_model->get_vendors_skills_records_all($skillsid,$exp_id);
                $allcount = count($all_records);
                
                $users_record = $this->Vendors_model->get_vendors_skills_records($rowperpage, $rowno, $skillsid,$exp_id);

            } else {
                $allcount =  $this->db->where('role_id','4')->from("tbl_customers")->count_all_results();
                $this->db->limit($rowperpage, $rowno);
                $this->db->where('role_type','V');
                $this->db->where('role_id','4');
                $this->db->order_by("customer_id", "desc");
                $users_record = $this->db->get('tbl_customers')->result_array();
            }

            $tmp_users_record = array();
            if(count($users_record)>0){
                foreach ($users_record as $k => $value) {
                    $tmp_users_record[$k] = $value;
                    $id= $value['customer_id'];
                   
                    // get all technologies name from employee id
                    $random_array = array("round-success","round-primary","round-danger","round-warning");

                    $random_keys=array_rand($random_array,1);
                    $backColor =  $random_array[$random_keys];
                    $image = $value['image'];
                    $imagePath= '';
                    if (empty($image) && is_null($image)) { 
                        $imagePath .= '<span class="round ';
                        $imagePath .= $backColor;
                        $imagePath .='">';
                        $firstCharacter = substr($value['username'] , 0, 1);
                        $imagePath .= trim(strtoupper($firstCharacter));
                        $imagePath .='</span>';
                    } else {
                        $imagefullpath = base_url().$image;
                        $returnpath = $this->config->item('customers_images_uploaded_path');
                        if(!file_exists($imagefullpath)){
                            $image = $value['image'];
                            $imagePath= '';
                            $imagefullpath = $returnpath.$image;
                            $imagePath .= '<img src="';
                            $imagePath .= $imagefullpath;
                            $imagePath .= '" class="img-circle img-responsive">';
                        } else {
                            $imagePath .= '<span class="round ';
                            $imagePath .= $backColor;
                            $imagePath .='">';
                            $firstCharacter = substr($value['username'] , 0, 1);
                            $imagePath .= trim(strtoupper($firstCharacter));
                            $imagePath .='</span>';
                        }
                    } 
                   
                        $email = "";
                        // $email = $value['email'];
                        if(isset($value['email']) && !empty($value['email'])){
                            $email .= '<a href="mailto:'.$value['email'].'" target="_blank">'.$value['email'].'</a>';
                        }

                        $mobileno = "";
                        if(isset($value['mobileno']) && !empty($value['mobileno'])){
                            $mobileno .= '<a href="tel:'.$value['mobileno'].'" target="_blank">+'.$value['mobileno'].'</a>';
                        }

                        // if(isset($value['alternateno']) && !empty($value['alternateno'])){
                        //     $mobileno .=" - "; 
                        //     $mobileno .= '<a href="tel:'.$value['alternateno'].'" target="_blank">+'.$value['alternateno'].'</a>';
                        // }

                    $tmp_users_record[$k]['image'] = $imagePath;
                   
                    $tmp_users_record[$k]['email'] = $email;
                    $tmp_users_record[$k]['mobileno'] = $mobileno;
                }
            } else {
                $tmp_users_record = array();
            }

            $config['base_url'] = base_url().'reports/fetch_vendors_listview';
            $config['use_page_numbers'] = TRUE;
            $config['total_rows'] = $allcount;
            $config['per_page'] = $rowperpage;
     
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tag_close']  = '<span aria-hidden="true"></span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tag_close']  = '</span></li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tag_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tag_close']  = '</span></li>';
     
            $this->pagination->initialize($config);
     
            $data['pagination'] = $this->pagination->create_links();
            $data['result'] = $tmp_users_record;
            $data['row'] = $rowno;
            echo json_encode($data);
        }
    }

    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }
    
    public function check_email_exist() {
        $email = $this->input->post("email");
        if(!empty($email)) {
            
            $check_email_exist = $this->Employees_model->check_email_exist($email);

            if($check_email_exist>0){
                 echo "false";
                 exit;
            } else {
                 echo "true";
                 exit;
            }
        }
    }
}
?>