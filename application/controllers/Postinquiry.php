<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Postinquiry extends CI_Controller {

    public function __construct() {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));

        $this->load->model("Common_model");
        $this->load->model("Postinquiry_model");

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }
    }

    // _is_customer_login for admin
    // _is_customer_login for customer

  
    public function deleteimages(){

        $data_insert = array(
            "image"=>NULL
        );
        $id=$_POST['id'];
        $this->load->model("common_model");      
        $category=$this->common_model->data_update("tbl_customers", $data_insert, array("customer_id" => $id));
        if($category){
            echo 1;
        } else {
            echo 0;
        }
    }


    public function change_status() {
        $table = $this->input->post("table");
        $id = $this->input->post("id");
        $on_off = $this->input->post("on_off");
        $id_field = $this->input->post("id_field");
        $status = $this->input->post("status");

        $this->Common_model->data_update($table, array("$status" => $on_off), array("$id_field" => $id));
        echo $_POST['on_off'];
    }

    public function Postinquiry_list() {
        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');

            $my_permission = get_my_permission();

                $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin List";
                $data['admin'] = "Admin";
                $data['title'] = "Admin";
                $data['title'] = "Post Inquiry";
                $data['page'] = "Post Inquiry";
                $data['action'] = "List";

                $this->load->model("Technology_model");
                $technologies = $this->Technology_model->get_active_technology_list();
                $data['technologies'] = $technologies;

                $this->load->model("Vendors_model");
                $vendors = $this->Vendors_model->get_vendors_by_role();
                $data['vendors'] = $vendors;
                $data['customer_role_type'] = $customer_role_type;
                
                $this->load->view('admin/postinquiry/list', $data);
                
           
        } else {
           redirect("admin"); exit;
        }
    }

    public function fetch_postinquiry_list(){
        if(_is_customer_login($this)) {
            
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $customer_role =  $this->session->userdata('customer_role_id');
            $user_id = $this->session->userdata('customer_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
            $my_permission = get_my_permission();

         

                $search_array = array();
                if(isset($_POST['extra_search']) && !empty($_POST['extra_search'])) {
                    foreach ($_POST['extra_search'] as $key => $value) {
                        $search_array[$value['name']] = $value['value'];
                    }
                }

                $this->load->model("Postinquiry_model");
                $fetch_data = $this->Postinquiry_model->get_postinquiry_list_by_ajax($user_id,$customer_role_type,$search_array);


                $data = array();  
                $no = 1;
                foreach($fetch_data as $row)  
                {  
                    $customer_id  = $row->id;
                    $date = date('d-M-Y',strtotime($row->post_date));
                    $username = $row->username;
                    $email = $row->email;
                    $mobile = $row->mobileno;
                    $duration = $row->duration;
                    $tname = $row->tname;
                    $budget = $row->budget;
                    $experience = $row->experience ."+" ;
                    $no_of_position = $row->no_of_position;
                    $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));

                    $action = '';


                    $view_link = '';


                    $pdf_link = '';
                    $pdf_link.= '<a href="'.site_url("Postinquiry/Postinquiry_pdf/".$row->id) .'" data-toggle="tooltip"  data-placement="left" title="View"  class="btn btn-primary"><i class="fa fa-download"></i></a> ';

                    if(in_array('interviewschedule_list', $my_permission)) {
                        $view_link.= ' <a href="'.site_url("Postinquiry/Postinquiry_interviewschedule_details/".$customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-warning"><i class="fa fa-eye"></i></a>';
                    }

                    $edit_link = '';
                    if(in_array('Postinquiry_edit', $my_permission)) {
                        $edit_link.= ' <a href="'.site_url("Postinquiry/Postinquiry_edit/".$customer_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-edit"></i></a>';
                    }

                    $delete_link = '';
                    if(in_array('Postinquiry_delete', $my_permission)) {
                        $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                        $delete_link .= "confirm_delete('";
                        $delete_link .= site_url("Postinquiry/Postinquiry_delete/".$customer_id);
                        $delete_link .= "'";
                        $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                    }

                    $sub_array = array();  
                    $sub_array[] = $no;  
                    $sub_array[] = $date;  
                    $sub_array[] = $username;  
                    $sub_array[] = $email;  
                    $sub_array[] = $mobile;  
                    $sub_array[] = $tname;  
                    $sub_array[] = $duration;  
                    $sub_array[] = $budget;  
                    $sub_array[] = $experience;  
                    $sub_array[] = $no_of_position;  

                    $data[] = $sub_array;  
                    $no = $no+1;
                }
                $output = array(  
                    "draw"                    =>     intval($_POST["draw"]),  
                    "recordsTotal"          =>      $this->Postinquiry_model->get_all_Postinquiry_result($user_id,$customer_role_type,$search_array),  
                    "recordsFiltered"     =>     $this->Postinquiry_model->get_filtered_data_Postinquiry($user_id,$customer_role_type,$search_array),  
                    "data"                    =>     $data  
                ); 
                echo json_encode($output);  
          
        } else {
            echo "Not access this function";
        }
    }


    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }

    public function check_email_exist() {
        $email = $this->input->post("email");
        if(!empty($email)) {
            $this->load->model("postjob_model");
            $check_email_exist = $this->postjob_model->check_email_exist($email);

            if($check_email_exist>0){
                 echo "false";
                 exit;
            } else {
                 echo "true";
                 exit;
            }
        }
    }
}
?>