<?php
defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');
class Backup extends CI_Controller {

    public function __construct()
    {  
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');

        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));

        if ($_SERVER['HTTP_HOST'] == "localhost") {
                $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
        } else {
            $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
        }

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }
    }
 

    public function index()
    {
        if(_is_customer_login($this)){
                redirect('admin/dashboard');
                exit;
        }else{
            redirect('admin');
            exit;
        }
    }

    public function create_backup() {
        if (_is_customer_login($this)) {

            $server_name   = HOSTNAME;
            $username      = USERNAME;
            $password      = PASSWORD;
            $database_name = DATABASE;
            $ct = date('Y-m-d H:i:s');
            $strtotime = strtotime($ct);
            $date_string   = $strtotime;
           
            $this->load->model("common_model");

            $cmd = "mysqldump --routines -h {$server_name} -u {$username} -p{$password} {$database_name} > " . BACKUP_PATH . "{$date_string}.sql";

            exec($cmd);

            if(file_exists(BACKUP_PATH.$date_string.".sql")) {
                    
                    $ct = date('Y-m-d H:i:s');

                    $data_insert_menu = array(
                        "name"=>$date_string,
                        "file"=>$date_string.".sql",
                        "created_at"=>$ct,
                        "updated_at"=>$ct,
                    );
                   
                    $temp =  $this->common_model->data_insert("tbl_backup", $data_insert_menu, TRUE);
                    $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> SQL Backup created successfully.</div>');
                        redirect("backup/backup_list");
                        exit;
            } else {
                
                    $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error ! </strong>Backup file is not created. try again.</div>');
                    redirect("admin/dashboard");
                    exit;
            }
        } else {
            redirect("admin");
            exit;
        }
    }

    public function backup_list() {
        if (_is_customer_login($this)) {

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            if($role >= 1 && $admin_id >= 1) {

                $data["error"] = "";
                $data["pageTitle"] = "Customers Dashboard";
                $data['admin'] = "Admin";
                $data['page'] = "Backup";
                $data['action'] = "List";
              
                $files = array_diff(scandir(BACKUP_PATH), array('.', '..'));
                $data['files'] = $files;

                $this->load->model("Backup_model");
                $backup = $this->Backup_model->get_all_backup_list();

                // echo "<pre>";
                // print_r($backup);
                // exit;

                $data['backup'] = $backup;
                $this->load->view("admin/backup/list",$data);

            } else {
                redirect("admin"); exit;
            }
        } else {
            redirect("admin");
            exit;
        }
    }

    public function backup_restore() {
        if (_is_customer_login($this)) {

            $files = array_diff(scandir(BACKUP_PATH), array('.', '..'));

            $files = $files[2];

            $server_name   = HOSTNAME;
            $username      = USERNAME;
            $password      = PASSWORD;
            $database_name = DATABASE;
           
            $cmd = "mysqldump --routines -h {$server_name} -u {$username} -p{$password} {$database_name} < " . BACKUP_PATH . "{$files}";
            
            exec($cmd);
            $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong>Backup file restore successfully</div>');
                        redirect("backup/backup_list");
                        exit;
           
        } else {
            redirect("admin");
            exit;
        }
    }
}
