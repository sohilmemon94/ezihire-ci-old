<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');

class Ezihire extends CI_Controller {

    public function __construct() {
        header("Cache-Control: no cache");
        session_cache_limiter("private_no_expire");

        parent::__construct();
        // Your own constructor cod
        $this->load->database();
        $this->load->helper('login_helper');
        $this->load->library("pagination");
        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));
        $this->load->library('curl');
        
        if ($_SERVER['HTTP_HOST'] == "localhost") {
            $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/";
        } else {
            $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/";
        }

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }   
       //  $this->visitors_details();
    }

    public function check_email_exist() {
        $email = $this->input->post("email");
        if(!empty($email)) {
            $this->load->model("Coaches_model");
            $check_email_exist = $this->Coaches_model->check_coaches_email_exist_or_not($email);
            if($check_email_exist>0){
                 echo "false";
                 exit;
            } else {
                 echo "true";
                 exit;
            }
        }
    }

    public function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function index() {
        $data = array();
        $data["error"] = "";
        $data["active"] = "Welcome";
        $data["title"] = PROJECT_NAME;
        $this->load->model("Admin_model");
        $data['adminmaster'] = $this->Admin_model->get_detail_by_id(1);
        $this->load->view("website/index", $data);
    }  

    public function signup() {
        $data = array();
        $data["error"] = "";
        $data["active"] = "Welcome";
        $data["title"] = PROJECT_NAME;
        $this->load->model("Admin_model");
        $data['adminmaster'] = $this->Admin_model->get_detail_by_id(1);

            if (isset($_POST)) {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('username', 'Company Name', 'trim|required');
                $this->form_validation->set_rules('mobileno', 'Mobile No', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                } else {

                    if ($_SERVER['HTTP_HOST'] != "localhost") {

                        $captcha_response = trim($this->input->post('g-recaptcha-response'));
                        if($captcha_response != ''){
                            $keySecret = $this->config->item('google_secret');
                            $check = array(
                                'secret'        =>  $keySecret,
                                'response'      =>  $this->input->post('g-recaptcha-response')
                            );

                            $startProcess = curl_init();
                            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
                            curl_setopt($startProcess, CURLOPT_POST, true);
                            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
                            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
                            $receiveData = curl_exec($startProcess);
                            $finalResponse = json_decode($receiveData, true);

                            if($finalResponse['success']){


                                $username = $this->input->post("username") ? $this->input->post("username") : '';
                                $ownername = $this->input->post("ownername") ? $this->input->post("ownername") : '';
                                $industry_id = $this->input->post("industry_id") ? $this->input->post("industry_id") : '';
                                $address = $this->input->post("address") ? $this->input->post("address") : '';
                                $mobileno = $this->input->post("mobileno") ? $this->input->post("mobileno") : '';
                                $alternateno = $this->input->post("alternateno") ? $this->input->post("alternateno") : '';
                                $linkedin = $this->input->post("linkedin") ? $this->input->post("linkedin") : '';
                                $skypeid = $this->input->post("skypeid") ? $this->input->post("skypeid") : '';
                                $email = $this->input->post("email") ? $this->input->post("email") : '';
                                $password = $this->input->post("password") ? $this->input->post("password") : '';
                                $role_id = '4';
                                $is_approve = ($this->input->post("is_approve") == "on") ? '1' : '0';
                                $status = ($this->input->post("status") == "on") ? '1' : '0';
                                $logo = NULL;

                                if($role_id == "4") {
                                    $role_type = "V";
                                    $roletype = "Vendor";
                                }

                                $code = "";
                                $code .= "EHVL_";
                                $code .= $this->get_random_number(5);

                                $data = array();
                                $this->load->model("common_model");
                                $created_at = date('Y-m-d H:i:s');
                                $data_insert = array(
                                    "code"=>$code,
                                    "image"=>$logo,
                                    "username"=>$username,
                                    "code"=>$code,
                                    "ownername"=>$ownername,
                                    "address"=>$address,
                                    "email"=>$email,
                                    "password"=>md5($password),
                                    "org_password"=>$password,
                                    "mobileno"=>$mobileno,
                                    "alternateno"=>$alternateno,
                                    "industry_id"=>$industry_id,
                                    "linkedin"=>$linkedin,
                                    "skypeid"=>$skypeid,
                                    "type_id"=>1,
                                    "role_id"=>$role_id,
                                    "role_type"=>$role_type,
                                    "roletype"=>$roletype,
                                    "is_approve"=>$is_approve,
                                    "status"=>$status,
                                    "is_front"=>'1',
                                    "created_at"=>$created_at,
                                    "updated_at"=>$created_at,
                                );
                                $last_id = $this->common_model->data_insert("tbl_customers", $data_insert, TRUE);
                                $this->load->model("Vendors_model");
                                $records = $this->Vendors_model->set_default_employee_permission();
                                if(count($records)>0){
                                    foreach ($records as $key => $value) {
                                        $data_insert = array();
                                        $data_insert['permission_id'] = $value->id;
                                        $data_insert['customer_id'] = $last_id;
                                        $data_insert['permission_name'] = $value->name;
                                        $data_insert['status'] = '1';
                                        
                                        $save = $this->common_model->data_insert("tbl_customers_permissions", $data_insert, TRUE);
                                    }
                                }

                                $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Vendor created successfully.wait for the approval.</div>');
                                redirect("signup");
                                exit;

                            } else {
                                $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error ! </strong> Google Recaptcha is not verified...</div>');
                                redirect("signup");
                                exit;
                            }
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error ! </strong> Google Recaptcha is not verified...</div>');
                            redirect("signup");
                            exit;
                        } 
                    } else {
                            $username = $this->input->post("username") ? $this->input->post("username") : '';
                            $ownername = $this->input->post("ownername") ? $this->input->post("ownername") : '';
                            $industry_id = $this->input->post("industry_id") ? $this->input->post("industry_id") : '';
                            $address = $this->input->post("address") ? $this->input->post("address") : '';
                            $mobileno = $this->input->post("mobileno") ? $this->input->post("mobileno") : '';
                            $alternateno = $this->input->post("alternateno") ? $this->input->post("alternateno") : '';
                            $linkedin = $this->input->post("linkedin") ? $this->input->post("linkedin") : '';
                            $skypeid = $this->input->post("skypeid") ? $this->input->post("skypeid") : '';
                            $email = $this->input->post("email") ? $this->input->post("email") : '';
                            $password = $this->input->post("password") ? $this->input->post("password") : '';
                            $role_id = '4';
                            $is_approve = ($this->input->post("is_approve") == "on") ? '1' : '0';
                            $status = ($this->input->post("status") == "on") ? '1' : '0';
                            $logo = NULL;

                            if($role_id == "4") {
                                $role_type = "V";
                                $roletype = "Vendor";
                            }

                            $code = "";
                            $code .= "EHVL_";
                            $code .= $this->get_random_number(5);

                            $data = array();
                            $this->load->model("common_model");
                            $created_at = date('Y-m-d H:i:s');
                            $data_insert = array(
                                "code"=>$code,
                                "image"=>$logo,
                                "username"=>$username,
                                "code"=>$code,
                                "ownername"=>$ownername,
                                "address"=>$address,
                                "email"=>$email,
                                "password"=>md5($password),
                                "org_password"=>$password,
                                "mobileno"=>$mobileno,
                                "alternateno"=>$alternateno,
                                "industry_id"=>$industry_id,
                                "linkedin"=>$linkedin,
                                "skypeid"=>$skypeid,
                                "type_id"=>1,
                                "role_id"=>$role_id,
                                "role_type"=>$role_type,
                                "roletype"=>$roletype,
                                "is_approve"=>$is_approve,
                                "status"=>$status,
                                "is_front"=>'1',
                                "created_at"=>$created_at,
                                "updated_at"=>$created_at,
                            );
                            $last_id = $this->common_model->data_insert("tbl_customers", $data_insert, TRUE);
                            $this->load->model("Vendors_model");
                            $records = $this->Vendors_model->set_default_employee_permission();
                            if(count($records)>0){
                                foreach ($records as $key => $value) {
                                    $data_insert = array();
                                    $data_insert['permission_id'] = $value->id;
                                    $data_insert['customer_id'] = $last_id;
                                    $data_insert['permission_name'] = $value->name;
                                    $data_insert['status'] = '1';
                                    
                                    $save = $this->common_model->data_insert("tbl_customers_permissions", $data_insert, TRUE);
                                }
                            }

                            $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Vendor created successfully.wait for the approval.</div>');
                            redirect("signup");
                            exit;
                    }
                }              
            }

        $this->load->view("website/signup", $data);
    } 

    public function inquiry(){
        $data = array();
        $data["error"] = "";
        $data["active"] = "Welcome";
        $data["title"] = PROJECT_NAME;
        $this->load->model("Admin_model");
        $data['adminmaster'] = $this->Admin_model->get_detail_by_id(1);

            if (isset($_POST)) {
                $this->load->library('form_validation');
                    $this->form_validation->set_rules('username', 'Company Name', 'trim|required');
                    $this->form_validation->set_rules('mobileno', 'Mobile No', 'trim|required');
                    $this->form_validation->set_rules('email', 'Email', 'trim|required');
                    $this->form_validation->set_rules('skill_id', 'Skill Name', 'trim|required');
                    $this->form_validation->set_rules('duration', 'duration', 'trim|required');
                    $this->form_validation->set_rules('description', 'description', 'trim|required');
                    $this->form_validation->set_rules('experience', 'experience', 'trim|required');
                    $this->form_validation->set_rules('budget', 'budget', 'trim|required');
                    $this->form_validation->set_rules('no_of_position', 'no_of_position', 'trim|required');
                    $this->form_validation->set_rules('candidate_availability', 'candidate_availability', 'trim|required');
                        
                if ($this->form_validation->run() == FALSE) {
                    $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                } else {

                    if ($_SERVER['HTTP_HOST'] != "localhost") {
                        $captcha_response = trim($this->input->post('g-recaptcha-response'));
                        if($captcha_response != ''){
                            $keySecret = $this->config->item('google_secret');
                            $check = array(
                                'secret'        =>  $keySecret,
                                'response'      =>  $this->input->post('g-recaptcha-response')
                            );

                            $startProcess = curl_init();
                            curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
                            curl_setopt($startProcess, CURLOPT_POST, true);
                            curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
                            curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
                            $receiveData = curl_exec($startProcess);
                            $finalResponse = json_decode($receiveData, true);

                            if($finalResponse['success']){

                                $username = $this->input->post("username") ? $this->input->post("username") : '';
                                $address = $this->input->post("address") ? $this->input->post("address") : '';
                                $mobileno = $this->input->post("mobileno") ? $this->input->post("mobileno") : '';
                                $email = $this->input->post("email") ? $this->input->post("email") : '';
                               
                                $date = date('Y-m-d');

                                $skill_id = $this->input->post("skill_id") ? $this->input->post("skill_id") : '';
                                $duration = $this->input->post("duration") ? $this->input->post("duration") : '';
                                $description = $this->input->post("description") ? $this->input->post("description") : '';
                                $experience = $this->input->post("experience") ? $this->input->post("experience") : '';
                                $budget = $this->input->post("budget") ? $this->input->post("budget") : '';
                                $no_of_position = $this->input->post("no_of_position") ? $this->input->post("no_of_position") : '';
                                $candidate_availability = $this->input->post("candidate_availability") ? $this->input->post("candidate_availability") : '';

                                $data = array();
                                $this->load->model("common_model");
                                $created_at = date('Y-m-d H:i:s');
                                $data_insert = array(
                                    "username"=>$username,
                                    "email"=>$email,
                                    "mobileno"=>$mobileno,
                                    "skill_id"=>$skill_id,
                                    "description"=>$description,
                                    "experience"=>$experience,
                                    "duration"=>$duration,
                                    "budget"=>$budget,
                                    "no_of_position"=>$no_of_position,
                                    "candidate_availability"=>$candidate_availability,
                                    "post_date"=>$date,
                                    "created_at"=>$created_at,
                                    "updated_at"=>$created_at,
                                );

                                $last_id = $this->common_model->data_insert("tbl_post_inquiry", $data_insert, TRUE);
                       
                                $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Thanks for submit your requirement. we will call you shortly.</div>');
                                redirect("inquiry");
                                exit;

                            } else {
                                $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error ! </strong> Google Recaptcha is not verified...</div>');
                                redirect("inquiry");
                                exit;
                            }
                        } else {
                            $this->session->set_flashdata("message", '<div class="alert alert-danger alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error ! </strong> Google Recaptcha is not verified...</div>');
                            redirect("inquiry");
                            exit;
                        }
                    } else {
                        $username = $this->input->post("username") ? $this->input->post("username") : '';
                        $address = $this->input->post("address") ? $this->input->post("address") : '';
                        $mobileno = $this->input->post("mobileno") ? $this->input->post("mobileno") : '';
                        $email = $this->input->post("email") ? $this->input->post("email") : '';

                        $date = date('Y-m-d');

                        $skill_id = $this->input->post("skill_id") ? $this->input->post("skill_id") : '';
                        $duration = $this->input->post("duration") ? $this->input->post("duration") : '';
                        $description = $this->input->post("description") ? $this->input->post("description") : '';
                        $experience = $this->input->post("experience") ? $this->input->post("experience") : '';
                        $budget = $this->input->post("budget") ? $this->input->post("budget") : '';
                        $no_of_position = $this->input->post("no_of_position") ? $this->input->post("no_of_position") : '';
                        $candidate_availability = $this->input->post("candidate_availability") ? $this->input->post("candidate_availability") : '';

                        $data = array();
                        $this->load->model("common_model");
                        $created_at = date('Y-m-d H:i:s');
                        $data_insert = array(
                            "username"=>$username,
                            "email"=>$email,
                            "mobileno"=>$mobileno,
                            "skill_id"=>$skill_id,
                            "description"=>$description,
                            "experience"=>$experience,
                            "duration"=>$duration,
                            "budget"=>$budget,
                            "no_of_position"=>$no_of_position,
                            "candidate_availability"=>$candidate_availability,
                            "post_date"=>$date,
                            "created_at"=>$created_at,
                            "updated_at"=>$created_at,
                        );

                        $last_id = $this->common_model->data_insert("tbl_post_inquiry", $data_insert, TRUE);

                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Thanks for submit your requirement. we will call you shortly.</div>');
                        redirect("inquiry");
                        exit;
                    }
                }              
            }

        $this->load->model("Technology_model");
        $technologies = $this->Technology_model->get_active_technology_list();
        $data['technologies'] = $technologies;

        $this->load->view("website/inquiry", $data);
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }

    public function getlatlng($address,$city,$state,$country){
        $GOOGLE_MAP_API_KEY = "AIzaSyC711vkhHG424lDbLWy3ZH0CIgPVDHb8Dc";
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address.$city.$state.$country).'&sensor=false&key='.$GOOGLE_MAP_API_KEY);
        $geo = json_decode($geo, true);
        if ($geo['status'] == 'OK') {
            $data['latitude'] = $geo['results'][0]['geometry']['location']['lat'];
            $data['longitude'] = $geo['results'][0]['geometry']['location']['lng'];
        }
        else
        {
            $data['latitude']="";
            $data['longitude']="";
        }
        return $data;   
    }

    public function get_random_string($length = 10) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }

    public function not_found() {
        $data = array();
        $data["error"] = "";
        $data['page'] = "404";
        $data["title"] = PROJECT_NAME;
        $data['action'] = "Page not found";
        $this->load->model("Admin_model");
        $data['adminmaster'] = $this->Admin_model->get_detail_by_id(1);

        if (_is_customer_login($this)) {

            $customer_role_type =  $this->session->userdata('customer_role_type');
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
          
            if($role >= 1 && $admin_id >= 1) {
                $this->load->view("admin/not_found", $data);
            } else {
                $this->load->view("website/not_found", $data);
            }
        } else {
            $this->load->view("website/not_found", $data);
        }
    }
    
    public function ajax_check_email_exist(){
        if (isset($_POST["email"]) && !empty($_POST["email"])) {
            $this->load->model("Customers_model");
            $email = strtolower($_POST['email']);
            $stateArray = $this->Customers_model->check_email_exist($email);
            if(count($stateArray)>0){
                echo "false";
            } else {
                echo "true";
            }
        }   
    }

    public function ajax_check_mobile_exist(){
        if (isset($_POST["mobile"]) && !empty($_POST["mobile"])) {
            $this->load->model("Users_model");
            $mobile = strtolower($_POST['mobile']);
            $stateArray = $this->Users_model->check_mobile_exist($mobile);
            if(count($stateArray)>0){
                echo "false";
            } else {
                echo "true";
            }
        }   
    }

    public function ajax_check_company_name_exist(){

        if (isset($_POST["username"]) && !empty($_POST["username"])) {
            $this->load->model("Vendors_model");
            $username = strtolower($_POST['username']);
            $customer_id =  $this->input->post("id") ? $this->input->post("id") : '';
           
            $Array = $this->Vendors_model->check_company_name_exist($username,$customer_id);
            if(count($Array)>0){
                echo "false";
            } else {
                echo "true";
            }
        }   
    }

}
