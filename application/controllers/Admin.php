<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');

class Admin extends CI_Controller {

    public function __construct() {

        header("Cache-Control: no cache");
        session_cache_limiter("private_no_expire");

        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->helper('login_helper');

        $this->load->library('javascript');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->library('session');
        $this->load->dbutil();
        $this->load->helper('file');
        $this->load->helper('download');
        $this->load->helper(array('form', 'url'));
        $this->load->model("Vendors_model");
        $this->load->model("Technology_model");
        $this->load->model("Employees_model");
        $this->load->model("Interviewschedule_model");

        
        if ($_SERVER['HTTP_HOST'] == "localhost") {
            $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
        } else {
            $this->dire_path = $_SERVER['DOCUMENT_ROOT'] . "/admin/";
        }

        $customer_status = get_user_status();
        if(isset($customer_status) && !empty($customer_status) && $customer_status == "1" || $customer_status == 1) {
        } else {
            auto_signout();
        }
    }

    function index() {
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            
            if($role >= 1 && $admin_id >= 1) {
                redirect('admin/dashboard');
            }
        } else {
            redirect('admin/login');
            exit;
        }

    }

    function dashboard() {
        if (_is_customer_login($this)) {
        
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
           
            if($role >= 1 && $admin_id >= 1) {
                // $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Dashboard";
                $data['admin'] = "Admin";
                $data['page'] = "Dashboard";
                $data['action'] = "Dashboard";
                $data['employees'] = $this->Employees_model->get_total_employees($admin_id,$customer_role_type);
                $data['role'] = $role;
                $data['vendors'] = $this->Vendors_model->get_vendors_by_role();

                $this->load->model("postjob_model");
                $active ="active";
                $postjob = $this->postjob_model->get_total_postjob($admin_id,$customer_role_type,$active);
                $no_of_position = 0;
                if(count($postjob)>0){
                    foreach($postjob as $p=>$pv){
                        $no_of_position = $no_of_position + $pv->no_of_position;
                    }
                }

                $data['postjob'] = $postjob;
                $data['no_of_position'] = $no_of_position;
               
                // get total interview details user wise
                $vendors = $this->Vendors_model->get_all_users_records();
                if(count($vendors)>0){
                    foreach ($vendors as $key => $value) {
                        $created_by = $value->customer_id;
                        // get record count from created by
                        $records = $this->Interviewschedule_model->get_records_from_created_by($created_by);
                        $vendors[$key]->interviewschedule = count($records);
                    }
                }

                $data['interview_by_users'] = $vendors;
                $statusArray = ['0'=>'Proposed','1'=>'Round 1','2'=>'Round 2','3'=>'Round 1 Awaiting','4'=>'Round 2 Awaiting','5'=>'Round 1 Reject','6'=>'Round 2 Reject','7'=>'Selected','8'=>'Hold','9'=>'Close'];
                    $tmp_array = array();
                   if(count($statusArray)>0){
                        foreach ($statusArray as $key => $values) {
                            // get record count from created by
                            $recordss = $this->Interviewschedule_model->get_records_from_status($values);
                            $tmp_array[$key]['status'] = $values;
                            $tmp_array[$key]['interviewschedule'] = count($recordss);
                        }
                   }
                $data['interview_by_status'] = $tmp_array;

                // get total skills
                $technologys = $this->Technology_model->get_active_technology_list();
                $total_skills = 0;
                if(count($technologys)>0){
                    foreach ($technologys as $key => $value) {
                        // get total count from technologies
                        $total_records=$this->Vendors_model->get_total_by_technologies($value->id);
                        $my_total_skill = 0;
                        if(count($total_records)>0){
                            foreach($total_records as $k=>$v) {
                                $my_total_skill = $my_total_skill +  $v->total_emp;
                            }
                        }
                        $technologys[$key]->total = $my_total_skill;
                        $total_skills = $total_skills + $my_total_skill;
                    }
                }
                $data['technologys'] = $technologys;
             
                $data['skills'] = $total_skills;
                 // dd($data['interview_by_status'],true);
                $this->load->view("admin/dashboard", $data);
            } else {
                redirect('admin/login');
            }
        } else {
            redirect('admin/login');
        }
    }

    public function change_status() {
        $table = $this->input->post("table");
        $id = $this->input->post("id");
        $on_off = $this->input->post("on_off");
        $id_field = $this->input->post("id_field");
        $status = $this->input->post("status");
        $this->db->update($table, array("$status" => $on_off), array("$id_field" => $id));
        echo $_POST['on_off'];
    }

    public function change_block_status() {
        $table = $this->input->post("table");
        $id = $this->input->post("id");
        $on_off = $this->input->post("on_off");
        $id_field = $this->input->post("id_field");
        $status = $this->input->post("status");
        $this->db->update($table, array("$status" => $on_off), array("$id_field" => $id));
        // echo $this->db->last_query();
        echo $_POST['on_off'];
    }

    public function emailTemplate() {
        echo "<pre>";
        print_r($_REQUEST);
        echo "</pre>";
        $this->load->view('admin/user/orderdetails', $data);
    }

   public function settings() {
        if (_is_customer_login($this)) {
            $data = array();
            $data["error"] = "";
            $data["pageTitle"] = "Admin Settings";
            $data["title"] = PROJECT_NAME;
            $data['admin'] = "Admin";
            $data['page'] = "Settings";
            
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            $customer_role_type =  $this->session->userdata('customer_role_type');
           
            if($role >= 1 && $admin_id >= 1) { 

                $this->load->model("Customers_model");
                $admindata = $this->Customers_model->get_detail_by_id($admin_id);
                $data['admindata'] = $admindata;
                $data['role'] = $role;
                $this->load->model("Admin_model");
                $data['adminmaster'] = $this->Admin_model->get_detail_by_id(1);

                    if (isset($_REQUEST['save_button']) && !empty($_REQUEST['save_button']) && $_REQUEST['save_button'] === "Update") {


                        if($role != 4) {

                            $this->load->library('form_validation');
                            $this->form_validation->set_rules('username', 'User Name', 'trim|required');
                            
                            if ($this->form_validation->run() == FALSE) {

                                $data["error"] = '<div class="alert alert-warning alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning !</strong> ' . $this->form_validation->error_string() . '</div>';

                                $this->session->set_flashdata("message", '<div class="alert alert-warning alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning !</strong> ' . $this->form_validation->error_string() . '</div>');

                                // $data["error"] = $this->form_validation->error_string();
                            } else {


                                $username = $this->input->post("username");
                                // $email = $this->input->post("email");
                               // $old_password = $this->input->post("oldpassword");
                                $new_password = $this->input->post("newpassword");
                                $con_password = $this->input->post("confirmpassword");
                                $update_array = array();
                                $update_array2 = array();


                                if ($_FILES["image"]["size"] > 0) {

                                    $temp = explode(".", $_FILES["image"]["name"]);
                                    $newfilename = time() . '.' . end($temp);
                                    $uploadpath = $this->config->item('customers_images_path');
                                    $returnpath = $this->config->item('customers_images_uploaded_path');
                                    $file_name = $this->file_upload($_FILES["image"], $uploadpath, $returnpath);
                                    $logo = $file_name;
                                    $update_array = array("image" => $logo);
                                } 

                                if ($_FILES["background_image"]["size"] > 0) {

                                    $temp = explode(".", $_FILES["background_image"]["name"]);
                                    $newfilename = time() . '.' . end($temp);
                                    $uploadpath = $this->config->item('customers_images_path');
                                    $returnpath = $this->config->item('customers_images_uploaded_path');
                                    $file_name = $this->file_upload($_FILES["background_image"], $uploadpath, $returnpath);
                                    $background_image = $file_name;
                                    $update_array2 = array("background_image" => $background_image);
                                } 
                                         
                                if (!empty($new_password)) {

                                    if (empty($con_password)) {
                                        $data["error"] = '<div class="alert alert-warning" role="alert"><button class="close" data-dismiss="alert"></button><strong>Warning ! </strong> Please insert confirm password !</div>';

                                        $this->session->set_flashdata("message",'<div class="alert alert-warning" role="alert"><button class="close" data-dismiss="alert"></button><strong>Warning ! </strong> Please insert confirm password !</div>'); 
                                        redirect("admin/settings");
                                        exit;
                                    } else {

                                        if (empty($new_password)) {

                                            $data["error"] = '<div class="alert alert-warning" role="alert"><button class="close" data-dismiss="alert"></button><strong>Warning ! </strong> Please Insert New Password !</div>';

                                            $this->session->set_flashdata("message",'<div class="alert alert-warning" role="alert"><button class="close" data-dismiss="alert"></button><strong>Warning ! </strong> Please Insert New Password !</div>');
                                            redirect("admin/settings");
                                            exit;
                                        } else {

                                            if ($new_password != $con_password) {
                                            $data["error"] = '<div class="alert alert-warning" role="alert"><button class="close" data-dismiss="alert"></button><strong>Warning ! </strong>  New password and confirm Password is not Match</div>';

                                            $this->session->set_flashdata("message",'<div class="alert alert-warning" role="alert"><button class="close" ata-dismiss="alert"></button><strong>Warning ! </strong>  New password and confirm Password is not Match</div>'); 
                                            redirect("admin/settings");
                                            exit;

                                            } else {
                                                        
                                                if (count($update_array) > 0) {
                                                    $update_array['password'] = md5($new_password);
                                                    $update_array['org_password'] = $new_password;
                                                } else {
                                                    $update_array = array("password" => md5($new_password),"org_password"=>$new_password);
                                                }
                                            }
                                        }
                                    }
                                }
                                   
                                if (count($data) >= 1) {
                                    $update_array1 = array("username" => $username);
                                    $update_array = array_merge($update_array1, $update_array);
                                   
                                    $this->load->model("common_model");
                                    $update =  $this->common_model->data_update_new("tbl_customers", $update_array, array("customer_id" => $admin_id));

                                    if(isset($update_array2) && !empty($update_array2)) {
                                        $adm_id = "1";
                                        $updateBackgroud = $this->common_model->data_update_new("tbl_admin", $update_array2, array("admin_id" => $adm_id));
                                    }

                                    $where = array("customer_id" => $admin_id);

                                    $this->db->select('*');
                                    $this->db->from('tbl_customers');
                                    $this->db->where($where);

                                    $q = $this->db->get()->result_array();
                                    
                                    if (!empty($q)) {
                                        $info = $q[0];

                                        $newdata = array(
                                            'customer_user_name' => $info['username'],
                                            'customer_name' => $info['username'],
                                            'customer_email' => $info['email'],
                                            'customer_logged_in' => TRUE,
                                            'customer_id' => $info['customer_id'],
                                            'customer_image' => $info['image'],
                                            'customer_password' => $info['password'],
                                            'customer_type_id' => $info['type_id'],
                                            'customer_role_id' => $info['role_id'],
                                            'customer_role_type' => $info['role_type'],
                                        );
                                        
                                        $this->session->set_userdata($newdata);

                                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Profile updated successfully</div>');
                                        redirect("admin/settings");
                                        exit;

                                    }
                                }
                            }
                        } else if($role == 4) {
                            $this->form_validation->set_rules('ownername', 'Name', 'trim|required');
                            $this->form_validation->set_rules('industry_id', 'Industry Type', 'trim|required');
                            $this->form_validation->set_rules('address', 'Address', 'trim|required');
                            $this->form_validation->set_rules('mobileno', 'Mobile No', 'trim|required');
                            $this->form_validation->set_rules('alternateno', 'Alternate No', 'trim|required');
                            $this->form_validation->set_rules('linkedin', 'linkedin', 'trim|required');
                            $this->form_validation->set_rules('skypeid', 'skypeid', 'trim|required');
                            // $this->form_validation->set_rules('email', 'Email', 'trim|required');
                            $this->form_validation->set_rules('password', 'Password', 'trim|required');

                            if ($this->form_validation->run() == FALSE) {
                                $this->session->set_flashdata("messages", '.$this->form_validation->error_string().');
                               
                            } else {

                                $ownername = $this->input->post("ownername") ? $this->input->post("ownername") : '';
                                $industry_id = $this->input->post("industry_id") ? $this->input->post("industry_id") : '';
                                $address = $this->input->post("address") ? $this->input->post("address") : '';
                                $mobileno = $this->input->post("mobileno") ? $this->input->post("mobileno") : '';
                                $alternateno = $this->input->post("alternateno") ? $this->input->post("alternateno") : '';
                                $linkedin = $this->input->post("linkedin") ? $this->input->post("linkedin") : '';
                                $skypeid = $this->input->post("skypeid") ? $this->input->post("skypeid") : '';
                                
                                $email = $data['admindata']->email;

                                $password = $this->input->post("password") ? $this->input->post("password") : '';
                                $role_id = '4';
                               
                                if ($_FILES["vendor_image"]["size"] > 0) {

                                    $temp = explode(".", $_FILES["vendor_image"]["name"]);
                                    $newfilename = time() . '.' . end($temp);
                                    $uploadpath = $this->config->item('customers_images_path');
                                    $returnpath = $this->config->item('customers_images_uploaded_path');
                                    $file_name = $this->file_upload($_FILES["vendor_image"], $uploadpath, $returnpath);
                                    $logo = $file_name;
                                } else {
                                    $logo = $admindata->image;
                                }

                                if ($_FILES["vendor_agreement"]["size"] > 0) {

                                    $temp = explode(".", $_FILES["vendor_agreement"]["name"]);
                                    $newfilename = time() . '.' . end($temp);
                                    $uploadpath = $this->config->item('customers_vendor_agreement_path');
                                    $returnpath = $this->config->item('customers_vendor_agreement_uploaded_path');
                                    $file_name = $this->file_upload($_FILES["vendor_agreement"], $uploadpath, $returnpath);
                                    $agreement = $file_name;
                                } else {
                                    $agreement = $admindata->agreement;
                                }

                                if($role_id == "4") {
                                    $role_type = "V";
                                    $roletype = "Vendor";
                                } elseif($role_id == "3") {
                                    $role_type = "U";
                                    $roletype = "User";
                                } else if($role_id == "2") {
                                    $role_type = "S";
                                    $roletype = "Sub Admin";
                                } else {
                                    $role_type = "A";
                                    $roletype = "Admin";
                                }

                                $data = array();
                                $this->load->model("common_model");
                                $created_at = date('Y-m-d H:i:s');
                                $data_insert = array(
                                    "image"=>$logo,
                                    "agreement"=>$agreement,
                                    "ownername"=>$ownername,
                                    "address"=>$address,
                                    "email"=>$email,
                                    "password"=>md5($password),
                                    "org_password"=>$password,
                                    "mobileno"=>$mobileno,
                                    "alternateno"=>$alternateno,
                                    "industry_id"=>$industry_id,
                                    "linkedin"=>$linkedin,
                                    "skypeid"=>$skypeid,
                                    "type_id"=>1,
                                    "role_id"=>$role_id,
                                    "role_type"=>$role_type,
                                    "roletype"=>$roletype,
                                    "updated_at"=>$created_at,
                                );
                               
                                $this->common_model->data_update("tbl_customers", $data_insert, array("customer_id" => $admin_id));

                                $where = array("customer_id" => $admin_id);

                                    $this->db->select('*');
                                    $this->db->from('tbl_customers');
                                    $this->db->where($where);

                                    $q = $this->db->get()->result_array();
                                    
                                    if (!empty($q)) {
                                        $info = $q[0];

                                        $newdata = array(
                                            'customer_user_name' => $info['username'],
                                            'customer_name' => $info['username'],
                                            'customer_email' => $info['email'],
                                            'customer_logged_in' => TRUE,
                                            'customer_id' => $info['customer_id'],
                                            'customer_image' => $info['image'],
                                            'customer_password' => $info['password'],
                                            'customer_type_id' => $info['type_id'],
                                            'customer_role_id' => $info['role_id'],
                                            'customer_role_type' => $info['role_type'],
                                        );
                                        
                                        $this->session->set_userdata($newdata);

                                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Profile updated successfully</div>');
                                        redirect("admin/settings");
                                        exit;

                                    }


                                $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> Vendor updated successfully</div>');
                                    redirect("admin/settings");
                                exit;
                            }
                        }
 
                    }

                    $this->load->model("industry_model");
                    $industrys = $this->industry_model->get_all_industry_list();
                    $data['industrys'] = $industrys;
                    $data['admin_id'] = $admin_id;

                    if($role != 4) {
                        $this->load->view('admin/profile/settings', $data);
                    } else if($role == 4) {
                        $this->load->view('admin/profile/settingsnew', $data);
                    }
            } else {
                redirect('login');exit;
            }
        } else {
            redirect('login');exit;
        }
    } 


    private function file_upload($arr, $path, $returnpath) {
        if ($arr['error'] == 0) {

            $temp = explode(".", $arr["name"]);
            $get_random_number = $this->get_random_number(5);
            $file_name = $get_random_number . time() . '.' . end($temp);

            $file_path = $path . $file_name;

            if (move_uploaded_file($arr["tmp_name"], $file_path) > 0) {
                $ret = $file_name;
            }
            else {
                $ret = "";
            }
        }

        return $ret;
    }

    private function get_random_number($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "012345678901234567890123456789";
        }
        else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0;$i < $length;$i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }


    function social_media() {
        if (_is_customer_login($this)) {
            $data = array();
            $data["error"] = "";
            $data["pageTitle"] = "Admin Settings";
            $data["title"] = "Nutronic UAE";
            $data['admin'] = "Admin";
            $data['page'] = "Social Media";

            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            if($admin_id >= 1) {
                $user_id = $this->session->userdata('user_id');

                $this->load->model("admin_model");
                
                if (!empty($_POST['btn_submit']) && isset($_POST['btn_submit'])) {

                        $ct = date('Y-m-d H:i:s');
                        $facebook = $_POST['facebook'];
                        $twitter = $_POST['twitter'];
                        $linkdin = $_POST['linkdin'];
                        $google = $_POST['google'];
                        
                        $update_array = array(
                            "facebook" => $facebook, 
                            "twitter" => $twitter, 
                            "linkdin" => $linkdin, 
                            "google" => $google, 
                        );

                        $id = '1';
                        $this->load->model("common_model");
                        $this->common_model->data_update("tbl_site_settings", $update_array, array("id" => $id)
                        );
                    
                        $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" role="alert" id="error">
                                      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                      <strong>Success ! </strong>Social media updated successfully
                                    </div>');
                    redirect("admin/social_media");
                    exit;
                } 
                $site_setting = $this->admin_model->get_social_media_detail();
                $data['site_setting'] = $site_setting;
                $this->load->view('admin/profile/social_media', $data);
            } else {
                redirect('admin/login');
            }
        } else {
            redirect('admin/login');
        }
    }

    public function get_random_otp($length = 10, $sting = "") {
        if (empty($sting)) {
            $alphabet = "0123456789";
        } else {
            $alphabet = $sting;
        }
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }
  
    function numberTowords($number) { 
       // $number = 24500.026;
       $no = round($number);
       $point = round($number - $no, 2) * 100;
       $hundred = null;
       $digits_1 = strlen($no);
       $i = 0;
       $str = array();
       $words = array('0' => '', '1' => 'One', '2' => 'Two',
        '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
        '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
        '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
        '13' => 'Thirteen', '14' => 'Fourteen',
        '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
        '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
        '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
        '60' => 'Sixty', '70' => 'Seventy',
        '80' => 'Eighty', '90' => 'Ninety');
       $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
       while ($i < $digits_1) {
         $divider = ($i == 2) ? 10 : 100;
         $number = floor($no % $divider);
         $no = floor($no / $divider);
         $i += ($divider == 10) ? 1 : 2;
         if ($number) {
            $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
            $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
            $str [] = ($number < 21) ? $words[$number] .
                " " . $digits[$counter] . $plural . " " . $hundred
                :
                $words[floor($number / 10) * 10]
                . " " . $words[$number % 10] . " "
                . $digits[$counter] . $plural . " " . $hundred;
         } else $str[] = null;
      }
      $str = array_reverse($str);
      $result = implode('', $str);
      $points = ($point) ?
        "." . $words[$point / 10] . " " . 
              $words[$point = $point % 10] : '';


      return $result . "Rupees  " . $points . " Paise";
    } 


    public function forgot_password() {
        if (isset($_POST['email'])) {

            $this->load->library('form_validation');

            $this->form_validation->set_rules('email', 'Email', 'trim|required');

            if ($this->form_validation->run() == FALSE) {

                if ($this->form_validation->error_string() != "") {
                    $data["error"] = '<div class="alert alert-warning alert-dismissible" role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning!</strong> ' . $this->form_validation->error_string() . '</div>';
                }
            } else {

                if ($_SERVER['HTTP_HOST'] != "localhost") {

                    $captcha_response = trim($this->input->post('g-recaptcha-response'));
                    if($captcha_response != ''){
                        $keySecret = $this->config->item('google_secret');
                        $check = array(
                            'secret'        =>  $keySecret,
                            'response'      =>  $this->input->post('g-recaptcha-response')
                        );

                        $startProcess = curl_init();
                        curl_setopt($startProcess, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
                        curl_setopt($startProcess, CURLOPT_POST, true);
                        curl_setopt($startProcess, CURLOPT_POSTFIELDS, http_build_query($check));
                        curl_setopt($startProcess, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($startProcess, CURLOPT_RETURNTRANSFER, true);
                        $receiveData = curl_exec($startProcess);
                        $finalResponse = json_decode($receiveData, true);

                        if($finalResponse['success']){

                            $email = $this->input->post("email");
                            $where = array("email" => $email);
                            $login = "SELECT * FROM  `tbl_customers` WHERE `email`='$email'";
                            $login_infos = $this->Basic->select_custom($login);
                            if (count($login_infos) > 0) {
                                $info = $login_infos[0];
                                $name = $info['username'];
                                $email = $info['email'];
                                $new_password = $info['org_password'];

                                if ($info['is_approve'] == '1') {
                                    // set check your email 

                                    if ($info['status'] == 1) {

                                        $secret_token = $this->get_random_string(50);

                                        $domainname = $_SERVER['SERVER_NAME'];
                                        $from_email = $this->config->item('SUPPORT_URL');
                                        $from_company = PROJECT_NAME;
                                        $logo = base_url()."img/logo12.png";

                                        $email_message = $this->load->view('email/admin_forgot_password', $data = array('name' => $name, 'from_email'=>$from_email, 'from'=>$from_company,'logo' => $logo, 'domainname' => $domainname,'newpassword'=>$new_password), TRUE);

                                        $to = strtolower($email);
                                        $subject = "Password for ".ucfirst($from_company)." Admin Account";
                                        $message = $email_message;

                                        $config['protocol']    = 'smtp';
                                        $config['smtp_host']    = 'ssl://smtp.gmail.com';
                                        $config['smtp_port']    = '465';
                                        $config['smtp_timeout'] = '7';
                                        $config['smtp_user']    = 'developer.infiraise@gmail.com';
                                        $config['smtp_pass']    = 'Developer@123';
                                        $config['charset']    = 'utf-8';
                                        $config['newline']    = "\r\n";
                                        $config['mailtype'] = 'html'; // or html
                                        $config['validation'] = TRUE; // bool whether to validate email or not      
                                        $this->load->library('email', $config);
                                        $this->load->library('encryption');

                                        $this->email->set_newline("\r\n");
                                        $this->email->initialize($config);

                                        $this->email->from('developer.infiraise@gmail.com', 'EziHire');
                                        $this->email->to($to); 

                                        $this->email->subject($subject);
                                        $this->email->message($message); 

                                        //Send email
                                        $send = $this->email->send();
                                        if($send) {
                                            echo json_encode("send");
                                        } else {
                                            $error = $this->email->print_debugger(array('headers'));
                                            echo json_encode($error);
                                        }

                                        $data["error"] = '<div class="alert alert-success alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> check your email you have received new password.</div>';
                                    } else {
                                        $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Currently your account is deactived,Please contact to Admin for active your account. </div>';
                                    }
                                } else {
                                    $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Currently your account is under verification,Please contact to Admin for active your account. </div>';
                                }
                            } else {
                                $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Invalid email address. </div>';
                            }

                        } else {
                            $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error ! </strong> Google Recaptcha is not verified...</div>';
                        }
                    } else {
                        $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error ! </strong> Google Recaptcha is not verified...</div>';
                    }
                } else {
                    $email = $this->input->post("email");
                    $where = array("email" => $email);
                    $login = "SELECT * FROM  `tbl_customers` WHERE `email`='$email'";
                    $login_infos = $this->Basic->select_custom($login);
                    if (count($login_infos) > 0) {
                        $info = $login_infos[0];
                        $name = $info['username'];
                        $email = $info['email'];
                        $new_password = $info['org_password'];

                        if ($info['is_approve'] == '1') {
                            // set check your email 

                            if ($info['status'] == 1) {

                                $secret_token = $this->get_random_string(50);

                                $domainname = $_SERVER['SERVER_NAME'];
                                $from_email = $this->config->item('SUPPORT_URL');
                                $from_company = PROJECT_NAME;
                                $logo = base_url()."img/logo12.png";

                                $email_message = $this->load->view('email/admin_forgot_password', $data = array('name' => $name, 'from_email'=>$from_email, 'from'=>$from_company,'logo' => $logo, 'domainname' => $domainname,'newpassword'=>$new_password), TRUE);

                                $to = strtolower($email);
                             
                                $subject = "Password for ".ucfirst($from_company)." Admin Account";
                                $message = $email_message;

                                $config['protocol']    = 'smtp';
                                $config['smtp_host']    = 'ssl://smtp.gmail.com';
                                $config['smtp_port']    = '587';
                                $config['smtp_timeout'] = '7';
                                $config['smtp_user']    = 'developer.infiraise@gmail.com';
                                $config['smtp_pass']    = 'Developer@123';
                                $config['charset']    = 'utf-8';
                                $config['newline']    = "\r\n";
                                $config['mailtype'] = 'html'; // or html
                                $config['validation'] = TRUE; // bool whether to validate email or not      
                                $this->load->library('email', $config);
                                $this->load->library('encryption');

                                $this->email->set_newline("\r\n");
                                $this->email->initialize($config);

                                $this->email->from('developer.infiraise@gmail.com', 'EziHire');
                                $this->email->to($to); 

                                $this->email->subject($subject);
                                $this->email->message($message); 

                                //Send email
                                $send = $this->email->send();
                                if($send) {
                                    echo json_encode("send");
                                } else {
                                    $error = $this->email->print_debugger(array('headers'));
                                    echo json_encode($error);
                                }

                                $data["error"] = '<div class="alert alert-success alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success ! </strong> check your email you have received new password.</div>';
                            } else {
                                $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Currently your account is deactived,Please contact to Admin for active your account. </div>';
                            }
                        } else {
                            $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Currently your account is under verification,Please contact to Admin for active your account. </div>';
                        }
                    } else {
                        $data["error"] = '<div class="alert alert-danger alert-dismissible " role="alert" id="error"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Error!</strong> Invalid email address. </div>';
                    }
                }
            }
        }
        $data["active"] = "login";

        $this->load->model("Admin_model");
        $data['adminmaster'] = $this->Admin_model->get_detail_by_id(1);
         $data["title"] = PROJECT_NAME;
        $this->load->view("admin/forgot_password", $data);
    }

    function profile() {
        if (_is_customer_login($this)) {
            $user_id = $this->session->userdata('user_id');
            $data = array();
            $this->load->model("admin_model");
            $admindata = $this->admin_model->get_detail_by_id($user_id);

            $data["admindata"] = $admindata;
            if (isset($_REQUEST)) {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('name', 'User Name', 'trim|required');
                $this->form_validation->set_rules('email', 'Email', 'trim|required');

                if ($this->form_validation->run() == FALSE) {
                    $data["error"] = $this->form_validation->error_string();
                } else {
                    $name = $this->input->post("name");
                    $email = $this->input->post("email");
                    $old_password = $this->input->post("oldpassword");
                    $new_password = $this->input->post("newpassword");
                    $con_password = $this->input->post("confirmpassword");

                    $update_array = array();
                    if (!empty($old_password)) {
                        $exist_password = $this->session->userdata('password');

                        if ($exist_password != md5($old_password)) {
                            $data["error"] = "<strong>Warning!</strong> Old password and Exist Password is not Match with system";
                        } else {

                            if (empty($new_password) && empty($con_password)) {
                                $data["error"] = "<strong>Warning!</strong> Please Insert New Password & Confirm Password !";
                            } else {

                                if (!empty($new_password)) {
                                    if (empty($con_password)) {
                                        $data["error"] = "<strong>Warning!</strong> Please Insert Confirm Password !";
                                    }
                                }

                                if (!empty($con_password)) {
                                    if (empty($new_password)) {
                                        $data["error"] = "<strong>Warning!</strong> Please Insert New Password !";
                                    }
                                }

                                if ($new_password != $con_password) {
                                    $data["error"] = "<strong>Warning!</strong> New password and confirm Password is not Match";
                                } else {
                                    $update_array = array("password" => md5($new_password),"org_password" => $new_password);
                                }
                            }
                        }
                    }

                    if (count($update_array) <= 0) {
                        $update_array = array();
                    }

                    if (count($data) == 1) {
                        $update_array1 = array("name" => $name, "email" => $email);
                        $update = array_merge($update_array1, $update_array);

                        $this->load->model("common_model");
                        $this->common_model->data_update("tbl_admin", $update, array("id" => $user_id));

                        echo $this->db->last_query();
                        exit;
                        
                        $data["error"] = "<strong>Success!</strong> Profile Updated Successfully";
                    }
                }
            }

            $this->load->view('admin/profile/edit', $data);
        } else {
            redirect('admin/login');
        }
    }

    public function get_random_string($length = 10) {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $token = "";
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $token .= $alphabet[$n];
        }
        return $token;
    }

    public function inquiry_list() {
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            if($role >= 1 && $admin_id >= 1) {
                // $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Dashboard";
                $data['admin'] = "Admin";
                $data['page'] = "Inquiry";
                $data["title"] = PROJECT_NAME;
                $data['action'] = "List";

                $this->load->model("admin_model");
                // $data["inquiries"] = $this->admin_model->inquiry_list();
                $this->load->view('admin/inquiry/list', $data);

            } else {
                redirect('admin/login');
            }
        } else {
            redirect('admin/login');
        }
    }

     public function fetch_inquiry_list() {
        if (_is_customer_login($this)) {
           
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            if($role >= 1 && $admin_id >= 1) {
                
                $this->load->model("Inquiry_model");
                $fetch_data = $this->Inquiry_model->get_visitorslist_by_ajax();

                $data = array();  
                foreach($fetch_data as $row){ 
                    $id = $row->i_id;
                    $name = $row->name;
                    $email = $row->email;
                    $message = $row->message;
                    $mobile = $row->mobile;
                    $ip_address = $row->ip_address;
                    $address = $row->address;
                    $country = $row->country;
                    $latitude = $row->latitude.",".$row->longtitude;

                    $created_at = date("d-M-Y H:i:s", strtotime($row->created_at));
                    
                     $action = '';
                      $delete_link = '';
                       
                            $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                            $delete_link .= "confirm_delete('";
                            $delete_link .= site_url("admin/inquiry_delete/".$id);
                            $delete_link .= "'";
                            $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                        

                    $action  =  $delete_link;
                    $sub_array = array();  
                    $sub_array[] = $id;  
                    $sub_array[] = $name;  
                    $sub_array[] = $email;  
                    $sub_array[] = $message;  
                    $sub_array[] = $mobile;  
                    $sub_array[] = $ip_address;  
                    $sub_array[] = $address;  
                    $sub_array[] = $country;  
                    $sub_array[] = $created_at;  
                    $sub_array[] = $action;  

                    $data[] = $sub_array;  
                  
                }
                $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->Inquiry_model->get_all_visitorslist_by_ajax(),  
                "recordsFiltered"     =>     $this->Inquiry_model->get_filtered_data_visitorslist_by_ajax(),  
                "data"                    =>     $data  
                ); 
                echo json_encode($output);  
            }
        } else {
            redirect('admin');
        }
    }


    public function inquiry_delete($id) {
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            if($role >= 1 && $admin_id >= 1) {
                $data = array();
                $this->load->model("Inquiry_model");
                $category = $this->Inquiry_model->get_detail_by_id($id);
                if ($category) {
                    $this->db->query("DELETE FROM `tbl_inquiry_list` WHERE `i_id` = '" . $category->i_id . "'");
                    $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" role="alert" id="error">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <strong>Success ! </strong> Inquiry deleted successfully
                                  </div>');
                    redirect("admin/inquiry_list");
                    exit;
                }
            } else {
                 redirect('admin/login');
            }
        } else {
             redirect('admin/login');
        }
    }



    public function visitors_list() {
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            if($role >= 1 && $admin_id >= 1) {
                // $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Dashboard";
                $data['admin'] = "Admin";
                $data['page'] = "Visitors";
                $data["title"] = PROJECT_NAME;
                $data['action'] = "List";

                $this->load->model("admin_model");
                // $data["visitors"] = $this->admin_model->visitors_list();
                $data["visitors"] = array();
                $this->load->view('admin/visitors/list', $data);

            } else {
                redirect('admin/login');
            }
        } else {
            redirect('admin/login');
        }
    }

    public function visitors_lisst(){
        if (_is_customer_login($this)) {
            $data["error"] = "";
            $data["pageTitle"] = "Admin Visitors";
            $data["title"] = "Admin";
            $data['admin'] = "Admin";
            $data['page'] = "Visitors";
            $data['action'] = "List";

            if (_get_current_user_type_id($this) == 1) {
                $this->load->model("mvservices_model");
                $data["famous"] = $this->mvservices_model->get_mvserviceslist();

                $this->load->view('admin/visitors/list', $data);
            }
        } else {
            redirect('admin');
        }
    }

    public function fetch_visitors_list() {
        if (_is_customer_login($this)) {
           
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            if($role >= 1 && $admin_id >= 1) {
                
                $this->load->model("Visitors_model");
                $fetch_data = $this->Visitors_model->get_visitorslist_by_ajax();

                $data = array();  
                foreach($fetch_data as $row){ 
                    $id = $row->id;
                    $ip = $row->ip;
                    $address = $row->address;
                    $country = $row->country;
                    $latitude = $row->latitude.",".$row->longtitude;

                    $created_at = date("d-M-Y H:i:s", strtotime($row->idate));
                    
                    $sub_array = array();  
                    $sub_array[] = $id;  
                    $sub_array[] = $ip;  
                    $sub_array[] = $address;  
                    $sub_array[] = $country;  
                    $sub_array[] = $latitude;  
                    $sub_array[] = $created_at;  

                    $data[] = $sub_array;  
                  
                }
                $output = array(  
                "draw"                    =>     intval($_POST["draw"]),  
                "recordsTotal"          =>      $this->Visitors_model->get_all_visitorslist_by_ajax(),  
                "recordsFiltered"     =>     $this->Visitors_model->get_filtered_data_visitorslist_by_ajax(),  
                "data"                    =>     $data  
                ); 
                echo json_encode($output);  
            }
        } else {
            redirect('admin');
        }
    }




    public function admission_list() {
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');
            if($role >= 1 && $admin_id >= 1) {
                // $data = array();
                $data["error"] = "";
                $data["pageTitle"] = "Admin Dashboard";
                $data['admin'] = "Admin";
                $data['page'] = "Admission";
                $data["title"] = "Join Aastha";
                $data['action'] = "List";

                $this->load->model("admin_model");
                $data["admission"] = $this->admin_model->admission_list();
                $this->load->view('admin/admission/list', $data);

            } else {
                redirect('admin/login');
            }
        } else {
            redirect('admin/login');
        }
    }

    public function delete_admission($id) {
        
        if (_is_customer_login($this)) {
            $admin_id = $this->session->userdata('customer_id');
            $role =  $this->session->userdata('customer_role_id');

            if($role >= 1 && $admin_id >= 1) {
                $data = array();
                $this->load->model("Admin_model");
                $category = $this->Admin_model->admin_get_admission_list_by_id($id);
                if ($category) {
                    $query = "DELETE FROM `tbl_online_admission` WHERE `id` = '" . $category->id . "'";
                    // $this->db->query($query);
                    $this->session->set_flashdata("message", '<div class="alert alert-success alert-dismissible" role="alert" id="error">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <strong>Success ! </strong> Admission deleted successfully
                                  </div>');

                    redirect("admin/admission_list");
                    exit;
                }
            } else {
                 redirect('admin/login');
            }
        } else {
             redirect('admin/login');
        }
    }
}
