<?php
class Postjob_model extends CI_Model{

    public function __construct() {
        parent::__construct();
    }

    public function get_detail_by_id($id){
        $this->db->from('tbl_postjob');
        $this->db->where('id', $id );
        $query = $this->db->get();
        return $query->row();
    }

    public function get_postjob_list_by_id($id){
        $this->db->from('tbl_postjob');
        $this->db->where('id', $id );
        $query = $this->db->get();
        return $query->row();
    }

    public function get_postjob_lists_by_id_pdf($id){
        $q = $this->db->query("SELECT `t1`.*, `t2`.`username`, `t3`.`username` as `created_by_name`, `t4`.`tname` FROM `tbl_postjob` `t1` LEFT JOIN `tbl_customers` `t2` ON `t1`.`vendor_id`=`t2`.`customer_id` LEFT JOIN `tbl_customers` `t3` ON `t1`.`created_by`=`t3`.`customer_id` LEFT JOIN `tbl_technology` `t4` ON `t1`.`skill_id`=`t4`.`id` WHERE `t1`.`id`='".$id."'");
        return $q->row();
    }

    public function get_postjob_lists_by_id($id){
        $this->db->select('t1.*,t2.username,t3.username as created_by_name,t4.tname');
        $this->db->from('tbl_postjob t1'); 
        $this->db->join('tbl_customers t2', 't1.vendor_id=t2.customer_id', 'left');
        $this->db->join('tbl_customers t3', 't1.created_by=t3.customer_id', 'left');
        $this->db->join('tbl_technology t4', 't1.skill_id=t4.id', 'left');
        $this->db->where("t1.id", $id);
        $query = $this->db->get();
        return $query->row();  
    }
    
    // get_all_customers 220919
    public function get_postjob_list_by_ajax_query($user_id,$customer_role_type,$search_array = array()) {
        if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) 
        { 
            $this->db->group_start();
                $this->db->or_like("t1.id", $_POST["search"]["value"]);  
                $this->db->or_like("t1.subskills", $_POST["search"]["value"]);  
                $this->db->or_like("t1.duration", $_POST["search"]["value"]);  
                $this->db->or_like("t1.description", $_POST["search"]["value"]);  
                $this->db->or_like("t1.experience", $_POST["search"]["value"]);  
                $this->db->or_like("t1.budget", $_POST["search"]["value"]);  
                $this->db->or_like("t1.no_of_position", $_POST["search"]["value"]);  
                $this->db->or_like("t1.no_of_interview", $_POST["search"]["value"]);  
                $this->db->or_like("t1.candidate_availability", $_POST["search"]["value"]);  
                $this->db->or_like("t2.username", $_POST["search"]["value"]);
                $this->db->or_like("t3.username", $_POST["search"]["value"]);
                $this->db->or_like("t4.tname", $_POST["search"]["value"]);
                $this->db->or_like("t1.post_date",  date("Y-m-d", strtotime($_POST["search"]["value"])));
            $this->db->group_end();
        }  
        $this->db->select('t1.*,t2.username,t3.username as created_by_name,t4.tname');
        $this->db->from('tbl_postjob t1'); 
        $this->db->join('tbl_customers t2', 't1.vendor_id=t2.customer_id', 'left');
        $this->db->join('tbl_customers t3', 't1.created_by=t3.customer_id', 'left');
        $this->db->join('tbl_technology t4', 't1.skill_id=t4.id', 'left');
        if(isset($user_id) && !empty($user_id) && isset($customer_role_type) && !empty($customer_role_type) && $customer_role_type == "V") {
            $this->db->where('t1.vendor_id',$user_id);
        }  

       
        $year_start_date = NULL;
        $year_end_date = NULL;

        $year_start_date = date('Y-m-d',strtotime($search_array['start_date']));

        $year_end_date = date('Y-m-d',strtotime($search_array['end_date']));

        if(isset($search_array['start_date']) && !empty($search_array['start_date']) && isset($search_array['end_date']) && !empty($search_array['end_date'])) { 

          $this->db->where('t1.post_date BETWEEN "'. date('Y-m-d', strtotime($year_start_date)). '" and "'. date('Y-m-d', strtotime($year_end_date)).'"');
        }

    
        if(isset($search_array) && count($search_array)>0){
            if(isset($search_array['skills']) && !empty($search_array['skills']) && $search_array['skills'] >0) {
                $this->db->where("t1.skill_id", $search_array['skills']);
            } 

            if(isset($search_array['vendor_id']) && !empty($search_array['vendor_id']) && $search_array['vendor_id'] >0) {
                $this->db->where("t1.vendor_id", $search_array['vendor_id']);
            }                
        }

        if(isset($_POST["order"])){  
            $column = $_POST['order']['0']['column'];
          
            if($column == 0) {
                $columnName = 't1.id';
            } else if($column == 1) {
               $columnName = 't1.date'; 
            } else if($column == 2) {
               $columnName = 't1.created_by'; 
            } else if($column == 3) {
               $columnName = 't1.vendor_id'; 
            } else if($column == 4) {
               $columnName = 't1.skill_id'; 
            } else if($column == 5) {
               $columnName = 't1.budget'; 
            } else if($column == 6) {
               $columnName = 't1.experience'; 
            } else if($column == 7) {
               $columnName = 't1.no_of_position'; 
            } else {
               $columnName = 't1.id';
            }

            $dir = $_POST['order']['0']['dir'];
            $this->db->order_by($columnName, $dir);  
        } else {  
            $this->db->order_by('t1.id','DESC');  
        }
    }

    public function get_postjob_list_by_ajax($user_id,$customer_role_type,$search_array = array()){  
      $this->get_postjob_list_by_ajax_query($user_id,$customer_role_type,$search_array);
       if(isset($_POST['length']) && $_POST["length"] != -1){  
        $this->db->limit($_POST['length'], $_POST['start']);  
       }  

       $query = $this->db->get();  
       // echo $this->db->last_query();exit;
       return $query->result();  
    }  

    function get_filtered_data_postjob($user_id,$customer_role_type,$search_array){  
       $this->get_postjob_list_by_ajax_query($user_id,$customer_role_type,$search_array);  
       $query = $this->db->get();  
       // echo $this->db->last_query();
       return $query->num_rows();  
    } 

    function get_all_postjob_result($user_id,$customer_role_type,$search_array){  
       $this->get_postjob_list_by_ajax_query($user_id,$customer_role_type,$search_array);
       return $this->db->count_all_results();  
    }  
    // nearest_customers end 120919

     public function get_postjob_list(){
        $q = $this->db->query("SELECT * FROM `tbl_postjob` WHERE `id` !='1' AND `role_type`='V' AND `roletype`='Vendor' ");
        return $q->result();
    }

    public function get_total_postjob($user_id=NULL,$customer_role_type=NULL,$action=""){
            
        $this->db->select('t1.*,t2.username,t3.username as created_by_name,t4.tname');
        $this->db->from('tbl_postjob t1'); 
        $this->db->join('tbl_customers t2', 't1.vendor_id=t2.customer_id', 'left');
        $this->db->join('tbl_customers t3', 't1.created_by=t3.customer_id', 'left');
        $this->db->join('tbl_technology t4', 't1.skill_id=t4.id', 'left');

        if(isset($user_id) && !empty($user_id) && isset($customer_role_type) && !empty($customer_role_type) && $customer_role_type == "V") {
            $this->db->where('t1.vendor_id',$user_id);
        } else {
            $q = $this->db->query("SELECT * FROM `tbl_postjob`");
        }
        if($action == "active") {
            $this->db->where('t1.status',"1");
        }
        $this->db->order_by('t1.post_date','DESC');  
        $query = $this->db->get();  
        return $query->result();   
    }
    
}
?>