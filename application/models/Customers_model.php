<?php
class Customers_model extends CI_Model{

    public function __construct() {
        parent::__construct();
    }

    function check_email_exist($email){
        $q = $this->db->query("SELECT * FROM `tbl_customers` WHERE `email`='".$email."' ");
        return $q->result();
    }
    
    public function get_detail_by_id($id){
        $this->db->from('tbl_customers');
        $this->db->where('customer_id', $id );
        $query = $this->db->get();
        return $query->row();
    }

    public function get_customers_list_by_id($id){
        $this->db->from('tbl_customers');
        $this->db->where('customer_id', $id );
        $query = $this->db->get();
        return $query->row();
    }
    
    public function permission_details_by_name($name) {
      $q = $this->db->query("SELECT * FROM `tbl_permissions` WHERE `name`='".$name."'");
        return $q->row();
    }
    public function all_permissions(){
        $q = $this->db->query("SELECT t1.* FROM `tbl_permissions` AS t1");
        return $q->result();
    }

    public function remove_permissions($id){
        $q = $this->db->query("DELETE FROM `tbl_customers_permissions` WHERE `customer_id`='".$id."'");
        return true;
    }

    public function get_customers_permission_list_by_id($id) {
        $q = $this->db->query("SELECT t1.name FROM `tbl_permissions` AS t1
         LEFT JOIN `tbl_customers_permissions` AS `t2` ON t2.permission_id=t1.id 
         LEFT JOIN `tbl_customers` AS t3 ON t2.customer_id=t3.customer_id
          WHERE t3.customer_id = '".$id."' ");
        return $q->result();
        
    }
 // get_all_customers 220919
    public function get_new_customers_list_by_ajax_query() {
        if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) 
        { 
            $this->db->group_start();
                $this->db->or_like("t1.customer_id", $_POST["search"]["value"]);  
                $this->db->or_like("t1.username", $_POST["search"]["value"]);  
                $this->db->or_like("t1.email", $_POST["search"]["value"]);
                $this->db->or_like("t1.org_password", $_POST["search"]["value"]);
                $this->db->or_like("t1.created_at",  date("Y-m-d h:i:s", strtotime($_POST["search"]["value"])));
            $this->db->group_end();
        }  

        $this->db->select('t1.*');
        $this->db->from('tbl_customers t1'); 
        $this->db->where('t1.is_approve','0');

        if(isset($_POST["order"])){  
            $column = $_POST['order']['0']['column'];
            if($column == 1) {
                $columnName = 't1.customer_id';
            } else if($column == 2) {
               $columnName = 't1.email'; 
            } else if($column == 3) {
               $columnName = 't1.is_social'; 
            }  else if($column == 4) {
               $columnName = 't1.created_at'; 
            } 

            $dir = $_POST['order']['0']['dir'];
            $this->db->order_by($columnName, $dir);  
        } else {  
            $this->db->order_by('t1.customer_id','DESC');  
        }
    }

    public function get_new_customers_list_by_ajax(){  
      $this->get_new_customers_list_by_ajax_query();
       if(isset($_POST['length']) && $_POST["length"] != -1){  
        $this->db->limit($_POST['length'], $_POST['start']);  
       }  

       $query = $this->db->get();  
       // echo $this->db->last_query();exit;
       return $query->result();  
    }  

    function get_filtered_data_new_customers(){  
       $this->get_new_customers_list_by_ajax_query();  
       $query = $this->db->get();  
       // echo $this->db->last_query();
       return $query->num_rows();  
    } 

    function get_all_new_customers_result(){  
       $this->get_new_customers_list_by_ajax_query();
       return $this->db->count_all_results();  
    }  
    // nearest_customers end 120919


    // get_all_customers 220919
    public function get_customers_list_by_ajax_query() {
        if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) 
        { 
            $this->db->group_start();
                $this->db->or_like("t1.customer_id", $_POST["search"]["value"]);  
                $this->db->or_like("t1.username", $_POST["search"]["value"]);  
                $this->db->or_like("t1.email", $_POST["search"]["value"]);
                $this->db->or_like("t1.org_password", $_POST["search"]["value"]);
                $this->db->or_like("t1.roletype", $_POST["search"]["value"]);
                $this->db->or_like("t1.created_at",  date("Y-m-d h:i:s", strtotime($_POST["search"]["value"])));
            $this->db->group_end();
        }  

        $this->db->select('t1.*');
        $this->db->from('tbl_customers t1'); 
        // $this->db->where('t1.is_approve','1');
        $this->db->where('t1.customer_id <> ','1');
        $this->db->where('t1.role_type <>','V');
        $this->db->where('t1.roletype <>','Vendor');

        if(isset($_POST["order"])){  
            $column = $_POST['order']['0']['column'];
              // echo $column;

            if($column == 1) {
                $columnName = 't1.customer_id';
            } else if($column == 2) {
               $columnName = 't1.username'; 
            }  else if($column == 3) {
               $columnName = 't1.email'; 
            }  else if($column == 4) {
               $columnName = 't1.password'; 
            }  else if($column == 5) {
               $columnName = 't1.roletype'; 
            }  else if($column == 6) {
               $columnName = 't1.status'; 
            } else {
               $columnName = 't1.customer_id';
            }

            $dir = $_POST['order']['0']['dir'];
            $this->db->order_by($columnName, $dir);  
        } else {  
            $this->db->order_by('t1.customer_id','DESC');  
        }
    }

    public function get_customers_list_by_ajax(){  
      $this->get_customers_list_by_ajax_query();
       if(isset($_POST['length']) && $_POST["length"] != -1){  
        $this->db->limit($_POST['length'], $_POST['start']);  
       }  

       $query = $this->db->get();  
       // echo $this->db->last_query();exit;
       return $query->result();  
    }  

    function get_filtered_data_customers(){  
       $this->get_customers_list_by_ajax_query();  
       $query = $this->db->get();  
       // echo $this->db->last_query();
       return $query->num_rows();  
    } 

    function get_all_customers_result(){  
       $this->get_customers_list_by_ajax_query();
       return $this->db->count_all_results();  
    }  
    // nearest_customers end 120919


       // get_all_customers 220919
    public function get_customers_rejected_list_by_ajax_query() {
        if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) 
        { 
            $this->db->group_start();
                $this->db->or_like("t1.customer_id", $_POST["search"]["value"]);  
                $this->db->or_like("t1.username", $_POST["search"]["value"]);  
                $this->db->or_like("t1.email", $_POST["search"]["value"]);
                $this->db->or_like("t1.org_password", $_POST["search"]["value"]);
                $this->db->or_like("t1.created_at",  date("Y-m-d h:i:s", strtotime($_POST["search"]["value"])));
            $this->db->group_end();
        }  

        $this->db->select('t1.*');
        $this->db->from('tbl_customers t1'); 
        $this->db->where('t1.is_approve','2');
        $this->db->where('t1.customer_id <> ','1');


        if(isset($_POST["order"])){  
            $column = $_POST['order']['0']['column'];

            if($column == 1) {
                $columnName = 't1.customer_id';
            } else if($column == 2) {
               $columnName = 't1.email'; 
            } else if($column == 3) {
               $columnName = 't1.is_social'; 
            }  else if($column == 4) {
               $columnName = 't1.created_at'; 
            } 

            $dir = $_POST['order']['0']['dir'];
            $this->db->order_by($columnName, $dir);  
        } else {  
            $this->db->order_by('t1.customer_id','DESC');  
        }
    }

    public function get_customers_rejected_list_by_ajax(){  
      $this->get_customers_rejected_list_by_ajax_query();
       if(isset($_POST['length']) && $_POST["length"] != -1){  
        $this->db->limit($_POST['length'], $_POST['start']);  
       }  

       $query = $this->db->get();  
       // echo $this->db->last_query();exit;
       return $query->result();  
    }  

    function get_filtered_rejected_list_data_customers(){  
       $this->get_customers_rejected_list_by_ajax_query();  
       $query = $this->db->get();  
       // echo $this->db->last_query();
       return $query->num_rows();  
    } 

    function get_all_rejected_list_customers_result(){  
       $this->get_customers_rejected_list_by_ajax_query();
       return $this->db->count_all_results();  
    }  
    // nearest_customers end 120919

    public function get_total_customers($status){
        $q = $this->db->query("SELECT * FROM `tbl_customers`");
        return $q->result();
    }


    public function get_customers_by_role(){
        $q = $this->db->query("SELECT * FROM `tbl_customers` WHERE `customer_id` !='1' ");
        return $q->result();
    }

    public function get_customers_by_role_user(){
        $q = $this->db->query("SELECT * FROM `tbl_customers` WHERE `customer_id` !='1' AND `roletype`='User' ");
        return $q->result();
    }


    
}
?>