<?php

class Role_model extends CI_Model {

    public function get_all_role_list(){
        $q = $this->db->query("SELECT * FROM `tbl_role` ORDER BY  `tbl_role`.`id` DESC");
        return $q->result();
    }

    public function get_active_role_list(){
        $q = $this->db->query("SELECT * FROM `tbl_role` WHERE `status`='1' ORDER BY `tbl_role`.`id` ASC");
        return $q->result();
    }

    public function get_role_by_id($donation_id) {
        $q = $this->db->query("SELECT * FROM `tbl_role` WHERE `id` = '" . $donation_id . "' limit 1");
        return $q->row();
    }

}

?>