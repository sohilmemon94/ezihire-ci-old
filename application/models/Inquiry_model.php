<?php

class Inquiry_model extends CI_Model {
    /* ========== Category========== */



     // MVT 120919
    public function get_query_visitorslist_by_ajax() {
        if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) { 
            $this->db->group_start();
                $this->db->or_like("i_id", $_POST["search"]["value"]);  
                $this->db->or_like("name", $_POST["search"]["value"]);  
                $this->db->or_like("email", $_POST["search"]["value"]);
                $this->db->or_like("mobile", $_POST["search"]["value"]);
                $this->db->or_like("message", $_POST["search"]["value"]);
                $this->db->or_like("ip_address", $_POST["search"]["value"]);
                $this->db->or_like("created_at", $_POST["search"]["value"]);
                $this->db->or_like("browser", $_POST["search"]["value"]);
            $this->db->group_end();
        }  
        $this->db->from('tbl_inquiry_list'); 

        if(isset($_POST["order"])){  
            $column = $_POST['order']['0']['column'];
            if($column == 0) {
                $columnName = 'i_id';
            } else if($column == 1) {
                $columnName = 'name';
            } else if($column == 2) {
               $columnName = 'email'; 
            } else if($column == 3) {
               $columnName = 'message'; 
            } else if($column == 4) {
               $columnName = 'mobile'; 
            }  else if($column == 5) {
               $columnName = 'ip_address'; 
            }  else if($column == 6) {
               $columnName = 'address'; 
            }  else if($column == 7) {
               $columnName = 'country'; 
            }  else if($column == 8) {
               $columnName = 'created_at'; 
            } 

            $dir = $_POST['order']['0']['dir'];
            $this->db->order_by($columnName, $dir);  
        } else {  
            $this->db->order_by('i_id', 'DESC');  
        }  
    }

    public function get_visitorslist_by_ajax(){  
      $this->get_query_visitorslist_by_ajax();
       if(isset($_POST['length']) && $_POST["length"] != -1){  
        $this->db->limit($_POST['length'], $_POST['start']);  
       }  

       $query = $this->db->get();  
       return $query->result();  
    }  

    function get_filtered_data_visitorslist_by_ajax(){  
       $this->get_query_visitorslist_by_ajax();  
       $query = $this->db->get();  
       return $query->num_rows();  
    } 

    function get_all_visitorslist_by_ajax(){  
       $this->get_query_visitorslist_by_ajax();
       return $this->db->count_all_results();  
    }  

    public function get_detail_by_id($id){
        $q = $this->db->query("select * from tbl_inquiry_list where  i_id = '".$id."' limit 1");
        return $q->row();
    }
    // MVT end 120919s


}

?>