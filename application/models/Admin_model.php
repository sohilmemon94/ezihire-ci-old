<?php
class Admin_model extends CI_Model{
        /* ========== Category========== */

        
    /* get users detail */
     public function get_detail_by_id($id){
        $q = $this->db->query("select * from tbl_admin where  admin_id = '".$id."' limit 1");
        return $q->row();
    }


    public function get_social_media_detail(){
        $q = $this->db->query("select * from tbl_site_settings where  id = '1' limit 1");
        return $q->row();
    }

        
    public function get_currency_symbol(){
        $q = $this->db->query("SELECT `currency_symbol` FROM `tbl_admin` where  `admin_id` = '1' LIMIT 0,1");
        return $q->row();
    }

       
    public function edit_category()
    {
        $slug = url_title($this->input->post('cat_title'), 'dash', TRUE);
        $parentid = $this->input->post("parent");
        $editcat = array(
                        "title"=>$this->input->post("cat_title"),
                        "slug"=>$slug,
                        "parent"=>$this->input->post("parent"),
                        "description"=>strip_tags($this->input->post("cat_descri")),
                        "status"=>$this->input->post("cat_status")
                        );
        
                if($_FILES["cat_img"]["size"] > 0){
                    $config['upload_path']          = './uploads/admin/category/';
                    $config['allowed_types']        = 'gif|jpg|png|jpeg';
                    $this->load->library('upload', $config);
    
                    if ( ! $this->upload->do_upload('cat_img'))
                    {
                            $error = array('error' => $this->upload->display_errors());
                    }
                    else
                    {
                        $img_data = $this->upload->data();
                        $editcat["image"]=$img_data['file_name'];
                    }
                    
               }
               if($parentid != "0"){
                $q = $this->db->query("select * from `categories` where id=".$parentid);
                $parent = $q->row();
                $leval = $parent->leval + 1;
                $editcat["leval"] = $leval;                       
                }
              
                $this->db->update("categories",$editcat,array("id"=>$this->input->post("cat_id"))); 
    }
       
    public function inquiry_list($start_date=NULL,$end_date=NULL) {
        $where = "";
        if(isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date)) {
            $where = "WHERE `created_at` BETWEEN  '".$start_date."' AND '".$end_date."'";

        } 
        $query = "SELECT * FROM `tbl_inquiry_list` ".$where." ORDER BY `tbl_inquiry_list`.`i_id` ASC";
        $q = $this->db->query($query);
        return $q->result();
   }

   public function visitors_list($start_date=NULL,$end_date=NULL) {
        $where = "";
        if(isset($start_date) && !empty($start_date) && isset($end_date) && !empty($end_date)) {
            $where = "WHERE `idate` BETWEEN  '".$start_date."' AND '".$end_date."'";

        } 
        $query = "SELECT * FROM `tbl_visitors` ".$where." ORDER BY `tbl_visitors`.`id` ASC";
        $q = $this->db->query($query);
        return $q->result();
   }

    public function getusercount(){

        $totalcount = $this->db->query("SELECT count(id) as totalcount FROM `tbl_visitors`")->row();

        $totaluser1=$this->db->query("SELECT count(id) as totalcount1 FROM `tbl_visitors` WHERE DATEDIFF(NOW(), DATE(`visitingdate`)) <= 1")->row();

        $totaluser10=$this->db->query("SELECT count(id) as totalcount10 FROM `tbl_visitors` WHERE DATEDIFF(NOW(), DATE(`visitingdate`)) <= 10")->row();

        $totaluser20=$this->db->query("SELECT count(id) as totalcount20 FROM `tbl_visitors` WHERE DATEDIFF(NOW(), DATE(`visitingdate`)) <= 20")->row();

        $totaluser30=$this->db->query("SELECT count(id) as totalcount30 FROM `tbl_visitors` WHERE DATEDIFF(NOW(), DATE(`visitingdate`)) <= 30")->row();

        $arr=array();
        
        $arr['totalvisitors1']=$totaluser1;
        $arr['totalvisitors10']=$totaluser10;
        $arr['totalvisitors20']=$totaluser20;
        $arr['totalvisitors30']=$totaluser30;
        $arr['totalvisitors']=$totalcount;
        return $arr;

    }

}
?>
