<?php

class Backup_model extends CI_Model {

    public function get_all_backup_list(){
        $q = $this->db->query("SELECT * FROM `tbl_backup` ORDER BY  `tbl_backup`.`id` DESC");
        return $q->result();
    }

    public function get_active_backup_list(){
        $q = $this->db->query("SELECT * FROM `tbl_backup` WHERE `status`='1' ORDER BY `tbl_backup`.`id` DESC");
        return $q->result();
    }

    public function get_backup_by_id($donation_id) {
        $q = $this->db->query("SELECT * FROM `tbl_backup` WHERE `id` = '" . $donation_id . "' limit 1");
        return $q->row();
    }
}

?>