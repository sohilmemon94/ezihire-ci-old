<?php

class Clients_model extends CI_Model {

    public function get_all_clients_list(){
        $q = $this->db->query("SELECT * FROM `tbl_clients` ORDER BY  `tbl_clients`.`id` DESC");
        return $q->result();
    }

    public function get_active_clients_list(){
        $q = $this->db->query("SELECT * FROM `tbl_clients` WHERE `status`='1' ORDER BY `tbl_clients`.`bname` ASC");
        return $q->result();
    }

    public function get_clients_by_id($donation_id) {
        $q = $this->db->query("SELECT * FROM `tbl_clients` WHERE `id` = '" . $donation_id . "' limit 1");
        return $q->row();
    }

}

?>