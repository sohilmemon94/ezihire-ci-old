<?php

class Education_model extends CI_Model {

    public function get_all_education_list(){
        $q = $this->db->query("SELECT * FROM `tbl_education` ORDER BY id ASC");
        return $q->result();
    }

    public function get_active_education_list(){
        $q = $this->db->query("SELECT * FROM `tbl_education` WHERE `status`='1' ORDER BY id ASC");
        return $q->result();
    }

    public function get_education_by_id($donation_id) {
        $q = $this->db->query("SELECT * FROM `tbl_education` WHERE `id` = '" . $donation_id . "' limit 1");
        return $q->row();
    }
    
}

?>