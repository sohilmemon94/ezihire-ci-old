<?php

class Industry_model extends CI_Model {

    public function get_all_industry_list(){
        $q = $this->db->query("SELECT * FROM `tbl_industry` ORDER BY id ASC");
        return $q->result();
    }

    public function get_active_industry_list(){
        $q = $this->db->query("SELECT * FROM `tbl_industry` WHERE `status`='1' ORDER BY id ASC");
        return $q->result();
    }

    public function get_industry_by_id($donation_id) {
        $q = $this->db->query("SELECT * FROM `tbl_industry` WHERE `id` = '" . $donation_id . "' limit 1");
        return $q->row();
    }
    
}

?>