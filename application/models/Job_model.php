<?php

class Job_model extends CI_Model {

    public function get_all_job_list(){
        $q = $this->db->query("SELECT * FROM `tbl_job` ORDER BY id ASC");
        return $q->result();
    }

    public function get_active_job_list(){
        $q = $this->db->query("SELECT * FROM `tbl_job` WHERE `status`='1' ORDER BY id ASC");
        return $q->result();
    }

    public function get_job_by_id($donation_id) {
        $q = $this->db->query("SELECT * FROM `tbl_job` WHERE `id` = '" . $donation_id . "' limit 1");
        return $q->row();
    }
    
}

?>