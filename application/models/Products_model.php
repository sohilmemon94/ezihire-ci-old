<?php

class Products_model extends CI_Model {

    public function get_all_product(){
        $q = $this->db->query("SELECT * FROM `tbl_product` ORDER BY  `tbl_product`.`id` DESC");
        return $q->result();
    }

    public function get_all_active_product(){
        $q = $this->db->query("SELECT * FROM `tbl_product` WHERE `status`='1' ORDER BY  `tbl_product`.`id` DESC");
        return $q->result();
    }

    public function get_all_product_with_seller_details(){
        $q = $this->db->query("SELECT t1.*,t2.company_name,t2.name FROM `tbl_product` as t1 LEFT JOIN `tbl_sellers` as t2 ON t1.seller_id=t2.id  ORDER BY  t1.id DESC");
        return $q->result();
    }

    public function get_all_product_with_seller_detailsss($ids){
        $q = $this->db->query("SELECT t1.*,t2.company_name,t2.name FROM `tbl_product` as t1 LEFT JOIN `tbl_sellers` as t2 ON t1.seller_id=t2.id WHERE t1.id  IN (".$ids.") ORDER BY t1.id DESC");
        return $q->result();
    }

    public function get_product_seller_details_by_product_id_seller_id($product_id){
        $q = $this->db->query("SELECT t2.* FROM `tbl_product` as t1 LEFT JOIN `tbl_sellers` as t2 ON t1.seller_id=t2.id WHERE t1.id = '".$product_id."' ORDER BY t1.id DESC");
        return $q->result();
    }

    public function get_active_banner_list(){
        $q = $this->db->query("SELECT * FROM `tbl_banner` WHERE `status`='1' ORDER BY `tbl_banner`.`id` DESC");
        return $q->result();
    }

    public function get_banner_by_id($donation_id) {
        $q = $this->db->query("SELECT * FROM `tbl_banner` WHERE `id` = '" . $donation_id . "' limit 1");
        return $q->row();
    }

    public function get_single_product($id) {
        $query="SELECT * FROM `tbl_product` WHERE `id`='".$id."'";
        return $this->db->query($query)->row();
    }

    public function get_product_by_id_with_first_image($id) {
        $query="SELECT t1.*,t2.p_image FROM `tbl_product` AS t1 LEFT JOIN `tbl_product_images` AS t2 ON t2.p_id=t1.id WHERE t1.id='".$id."' AND t1.status='1' ORDER BY t2.pi_id ASC LIMIT 0,1";
        return $this->db->query($query)->result();
    }

    public function get_category_by_id($id) {
        $query="SELECT tpc.p_id,tpc.c_id,tc.cname,tpc.sub_cat_id,tpc.third_level_id,tc.slug FROM tbl_product_category as tpc LEFT JOIN tbl_category as tc ON tpc.c_id=tc.id WHERE tpc.p_id='".$id."'";
        return $this->db->query($query)->row();
    }

    public function get_variation_from_product_id($id){
        $query="SELECT tpv.v_id,tcv.vname FROM `tbl_product_variation` as tpv LEFT JOIN tbl_category_variation as tcv ON tpv.v_id=tcv.id WHERE tpv.p_id='".$id."'";
        return $this->db->query($query)->result(); 
    }

    public function get_variation_detail_from_product_id($product_id,$v_id) {
        $query="SELECT t2.id,t2.variation_id,t2.vname FROM `tbl_product_variation_details` as t1 LEFT JOIN tbl_category_variation_details as t2 ON t1.vd_id=t2.id WHERE t1.p_id='".$product_id."' AND t1.v_id = '".$v_id."'";
        return $this->db->query($query)->result();    
    }

    public function get_all_level_category_by_product_id($id) {
        $query="SELECT * FROM `tbl_product_category` WHERE `p_id`='".$id."'";
        return $this->db->query($query)->row();
    }

    public function get_sub_category($cat_id=""){
        if(!empty($cat_id)) {
            $query="SELECT id as subcat_id,cname as subcat FROM tbl_category WHERE id='".$cat_id."'";
            return $this->db->query($query)->row();     
        } else{
            return 0;
        }
    }

    public function more_product_same_seller($product_seller_id,$product_id) {
        if(isset($product_seller_id) && !empty($product_seller_id)) {
            $query="SELECT * FROM `tbl_product` WHERE `id` NOT IN ('".$product_id."') AND `seller_id`='".$product_seller_id."' ORDER BY `id` DESC LIMIT 0,20";
        } else {
            $query="SELECT * FROM `tbl_product` WHERE `id` NOT IN ('".$product_id."') AND `seller_id` IS NULL ORDER BY `id` DESC LIMIT 0,20";            
        }
        return $this->db->query($query)->result();
    }

    public function get_my_wishlist_products($user_id) {
       $query="SELECT t1.user_id,t2.* FROM `tbl_wishlist` AS t1 LEFT JOIN `tbl_product` AS t2  ON t1.product_id = t2.id WHERE t1.user_id='".$user_id."' AND t2.status='1' ORDER BY t1.id DESC";
        $result = $this->db->query($query)->result();
        return $result;
    }

    public function get_variation($id,$cat_id) {
            $query="SELECT tcv.vname,tpv.v_id FROM `tbl_product_variation` as tpv LEFT JOIN tbl_category_variation as tcv ON tpv.v_id=tcv.id WHERE tpv.p_id='".$id."' AND tcv.category_id='".$cat_id."'";
            return $this->db->query($query)->result();     
    }

    public function get_variation_details($id,$cat_id,$var_id) {
            $query="SELECT tcvd.vname,tcvd.id as tcvd_id FROM `tbl_product_variation_details` AS tpvd LEFT JOIN `tbl_category_variation_details` as tcvd ON tpvd.`vd_id`=tcvd.id WHERE tpvd.p_id='".$id."' AND tpvd.c_id='".$cat_id."' AND tpvd.v_id='".$var_id."'";
            return $this->db->query($query)->result();     
    }

    public function get_product_images_by_id($id){
        $query="SELECT * FROM `tbl_product_images` WHERE p_id='".$id."' limit 0,2";
        return $this->db->query($query)->result();
    }

    public function get_all_product_images_by_id($id){
        $query="SELECT * FROM `tbl_product_images` WHERE p_id='".$id."'";
        return $this->db->query($query)->result();
    }

    public function deleteproductimage($imageid){
        $query="DELETE FROM `tbl_product_images` WHERE `pi_id`='".$imageid."'";
        return $this->db->query($query);
    }

    public function product_by_seller_id($sellerid){
        $q = $this->db->query("SELECT * FROM `tbl_product` WHERE `seller_id`='".$sellerid."' ORDER BY `tbl_product`.`my_stock` ASC");
        return $q->result();
    }

    public function get_product_by_category($id){
        $query="SELECT * FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id WHERE tpc.c_id='".$id."'";
        return $this->db->query($query)->result();
    }

    public function check_product_in_wish_list_or_not($customer_id,$product_id) {
        $query="SELECT * FROM `tbl_wishlist` WHERE `user_id`='".$customer_id."' AND `product_id`='".$product_id."'";
        $result = $this->db->query($query)->num_rows();
        return $result;
    }

    public function getproductbycategory($cat_id){
       //$query="SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id WHERE tpc.c_id='".$cat_id."' AND tp.status='1'";
     $query="SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id WHERE (tpc.c_id='".$cat_id."' OR tpc.sub_cat_id='".$cat_id."' OR tpc.third_level_id='".$cat_id."' ) AND tp.status='1' ORDER BY tp.id DESC ";
       
       return $this->db->query($query)->result();
    }

    public function getminmax($cat_id){
        $query="SELECT min(tp.reg_price) as minprice,max(tp.reg_price) as maxprice FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id  WHERE (tpc.c_id='".$cat_id."' OR tpc.sub_cat_id='".$cat_id."' OR tpc.third_level_id='".$cat_id."' ) AND tp.status='1'  ORDER BY tp.id DESC";
        return $this->db->query($query)->row();
    }

    public function getproductbycategorypagination($cat_id,$record=0,$recordPerPage,$filter_v_detail=''){
       //$query="SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id WHERE tpc.c_id='".$cat_id."' AND tp.status='1'";

        if(count($filter_v_detail) > 0) {
            $filter_where  = '';
            $numItems = count($filter_v_detail);
            $i = 0;
            foreach($filter_v_detail as $k=>$v) {
                $filter_where .= "tpvd.vd_id='$v'";
                    if(++$i === $numItems) { } else {
                        $filter_where .= " OR ";
                    }
            }

            // $query="SELECT tp.* FROM `tbl_product` AS tp LEFT JOIN `tbl_product_category` AS tpc ON tp.id=tpc.p_id LEFT JOIN `tbl_product_variation_details` AS tpvd ON tp.id=tpvd.p_id WHERE (".$filter_where.") AND (tpc.c_id='".$cat_id."' OR tpc.sub_cat_id='".$cat_id."' OR tpc.third_level_id='".$cat_id."') AND tp.status='1' GROUP BY tp.id ORDER BY tp.id DESC LIMIT $record,$recordPerPage";
            $query="SELECT tp.* FROM `tbl_product` AS tp LEFT JOIN `tbl_product_category` AS tpc ON tp.id=tpc.p_id LEFT JOIN `tbl_product_variation_details` AS tpvd ON tp.id=tpvd.p_id LEFT JOIN `tbl_category` AS t1 ON tpc.c_id=t1.id  LEFT JOIN `tbl_category` AS t2 ON tpc.sub_cat_id=t2.id  LEFT JOIN `tbl_category` AS t3 ON tpc.third_level_id=t3.id WHERE (".$filter_where.") AND (tpc.c_id='".$cat_id."' OR tpc.sub_cat_id='".$cat_id."' OR tpc.third_level_id='".$cat_id."') AND tp.status='1' AND t1.status='1' AND t2.status='1' AND t3.status='1' GROUP BY tp.id ORDER BY tp.id DESC";
        } else {    
            $query = "SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id LEFT JOIN `tbl_category` AS t1 ON tpc.c_id=t1.id  LEFT JOIN `tbl_category` AS t2 ON tpc.sub_cat_id=t2.id  LEFT JOIN `tbl_category` AS t3 ON tpc.third_level_id=t3.id  WHERE (tpc.c_id='".$cat_id."' OR tpc.sub_cat_id='".$cat_id."' OR tpc.third_level_id='".$cat_id."') AND tp.status='1' AND t1.status='1' AND t2.status='1' AND t3.status='1' ORDER BY tp.id DESC LIMIT $record,$recordPerPage";    
        }

        // echo $query;
        // exit;
        
       return $this->db->query($query)->result();
    }

    public function getproductbycategorynumrows($cat_id){
       //$query="SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id WHERE tpc.c_id='".$cat_id."' AND tp.status='1'";

      // $query="SELECT tp.*,tcvd.vname FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id LEFT JOIN tbl_product_variation_details tpvd ON tp.id=tpvd.p_id LEFT JOIN tbl_category_variation_details as tcvd ON tpvd.vd_id=tcvd.id WHERE (tpc.c_id='".$cat_id."' OR tpc.sub_cat_id='".$cat_id."' OR tpc.third_level_id='".$cat_id."') AND tp.status='1' ORDER BY tp.id DESC ";

       $query="SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id  LEFT JOIN `tbl_category` AS t1 ON tpc.c_id=t1.id  LEFT JOIN `tbl_category` AS t2 ON tpc.sub_cat_id=t2.id  LEFT JOIN `tbl_category` AS t3 ON tpc.third_level_id=t3.id WHERE (tpc.c_id='".$cat_id."' OR tpc.sub_cat_id='".$cat_id."' OR tpc.third_level_id='".$cat_id."') AND tp.status='1'  AND t1.status='1' AND t2.status='1' AND t3.status='1' ORDER BY tp.id DESC";

       return $this->db->query($query)->num_rows();
    }

    public function getpricewisefilter($id,$min,$max,$record=0,$recordPerPage){
        $query="SELECT tp.*,tcvd.vname FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id LEFT JOIN tbl_product_variation_details tpvd ON tp.id=tpvd.p_id LEFT JOIN tbl_category_variation_details as tcvd ON tpvd.vd_id=tcvd.id WHERE (tpc.c_id='".$id."' OR tpc.sub_cat_id='".$id."' ) AND tp.status='1' AND tp.reg_price BETWEEN '$min' AND '$max' ";
        $query1="SELECT tp.*,tcvd.vname FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id LEFT JOIN tbl_product_variation_details tpvd ON tp.id=tpvd.p_id LEFT JOIN tbl_category_variation_details as tcvd ON tpvd.vd_id=tcvd.id WHERE (tpc.c_id='".$id."' OR tpc.sub_cat_id='".$id."' ) AND tp.status='1' AND tp.reg_price BETWEEN '$min' AND '$max' ";
        $result['rows']=$this->db->query($query1)->num_rows();
        $result['data']=$this->db->query($query)->result();
        return $result;
    }

    public function getvarition($cat_id){
        $query="SELECT tcv.* FROM tbl_product_variation as tpv LEFT JOIN tbl_category_variation as tcv ON tpv.v_id = tcv.id WHERE (tpv.c_id='".$cat_id."' OR tpv.c_id='".$cat_id."' ) GROUP BY tcv.id ORDER BY tcv.vname ASC";
        return $this->db->query($query)->result();
    }

    public function get_product_by_variations($catid,$varid){
        $query="SELECT tp.*, tcvd.vname FROM tbl_product AS tp LEFT JOIN tbl_product_category AS tpc ON tp.id = tpc.p_id LEFT JOIN tbl_product_variation_details AS tpvd ON tp.id = tpvd.p_id LEFT JOIN tbl_category_variation_details AS tcvd ON tpvd.vd_id = tcvd.id LEFT JOIN tbl_product_variation as tpv ON tp.id=tpv.p_id LEFT JOIN tbl_category_variation as tcv ON tpv.v_id=tcv.id WHERE tpc.c_id = '".$catid."' AND tp.status = '1' AND tcv.id='".$varid."'";
        $result['rows']=$this->db->query($query)->num_rows();
        $result['data']=$this->db->query($query)->result();
        return $result;   
    }

    public function getvariationdetails($id){
        $query="SELECT tcvd.* FROM tbl_product_variation_details as tpvd LEFT JOIN tbl_category_variation_details as tcvd ON tpvd.vd_id = tcvd.id WHERE tcvd.category_id='".$id."' GROUP BY tcvd.id ORDER BY tcvd.vname ASC";
        return $this->db->query($query)->result();
    }

    public function get_product_by_variations_details($cat_id,$vard_id){
       //$query="SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id WHERE tpc.c_id='".$cat_id."' AND tp.status='1'";
       $query="SELECT tp.*,tcvd.vname FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id LEFT JOIN tbl_product_variation_details tpvd ON tp.id=tpvd.p_id LEFT JOIN tbl_category_variation_details as tcvd ON tpvd.vd_id=tcvd.id WHERE (tpc.c_id='".$cat_id."' OR tpc.sub_cat_id='".$cat_id."' ) AND tp.status='1' AND tpvd.vd_id='".$vard_id."'";
       $result['rows']=$this->db->query($query)->num_rows();
       $result['data']=$this->db->query($query)->result();
        return $result;
    }

    public function related_product($pid,$cid){
        $query="SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id WHERE tp.id NOT IN ('".$pid."') AND tpc.c_id='".$cid."'";
        return $this->db->query($query)->result();
    }

    public function get_id_from_slug_prouct($slug){
        $query="SELECT * FROM `tbl_product` WHERE slug='".$slug."' AND status='1'";
        return $this->db->query($query)->row();
    }

    public function get_product_details_by_product_id($pid){
        $query="SELECT * FROM `tbl_product` WHERE `id` = '".$pid."' ";
        return $this->db->query($query)->row();
    }

    public function get_product_review_by_id($product_id) {
        $query="SELECT t1.*,t2.id as user_id,t2.name FROM `tbl_product_review_rating` AS t1 LEFT JOIN `tbl_customers` AS t2 ON t2.id = t1.customer_id WHERE t1.product_id='".$product_id."' ORDER BY t1.id DESC";
        return $this->db->query($query)->result();
    }

    public function getAvgProductRating_by_id($product_id) {
        $query="SELECT COUNT(rating) as rating_num, FORMAT((SUM(rating) / COUNT(rating)),1) as average_rating FROM `tbl_product_review_rating` WHERE product_id='".$product_id."' ORDER BY id DESC";
        return $this->db->query($query)->result();
    }

    public function check_user_have_submited_reivew($user_id,$product_id) {
        $query ="SELECT * FROM `tbl_product_review_rating` WHERE `customer_id`='".$user_id."' AND `product_id` ='".$product_id."' ";
        return $this->db->query($query)->result();
    }

    public function get_review_id_from_slug_prouct($review_id){
        $query ="SELECT t2.slug FROM `tbl_product_review_rating` AS t1 LEFT JOIN `tbl_product` AS t2 ON t1.product_id=t2.id WHERE t1.id ='".$review_id."' ";
        return $this->db->query($query)->result();
    }

    public function get_v_detail_bv_vid_and_category_id($category_id,$v_id) {
       $query = "SELECT * FROM `tbl_category_variation_details` WHERE `category_id`='".$category_id."' AND `variation_id`='".$v_id."' AND `status`='1' ORDER BY `vname` ASC";
        return $this->db->query($query)->result();
    }

    public function get_products_by_search($search) {
        $query="SELECT * FROM `tbl_product` WHERE (`product_name` LIKE '%$search%' OR `slug` LIKE '%$search%' OR `product_desc` LIKE '%$search%' OR `product_log_desc` LIKE '%$search%' OR `sku` LIKE '%$search%' ) AND `status` = '1' ORDER BY `id` DESC";
        return $this->db->query($query)->result();
    }

    public function get_variation_from_product_ids($product_ids){
        $query="SELECT tcv.* FROM `tbl_product_variation` as tpv LEFT JOIN tbl_category_variation as tcv ON tpv.v_id=tcv.id WHERE tpv.p_id IN ($product_ids) GROUP BY tcv.id ORDER BY tcv.id ASC";
        return $this->db->query($query)->result(); 
    }

    public function getproductbysearchpagination($cat_id,$record=0,$recordPerPage,$filter_v_detail=''){
      
        if(count($filter_v_detail) > 0) {
            $filter_where  = '';
            $numItems = count($filter_v_detail);
            $i = 0;
            foreach($filter_v_detail as $k=>$v) {
                $filter_where .= "tpvd.vd_id='$v'";
                    if(++$i === $numItems) { } else {
                        $filter_where .= " OR ";
                    }
            }

            $query="SELECT tp.* FROM `tbl_product` AS tp LEFT JOIN `tbl_product_category` AS tpc ON tp.id=tpc.p_id LEFT JOIN `tbl_product_variation_details` AS tpvd ON tp.id=tpvd.p_id WHERE (".$filter_where.") AND (tp.product_name LIKE '%$cat_id%' OR tp.slug LIKE '%$cat_id%' OR tp.product_desc LIKE '%$cat_id%' OR tp.product_log_desc LIKE '%$cat_id%' OR tp.sku LIKE '%$cat_id%') AND tp.status='1' GROUP BY tp.id ORDER BY tp.id DESC";
        } else {    
            // $query = "SELECT tp.* FROM tbl_product as tp WHERE (tp.product_name LIKE '%$cat_id%' OR tp.slug LIKE '%$cat_id%' OR tp.product_desc LIKE '%$cat_id%' OR tp.product_log_desc LIKE '%$cat_id%' OR tp.sku LIKE '%$cat_id%') AND tp.status='1' ORDER BY tp.id DESC LIMIT $record,$recordPerPage";    


               $query = "SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id LEFT JOIN `tbl_category` AS t1 ON tpc.c_id=t1.id  LEFT JOIN `tbl_category` AS t2 ON tpc.sub_cat_id=t2.id  LEFT JOIN `tbl_category` AS t3 ON tpc.third_level_id=t3.id  WHERE (tp.product_name LIKE '%$cat_id%' OR tp.slug LIKE '%$cat_id%' OR tp.product_desc LIKE '%$cat_id%' OR tp.product_log_desc LIKE '%$cat_id%' OR tp.sku LIKE '%$cat_id%') AND tp.status='1' AND t1.status='1' AND t2.status='1' AND t3.status='1' ORDER BY tp.id DESC LIMIT $record,$recordPerPage";


        }
        return $this->db->query($query)->result();
    }

    public function getproductbysearchnumrows($cat_id){
        $query="SELECT tp.* FROM tbl_product as tp WHERE (tp.product_name LIKE '%$cat_id%' OR tp.slug LIKE '%$cat_id%' OR tp.product_desc LIKE '%$cat_id%' OR tp.product_log_desc LIKE '%$cat_id%' OR tp.sku LIKE '%$cat_id%') AND tp.status='1' ORDER BY tp.id DESC";
        return $this->db->query($query)->num_rows();
    }

    public function getProductsfromcategory_id($cat_id){
       $query="SELECT tp.* FROM tbl_product as tp LEFT JOIN tbl_product_category as tpc ON tp.id=tpc.p_id WHERE  (tpc.c_id='".$cat_id."' OR tpc.sub_cat_id='".$cat_id."' OR tpc.third_level_id='".$cat_id."') AND tp.status='1' ORDER BY RAND() LIMIT 0,15";

       return $this->db->query($query)->result();
    }

    public function update_stock_details($product_id,$qty) {
        $query = "UPDATE `tbl_product` SET `my_stock`= my_stock - $qty WHERE  `id`='".$product_id."'";
        $this->db->query($query);
        return true;
    }
}


?>