<?php
class Employees_model extends CI_Model{

    public function __construct() {
        parent::__construct();
    }

    function check_email_exist($email){
        $q = $this->db->query("SELECT * FROM `tbl_employees` WHERE `email`='".$email."' ");
        return $q->result();
    }

    function check_company_name_exist($username,$customer_id=NULL){
        $where = "";
        if(isset($customer_id) && !empty($customer_id)) {
          $where .= "AND `customer_id` !='".$customer_id."'";
        }

        $q = $this->db->query("SELECT * FROM `tbl_employees` WHERE `username`='".$username."' AND `role_type`='V' AND `roletype`='Vendor' AND `role_id`='4' $where");
        return $q->result();
    }
    
    public function get_detail_by_id($id){
        $this->db->from('tbl_employees');
        $this->db->where('customer_id', $id );
        $query = $this->db->get();
        return $query->row();
    }

    public function get_employees_list_by_id($id){
        $this->db->from('tbl_employees');
        $this->db->where('id', $id );
        $query = $this->db->get();
        return $query->row();
    }

     public function get_my_employees_list_by_id($id){
        $this->db->from('tbl_employees_data');
        $this->db->where('id', $id );
        $query = $this->db->get();
        return $query->row();
    }

    public function get_employees_education_list_by_id($id){
        $this->db->from('tbl_employees_education');
        $this->db->where('employee_id', $id );
        $query = $this->db->get();
        return $query->result();
    }

    public function get_employees_company_list_by_id($id){
        $this->db->from('tbl_employees_company');
        $this->db->where('employee_id', $id );
        $query = $this->db->get();
        return $query->result();
    }

    public function get_employees_project_list_by_id($id){
        $this->db->from('tbl_employees_projects');
        $this->db->where('employee_id', $id );
        $query = $this->db->get();
        return $query->result();
    }

    public function get_employees_project_skill_list_by_id($id){
        $this->db->from('tbl_employees_project_skills');
        $this->db->where('project_id', $id );
        $query = $this->db->get();
        return $query->result();
    }


    public function get_employees_skill_list_by_id($id){
        $this->db->from('tbl_employees_skills');
        $this->db->where('employee_id', $id );
        $query = $this->db->get();
        return $query->result();
    }


    // get_all_employees 220919
    public function get_employees_list_by_ajax_query($user_id=NULL,$customer_role_type=NULL) {
        if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) { 
            $this->db->group_start();
                $this->db->or_like("t1.customers_id", $_POST["search"]["value"]);  
                $this->db->or_like("t1.code", $_POST["search"]["value"]);  
                $this->db->or_like("t1.name", $_POST["search"]["value"]);
                $this->db->or_like("t1.email", $_POST["search"]["value"]);
                $this->db->or_like("t1.address", $_POST["search"]["value"]);
                $this->db->or_like("t1.mobileno", $_POST["search"]["value"]);
                $this->db->or_like("t1.alternateno", $_POST["search"]["value"]);
                $this->db->or_like("t1.about_me", $_POST["search"]["value"]);
                $this->db->or_like("t1.gender", $_POST["search"]["value"]);
                $this->db->or_like("t1.linkedin", $_POST["search"]["value"]);
                $this->db->or_like("t2.username", $_POST["search"]["value"]);
                $this->db->or_like("t1.designation", $_POST["search"]["value"]);
                $this->db->or_like("t1.created_at",  date("Y-m-d h:i:s", strtotime($_POST["search"]["value"])));
            $this->db->group_end();
        }  

        $this->db->select('t1.*,t2.username');
        $this->db->from('tbl_employees t1'); 
        $this->db->join('tbl_customers t2', 't1.customers_id=t2.customer_id', 'left');
        if(isset($user_id) && !empty($user_id) && isset($customer_role_type) && !empty($customer_role_type) && $customer_role_type == "V") {
            $this->db->where('t1.customers_id',$user_id);
        }  

        if(isset($_POST["order"])){  
            $column = $_POST['order']['0']['column'];
            //echo $column;

            if($column == 0) {
                $columnName = 't1.id';
            } else if($column == 1) {
               $columnName = 't1.image'; 
            } else if($column == 2) {
               $columnName = 't1.code'; 
            } else if($column == 3) {
               $columnName = 't1.customers_id'; 
            } else if($column == 4) {
               $columnName = 't1.name'; 
            } else if($column == 5) {
               $columnName = 't1.email'; 
            } else if($column == 6) {
               $columnName = 't1.mobileno'; 
            } else if($column == 7) {
               $columnName = 't1.linkedin'; 
            }  else {
               $columnName = 't1.id';
            }

            $dir = $_POST['order']['0']['dir'];
            $this->db->order_by($columnName, $dir);  
        } else {  
            $this->db->order_by('t1.id','DESC');  
        }
    }

    public function get_employees_list_by_ajax($user_id,$customer_role_type){  
      $this->get_employees_list_by_ajax_query($user_id,$customer_role_type);
       if(isset($_POST['length']) && $_POST["length"] != -1){  
        $this->db->limit($_POST['length'], $_POST['start']);  
       }  

       $query = $this->db->get();  
       // echo $this->db->last_query();exit;
       return $query->result();  
    }  

    function get_filtered_data_employees($user_id,$customer_role_type){  
       $this->get_employees_list_by_ajax_query($user_id,$customer_role_type);  
       $query = $this->db->get();  
       // echo $this->db->last_query();
       return $query->num_rows();  
    } 

    function get_all_employees_result($user_id,$customer_role_type){  
       $this->get_employees_list_by_ajax_query($user_id,$customer_role_type);
       return $this->db->count_all_results();  
    }  


    // get_all_my_employees 220919
    public function get_my_employees_list_by_ajax_query($user_id=NULL,$customer_role_type=NULL) {
        if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"])) { 
            $this->db->group_start();
                $this->db->or_like("t1.customers_id", $_POST["search"]["value"]);  
                $this->db->or_like("t1.name", $_POST["search"]["value"]);
                $this->db->or_like("t1.gender", $_POST["search"]["value"]);
                $this->db->or_like("t1.budget", $_POST["search"]["value"]);
                $this->db->or_like("t1.candidate_availability", $_POST["search"]["value"]);
                $this->db->or_like("t2.username", $_POST["search"]["value"]);
                $this->db->or_like("t4.tname", $_POST["search"]["value"]);
                $this->db->or_like("t1.created_at",  date("Y-m-d h:i:s", strtotime($_POST["search"]["value"])));
            $this->db->group_end();
        }  

        $this->db->select('t1.*,t2.username,t4.tname');
        $this->db->from('tbl_employees_data t1'); 
        $this->db->join('tbl_customers t2', 't1.customers_id=t2.customer_id', 'left');
        $this->db->join('tbl_technology t4', 't1.primary_skills=t4.id', 'left');
        if(isset($user_id) && !empty($user_id) && isset($customer_role_type) && !empty($customer_role_type) && $customer_role_type == "V") {
            $this->db->where('t1.customers_id',$user_id);
        }  

        if(isset($_POST["order"])){  
            $column = $_POST['order']['0']['column'];
            //echo $column;

            if($column == 0) {
                $columnName = 't1.id';
            } else if($column == 1) {
               $columnName = 't1.gender'; 
            } else if($column == 2) {
               $columnName = 't2.username'; 
            } else if($column == 3) {
               $columnName = 't1.name'; 
            } else if($column == 4) {
               $columnName = 't4.tname'; 
            } else if($column == 5) {
               $columnName = 't1.budget'; 
            } else if($column == 6) {
               $columnName = 't1.candidate_availability'; 
            } else {
               $columnName = 't1.id';
            }

            $dir = $_POST['order']['0']['dir'];
            $this->db->order_by($columnName, $dir);  
        } else {  
            $this->db->order_by('t1.id','DESC');  
        }
    }

    public function get_my_employees_list_by_ajax($user_id,$customer_role_type){  
      $this->get_my_employees_list_by_ajax_query($user_id,$customer_role_type);
       if(isset($_POST['length']) && $_POST["length"] != -1){  
        $this->db->limit($_POST['length'], $_POST['start']);  
       }  

       $query = $this->db->get();  
       // echo $this->db->last_query();exit;
       return $query->result();  
    }  

    function get_filtered_data_my_employees($user_id,$customer_role_type){  
       $this->get_my_employees_list_by_ajax_query($user_id,$customer_role_type);  
       $query = $this->db->get();  
       // echo $this->db->last_query();
       return $query->num_rows();  
    } 

    function get_all_my_employees_result($user_id,$customer_role_type){  
       $this->get_my_employees_list_by_ajax_query($user_id,$customer_role_type);
       return $this->db->count_all_results();  
    }  


    // nearest_employees end 120919

    public function get_total_employees($user_id=NULL,$customer_role_type=NULL){

        if(isset($user_id) && !empty($user_id) && isset($customer_role_type) && !empty($customer_role_type) && $customer_role_type == "V") {
            $q = $this->db->query("SELECT * FROM `tbl_employees` WHERE `customers_id`='".$user_id."' AND `status`='1' " );
        } else {
            $q = $this->db->query("SELECT * FROM `tbl_employees` WHERE `status`='1' ");
        } 
        return $q->result();
    }

    public function get_employees_by_role(){
        $q = $this->db->query("SELECT * FROM `tbl_employees` WHERE `customer_id` !='1' ");
        return $q->result();
    }

    public function get_employees_by_role_user(){
        $q = $this->db->query("SELECT * FROM `tbl_employees` WHERE `customer_id` !='1' AND `roletype`='User' ");
        return $q->result();
    }

    public function get_skills_list_by_id($id){
        $q = $this->db->query("SELECT `tname` FROM `tbl_employees_skills` WHERE `employee_id` ='".$id."'");
        return $q->result_array();
    }

    public function get_employees_skills_records($rowperpage, $rowno, $user_id,$customer_role_type,$skillsid,$vendor_id,$search_type) {
        $where = "";
        $whereArr = array();
        if(isset($user_id) && !empty($user_id) && isset($customer_role_type) && !empty($customer_role_type) && $customer_role_type == "V") {
            $where .= " AND t1.customers_id = '".$user_id."'";
            $whereArr[] =  " t1.customers_id = '".$user_id."'";
        }  

        if(isset($vendor_id) && !empty($vendor_id) && $vendor_id>0) {
            $where .= " AND t1.customers_id = '".$vendor_id."'";
            $whereArr[] =  " t1.customers_id = '".$vendor_id."'";
        } 

        if($search_type == 1) {

            $where .= " AND t1.primary_skills = '".$skillsid."'";
            $whereArr[] =  " t1.primary_skills = '".$skillsid."'";

        } else if($search_type == 2) {

            if(isset($skillsid) && !empty($skillsid) && $skillsid>0) {
                $where .= " AND t2.technology_id ='".$skillsid."'";
                $whereArr[] =  "t2.technology_id ='".$skillsid."'";
            }
        }


        $whereCondition = "";
        if(count($whereArr)>1){
            $whereCondition  = implode('AND ',$whereArr);
        } else {
            $whereCondition = $whereArr[0];
        }
        

        $q = $this->db->query("SELECT t1.*,t2.technology_id FROM `tbl_employees` as t1 LEFT JOIN `tbl_employees_skills` AS t2 ON t1.id=t2.employee_id WHERE $whereCondition  GROUP BY t1.id ORDER BY t1.id DESC LIMIT $rowno,$rowperpage ");
        return $q->result_array();
    }

    public function get_employees_skills_records_all($user_id,$customer_role_type,$skillsid,$vendor_id,$search_type) {
        $where = "";
        $whereArr = array();
        if(isset($user_id) && !empty($user_id) && isset($customer_role_type) && !empty($customer_role_type) && $customer_role_type == "V") {
            $whereArr[] =  " t1.customers_id = '".$user_id."'";
        }  

        if(isset($vendor_id) && !empty($vendor_id) && $vendor_id>0) {
            $where .= " t1.customers_id = '".$vendor_id."'";
            $whereArr[] =  " t1.customers_id = '".$vendor_id."'";
        } 

        if(isset($skillsid) && !empty($skillsid) && $skillsid > 0) {
            if($search_type == 1) {
                $where .= " AND t1.primary_skills = '".$skillsid."'";
                $whereArr[] =  " t1.primary_skills = '".$skillsid."'";
            } else if($search_type == 2) {
                if(isset($skillsid) && !empty($skillsid) && $skillsid>0) {
                    $where .= " AND t2.technology_id ='".$skillsid."'";
                    $whereArr[] =  "t2.technology_id ='".$skillsid."'";
                }
            }
        }

        $whereCondition = "";
        if(count($whereArr)>1){
            $whereCondition  = implode('AND ',$whereArr);
        } else {
            $whereCondition = $whereArr[0];
        }
        

        $q = $this->db->query("SELECT t1.*,t2.technology_id FROM `tbl_employees` as t1 LEFT JOIN `tbl_employees_skills` AS t2 ON t1.id=t2.employee_id WHERE  $whereCondition  GROUP BY t1.id ORDER BY t1.id DESC ");
        return $q->result_array();
    }
}
?>