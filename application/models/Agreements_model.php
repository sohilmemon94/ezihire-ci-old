<?php

class Agreements_model extends CI_Model {

    public function get_all_agreements_list(){
        $q = $this->db->query("SELECT * FROM `tbl_agreements` ORDER BY id ASC");
        return $q->result();
    }

    public function get_active_agreements_list(){
        $q = $this->db->query("SELECT * FROM `tbl_agreements` WHERE `status`='1' ORDER BY id ASC");
        return $q->result();
    }

    public function get_agreements_by_id($donation_id) {
        $q = $this->db->query("SELECT * FROM `tbl_agreements` WHERE `id` = '" . $donation_id . "' limit 1");
        return $q->row();
    }
    
}

?>