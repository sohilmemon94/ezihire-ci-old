<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
   
    <title><?php echo $title; ?> | Admin - <?php echo $page; ?></title>
      <?php  $this->load->view("admin/common/common_css"); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}
</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view("admin/common/common_header"); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
         <?php $this->load->view("admin/common/common_sidebar"); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item active"><?php echo $page; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                
                                <h4 class="card-title">Admin Profile</h4>
                                <h6 class="card-subtitle"></h6>
                                 <?php if(isset($error)){ echo $error; }
                                   echo $this->session->flashdata("message"); ?>

                                    <form role="form" name="form"  id="form_edit" action="" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Company Name <span class="text-danger">*</span></label>
                                                    <?php echo $admindata->username; ?>
                                                </div>
                                            </div>
                                           

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Owner Name <span class="text-danger">*</span></label>
                                                     <input class="form-control" type="text" id="ownername" name="ownername" value="<?php echo $admindata->ownername; ?>" placeholder="Owner Name">
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Industry <span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="industry_id" id="industry_id">
                                                    <option>--- Select Industry ---</option>
                                                    <?php foreach($industrys as $r=>$v) { ?>
                                                        <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" <?php if($v->id == $admindata->industry_id) { echo "selected=selected"; } ?> id="<?php echo $v->id; ?>"><?php echo $v->iname; ?></option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Address <span class="text-danger">*</span></label>
                                                    <textarea class="form-control" placeholder="Address" id="address" name="address"><?php echo $admindata->address; ?></textarea>
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Mobile No <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="number"  id="mobileno" name="mobileno" value="<?php echo $admindata->mobileno; ?>" placeholder="Mobile No">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Altername Number <span class="text-danger">*</span></label>
                                                     <input class="form-control" type="number"  id="alternateno" name="alternateno" value="<?php echo $admindata->alternateno; ?>" placeholder="Altername No">
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Linked In <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="url"  id="linkedin" name="linkedin" value="<?php echo $admindata->linkedin; ?>" placeholder="Linked In">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Skype ID <span class="text-danger">*</span></label>
                                                     <input class="form-control" type="text" id="skypeid" name="skypeid" value="<?php echo $admindata->skypeid; ?>" placeholder="Skype ID">
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Email ID <span class="text-danger">*</span></label>
                                                    <b><?php echo $admindata->email; ?></b>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Password <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="password" name="password" value="<?php echo $admindata->org_password; ?>" id="password" >
                                                </div>
                                            </div>                                      
                                        </div>

                                      
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Profile Pic</label>
                                                   <input type="file" name="vendor_image" id="vendor_image" class="dropify" />
                                                </div>
                                            </div> 
                                            <div class="col-md-6">
                                                 <?php 
                                                $image = $admindata->image;
                                                $imagefullpath = base_url().$image;
                                                if(!empty($image)) {
                                                    $returnpath = $this->config->item('customers_images_uploaded_path');
                                                
                                                    if(!file_exists($imagefullpath)){
                                                        $imagePath = "";
                                                        $imagefullpath = $returnpath.$image;
                                                        $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpath.'"><img src="';
                                                        $imagePath .= $imagefullpath;
                                                        $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                                        </span>';
                                                            echo $imagePath;
                                                    }
                                                }
                                            ?>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">My Agreement</label>
                                                   <input type="file" name="vendor_agreement" id="vendor_agreement" class="dropify"  accept="application/pdf,application/vnd.ms-excel"  />
                                                </div>
                                            </div> 
                                            <div class="col-md-6">
                                                 <?php 
                                                $agreement = $admindata->agreement;
                                            if(isset($agreement) && !empty($agreement)) {
                                                $imagefullpath = base_url().$agreement;
                                                if(!empty($agreement)) {
                                                    $returnpath = $this->config->item('customers_vendor_agreement_uploaded_path');
                                                
                                                    if(!file_exists($imagefullpath)){
                                                        $imagePath = "";
                                                        $imagefullpath = $returnpath.$agreement;
                                                        $imagePath .= '<a class="fresco" data-fresco-group="example" target="_blank" href="'.$imagefullpath.'">Download Aagreement</a>
                                                        ';
                                                        echo "<br><br>";
                                                            echo $imagePath;
                                                    }
                                                } 
                                            } else {
                                                $imagePath= "";
                                                $imagefullpath = "";
                                                $imagePath .= '<a href="'.site_url("vendors/vendors_agreement/".$admin_id) .'" data-toggle="tooltip"  data-placement="left" title="Edit"  style="text-align: center; margin: 10px;padding: 10px;"> Download My Agreement</a>
                                                ';
                                                echo "<br><br>";
                                                echo $imagePath;
                                            }
                                            ?>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <button type="submit" class="btn btn-info" id="save_button"  id="save_button" name="save_button" value="Update">Edit</button>
                                                <a href="<?php echo site_url("vendors/approve_vendors_list"); ?>" class="btn btn-inverse">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->






            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
             <?php  $this->load->view("admin/common/common_footer"); ?>

            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
     <?php  $this->load->view("admin/common/common_js"); ?>
</body>

</html>


 <script>
   $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
  });


var myfile="";
$('#vendor_agreement').on( 'change', function() {
   myfile= $( this ).val();
   var ext = myfile.split('.').pop();
   if(ext=="pdf" || ext=="docx" || ext=="doc"){
      
   } else{
       alert("PDF, Doc, Docx files allowed");
   }
});


</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>


<script>
    jQuery.validator.addMethod("password_check", function (value, element) {
        return this.optional(element) || /(?=.*\d)(?=.*[a-z])(?=.*[!@#\$%\^&\*\?\[({\]})"\';:_\-<>\., =\+\/\\])(?=.*[A-Z]).{8,}/.test(value);
    }, "Please provide a Strong Password. 'e.g:Password@123'");

  // Wait for the DOM to be ready

$('#image').bind('change', function() {
    var a=(this.files[0].size);
    if(a > 1024000) {
        alert('Not accept more than 1MB image');
        $("#image").val(null);
    };
});


    $(function() {
        $("#form_edit").validate({
        // Specify validation rules
        rules: {
            username:{
                required:true,
                remote: {
                    url: "<?php echo base_url(); ?>check_company_name_exist",
                    type: "POST",
                    data: {
                        username: function(){ return $("#username").val(); },
                    }
                },
            },
            ownername:{
                required:true,
            },
            industry_id:{
                <?php if($role==1){ ?>
                    required:false,
                <?php } else { ?> 
                    required:true,
                    min:1,
                <?php } ?>
               
            },
            address:{
                required:true,
            },
            mobileno:{
                required:true,
                number:true,
            },
            alternateno:{
                required:true,
                number:true,
            },
            linkedin:{
                required:true,
                url:true,
            },
            skypeid:{
                required:true,
            },
           
            password: {
                required:true,
            },
        },
        messages: {
            username:{
                required:"Please enter company name",
                remote: "Company already exist !",
            },
            ownername:{
                required:"Please enter Owner Name",
            },
            industry_id:{
                required:"Please select type of industry",
                min:"Please select type of industry",
            },
            address:{
                required:"Please enter addresss",
            },
            mobileno:{
                required:"Please enter mobile no",
            },
            alternateno:{
                required:"Please enter mobile no",
            },
            linkedin:{
                required:"Please enter linkedin URL",
            },
            skypeid:{
                required:"Please enter skype ID",
            },
           
            password: {
                required:"Please enter password",
            },
        },
        submitHandler: function(form) {
           form.submit();
        }
    });

    // $('#save_button').click(function() {
    //     if($('#form_edit').valid()){
    //         $(this).attr('disabled', 'disabled');
    //         $(this).html('Saving...');
    //         $(this).parents('form').submit();
    //     }
    // });
    
});
</script>
