<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
   
    <title><?php echo $title; ?> | Admin - <?php echo $page; ?></title>
      <?php  $this->load->view("admin/common/common_css"); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}
</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view("admin/common/common_header"); ?>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
         <?php $this->load->view("admin/common/common_sidebar"); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item active"><?php echo $page; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                
                                <h4 class="card-title">Admin Profile</h4>
                                <h6 class="card-subtitle"></h6>
                                 <?php if(isset($error)){ echo $error; }
                                   echo $this->session->flashdata("message"); ?>
                                <form class="form" name="form" id="form_edit" role="form" method="post" enctype="multipart/form-data">
                                    <div class="form-group m-t-40 row">
                                        <label for="example-text-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Username <span class="text-danger">*</span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <input class="form-control" type="text" name="username" value="<?php echo $admindata->username; ?>" id="example-text-input">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Email ID <span class="text-danger">*</span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                           <input class="form-control" type="text" name="email" value="<?php echo $admindata->email; ?>" id="email" readonly="readonly" disabled="disabled">
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <label for="example-password-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Image <span class="text-danger"></span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <input type="file" name="image" id="image"  class="dropify"  accept="image/*" />
                                        </div>
                                            <?php if(isset($admindata->image) && !empty($admindata->image)) { ?>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                             <?php 
                                                $image = $admindata->image;

                                                $imagefullpath = base_url().$image;
                                                if(!empty($image)) {
                                                    $returnpath = $this->config->item('customers_images_uploaded_path');
                                                
                                                    if(!file_exists($imagefullpath)){
                                                        $imagefullpath = $returnpath.$image;
                                                        $imagePath = "";
                                                        $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpath.'"><img src="';
                                                        $imagePath .= $imagefullpath;
                                                        $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                                        </span>';
                                                            echo $imagePath;
                                                    }
                                                }
                                            ?>
                                            
                                            
                                        </div>
                                            <?php  } ?>
                                    </div>
                                   
                                    
                                    <div class="form-group row">
                                        <label for="example-password-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Background Image <span class="text-danger"></span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <input type="file" name="background_image" id="background_image"  class="dropify"  accept="image/*" />
                                        </div>
                                            <?php if(isset($adminmaster->background_image) && !empty($adminmaster->background_image)) { ?>
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                             <?php 
                                                $background_image = $adminmaster->background_image;

                                                $imagefullpath = base_url().$background_image;
                                                if(!empty($background_image)) {
                                                    $returnpath = $this->config->item('customers_images_uploaded_path');
                                                
                                                    if(!file_exists($imagefullpath)){
                                                        $imagefullpath = $returnpath.$background_image;
                                                        $imagePath = "";
                                                        $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpath.'"><img src="';
                                                        $imagePath .= $imagefullpath;
                                                        $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                                        </span>';
                                                            echo $imagePath;
                                                    }
                                                }
                                            ?>
                                            
                                            
                                        </div>
                                            <?php  } ?>
                                    </div>
                                 
                                    <div class="form-group row">
                                        <label for="example-password-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">New Password <span class="text-danger"></span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <input class="form-control" type="password" name="newpassword" value="" id="newpassword">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-password-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Confirm Password <span class="text-danger"></span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <input class="form-control" type="password" name="confirmpassword" value="" id="confirmpassword">
                                        </div>
                                    </div>

                                   
                                     
                                    <div class="form-group row">
                                     
                                         <div class="col-lg-2 col-md-2 col-sm-2">
                                         </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <button type="submit" class="btn btn-info"  id="save_button" name="save_button" value="Update">Submit</button>
                                        <a href="<?php echo site_url("admin/dashboard"); ?>" class="btn btn-inverse">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->






            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            
             <?php  $this->load->view("admin/common/common_footer"); ?>

            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
     <?php  $this->load->view("admin/common/common_js"); ?>
</body>

</html>


 <script>
   $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
  });


</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>


<script>
    jQuery.validator.addMethod("password_check", function (value, element) {
        return this.optional(element) || /(?=.*\d)(?=.*[a-z])(?=.*[!@#\$%\^&\*\?\[({\]})"\';:_\-<>\., =\+\/\\])(?=.*[A-Z]).{8,}/.test(value);
    }, "Please provide a Strong Password. 'e.g:Password@123'");

  // Wait for the DOM to be ready

$('#image').bind('change', function() {
    var a=(this.files[0].size);
    if(a > 1024000) {
        alert('Not accept more than 1MB image');
        $("#image").val(null);
    };
});

$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("#form_edit").validate({
    // Specify validation rules
    rules: {
        'username': {required:true,normalizer: function(value){return $.trim(value);},},
        
        'image': {
            accept: "image/*"
        },
        'newpassword': { minlength:5, password_check:false,normalizer: function(value){return $.trim(value);}, },
        'confirmpassword': { minlength:5, password_check: false, equalTo:'[id="newpassword"]',normalizer: function(value){return $.trim(value);},},
    },
    messages: {
    'username':  { required: "Please enter username"},
   
    'image': {
        accept: 'Please provide valid Image File'
      },
    'newpassword': {minlength:"Please enter minimum 5 character" },
    'confirmpassword': {minlength:"Please enter minimum 5 character" , equalTo: "Password and confirm password must be same."},
    },

    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
    $('#save_button').click(function() {
        if($('#form_edit').valid()){
            $(this).attr('readonly', true);
            $(this).html('Updating...');
            $(this).parents('form').submit();
        }
    });

});


</script>
