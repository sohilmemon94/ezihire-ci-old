<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
   
    <title><?php echo PROJECT_NAME; ?>| Admin - <?php echo $page; ?></title>
      <?php  $this->load->view("admin/common/common_css"); ?>
</head>
<style>
    .userform label {
        width: 120px;
        color: #333;
        float: left;
    }
    input.error {
        border: 1px solid red;
    }
    label.error{
        width: 100%;
        color: red;
        font-style: normal !important;
        margin-left: 0px !important;
        margin-bottom: 5px;
    }
</style>
<body class="fix-header fix-sidebar card-no-border">
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <div id="main-wrapper">
        <?php $this->load->view("admin/common/common_header"); ?>

        <?php $this->load->view("admin/common/common_sidebar"); ?>
        <div class="page-wrapper">
            <div class="container-fluid">
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item active"><?php echo $page; ?></li>
                        </ol>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Social Media Link</h4>
                                <h6 class="card-subtitle"></h6>
                                <?php if(isset($error)){ echo $error; }
                                   echo $this->session->flashdata("message"); ?>
                                <form class="form" name="form" id="form_edit" role="form" method="post" enctype="multipart/form-data">
                                      
                                    <div class="form-body">
                                        <h3 class="box-title"></h3>
                                        <hr class="mt-0 mb-5">
                                        
                                        <div class="row">
                                          <div class="col-lg-12 col-md-12">
                                            <div class="form-group row">
                                              <label class="control-label text-right col-md-3">Facebook Url<span class="text-danger">*</span></label>
                                              <div class="col-md-9">
                                                <input type="text" name="facebook" value="<?php echo urldecode($site_setting->facebook);?>" class="form-control" id="facebook" placeholder="Facebook">
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <?php /* <div class="row">
                                          <div class="col-lg-12 col-md-12">
                                            <div class="form-group row">
                                              <label class="control-label text-right col-md-3">Instagram Url<span class="text-danger">*</span></label>
                                              <div class="col-md-9">
                                                <input type="text" name="instagram" value="<?php echo urldecode($site_setting->instagram);?>" class="form-control" id="instagram" placeholder="Instagram">
                                              </div>
                                            </div>
                                          </div>
                                        </div> */ ?>

                                        <div class="row">
                                          <div class="col-lg-12 col-md-12">
                                            <div class="form-group row">
                                              <label class="control-label text-right col-md-3">Twitter Url<span class="text-danger">*</span></label>
                                              <div class="col-md-9">
                                                <input type="text" name="twitter" value="<?php echo urldecode($site_setting->twitter);?>" class="form-control" id="twitter" placeholder="Twitter">
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                       
                                        <div class="row">
                                          <div class="col-lg-12 col-md-12">
                                            <div class="form-group row">
                                              <label class="control-label text-right col-md-3">Linkdin Url<span class="text-danger">*</span></label>
                                              <div class="col-md-9">
                                                <input type="text" name="linkdin" value="<?php echo urldecode($site_setting->linkdin);?>" class="form-control" id="linkdin" placeholder="Linkdin">
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                         <div class="row">
                                          <div class="col-lg-12 col-md-12">
                                            <div class="form-group row">
                                              <label class="control-label text-right col-md-3">Google Url<span class="text-danger">*</span></label>
                                              <div class="col-md-9">
                                                <input type="text" name="google" value="<?php echo urldecode($site_setting->google);?>" class="form-control" id="google" placeholder="Google">
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                       <?php /*  <div class="row">
                                          <div class="col-lg-12 col-md-12">
                                            <div class="form-group row">
                                              <label class="control-label text-right col-md-3">Youtube Url<span class="text-danger">*</span></label>
                                              <div class="col-md-9">
                                                <input type="text" name="youtube" value="<?php echo urldecode($site_setting->youtube);?>" class="form-control" id="youtube" placeholder="youtube">
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="col-lg-12 col-md-12">
                                            <div class="form-group row">
                                              <label class="control-label text-right col-md-3">Youtube Video Link<span class="text-danger">*</span></label>
                                              <div class="col-md-9">
                                                <input type="text" name="youtubelink" value="<?php echo urldecode($site_setting->youtubelink);?>" class="form-control" id="youtubelink" placeholder="youtubelink">
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                    </div> */ ?>

                                    
                             <hr>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                             <input type="submit" class="btn btn-info" id="save_button" name="btn_submit" value="Update">
                                             <a href="<?php echo site_url("admin/dashboard"); ?>" class="btn btn-inverse">Cancel</a>
                                           </div>
                                       </div>
                                   </div>

                               </div>
                           </div>


                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <?php  $this->load->view("admin/common/common_footer"); ?>
        </div>
    </div>
     <?php  $this->load->view("admin/common/common_js"); ?>
</body>

</html>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script>
    $(document).ready( function() {
        $('#error').delay(3000).fadeOut();
    });

    $(function() {

        function validateURL(value){
            var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
            var regex = new RegExp(expression);
            return value.match(regex);
        }

        $.validator.addMethod("urlvalidation", function(value, element) {
            return this.optional(element) || validateURL(value);
        }, "Please enter valid Website Like Example ( www.google.com )");


        $("#form_edit").validate({
            rules: {
                'facebook': {required:true,urlvalidation:true},
                'twitter': {required:true,urlvalidation:true},
                'linkdin': {required:true,urlvalidation:true},
                'google': {required:true,urlvalidation:true},
            },
            messages: {
                'facebook':{required:"Please enter facebook URL"},
                'twitter':{required:"Please enter twitter URL"},
                'linkdin':{required:"Please enter linkdin URL"},
                'google':{required:"Please enter google URL"},
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        $('#save_button').click(function() {
            if($('#form_edit').valid()){
                $(this).attr('readonly', true);
                $(this).html('Updating...');
                $(this).parents('form').submit();
            }
        });
    });
</script>