<?php
defined('BASEPATH') OR exit('No direct script access allowed');  
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>images/favicon.ico">
    <title>Infiraise - Employee</title>
    <style>
        section{padding-left:15px !important;padding-right:15px !important;padding-bottom:25px !important;padding-top:25px !important;margin-left:15px !important;margin-right:15px !important;margin-bottom:25px !important;margin-top:25px !important;
        }
        body{
            border: solid black 1px;padding-left:15px !important;padding-right:15px !important;margin-left:15px !important;margin-right:15px !important;background: #ffffff;font-family: 'Open Sans', sans-serif;font-size: 14px;font-weight: normal;margin: 15px;
        } 
        .top-head a,img{text-align:center !important;display: block !important; }
        .top-head p{text-align:center !important;line-height: 24px;margin: 0;color: #000;}
        p{margin: 0!important;}
        h2{font-size: 26px;color: #232323;font-weight: normal;margin: 0px 0 25px 0;}
        h6{font-size: 16px;color: #000;font-family: 'Open Sans', sans-serif;font-weight: 400;margin: 20px 0 20px 0;line-height: 28px;}
        .employee-info{margin: 40px 0 0 40px;}
        .employee-info h2{margin: 0 0 0 0p;}
        .employee-info ul {padding: 0;}
        .employee-info ul li {margin: 10px 0;}
        .message-item:before {content: ""; background: #8a8a8a;border-radius: 2px;bottom: -30px;height: 100%;left: -30px;position: absolute;width: 1px;}
        .message-item:after {content: "";background: #000000;border: 2px solid #000000;border-radius: 50%;box-shadow: 0 0 5px rgb(0 0 0 / 10%);height: 15px;left: -36px;position: absolute;top: 10px;width: 15px;}
        .message-item {margin-bottom: 25px;margin-left: 40px;position: relative;}
        .message-item .message-inner:before {border-right: 10px solid rgb(0, 0, 0);border-style: solid;border-width: 10px;color: rgba(0,0,0,0);content: "";display: block;height: 0;position: absolute;left: -21px;top: 6px;width: 0;}
        .message-inner{border: 1px solid #797979;border-radius: 3px;padding: 10px;position: relative;}
        .message-item:after {content: "";background: #000000;border: 2px solid #000000;border-radius: 50%;box-shadow: 0 0 5px rgb(0 0 0 / 10%);height: 15px;left: -36px;position: absolute;top: 10px;width: 15px;}
        .message-item .message-head {border-bottom: 1px solid #656565;margin-bottom: 8px;padding-bottom: 8px;color: #fff;}
        .clearfix:before, .clearfix:after {content: " ";display: table;}
        .clearfix:before, .clearfix:after {content: '\0020';display: block;overflow: hidden;visibility: hidden;width: 0;height: 0;}
        .message-item .message-head .user-detail h5 {font-size: 16px;font-weight: bold;margin: 0;color: #000;}
        .message-item .message-head .post-meta {float: left;padding: 0 15px 0 0;}
        .message-item .message-head .post-meta >div {color: black;font-weight: bold;text-align: right;}
        .post-meta > div {color: black;font-size: 12px;line-height: 22px;}
        .qa-message-content p {line-height: 24px;margin: 0;color: #000;}
        .user-detail{overflow: hidden;}
        table {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid black;
}

th, td {
  text-align: center;
  padding: 8px;
  border-left: 1px solid black;
  border-top: 1px solid black;
}
tbody tr:nth-child(odd) {background-color: #f2f2f2;}
@media screen and (max-width:767px)
{
    .employee-info{margin: 40px 0 0 0;}
}

@media screen and (max-width:576px)
{
    p{font-size: 8px;}
}
   </style>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<section class="top-head">   
    <div class="container" >
        
        <p><img src="<?php echo base_url(); ?>uploads/logo.png" alt="logo"></img></p>
        <p><b><?php echo $employees->name; ?></b></p>
        <p><b><?php echo $employees->designation; ?></b></p>
        <p>Working on <?php echo $skills_list; ?> Language</p>
            
        <div class="row employee-info">
            <div class="col-md-12">
            <h2>About Employee</h2>
                <?php echo $employees->about_me; ?>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->  
    </div> 
</section>
<?php if(count($empEdu)>0) { ?>
<section>        
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Education Information</h2>
                <div class="table-responsive m-t-40">
                    <table id="example23" class="table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Graduation/PG/Any Other Courses</th>
                                <th scope="col">Passing Year</th>
                                <th scope="col">Board/University Name</th>
                                <th scope="col">Percentage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $rowno = 1;
                                foreach($empEdu as $k=> $v){ ?>
                                    <tr>
                                        <td scope="col"><?php echo $rowno; ?></td>
                                        <td scope="col"><?php echo $v->graduation; ?></td>
                                        <td scope="col"><?php echo $v->passing_year; ?></td>
                                        <td scope="col"><?php echo $v->board_university; ?></td>
                                        <td scope="col"><?php echo $v->percentage; ?></td>
                                      </tr>
                                <?php 
                                      $rowno++;
                                  } ?> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if(count($empCompany)>0) { ?>
<section id="experience" class="timeline">
    <div class="contaisner">
        <h2>Experience</h2>
        <h6>Lorem ipsum dolor sit amet, consectetur Morbi sagittis, sem quisci ipsum</h6>
        <div class="qa-message-list" id="wallmessages">
            <?php foreach($empCompany as $ec=>$c) {  ?>
                <div class="message-item" id="m16">
                    <div class="message-inner">
                        <div class="message-head clearfix"> 
                            <div class="user-detail">
                                <h5 class="handle"><?php echo $c->company_name; ?> </h5>
                                <div class="post-meta">
                                    <div class="asker-meta">
                                        <span class="qa-message-what"></span>
                                        <span class="qa-message-when">
                                            <span class="qa-message-when-data">( <?php echo $c->exprience_year; ?>  )</span>
                                        </span> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="qa-message-content">
                            <p>Working as a <?php echo $c->job_position; ?> in <?php echo $c->job_location; ?></p>
                            <p><?php echo $c->job_role; ?></p>
                        </div>
                </div></div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
         
<?php if(count($empProject)>0) { ?> 
<section id="experience" class="timeline">
    <div class="contdainer">
        <h2>Projects</h2>
        <h6>Lorem ipsum dolor sit amet, consectetur Morbi sagittis, sem quisci ipsum</h6>
        <div class="qa-message-list" id="wallmessages">

            <?php foreach($empProject as $ep=>$p) {   ?>
            <div class="message-item" id="m16">
                <div class="message-inner">
                    <div class="message-head clearfix"> 
                        <div class="user-detail">
                            <h5 class="handle"><b><?php echo $p->project_name; ?></b></h5>
                        </div>
                    </div>
                    <div class="qa-message-content">
                       <p><?php echo $p->project_details; ?></p>
                    </div>
            </div></div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
</body>
</html>
