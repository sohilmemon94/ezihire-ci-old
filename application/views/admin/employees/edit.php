<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo $title; ?> | <?php echo $page; ?> <?php echo $action; ?></title>
        <?php // $this->load->view("admin/common/common_css"); ?>
       
        <link rel="icon" type="image/ico" sizes="16x16" href="<?php echo base_url(); ?>img/favicon.png">
        <!-- Bootstrap Core CSS -->

        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- chartist CSS -->
        <link href="<?php echo base_url(); ?>assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/css-chart/css-chart.css" rel="stylesheet">

        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url(); ?>assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />

        <link href="<?php echo base_url(); ?>assets/plugins/wizard/steps.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/plugins/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/fresco.css" /> 
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <!-- You can change the theme colors from here -->
        <link href="<?php echo base_url(); ?>css/colors/default-dark.css" id="theme" rel="stylesheet">

        <!-- for image upload -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dropify/dist/css/dropify.min.css">

        <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
        <script src="<?php echo base_url(); ?>js/additional-methods.min.js"></script>
    </head>

  <style>
.userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}

.help-block{
    border: :1px solid red !important;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}

input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}
#education_information,
#project_information,
#skill_information,
#company_information{
    display:none;
}
.education-section,.company-section,.project-section{
    border:2px solid #ddd;
    background: #f0f2f6;
    margin-top: 20px;
}

.padding{
    padding: 10px;
}
.previous-btn {
    width: 50%;
    float: left;
}

.remove-me,.cremove-me,.premove-me,.remove-me-project,#previousEducation,.next,#previousCompany,#previousProject,#previousSkills{
    color: #fff !important;
}
#add-more,#exprience-add-more,#project-add-more{
    margin-top: 25px;
}
</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                           <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>employees/employees_list"><?php echo $page; ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $action; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                 <!-- Validation wizard -->
                 <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                <h4 class="card-title">New <?php echo $page; ?></h4>
                                <h6 class="card-subtitle"></h6>
                                <?php if (validation_errors())
                                {   
                                echo '<div class="alert alert-warning alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning!</strong> ';

                                echo validation_errors();
                                echo '</div>';

                                }

                                $exist_vendor_id = $admin_id;
                                $is_access = "No";
                                if($customer_role_type == "V") {
                                    if($admin_id == $exist_vendor_id) {
                                        $is_access = "Yes";
                                    }
                                } else if($customer_role_type == "A") {
                                    $is_access = "No";
                                } else {
                                    $is_access = "No";
                                }
                               
                                ?>
                                <div class="alert alert-success alert-dismissible " role="alert" id="myElem" style="display:none">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Success !</strong> Image deleted successfully. </div>
                                 <form class="form" name="form" id="form_edit" role="form" method="post" enctype="multipart/form-data">

                                    <fieldset id="personal_information" class="">
                                        <legend>Personal Information</legend>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Vendor Name <span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="vendor_id" id="vendor_id">
                                                    <option>--- Select Vendor ---</option>
                                                    <?php foreach($vendors as $r=>$v) { 
                                                        if($is_access == "Yes") {
                                                            if($v->customer_id == $employees->customers_id) {
                                                        ?>
                                                            <option value="<?php echo $v->customer_id; ?>" name="<?php echo $v->customer_id; ?>" id="<?php echo $v->customer_id; ?>"  <?php if($v->customer_id == $employees->customers_id) { echo "selected=selected"; } ?>><?php echo $v->username; ?></option>
                                                            <?php }
                                                        } else { ?>
                                                            <option value="<?php echo $v->customer_id; ?>" name="<?php echo $v->customer_id; ?>" id="<?php echo $v->customer_id; ?>"  <?php if($v->customer_id == $employees->customers_id) { echo "selected=selected"; } ?>><?php echo $v->username; ?></option>

                                                    <?php } }  ?>
                                                    </select> 
                                                </div>
                                            </div> 
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Employee Name <span class="text-danger">*</span></label>
                                                     <input class="form-control" type="text" id="name" name="name" value="<?php echo $employees->name; ?>" placeholder="Employee Name" >
                                                </div>
                                            </div> 
                                                                                   
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Email ID <span class="text-danger"></span></label>
                                                    <input class="form-control" type="text" id="email" name="email" value="<?php echo $employees->email; ?>" placeholder="Email ID " aria-required="true">
                                                </div>
                                            </div>

                                           
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Mobile No <span class="text-danger"></span></label>
                                                    <input class="form-control" type="number"  id="mobileno" name="mobileno" value="<?php echo $employees->mobileno; ?>" placeholder="Mobile No">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Gender <span class="text-danger">*</span></label>
                                                        <select class="form-control" name="gender" id="gender">
                                                            <option name="Male" <?php if($employees->gender == "Male") { echo "selected=selected"; } ?> id="Male" value="Male" >Male</option>
                                                            <option name="Female"  <?php if($employees->gender == "Female") { echo "selected=selected"; } ?> id="Female" value="Female">Female</option>
                                                        <select>                                                     
                                                </div>
                                            </div>                                    
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Profile Pic</label>
                                                   <input type="file" name="vendor_image" id="vendor_image" class="dropify" />
                                                </div>
                                            </div> 
                                            <div class="col-md-6">

                                                 <?php 
                                        $image = $employees->image;
                                        $imagefullpath = base_url().$image;
                                        if(!empty($image)) {
                                        $returnpath = $this->config->item('customers_images_uploaded_path');

                                        if(!file_exists($imagefullpath)){
                                        $imagePath = "";
                                        $imagefullpath = $returnpath.$image;
                                        $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpath.'"><img src="';
                                        $imagePath .= $imagefullpath;
                                        $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                        </span>';
                                        echo $imagePath;

                                        ?>
                                        <br><span><a href="javascript:void(0);"  data-toggle="tooltip" class="imagedelete" data-placement="left" id="<?php echo $employees->id; ?>" title="Delete" ><i class="fa fa-trash"></i></a></span>
                                        <?php
                                        }
                                        }
                                        ?>

                                              
                                            </div>
                                        </div>

                                        
                                        <div class="previous-btn">
                                        <p><a class="btn btn-warning next">Next</a></p>
                                        </div> 

                                        <div class="submit-btn">
                                    <p style="text-align:right;"><button type="submit" class="btn btn-info" id="save_button" name="save_button" value="Update">Update</button>
                                            <input type="hidden" name="save_button" id="save_button" value="Update">

                                    <a href="<?php echo site_url("employees/employees_list"); ?>" class="btn btn-inverse">Cancel</a></p>
                                    </div>   
                                   
                                    </fieldset>

                                    <fieldset id="education_information" class="">
                                        <legend>Education Information</legend>
                                        <div class="row">
                                            <div class="col-md-12" >
                                                <div id="field">
                                                <?php 
                                                $totalempEdu = count($empEdu); 
                                                if(count($empEdu)>0) {
                                                   
                                                    foreach($empEdu as $e=>$v) { 
                                                        $index = $e+1; 
                                                        ?>
                                                        <div class="pull-right" style="padding:15px;margin-top:20px;">
                                                            <a id="remove_<?php echo $index; ?>" class="btn btn-danger" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                                                        </div>

                                                        <div id="field<?php echo $index; ?>" name="field<?php echo $index; ?>" class="education-section padding">
                                                            
                                                            <div class="row"><div class="col-md-3"><div class="mb-3"><input type="text" class="form-control" id="graduation" name="graduation[]" placeholder="Graduation/PG/Any Other Courses" value="<?php echo $v->graduation; ?>"></div></div><div class="col-md-3"><div class="mb-3"><input type="text" class="form-control" id="passing_year" name="passing_year[]" placeholder="Passing Year" value="<?php echo $v->passing_year; ?>"></div></div><div class="col-md-3"><div class="mb-3"><input type="text" class="form-control" id="board_university" name="board_university[]" placeholder="Board/University Name" value="<?php echo $v->board_university; ?>"></div></div><div class="col-md-3"><div class="mb-3"><input type="text" class="form-control" id="percentage" name="percentage[]" placeholder="Percentage" value="<?php echo $v->percentage; ?>"></div></div></div></div>
                                                    <?php }
                                                } ?>
                                                    
                                                <div id="field<?php echo $totalempEdu; ?>" name="field<?php echo $totalempEdu; ?>"></div>
                                                    
                                                </div><!--end field-->

                                                <div class="form-group">
                                                    <div class="col-md-4 col-md-offset-8">
                                                        <button id="add-more" name="add-more" class="btn btn-success" ><i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="previous-btn">
                                        <p><a id="previousEducation" class="btn btn-warning">Previous</a>&nbsp;&nbsp;<a class="btn btn-warning next">Next</a></p>
                                        </div>
                                       

                                        <div class="submit-btn">
                                            <p style="text-align:right;"><button type="submit" class="btn btn-info" id="save_button" name="save_button" value="Update">Update</button>
                                            <input type="hidden" name="save_button" id="save_button" value="Update">

                                            <a href="<?php echo site_url("employees/employees_list"); ?>" class="btn btn-inverse">Cancel</a></p>
                                        </div> 
                                    </fieldset>

                                    <fieldset id="company_information" class="">
                                        <legend>Company Information</legend>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div id="exprience">
                                                    <?php 
                                                    $totalempCompany = count($empCompany); 
                                                    if(count($empCompany)>0) {
                                                       
                                                        foreach($empCompany as $ec=>$c) { 
                                                            $cindex = $ec+1; 
                                                            ?>
                                                            <div class="pull-right" style="padding:15px;margin-top:20px;">
                                                                <a id="cremove_<?php echo $cindex; ?>" class="btn btn-danger" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                                                            </div>
                                                            <div class="company-section container-fluid" id="exprience<?php echo $ec+1; ?>" name="exprience<?php echo $ec+1; ?>"><div class="row"><div class="col-md-6"><div class="mb-3"><input type="text" class="form-control" id="company_name" name="company_name[]" placeholder="Company Name" required="required" value="<?php echo $c->company_name; ?>"></div></div><div class="col-md-6"><div class="mb-3"><input type="text" class="form-control" id="job_position" name="job_position[]" placeholder="Job Position" required="required" value="<?php echo $c->job_position; ?>"></div></div><div class="col-md-12"><div class="mb-3"><textarea cols="5" rows="5" class="form-control editor" id="job_role" name="job_role[]" placeholder="Job Role"><?php echo $c->job_role; ?></textarea></div></div></div><br></div>
                                                        <?php }
                                                    } ?>
                                                    <div id="exprience<?php echo $totalempCompany; ?>"></div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-4 col-md-offset-8">
                                                        <button id="exprience-add-more" name="exprience-add-more" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      
                                        <div class="previous-btn">
                                            <p><a id="previousCompany" class="btn btn-warning">Previous</a>&nbsp;&nbsp;<a class="btn btn-warning next">Next</a></p>
                                        </div>

                                        <div class="submit-btn">
                                            <p style="text-align:right;"><button type="submit" class="btn btn-info" id="save_button" name="save_button" value="Update">Update</button>
                                            <input type="hidden" name="save_button" id="save_button" value="Update">

                                            <a href="<?php echo site_url("employees/employees_list"); ?>" class="btn btn-inverse">Cancel</a></p>
                                        </div> 
                                    </fieldset>

                                    <fieldset id="project_information" class="">
                                        <legend>Project Information</legend>

                                        <div class="row">
                                            <div class="col-md-12" >
                                                <div id="project">

                                                    <?php 
                                                    $totalempProject = count($empProject); 
                                                    if(count($empProject)>0) {
                                                       
                                                        foreach($empProject as $ep=>$p) { 
                                                             $pindex = $ep+1; 
                                                            ?>
                                                            <div class="pull-right" style="padding:15px;margin-top:20px;">
                                                            <a id="premove_<?php echo $pindex; ?>" class="btn btn-danger" href="javascript:void(0)"><i class="fa fa-trash"></i></a></div>
                                                            <div class="project-section container-fluid" id="project<?php echo $ep+1; ?>" name="project<?php echo $ep+1; ?>"><div class="row">

                                                            <div class="col-md-12"><div class="mb-3"><input type="text" class="form-control" id="project_name" value="<?php echo $p->project_name; ?>" name="project_name[]" placeholder="project Name" required="required"></div></div><div class="col-md-12"><div class="mb-3"><textarea name="project_details[]" class="form-control editor" cols="5" rows="5" id="project_details" placeholder="Project Details" required="required"><?php echo $p->project_details; ?></textarea></div></div><div class="col-md-12"><div class="mb-3"><textarea name="skills_multiple[]" cols="5" rows="5" id="skills_multiple"placeholder="Project Skills" class="form-control"><?php echo $p->skills_multiple; ?></textarea></div></div><div class="col-md-12"><div class="mb-3"> <input type="text" class="form-control" id="project_url" name="project_url[]" value="<?php echo $p->project_url; ?>" placeholder="Website URL" ></div></div><div class="col-md-12"><div class="mb-3"><input type="text" class="form-control" id="project_url_android" name="project_url_android[]" value="<?php echo $p->project_url_android; ?>" placeholder="Android URL" ></div></div><div class="col-md-12"><div class="mb-3"><input type="text" class="form-control" id="project_url_ios" value="<?php echo $p->project_url_ios; ?>" name="project_url_ios[]" placeholder="iOS URL" ></div></div>

                                                        </div><br></div>
                                                        <?php }
                                                    } ?> 

                                                        <div id="project<?php echo $totalempProject; ?>"></div>
                                                </div><!--end field-->

                                                <div class="form-group">
                                                    <div class="col-md-4 col-md-offset-8">
                                                        <button id="project-add-more" name="project-add-more" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      
                                        <div class="previous-btn">
                                            <p><a id="previousProject" class="btn btn-warning">Previous</a>&nbsp;&nbsp;<a class="btn btn-warning next">Next</a></p>
                                        </div>

                                        <div class="submit-btn">
                                            <p style="text-align:right;"><button type="submit" class="btn btn-info" id="save_button" name="save_button" value="Update">Update</button>
                                            <input type="hidden" name="save_button" id="save_button" value="Update">

                                            <a href="<?php echo site_url("employees/employees_list"); ?>" class="btn btn-inverse">Cancel</a></p>
                                        </div> 
                                    </fieldset>

                                    <fieldset id="skill_information" class="">
                                        <legend>Skills and Other Information</legend>
                                        <?php 
                                        $tmpSkill = array();
                                        if(count($empSkill)>0) {
                                            foreach($empSkill as $es=>$evv) {
                                                $tmpSkill[] = $evv->technology_id;
                                            }
                                        } ?>
                                        <div class="form-group row">

                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="control-label">Primary Skills <span class="text-danger">*</span></label>
                                                    <select class="select2 form-control" name="primary_skills" id="primary_skills" required="required"  style="height: 36px;width: 100%;">
                                                    <option>--- Select Your Primary skills ---</option>
                                                    <?php foreach($technologies as $r=>$v) { ?>
                                                        <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" <?php if($v->id == $employees->primary_skills) { echo "selected=selected";  } ?> id="<?php echo $v->id; ?>"><?php echo $v->tname; ?></option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div> 

                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="control-label">Secondray Skills <span class="text-danger">*</span></label>
                                                    <select class="select2 form-control" name="skills[]" id="skills" multiple="multiple" required="required"  style="height: 36px;width: 100%;">
                                                    <option>--- Select Your skills ---</option>
                                                    <?php foreach($technologies as $r=>$v) { ?>
                                                        <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" id="<?php echo $v->id; ?>" <?php if(in_array($v->id,$tmpSkill)) { echo "selected=selected"; } ?>><?php echo $v->tname; ?></option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div> 
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="control-label">About Me</label>
                                                    <textarea name="about_me" id="about_me" class="form-control" rows="5" cols="5" rows="5" ><?php echo $employees->about_me; ?></textarea>
                                                </div>
                                            </div> 

                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Linked In <span class="text-danger"></span></label>
                                                    <input class="form-control" type="url"  id="linkedin" name="linkedin" value="<?php echo $employees->linkedin; ?>" placeholder="Linked In">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Current Designation <span class="text-danger"></span></label>
                                                    <input class="form-control" type="text"  id="designation" name="designation" value="<?php echo $employees->designation; ?>" placeholder="Current Designation">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Exp Year <span class="text-danger">*</span></label>
                                                    <select class="form-control" name="exp_year" id="exp_year">
                                                        <option>--- Select Year ---</option>
                                                    <?php for($i=0;$i<=10;$i++) { ?>
                                                        <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>" <?php if($employees->exp_year == $i) { echo "selected=selected"; } ?> ><?php echo $i; ?> Year</option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Exp Month <span class="text-danger">*</span></label>
                                                    <select class="form-control" name="exp_month" id="exp_month">
                                                   <option>--- Select Month ---</option>
                                                    <?php for($i=0;$i<=12;$i++) { ?>
                                                        <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>" <?php if($employees->exp_month == $i) { echo "selected=selected"; } ?> ><?php echo $i; ?> Month</option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="previous-btn">
                                        <p><a id="previousSkills" class="btn btn-warning">Previous</a></p>
                                        </div>

                                        <div class="submit-btn">
                                    <p style="text-align:right;"><button type="submit" class="btn btn-info" id="save_button" name="save_button" value="Update">Update</button>
                                            <input type="hidden" name="save_button" id="save_button" value="Update">

                                    <a href="<?php echo site_url("employees/employees_list"); ?>" class="btn btn-inverse">Cancel</a></p>
                                    </div> 
                                    </fieldset>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- vertical wizard -->

                         <!-- ============================================================== -->
                <!-- End PAge Content -->
  
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- footer -->
 <!-- ============================================================== -->

 <?php  $this->load->view("admin/common/common_footer"); ?>

 <!-- ============================================================== -->
 <!-- End footer -->
 <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->


<?php $this->load->view('general/model'); ?>
<?php $this->load->view('general/functions'); ?>

</body>

</html>


<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url(); ?>js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>js/waves.js"></script>
<!--Menu sidebar -->
<script src="<?php echo base_url(); ?>js/sidebarmenu.js"></script>

<script src="<?php echo base_url(); ?>js/jquery.reel.js"></script>
<!--stickey kit -->
<script src="<?php echo base_url(); ?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->
<script src="<?php echo base_url(); ?>js/custom.min.js"></script>

<!-- for image upload -->
<script src="<?php echo base_url(); ?>assets/plugins/dropify/dist/js/dropify.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/wizard/jquery.steps.min.js"></script>
 <script src="<?php echo base_url(); ?>assets/plugins/wizard/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/wizard/steps.js"></script>

<!-- This is data table -->
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
 <script src="<?php echo base_url(); ?>assets/plugins/js/dataTables.buttons.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
 <?php /* */ ?>


<script src="<?php echo base_url(); ?>assets/plugins/switchery/dist/switchery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>


<script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>js/additional-methods.min.js"></script>
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>
    CKEDITOR.replace('about_me');
</script>    
<script>
    $(document).ready( function() {
        $('#error').delay(5000).fadeOut();
        $('.dropify').dropify();
        $(".select2").select2();
    });

    setTimeout(function() { 
        $(".select2").select2();
    }, 1000);
</script>


<script src="<?php echo base_url(); ?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>


<script>
 $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>
<script>

        $(".imagedelete").on("click",function(e){
            e.preventDefault();
            var id =$(this).attr('id');
            if(id!="" && id!=0){
                $.ajax({
                    url: "<?php echo site_url("employees/deleteimages");?>",
                    type: "post",
                    data: {
                    'id': id
                    },
                    success: function (response) {
                        if(response==1){
                            $("#myElem").show();
                            setTimeout(function() { $("#myElem").hide(); }, 3000);
                            location.reload();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        });

        $(document).ready(function(){
            // Custom method to validate username
            $.validator.addMethod("usernameRegex", function(value, element) {
                return this.optional(element) || /^[a-zA-Z0-9]*$/i.test(value);
            }, "Username must contain only letters, numbers");

            $(".next").click(function(){
                var form = $("#form_edit");
                form.validate({
                    errorElement: 'label',
                    errorClass: 'error',
                    highlight: function(element, errorClass, validClass) {
                        $(element).closest('.form-group').addClass("error");
                    },
                    unhighlight: function(element, errorClass, validClass) {
                        $(element).closest('.form-group').removeClass("error");
                    },
                    rules: {
                        name:{
                            required:true,
                        },
                        vendor_id:{
                            required:true,
                            min:1,
                        },
                        address:{
                            required:true,
                        },
                        mobileno:{
                            required:false,
                            number:true,
                        },
                        alternateno:{
                            required:true,
                            number:true,
                        },
                        email: {
                            required:false,
                            email:true,
                        },
                        "skills[]":{
                            required:true,
                            // min:1,
                        },
                        about_me:{
                            required:true,
                        },
                        linkedin:{
                            url:true,
                        },
                        primary_skills:{
                            required:true,
                            min:1,
                        },
                        designation:{
                            required:true,
                        },
                        exp_year:{
                            required:true,
                            min:0,
                        },
                        exp_month:{
                            required:true,
                            min:0,
                        }
                    },
                    messages: {
                        name:{
                            required:"Please enter Name",
                        },
                        vendor_id:{
                            required:"Please select vendor",
                            min:"Please select vendor",
                        },
                        address:{
                            required:"Please enter addresss",
                        },
                        mobileno:{
                            required:"Please enter mobile no",
                        },
                        alternateno:{
                            required:"Please enter mobile no",
                        },
                        email: {
                            required:"Please enter email",
                            email:"Please enter valid email",
                        },
                        "skills[]":{
                            required:"Please select your skills",
                            min:"Please select your skills",
                        },
                        primary_skills:{
                            required:"Please select primary skill",
                            min:"Please select primary skill",
                        },
                        about_me:{
                            required:"Please enter your about us",
                        },
                        linkedin:{
                            url:"Please enter linkedin URL",
                        },
                        designation:{
                            required:"Please enter designation",
                        },
                        exp_year:{
                            required:"Please select your total exprience in year",
                            min:"Please select your total exprience in year",
                        },
                        exp_month:{
                            required:"Please select your total exprience in month",
                            min:"Please select your total exprience in month",
                        }

                    }
                });
                if (form.valid() === true){
                    if ($('#personal_information').is(":visible")){
                        current_fs = $('#personal_information');
                        next_fs = $('#education_information');
                    } else if($('#education_information').is(":visible")){
                        current_fs = $('#education_information');
                        next_fs = $('#company_information');
                    } else if($('#company_information').is(":visible")){
                        current_fs = $('#company_information');
                        next_fs = $('#project_information');
                    } else if($('#project_information').is(":visible")){
                        current_fs = $('#project_information');
                        next_fs = $('#skill_information');
                    }
                    next_fs.show(); 
                    current_fs.hide();
                }
            });

            $('#previousEducation').click(function(e){
                e.preventDefault();
                current_fs = $('#education_information');
                next_fs = $('#personal_information');
                next_fs.show(); 
                current_fs.hide();
            });

            $('#previousCompany').click(function(e){
                e.preventDefault();
                current_fs = $('#company_information');
                next_fs = $('#education_information');
                next_fs.show(); 
                current_fs.hide();
            });

            $('#previousProject').click(function(e){
                e.preventDefault();
                current_fs = $('#project_information');
                next_fs = $('#company_information');
                next_fs.show(); 
                current_fs.hide();
            });

            $('#previousProject').click(function(e){
                e.preventDefault();
                current_fs = $('#project_information');
                next_fs = $('#company_information');
                next_fs.show(); 
                current_fs.hide();
            });

            $('#previousSkills').click(function(e){
                e.preventDefault();
                current_fs = $('#skill_information');
                next_fs = $('#project_information');
                next_fs.show(); 
                current_fs.hide();
            });
            
         
            //@naresh action dynamic childs
            var nextedu = <?php echo $totalempEdu; ?>;
            $("#add-more").click(function(g){

                g.preventDefault();
                var addto = "#field" + nextedu;
                var addRemove = "#field" + (nextedu);
                nextedu = nextedu + 1;
              
                var newIn = '<div id="field'+ nextedu +'" name="field'+ nextedu +'" class="education-section padding"><div class="row"><div class="col-md-3"><div class="mb-3"><input type="text" class="form-control" id="graduation" name="graduation[]" placeholder="Graduation/PG/Any Other Courses"></div></div><div class="col-md-3"><div class="mb-3"><input type="text" class="form-control" id="passing_year" name="passing_year[]" placeholder="Passing Year"></div></div><div class="col-md-3"><div class="mb-3"><input type="text" class="form-control" id="board_university" name="board_university[]" placeholder="Board/University Name"></div></div><div class="col-md-3"><div class="mb-3"><input type="text" class="form-control" id="percentage" name="percentage[]" placeholder="Percentage"></div></div></div></div>';
               
                var newInput = $(newIn);
            
                var removeBtn = '<div class="pull-right" style="padding:15px;margin-top:20px;"><a id="remove_' + (nextedu) + '" class="btn btn-danger remove-me" ><i class="fa fa-trash"></i></a></div> </div></div><div id="field">';
                var removeButton = $(removeBtn);
                $(addto).after(newInput);
                $(addRemove).after(removeButton);
                $("#field" + nextedu).attr('data-source',$(addto).attr('data-source'));
                $("#count").val(nextedu);  
                
                    $('.remove-me').click(function(e){
                        e.preventDefault();
                        var fieldNum = this.id.charAt(this.id.length-1);
                        var fieldID = "#field" + fieldNum;
                        $(this).remove();
                        $(fieldID).remove();
                    });
            }); 

            <?php 
            if(count($empEdu)>0) {
            foreach($empEdu as $e=>$v) { ?>
                $("#remove_<?php echo $e+1; ?>").click(function(e){
                    e.preventDefault();
                    $("#field<?php echo $e+1; ?>").remove();
                    $("#remove_<?php echo $e+1; ?>").remove();
                });
            <?php } } ?>

            var nextexp = <?php echo $totalempCompany; ?>;
            $("#exprience-add-more").click(function(e){

                e.preventDefault();
                var addto = "#exprience" + nextexp;
                var addRemove = "#exprience" + (nextexp);
                nextexp = nextexp + 1;
                var newInExp = '<div class="company-section container-fluid" id="exprience'+ nextexp +'" name="exprience'+ nextexp +'" class="company-section padding"><div class="row"><div class="col-md-6"><div class="mb-3"><input type="text" class="form-control" id="company_name" name="company_name[]" placeholder="Company Name" required="required"></div></div><div class="col-md-6"><div class="mb-3"><input type="text" class="form-control" id="job_position" name="job_position[]" placeholder="Job Position" required="required"></div></div><div class="col-md-12"><div class="mb-3"><textarea class="form-control editor" id="job_role" name="job_role[]" placeholder="Job Role" cols="5" rows="5"></textarea></div></div></div><br></div>';
               
                var newInputExp = $(newInExp);
              
                var removeBtn = '<div class="pull-right" style="padding:15px;margin-top:20px;"><a id="cremove_' + (nextexp) + '" class="btn btn-danger cremove-me" ><i class="fa fa-trash"></i></a></div></div></div><div id="field">';
                var removeButton = $(removeBtn);
                $(addto).after(newInputExp);
                $(addRemove).after(removeButton);
                $("#exprience" + nextexp).attr('data-source',$(addto).attr('data-source'));
                $("#count").val(nextexp);  

                $('.cremove-me').click(function(e){
                    e.preventDefault();
                    var fieldNum = this.id.charAt(this.id.length-1);
                    var fieldID = "#exprience" + fieldNum;
                    $(this).remove();
                    $(fieldID).remove();
                });
            });

            <?php 
            
            if(count($empCompany)>0) {
            foreach($empCompany as $ec=>$c) { ?>
                $("#cremove_<?php echo $ec+1; ?>").click(function(e){
                    e.preventDefault();
                    $("#exprience<?php echo $ec+1; ?>").remove();
                    $("#cremove_<?php echo $ec+1; ?>").remove();
                });
            <?php } } ?>


            var nextproject = <?php echo $totalempProject; ?>;
            $("#project-add-more").click(function(e){

                e.preventDefault();
                var addto = "#project" + nextproject;
                var addRemove = "#project" + (nextproject);
                nextproject = nextproject + 1;
                var newInExp = ' <div class="project-section container-fluid" id="project'+ nextproject +'" name="project'+ nextproject +'"><div class="row"><div class="col-md-12"><div class="mb-3"><input type="text" class="form-control" id="project_name" name="project_name[]" placeholder="project Name" required="required"></div></div><div class="col-md-12"><div class="mb-3"><textarea name="project_details[]" class="form-control editor" id="project_details" cols="5" rows="5" placeholder="Project Details" required="required"></textarea></div></div><div class="col-md-12"><div class="mb-3"><textarea name="skills_multiple[]" id="skills_multiple"placeholder="Project Skills" class="form-control"  cols="5" rows="5"></textarea></div></div><div class="col-md-12"><div class="mb-3"> <input type="text" class="form-control" id="project_url" name="project_url[]" placeholder="Website URL" ></div></div><div class="col-md-12"><div class="mb-3"><input type="text" class="form-control" id="project_url_android" name="project_url_android[]" placeholder="Android URL" ></div></div><div class="col-md-12"><div class="mb-3"><input type="text" class="form-control" id="project_url_ios" name="project_url_ios[]" placeholder="IOS URL" ></div></div></div><br></div>';
               
                var newInputExp = $(newInExp);
            
                var removeBtn = '<div class="pull-right" style="padding:15px;margin-top:20px;"><a id="premove_' + (nextproject) + '" class="btn btn-danger premove-me" ><i class="fa fa-trash"></i></a></div> </div></div><div id="field">';
                var removeButton = $(removeBtn);
                $(addto).after(newInputExp);
                $(addRemove).after(removeButton);
                $("#project" + nextproject).attr('data-source',$(addto).attr('data-source'));
                $("#count").val(nextproject);  
                
                    $('.premove-me').click(function(e){
                        e.preventDefault();
                        var fieldNum = this.id.charAt(this.id.length-1);
                        var fieldID = "#project" + fieldNum;
                        $(this).remove();
                        $(fieldID).remove();
                    });
            });


            <?php 
            
            if(count($empProject)>0) {
            foreach($empProject as $ep=>$p) { ?>
                $("#premove_<?php echo $ep+1; ?>").click(function(e){
                    e.preventDefault();
                    $("#project<?php echo $ep+1; ?>").remove();
                    $("#premove_<?php echo $ep+1; ?>").remove();
                });
            <?php } } ?>

        });
</script>
