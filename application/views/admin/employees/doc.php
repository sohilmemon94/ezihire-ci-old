<html>
<head>
<style type="text/css">
    section{margin-left:15px !important;margin-right:15px !important;}
    body{background:#ffffff;font-family:'Open Sans', sans-serif;font-size:14px;font-weight:normal;margin:15px;} 
    .about_me{text-align:left !important;padding:25px !important;}
    section.edu-info {margin:40px 0;}
    section.projects{padding:40px 0 0 0;}
    section.projects .message-item {margin:20px 0 0 0;border:1px solid black !important;padding:0 20px 10px !important;}
    section.projects .message-item h5{font-size:20px !important;margin:10px 0 !important;}
    section.projects .message-item p{line-height:20px !important;}
    section.experience-info{padding:40px 0 0 0;}
    section.experience-info .message-item {margin:20px 0 0 0;border:1px solid black !important;padding:0 20px 10px !important;}
    section.experience-info .message-item h5{font-size:20px !important;margin:10px 0 !important;}
    section.experience-info .message-item p{line-height:20px !important;}
    h2{font-size:26px;color:#232323;font-weight:normal;margin:0px 0 10px 0;}
    h6{font-size:16px;color:#000;font-family:'Open Sans', sans-serif;font-weight:400;margin:0;line-height:28px;}
    .employee-info{margin:40px 0 0 0;}
    .employee-info h2{margin:0 0 0 0;}
    .employee-info ul {padding:0;}
    .employee-info ul li {margin:10px 0;}
    .row.employee-info h3 { font-weight:normal; margin:10px 0;line-height:20px;}
    .edu-info th,td {width:auto;border-top:1px solid black !important;border-right:1px solid black !important;}
    .edu-info td:nth-child(odd) {border-bottom:1px solid black !important;}
    .edu-info td:nth-child(even) {border-bottom:1px solid black !important;}
    .edu-info th, td {text-align:center;padding:8px;font-weight:normal;}
    .edu-info td:nth-child(1) {border-left:1px solid black;}
    .edu-info th:nth-child(1) {border-left:1px solid black;}
    #message-item{border:2px solid black !important;padding:10px !important;}
    p#border-bottom{border-bottom:1px solid black !important;}
    p#line-height{line-height:22px !important;}
</style>   
</head>
<body>
<div class="content">
<section class="top-head">   
    <div class="container" >
        <div class="logo"><center><img src="<?php echo base_url(); ?>uploads/logo.png" style="text-align:center !important; margin:0 auto !important;" alt="logo" /></center></div>
        <p style="text-align:center !important; margin:0 auto !important;"><b><center><?php echo $employees->name; ?></center></b></p>
            <center>
                <b><?php echo $employees->designation; ?>
                <?php if($employees->exp_year > 0) { ?> - <?php echo $employees->exp_year; ?> Years  <?php } ?> <?php if($employees->exp_month > 0) { ?> - <?php echo $employees->exp_month; ?> Months <?php } ?></b></center>
        
        <div class="row">
            <div class="col-md-12">
            <h2>About Employee</h2>
                <div class="about_me">
                    <?php echo $employees->about_me; ?>
                </div>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->  

         <div class="row">
            <div class="col-md-12">
            <h2>Skills</h2>
                    <?php echo $skills_list; ?>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->  

    </div> 
</section>
<br>
<?php if(count($empEdu)>0) { ?>
<section class="edu-info">        
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Education Information</h2>
                <div class="">
                    <table  width="100%" >
                        <thead>
                            <tr>
                                <th scope="col" style="width:25% !important;">Graduation/PG/Any Other Courses</th>
                                <th scope="col" style="width:25% !important;">Passing Year</th>
                                <th scope="col" style="width:25% !important;">Board/University Name</th>
                                <th scope="col" style="width:25% !important;">Percentage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $rowno = 1;
                                foreach($empEdu as $k=> $v){ ?>
                                    <tr>
                                        <td scope="col"><?php echo $v->graduation; ?></td>
                                        <td scope="col"><?php echo $v->passing_year; ?></td>
                                        <td scope="col"><?php echo $v->board_university; ?></td>
                                        <td scope="col"><?php echo $v->percentage; ?></td>
                                    </tr>
                                <?php 
                                      $rowno++;
                                } ?> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>
<br>
<?php if(count($empCompany)>0) { ?>
<section id="experience" class="experience-info">
    <div class="container">
        <h2>Experience</h2>
        <h6></h6>
        <div class="qa-message-list" id="wallmessages">
            <?php foreach($empCompany as $ec=>$c) {  ?>
                <div class="message-item" id="message-item">
                        <p id="border-bottom"><font size="3"><b><?php echo $c->company_name; ?></b></font></p>
                        <p>Working as a <?php echo $c->job_position; ?></p>
                        <p id="line-height"><?php echo $c->job_role; ?></p>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
<br>     
<?php if(count($empProject)>0) { ?> 
<section id="experience" class="projects">
    <div class="container">
        <h2>Projects</h2>
        <h6></h6>
        <div class="qa-message-list" id="wallmessages">
            <?php foreach($empProject as $ep=>$p) {   ?>
                    <div class="message-item" id="message-item">
                        <p id="border-bottom"><font size="3"><b><?php echo $p->project_name; ?></b></font></p>
                        <p id="line-height"><?php echo $p->project_details; ?></p>
                        <?php if(isset($p->skills_multiple) && !empty($p->skills_multiple)){ ?>
                                <p><b>Project Skills : </b><?php echo $p->skills_multiple; ?></p>
                       <?php } ?>
                        <?php if(isset($p->project_url) && !empty($p->project_url)){ ?>
                        <p><a href="<?php echo $p->project_url; ?>" target="_blank">View Web Project</a></p>
                       <?php } ?>
                       <?php if(isset($p->project_url_android) && !empty($p->project_url_android)){ ?>
                        <p><a href="<?php echo $p->project_url_android; ?>" target="_blank">View Android Project</a></p>
                       <?php } ?>
                       <?php if(isset($p->project_url_ios) && !empty($p->project_url_ios)){ ?>
                        <p><a href="<?php echo $p->project_url_ios; ?>" target="_blank">View ios Project</a></p>
                       <?php } ?>
                    </div>
             <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
</div>
</body>
</html>