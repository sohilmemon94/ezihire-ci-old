<?php
defined('BASEPATH') OR exit('No direct script access allowed');  
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>images/favicon.ico">
    <title>Infiraise - Employee</title>
    <style>
        section{margin-left:15px !important;margin-right:15px !important;}
        body{
            border:5px solid black;padding-left:10px !important;padding-right:10px !important;margin-left:100px !important;margin-right:100px !important;background: #ffffff;font-family: 'Open Sans', sans-serif;font-size: 14px;font-weight: normal;margin: 15px;
        } 
        .about_me{text-align:left !important;padding:25px !important;}
        section.edu-info {margin: 70px 0;}
        section.experience-info {padding: 30px 0;}
        section.projects {padding: 0 0 30px 0;}
        section.projects .message-item {margin: 40px 0 0 0;}
        .experience h2{margin:10px 0 0 0;}
        .top-head img{text-align:center !important;}
        .top-head p{text-align:center !important;line-height: 24px;margin: 0;color: #000;}
        p{margin: 0!important;}
        h2{font-size: 26px;color: #232323;font-weight: normal;margin: 0px 0 10px 0;}
        h6{font-size: 16px;color: #000;font-family: 'Open Sans', sans-serif;font-weight: 400;margin: 0;line-height: 28px;}
        .employee-info{margin: 40px 0 0 0;}
        .employee-info h2{margin: 0 0 0 0;}
        .employee-info ul {padding: 0;}
        .employee-info ul li {margin: 10px 0;}
        .message-item:before {content: ""; background: #8a8a8a;border-radius: 2px;bottom: -30px;height: 100%;left: -30px;position: absolute;width: 1px;}
        .message-item:after {content: "";background: #000000;border: 2px solid #000000;border-radius: 50%;box-shadow: 0 0 5px rgb(0 0 0 / 10%);height: 15px;left: -36px;position: absolute;top: 10px;width: 15px;}
        .message-item {margin: 24px 0 0 0;position: relative;}
        .message-item .message-inner:before {border-right: 10px solid rgb(0, 0, 0);border-style: solid;border-width: 10px;color: rgba(0,0,0,0);content: "";display: block;height: 0;position: absolute;left: -21px;top: 6px;width: 0;}
        .message-inner{border: 1px solid #797979;border-radius: 3px;padding: 10px;position: relative;}
        .message-item:after {content: "";color: black;border: 2px solid #000000;border-radius: 50%;box-shadow: 0 0 5px rgb(0 0 0 / 10%);height: 15px;left: -39px;position: absolute;top: 10px;width: 15px;}
        .message-item .message-head {border-bottom: 1px solid #656565;margin-bottom: 8px;padding-bottom: 8px;color: #fff;}
        .clearfix:before, .clearfix:after {content: " ";display: table;}
        .clearfix:before, .clearfix:after {content: '\0020';display: block;overflow: hidden;visibility: hidden;width: 0;height: 0;}
        .message-item .message-head .user-detail h5 {font-size: 16px;font-weight: bold;margin: 0;color: #000;}
        .message-item .message-head .post-meta {float: left;padding: 0 15px 0 0;}
        .message-item .message-head .post-meta >div {color: black;font-weight: bold;text-align: right;}
        .post-meta > div {color: black;font-size: 12px;line-height: 22px;}
        .qa-message-content p {line-height: 24px;margin: 0;color: #000;}
        .user-detail{overflow: hidden;}
        .edu-info table {
  border-collapse: collapse;
  width: 100%;
  border: 1px solid black;
}

.edu-info th, td {
  text-align: center;
  padding: 8px;
  border-left: 1px solid black;
  border-top: 1px solid black;
}

tbody tr:nth-child(odd) {background-color: #f2f2f2;}
span.skills {background-color: black !important;padding:10px !important;color:#fff !important;border-radius: 7px !important;margin-right:5px;display:block !important;float:left !important;}
@media screen and (max-width:767px)
{
    .employee-info{margin: 40px 0 0 0;}
}

@media screen and (max-width:576px)
{
    p{font-size: 8px;}
}
   </style>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="content">
<section class="top-head">   
    <div class="container" >
        
        <p><img src="<?php echo base_url(); ?>uploads/logo.png" alt="logo"></img></p>
        <p><b><?php echo $employees->name; ?></b></p>
        <p><b><?php echo $employees->designation; ?></b><?php if($employees->exp_year > 0) { ?> - <b><?php echo $employees->exp_year; ?> Years </b> <?php } ?> <?php if($employees->exp_month > 0) { ?> - <b><?php echo $employees->exp_month; ?> Months</b>  <?php } ?></p>
            
        <div class="row">
            <div class="col-md-12">
            <h2>About Employee</h2>
                <div class="about_me">
                    <?php echo $employees->about_me; ?>
                </div>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->  


        <?php if(count($skill_arr)>0) { ?>
        <div class="row">
            <div class="col-md-12">
            <h2>Skills</h2>
            <?php foreach($skill_arr as $s=>$sk){ ?>
                <span class="skills"><?php echo $sk; ?></span>
            <?php } ?>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->  
        <?php } ?>
    </div> 
</section>
<?php if(count($empEdu)>0) { ?>
<section class="edu-info">        
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Education Information</h2>
                <div class="table-responsive m-t-40">
                    <table id="example23" class="table table-hover table-striped table-bordered" cellspacing="0" width="auto">
                        <thead>
                            <tr>
                                <th scope="col" style="width:25% !important;">Graduation/PG/Any Other Courses</th>
                                <th scope="col" style="width:25% !important;">Passing Year</th>
                                <th scope="col" style="width:25% !important;">Board/University Name</th>
                                <th scope="col" style="width:25% !important;">Percentage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $rowno = 1;
                                foreach($empEdu as $k=> $v){ ?>
                                    <tr>
                                    
                                        <td scope="col"><?php echo $v->graduation; ?></td>
                                        <td scope="col"><?php echo $v->passing_year; ?></td>
                                        <td scope="col"><?php echo $v->board_university; ?></td>
                                        <td scope="col"><?php echo $v->percentage; ?></td>
                                      </tr>
                                <?php 
                                      $rowno++;
                                  } ?> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<?php if(count($empCompany)>0) { ?>
<section id="experience" class="experience-info">
    <div class="contaisner">
        <h2>Experience</h2>
        <h6></h6>
        <div class="qa-message-list" id="wallmessages">
            <?php foreach($empCompany as $ec=>$c) {  ?>
                <div class="message-item" id="m16">
                    <div class="message-inner">
                        <div class="message-head clearfix"> 
                            <div class="user-detail">
                                <h5 class="handle"><?php echo $c->company_name; ?> </h5>
                                <div class="post-meta">
                                    <div class="asker-meta">
                                        <span class="qa-message-what"></span>
                                        <span class="qa-message-when">
                                            <span class="qa-message-when-data"></span>
                                        </span> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="qa-message-content">
                            <p>Working as a <?php echo $c->job_position; ?></p>
                            <p><?php echo $c->job_role; ?></p>
                        </div>
                </div></div>
            <?php } ?>
        </div>
    </div>
</section>
<?php } ?>
         
<?php if(count($empProject)>0) { ?> 
<section id="experience" class="projects">
    <div class="contdainer">
        <h2>Projects</h2>
        <h6></h6>
        <div class="qa-message-list" id="wallmessages">

            <?php foreach($empProject as $ep=>$p) {   ?>
            <div class="message-item" id="m16">
                <div class="message-inner">
                    <div class="message-head clearfix"> 
                        <div class="user-detail">
                            <h5 class="handle"><b><?php echo $p->project_name; ?></b></h5>
                        </div>
                    </div>
                    <div class="qa-message-content">
                        <p><?php echo $p->project_details; ?></p>
                        <?php if(isset($p->skills_multiple) && !empty($p->skills_multiple)){ ?>
                            <p><b>Project Skills : </b><?php echo $p->skills_multiple; ?></p>
                        <?php } ?>
                        <?php if(isset($p->project_url) && !empty($p->project_url)){ ?>
                        <p><a href="<?php echo $p->project_url; ?>" target="_blank">View Web Project</a></p>
                       <?php } ?>
                       <?php if(isset($p->project_url_android) && !empty($p->project_url_android)){ ?>
                        <p><a href="<?php echo $p->project_url_android; ?>" target="_blank">View Android Project</a></p>
                       <?php } ?>
                       <?php if(isset($p->project_url_ios) && !empty($p->project_url_ios)){ ?>
                        <p><a href="<?php echo $p->project_url_ios; ?>" target="_blank">View ios Project</a></p>
                       <?php } ?>
                    </div>
            </div></div>
            <?php } ?>
        </div>
    </div>
</section>
</div>
<?php } ?>
</body>
</html>
