<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title>Admin - <?php echo $page; ?> <?php echo $action; ?></title>
        <?php $this->load->view("customers/common/common_css"); ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <style type="text/css">
            .input-controls {
                margin-top: 10px;
                border: 1px solid transparent;
                border-radius: 2px 0 0 2px;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
                height: 32px;
                outline: none;
                box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            }
            #searchInput {
                background-color: #fff;
                font-family: Roboto;
                font-size: 15px;
                font-weight: 300;
                margin-left: 12px;
                padding: 0 11px 0 13px;
                text-overflow: ellipsis;
                width: 50%;
            }
            #searchInput:focus {
                border-color: #4d90fe;
            }
        </style>

    </head>
    <body class="fix-header fix-sidebar card-no-border">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("customers/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("customers/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    <div class="row page-titles">
                        <div class="col-md-6 col-12 align-self-center">
                            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>customers/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>customers/unapprove_customers_list"><?php echo $page; ?></a></li>
                                <li class="breadcrumb-item active"><?php echo $action; ?></li>
                            </ol>
                        </div>
                        <!--
                        <div class="col-md-6 col-4 align-self-center">
                            <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
                            <button class="btn pull-right hidden-sm-down btn-success"><i class="mdi mdi-plus-circle"></i> Create</button>
                            <div class="dropdown pull-right m-r-10 hidden-sm-down">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    January 2017
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="index5.html#">February 2017</a>
                                    <a class="dropdown-item" href="index5.html#">March 2017</a>
                                    <a class="dropdown-item" href="index5.html#">April 2017</a>
                                </div>
                            </div>
                        </div>
                        -->
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">


                                    <h6 class="card-subtitle"></h6>
                                    <div class="table-responsive m-t-40">
                                        <h3>Customer Details</h3>
                                         <form role="form" name="form"  id="demo-form2" action="" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                        <table class="table table-hover table-bordered detail-view">

                                            <tbody>
                                                <tr>
                                                    <td>Profile</td>
                                                    <td>
                                                        <?php if (!empty($customers->image)) { 

                                                            $image = $customers->image;
                                                            $imagefullpath = base_url().$image;
                                                            $returnpath = $this->config->item('customers_images_uploaded_path');
                                                            $imagefullpath = $returnpath.$image;

                                                            ?>
                                                            <span class="avatar">
                                                                <a class="single_image" href="<?php echo $imagefullpath; ?>">
                                                                    <span class="round">
                                                                        <img src="<?php echo $imagefullpath; ?>"   style="height: 50px;width: 50px;">
                                                                    </span>
                                                                </a>
                                                            </span>

                                                        <?php } else { 
                                                $random_array = array("round-success","round-primary","round-danger","round-warning");
                                                $random_keys=array_rand($random_array,1);
                                                $backColor =  $random_array[$random_keys];
                                                            ?>
                                                            <span class="round <?php echo $backColor; ?>">
                                            <?php  
                                            $firstCharacter = substr($customers->username , 0, 1);
                                            echo trim(strtoupper($firstCharacter));
                                            ?>
                                            </span>
                                                        <?php } ?>
                                                    </td>

                                                </tr>

                                               
                                                <tr>
                                                    <td> Username</td>
                                                    <td><?php echo $customers->username; ?></td>
                                                </tr>

                                               

                                                <tr>
                                                    <td>Email</td>
                                                    <td><?php echo $customers->email; ?></td>
                                                </tr>

                                                <tr>
                                                    <td>Is Social Account ( Account Signup By )</td>
                                                    <td><?php 
                                                        if($customers->is_social == "1"){
                                                            echo "Yes";

                                                            if(isset($customers->google_id) && !empty($customers->google_id)) {
                                                                echo " ( Google )";
                                                            } 

                                                           if(isset($customers->fb_id) && !empty($customers->fb_id)) {
                                                                echo " ( Facebook )";
                                                            }                                                            

                                                        } else {
                                                            echo "No";
                                                        } ?></td>
                                                </tr>

                                               
                                               

                                               <?php if($customers->is_approve == "0") { ?>
                                                <tr>
                                                    <td>Is Approve</td>
                                                    <td><input class='tgl tgl-ios tgl_checkbox js-switch' name="is_approve" type='checkbox' <?php echo ($customers->is_approve == '1') ? "checked" : ""; ?> "/></td>
                                                </tr>
                                                <?php } ?>
                                                <?php if($customers->status == "0") { ?>
                                                  <tr>
                                                    <td>Status</td>
                                                    <td><input class='tgl tgl-ios tgl_checkbox js-switch' name="status" type='checkbox' checked="checked" /></td>
                                                </tr>
                                                <?php } ?>
                                                


                                                <tr>
                                                    <td>Registered Date</td>
                                                    <td>  <?php
                                                        $date = date("d-M-Y", strtotime($customers->created_at));
                                                        $time = date("h:i:s", strtotime($customers->created_at));
                                                        echo $date . " " . $time;
                                                        ?></td>
                                                </tr>

                                                <tr>
                                                    <th scope="row">
                                                    </th>
                                                    <td>
                                                    <?php if($customers->is_approve == "0" && $customers->status == "0") { ?>
                                                    <button type="submit" class="btn btn-info">Approve</button>
                                                    <?php } ?>
                                                    <a href="<?php echo site_url("customers/unapprove_customers_list"); ?>" class="btn btn-primary">Back</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
 </form>
                                        

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <?php /* if (count($block_user) > 0) { ?>



                        <div class="row">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body">

                                        <h6 class="card-subtitle"></h6>
                                        <div class="table-responsive m-t-40">
                                            <h3>Block Student Details</h3>
                                            <table class="table table-hover table-bordered detail-view">


                                                <tbody>
                                                    <tr>
                                                        <td></td>
                                                        <td>


                                                            <?php
                                                            foreach ($block_user as $k => $v) {
                                                                ?>

                                                                <?php if (!empty($v->profile_pic)) { ?>
                                                                    <span class="round" style="margin-right: 5px;">
                                                                        <img src="<?php echo base_url(); ?><?php echo $v->profile_pic; ?>"   style="height: 50px;width: 50px;" title="<?php echo $v->name; ?>">

                                                                    </span>
                                                                <?php } else { ?>
                                                                    <span class="round">
                                                                        <img src="<?php echo base_url(); ?><?php echo "uploads/images/profilepic/default.png"; ?>" title="<?php echo $v->name; ?>"  style="height: 50px;width: 50px;">
                                                                    </span>
                                                                <?php } ?>


                                                            <?php } ?>

                                                        </td>


                                                    </tr>


                                                </tbody>
                                            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php } */ ?>

                    <!-- Row -->
                    <!-- ============================================================== -->
                    <!-- End PAge Content -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Right sidebar -->
                    <!-- ============================================================== -->
                    <!-- .right-sidebar -->

                    <!-- ============================================================== -->
                    <!-- End Right sidebar -->
                    <!-- ============================================================== -->
                </div>
                <!-- ============================================================== -->
                <!-- End Container fluid  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- footer -->
                <!-- ============================================================== -->

                <?php $this->load->view("customers/common/common_footer"); ?>

                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <?php $this->load->view("customers/common/common_js"); ?>
    </body>

</html>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIPTodmce4KgsMipR3f25tDbha3rz5sbE&libraries=places"></script>
<script type="text/javascript">
    $(function () {
        $("a.single_image").fancybox();
    });
</script>
<script>
    $(document).ready(function () {
        $('#error').delay(3000).fadeOut();
    });

    $(function () {

        $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
        $("body").on("change", ".tgl_checkbox", function () {
            var table = $(this).data("table");
            var status = $(this).data("status");
            var id = $(this).data("id");
            var id_field = $(this).data("idfield");
            var bin = 'N';

            if ($(this).is(':checked')) {
                bin = 'Y';
            }

            /*alert(table+'---'+status+'---'+id+'---'+id_field+'---'+bin);
             return false;  */
            $.ajax({
                method: "POST",
                url: "<?php echo site_url("customers/change_status"); ?>",
                data: {table: table, status: status, id: id, id_field: id_field, on_off: bin}
            })
                    .done(function (msg) {
                        //  alert(msg);
                        if (msg == 'Y') {
                            $("#myElem").show();
                            setTimeout(function () {
                                $("#myElem").hide();
                            }, 3000);

                        } else if (msg == 'N') {
                            $("#myElemNo").show();
                            setTimeout(function () {
                                $("#myElemNo").hide();
                            }, 3000);
                        }
                    });
        });
    });
</script>

<script type="text/javascript">
    $(function () {
        $("a.single_image").fancybox();
    });
</script>



<script type="text/javascript">

    var lat = document.getElementById('lat').value;
    var lng = document.getElementById('lng').value;


   



    function initialize() {
        var latlng = new google.maps.LatLng(lat, lng);
        var map = new google.maps.Map(document.getElementById('map'), {
            center: latlng,
            zoom: 13
        });
        var marker = new google.maps.Marker({
            map: map,
            position: latlng,
            draggable: false,
            anchorPoint: new google.maps.Point(0, -29)
        });
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'click', function () {
            var iwContent = '<div id="iw_container">' +
                    '<div class="iw_title"><b>Location</b> : </div></div>';
            // including content to the infowindow
            infowindow.setContent(iwContent);
            // opening the infowindow in the current map and at the current marker location
            infowindow.open(map, marker);
        });


        var latlng1 = new google.maps.LatLng(unilat, unilng);
        var unimap = new google.maps.Map(document.getElementById('unimap'), {
            center: latlng1,
            zoom: 13
        });
        var marker1 = new google.maps.Marker({
            map: unimap,
            position: latlng1,
            draggable: false,
            anchorPoint: new google.maps.Point(0, -29)
        });
        var infowindow1 = new google.maps.InfoWindow();
        google.maps.event.addListener(marker1, 'click', function () {
            var iwContent1 = '<div id="iw_container">' +
                    '<div class="iw_title"><b>Location</b> : </div></div>';
            // including content to the infowindow
            infowindow1.setContent(iwContent1);
            // opening the infowindow in the current map and at the current marker location
            infowindow1.open(unimap, marker1);
        });


    }
    google.maps.event.addDomListener(window, 'load', initialize);

</script>
<script type="text/javascript">
    $(function () {
        $("a.single_image").fancybox();
    });
</script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIPTodmce4KgsMipR3f25tDbha3rz5sbE&libraries=places"></script>


<script>

    $(document).ready(function () {
        $('#example23').DataTable({
            //   dom: 'Bfrtip',
            //   buttons: [
            //     'copy', 'csv', 'excel', 'pdf', 'print'
            // ]

        });
        
    });
    // $('#example23').DataTable({
    //     dom: 'Bfrtip',
    //     buttons: [
    //         'copy', 'csv', 'excel', 'pdf', 'print'
    //     ]
    // });
</script>




