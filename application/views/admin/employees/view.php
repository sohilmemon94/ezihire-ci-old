<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo $title; ?> | Admin - <?php echo $page; ?> <?php echo $action; ?></title>
        <?php $this->load->view("admin/common/common_css"); ?>
      
    </head>

  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}

input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}

</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
       <?php $this->load->view("admin/common/common_header"); ?>

                    
                            <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
         <?php $this->load->view("admin/common/common_sidebar"); ?>




        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>customers/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>employees/employees_list"><?php echo $page; ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $action; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-md-12 col-lg-4 col-xlg-3">
                        <div class="card">
                            <div class="card-body">
                                <center class="mt-4"> 

                                    <?php 
                                                $image = $employees->image;
                                                $imagefullpath = base_url().$image;
                                                $returnpath = $this->config->item('customers_images_uploaded_path');
                                                if(!empty($image)) {
                                                
                                                    if(!file_exists($imagefullpath)){
                                                        if($employees->gender == "Male") {
                                                            $imagefullpaths = base_url()."img/boy.png";
                                                        } else if($employees->gender == "Female"){
                                                            $imagefullpaths = base_url()."img/girl.png";
                                                        }
                                                        $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpaths.'"><img src="';
                                                        $imagePath .= $imagefullpaths;
                                                        $imagePath .= '"style="height: 50px;width: 50px;" class="img-circle"></a>
                                                        </span>';
                                                    } else {

                                                        if($employees->gender == "Male") {
                                                            $imagefullpaths = base_url()."img/boy.png";
                                                        } else if($employees->gender == "Female"){
                                                            $imagefullpaths = base_url()."img/girl.png";
                                                        }
                                                        $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpaths.'"><img src="';
                                                        $imagePath .= $imagefullpaths;
                                                        $imagePath .= '"style="height: 75px;width: 75px; class="img-circle"></a>
                                                        </span>';

                                                        // $imagePath = "";
                                                        // $imagefullpath = $returnpath."No_user.png";
                                                        // $imagePath .= '<img src="';
                                                        // $imagePath .= $imagefullpath;
                                                        // $imagePath .= '" class="img-circle" width="150" />';
                                                            echo $imagePath;
                                                    }
                                                } else {
                                                        if($employees->gender == "Male") {
                                                            $imagefullpaths = base_url()."img/boy.png";
                                                        } else if($employees->gender == "Female"){
                                                            $imagefullpaths = base_url()."img/girl.png";
                                                        }
                                                        $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpaths.'"><img src="';
                                                        $imagePath .= $imagefullpaths;
                                                        $imagePath .= '"style="height: 50px;width: 50px;" class="img-circle"></a>
                                                        </span>';

                                                        // $imagePath = "";
                                                        // $imagefullpath = $returnpath."No_user.png";
                                                        // $imagePath .= '<img src="';
                                                        // $imagePath .= $imagefullpath;
                                                        // $imagePath .= '" class="img-circle" width="150" />';
                                                            echo $imagePath;
                                                    }
                                            ?>
                                    <h4 class="card-title mt-2"><?php echo $employees->name; ?></h4>
                                    <h6 class="card-subtitle"><?php echo $employees->designation; ?></h6>
                                    <?php if($employees->exp_year > 0) { ?><b><?php echo $employees->exp_year; ?> Years </b> <?php } ?> <?php if($employees->exp_month > 0) { ?> - <b><?php echo $employees->exp_month; ?> Months</b>  <?php } ?>
                                    
                                </center>
                            </div>
                            <div>
                                <hr> </div>
                            <div class="card-body">
                                <?php if(isset($employees->email) && !empty($employees->email)) { ?>
                                <small class="text-muted">Email address </small>
                                <h6><a href="mailto:<?php echo $employees->email; ?>" class="__cf_email__" data-cfemail="e28a838c8c83858d948790a2858f838b8ecc818d8f"><?php echo $employees->email; ?></a></h6> 

                                <?php } ?>
                                <?php if(isset($employees->mobileno) && !empty($employees->mobileno)) { ?>
                                <small class="text-muted p-t-30 db">Phone</small>
                                <h6><a href="tel:<?php echo $employees->mobileno; ?>">+<?php echo $employees->mobileno; ?></a></h6>
                                <h6><a href="tel:<?php echo $employees->mobileno; ?>">+<?php echo $employees->alternateno; ?></a></h6>
                                 <?php } ?>
                                <small class="text-muted p-t-30 db">Address</small>
                                <h6><?php echo $employees->address; ?></h6>
                                <div class="map-box">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470029.1604841957!2d72.29955005258641!3d23.019996818380896!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x395e848aba5bd449%3A0x4fcedd11614f6516!2sAhmedabad%2C+Gujarat!5e0!3m2!1sen!2sin!4v1493204785508" width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div> <small class="text-muted p-t-30 db">Social Profile</small>
                                <br/>
                                <?php if(isset($employees->linkedin) && !empty($employees->linkedin)) { ?>
                                <a class="btn btn-circle btn-secondary" href="<?php echo $employees->linkedin; ?>" target="_blank"><i class="fa fa-linkedin"></i></a>

                                 <?php } ?>
                                <?php echo '<a href="'.site_url("employees/employees_pdf/".$employees->id) .'" data-toggle="tooltip"  data-placement="left" title="PDF Download"  class="btn btn-primary"><i class="fa fa-download"></i></a>'; ?>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-md-12 col-lg-8 col-xlg-9">
                        <div class="card">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs profile-tab" role="tablist">
                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#about" role="tab">About</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#education" role="tab">Education</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#company" role="tab">Company</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#project" role="tab">Project</a> </li>
                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#skills" role="tab">Skills</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="about" role="tabpanel">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="mt-4"><?php echo $employees->about_me; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <div class="tab-pane" id="education" role="tabpanel">
                                    <div class="card-body">
                                        <div class="table-responsive m-t-40">
                                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                       <th scope="col">No</th>
                                                        <th scope="col">Graduation/PG/Any Other Courses</th>
                                                        <th scope="col">Passing Year</th>
                                                        <th scope="col">Board/University Name</th>
                                                        <th scope="col">Percentage</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                $rowno = 1;
                                                foreach($empEdu as $k=> $v)
                                                { 
                                                ?>
                                                <tr>
                                                <td scope="col"><?php echo $rowno; ?></td>
                                                <td scope="col"><?php echo $v->graduation; ?></td>
                                                <td scope="col"><?php echo $v->passing_year; ?></td>
                                                <td scope="col"><?php echo $v->board_university; ?></td>
                                                <td scope="col"><?php echo $v->percentage; ?></td>
                                                </tr>
                                                <?php 
                                                $rowno++;
                                                }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!--second tab-->
                                <div class="tab-pane" id="company" role="tabpanel">
                                    <div class="card-body">
                                        <h4 class="font-medium mt-4">Experience</h4>
                                         <hr>
                                        <?php if(count($empCompany)>0) { ?>
                                        <?php foreach($empCompany as $ec=>$c) {  ?>
                                        <ul>
                                        <li><b><?php echo $c->company_name; ?></b></li>
                                        </ul>

                                        <p class="mt-4">Working as a <?php echo $c->job_position; ?></p>
                                         <p class="mt-4"><?php echo $c->job_role; ?></p>
                                         <hr>
                                        <?php } ?>
                                        <?php } ?>

                                       
                                        
                                    </div>
                                </div>
                                <div class="tab-pane" id="project" role="tabpanel">
                                    <div class="card-body">
                                    <?php 
                                    
                                    if(count($empProject)>0) { ?>
                                    <h4 class="card-title">Projects</h4>.
                                    <?php foreach($empProject as $ep=>$p) { ?>
                                    <h5  class="card-title"><b><?php echo $p->project_name; ?></b></h5>
                                    <p><?php echo $p->project_details; ?></p>
                                    
                                    <?php if(isset($p->skills_multiple) && !empty($p->skills_multiple)){ ?>
                                    <p><?php echo $p->skills_multiple; ?>
                                    <?php } ?>

                                    <?php if(isset($p->project_url) && !empty($p->project_url)){ ?>
                                    <p><a href="<?php echo $p->project_url; ?>" target="_blank">View Web URL</a></p>
                                   <?php } ?>

                                   <?php if(isset($p->project_url_android) && !empty($p->project_url_android)){ ?>
                                    <p><a href="<?php echo $p->project_url_android; ?>" target="_blank">View Android URL</a></p>
                                   <?php } ?>

                                   <?php if(isset($p->project_url_ios) && !empty($p->project_url_ios)){ ?>
                                    <p><a href="<?php echo $p->project_url_ios; ?>" target="_blank">View iOS URL</a></p>
                                   <?php } ?>

                                    <?php } ?>
                                    <?php } ?>
                                    </div>
                                </div>
                                <div class="tab-pane" id="skills" role="tabpanel">
                                    <div class="card-body">

                                        <?php if(isset($employees->primary_skills) && !empty($employees->primary_skills)){ 
                                            echo "<b>Primary Skills:</b> &nbsp; &nbsp;";
                                            echo $primary_skills; 
                                            ?><br/>
                                        <?php } ?>
                                        <?php if(count($empSkill)>0) {
                                            echo "<b>Secondary Skills:</b> &nbsp; &nbsp;";
                                            $tmp_emp = array();
                                            foreach ($empSkill as $key => $value) {
                                                $tname = $value->tname;
                                                $tmp_emp[] = $tname; 
                                            }
                                            echo implode(', <br>',$tmp_emp);

                                        } else { 
                                            echo "N.A.";
                                        } ?>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->

            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php  $this->load->view("admin/common/common_footer"); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

   <?php $this->load->view("admin/common/common_js"); ?>
</body>
</html>


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=<?php echo API_KEY; ?>&libraries=places"></script>

<script>
    $(document).ready(function () {
        $('#error').delay(3000).fadeOut();
    });

    $(function () {      

        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;

        var latlng = new google.maps.LatLng(lat,lng);
        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });

        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -14)
        });
        var infowindow = new google.maps.InfoWindow();   
        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map, marker);
        });

    });
</script>

