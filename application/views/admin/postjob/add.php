<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->

    <title><?php echo PROJECT_NAME; ?> - <?php echo $page; ?> <?php echo $action; ?></title>
    <?php  $this->load->view("admin/common/common_css"); ?>
</head>

  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}

input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}

</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                           <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>postjob/postjob_list"><?php echo $page; ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $action; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                <h4 class="card-title">New <?php echo $page; ?></h4>
                                <h6 class="card-subtitle"></h6>
                                <?php if (validation_errors())
                                {   
                                echo '<div class="alert alert-warning alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning!</strong> ';

                                echo validation_errors();
                                echo '</div>';

                                }
                                
                                $exist_vendor_id = $admin_id;
                                $is_access = "No";
                                if($customer_role_type == "V") {
                                    if($admin_id == $exist_vendor_id) {
                                        $is_access = "Yes";
                                    }
                                } else if($customer_role_type == "A") {
                                    $is_access = "No";
                                } else {
                                    $is_access = "No";
                                }
                                
                                ?>
                                 <form class="form" name="form" id="form_edit" role="form" method="post" enctype="multipart/form-data">

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Company Name <span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="vendor_id" id="vendor_id"> 
                                                    <option>--- Select Vendor ---</option>
                                                    <?php foreach($vendors as $r=>$v) { 
                                                        if($is_access == "Yes") {
                                                            if($v->customer_id == $admin_id) {
                                                        ?>
                                                            <option value="<?php echo $v->customer_id; ?>" name="<?php echo $v->customer_id; ?>" id="<?php echo $v->customer_id; ?>" <?php if($v->customer_id == $admin_id) { ?> selected="selected" <?php } ?>><?php echo $v->username; ?></option>
                                                            <?php }
                                                        } else { ?>
                                                            <option value="<?php echo $v->customer_id; ?>" name="<?php echo $v->customer_id; ?>" id="<?php echo $v->customer_id; ?>" <?php if($v->customer_id == $admin_id) { ?> selected="selected" <?php } ?>><?php echo $v->username; ?></option> 
                                                    <?php } } ?>
                                                    </select> 
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Skills <span class="text-danger">*</span></label>
                                                    <select class="select2 form-control" name="skill_id" id="skills" required="required"  style="height: 36px;width: 100%;">
                                                    <option>--- Select Your skills ---</option>
                                                    <?php foreach($technologies as $r=>$v) { ?>
                                                        <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" id="<?php echo $v->id; ?>"><?php echo $v->tname; ?></option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>                                   
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Sub Skills <span class="text-danger"></span></label>
                                                    <textarea class="form-control" placeholder="Sub Skills " id="subskills" name="subskills"></textarea>
                                                </div>
                                            </div>   

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Job Duration<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="duration" id="duration">
                                                    <option>--- Select Job Duration ---</option>
                                                    <?php for($i=1;$i<=12;$i++) { ?>
                                                        <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i; ?> Month</option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Job Description <span class="text-danger">*</span></label>
                                                    <textarea class="form-control" placeholder="Job Description" id="description" cols="5" name="description"></textarea>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Experience<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="experience" id="experience">
                                                    <option>--- Select Job Experience ---</option>
                                                    <?php for($i=1;$i<=10;$i++) { ?>
                                                        <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i; ?> + Exp</option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Budget<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="budget" id="budget">
                                                    <option>--- Select Job Budget ---</option>
                                                    <?php for($i=100000;$i<=250000;$i=$i+10000) { ?>
                                                        <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>       

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">No of Position<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="no_of_position" id="no_of_position">
                                                    <option>--- Select No Of Position ---</option>
                                                    <?php for($i=1;$i<=50;$i++) { ?>
                                                        <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i; ?> </option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">No of Interview Round<span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="no_of_interview" id="no_of_interview">
                                                    <option>--- Select No of Interview ---</option>
                                                    <?php for($i=1;$i<=5;$i++) { ?>
                                                        <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i; ?> </option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Candidate Availibity <span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="candidate_availability" id="candidate_availability">
                                                        <option value="immediate" name="immediate" id="immediate">Immediate</option>
                                                        <option value="7 days" name="7 days" id="7 days">7 Days</option>
                                                        <option value="15 days" name="15 days" id="15 days">15 Days</option>
                                                        <option value="30 days" name="30 days" id="30 days">30 Days</option>
                                                    </select>
                                                </div>
                                            </div>                                      
                                        </div>

                                      


                                    <div class="form-group row">
                                     
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                         </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <button type="submit" class="btn btn-info" id="save_button"  name="save_button" value="Insert">Insert</button>
                                            <input type="hidden" name="save_button" id="save_button" value="Insert">
                                            <a href="<?php echo site_url("postjob/approve_postjob_list"); ?>" class="btn btn-inverse">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Validation wizard -->
   
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- footer -->
 <!-- ============================================================== -->

 <?php  $this->load->view("admin/common/common_footer"); ?>

 <!-- ============================================================== -->
 <!-- End footer -->
 <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php  $this->load->view("admin/common/common_js"); ?>
</body>

</html>

<script>
 $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script>
$(function() {
    $("#form_edit").validate({
        // Specify validation rules
        rules: {
            vendor_id:{
                required:true,
                min:1,
            },
            skill_id:{
                required:true,
                min:1,
            },
            subskills:{
                required:true,
            },
            duration:{
                required:true,
                 min:1,
            },
            description:{
                required:false,
            },
            experience:{
                required:true,
                min:1,
            },
            budget:{
                required:true,
                min:1,
            },
            no_of_position:{
                required:true,
                min:1,
            },
            no_of_interview:{
                required:true,
                min:1,
            },
            candidate_availability: {
                required:true,
            },
        },
        messages: {
            vendor_id:{
                required:"Please select Vendor",
                min:"Please select Vendor",
            },
            skill_id:{
                required:"Please select job skill",
                min:"Please select job skill",
            },
            subskills:{
                required:"Please enter sub skills",
            },
            duration:{
                required:"Please select job duration",
            },
            description:{
                required:"Please enter job description",
            },
            experience:{
                required:"Please select job experience",
                min:"Please select job experience",
            },
            budget:{
                required:"Please select budget",
                min:"Please select budget",
            },
            no_of_position:{
                required:"Please select no of position",
                min:"Please select no of position",
            },
            no_of_interview:{
                required:"Please select Interview round",
                min:"Please select Interview round",
            },
            candidate_availability: {
                required:"Please select candidate availability",
            },
        },
        submitHandler: function(form) {
           form.submit();
        }
    });

    $('#save_button').click(function() {
        if($('#form_edit').valid()){
            $(this).attr('disabled', 'disabled');
            $(this).html('Saving...');
            $(this).parents('form').submit();
        }
    });
});
</script>
