<!DOCTYPE html>
<html lang="en">
    <?php 
        $my_permission = get_my_permission();
    ?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo PROJECT_NAME; ?> - <?php echo $page; ?> <?php echo $action; ?></title>
        <?php $this->load->view("admin/common/common_css"); ?>
    </head>

    <body class="fix-header fix-sidebar card-no-border">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    <div class="row page-titles">
                        <div class="col-md-6 col-12 align-self-center">
                            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>postjob/postjob_list"><?php echo $page; ?></a></li>
                                <li class="breadcrumb-item active"><?php echo $action; ?></li>
                            </ol>
                        </div>
                       
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">



                                    <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                    
                                    <h4 class="card-title">
                                        <?php if(in_array('interviewschedule_add', $my_permission)) { ?>

                                            <div class="pull-right">
                                                <a href="<?php echo site_url("interviewschedule/interviewschedule_add/$postjob_id"); ?>" class="btn btn-info" style="color: rgb(255, 255, 255); padding: 5px 13px;"><i class="fa fa-plus"></i></a>
                                            </div>
                                        <?php } ?>  

                                    </h4>
                                    <h6 class="card-subtitle"></h6>
                                    <div class="table-responsive m-t-40">
                                        <p><?php echo $postjob->username; ?> was need  
                                        <?php echo $postjob->tname; ?> resources bugdet 
                                        <?php echo $postjob->budget; ?> for
                                        <?php echo $postjob->experience; ?> developers.</p>
                                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date&nbsp;1</th>
                                                    <th>Date&nbsp;2</th>
                                                    <th>Post&nbsp;By</th>
                                                    <th>Vendor</th>
                                                    <th>Candidate&nbsp;Name</th>
                                                    <th>Skills</th>
                                                    <th width="5%">Status</th>
                                                    <th style="min-width:150px">Action</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
 
                                              <?php
                                            if(in_array('interviewschedule_list', $my_permission)) {
                                               
                                                if (count($interviews) > 0) {
                                                    $no = 1;
                                                    foreach ($interviews as $k=> $row) {

                                                    if(isset($row->interview_one_date) && !empty($row->interview_one_date)) {
                                                        $interview_one_date = date('d-M-Y',strtotime($row->interview_one_date));
                                                    } else {
                                                        $interview_one_date = "-";
                                                    }

                                                    if(isset($row->interview_two_date) && !empty($row->interview_two_date)) {
                                                        $interview_two_date = date('d-M-Y',strtotime($row->interview_two_date));
                                                    } else {
                                                        $interview_two_date = "-";
                                                    }

                                                    $username = $row->username;
                                                    $created_by_name = $row->created_by_name;
                                                    $candidate_name = $row->candidate_name;
                                                    $tname = $row->tname;
                                                    $budget = $row->budget;
                                                    $experience = $row->experience;

                                                    $status = $row->status;

                                                    $action = '';

                                                    $edit_link = '';
                                                    if(in_array('interviewschedule_edit', $my_permission)) {
                                                        $edit_link.= ' <a href="'.site_url("interviewschedule/interviewschedule_edit/".$row->id) .'" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-edit"></i></a>';
                                                    }

                                                    $delete_link = '';
                                                    if(in_array('interviewschedule_delete', $my_permission)) {
                                                        $delete_link .= ' <a href="javascript:void(0);"  onclick="';
                                                        $delete_link .= "confirm_delete('";
                                                        $delete_link .= site_url("interviewschedule/interviewschedule_delete/".$row->id);
                                                        $delete_link .= "'";
                                                        $delete_link .= ')"; data-toggle="tooltip" data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a>';
                                                    }

                                                        $action  =  $pdf_link.$view_link.$edit_link.$delete_link;

                                                        ?>  

                                                        <tr>
                                                                <td><?php echo $no; ?></td>
                                                                <td><?php echo $interview_one_date; ?></td>
                                                                <td><?php echo $interview_two_date; ?></td>
                                                                <td><?php echo $username; ?></td>
                                                                <td><?php echo $created_by_name; ?></td>
                                                                <td><?php echo $candidate_name; ?></td>
                                                                <td><?php echo $tname; ?></td>
                                                               
                                                                <td><?php echo $status; ?></td>
                                                                <td><?php echo $action; ?></td>
                                                        </tr>
                                                    <?php
                                                    }
                                                    $no = $no + 1;
                                                } 

                                            } else { ?> 
                                            <tr><td colspan="9">
                                                You have not access to module.
                                            </td></tr>
                                            <?php } ?>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php $this->load->view("admin/common/common_footer"); ?>

                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
<?php $this->load->view("admin/common/common_js"); ?>
    </body>

</html>


<script>
    $(document).ready(function () {
        $('#error').delay(3000).fadeOut();
    });

    $(function () {

        $("body").on("change", ".tgl_checkbox", function () {
            var table = $(this).data("table");
            var status = $(this).data("status");
            var id = $(this).data("id");
            var id_field = $(this).data("idfield");
            var bin = 0;

            if ($(this).is(':checked')) {
                bin = 1;
            }

            /*               alert(table+'---'+status+'---'+id+'---'+id_field+'---'+bin);
             return false; */
            $.ajax({
                method: "POST",
                url: "<?php echo site_url("admin/change_status"); ?>",
                data: {table: table, status: status, id: id, id_field: id_field, on_off: bin}
            })
                    .done(function (msg) {
                        //  alert(msg);
                        if (msg == '1') {
                            $("#myElem").show();
                            setTimeout(function () {
                                $("#myElem").hide();
                            }, 3000);

                        } else if (msg == '0') {
                            $("#myElemNo").show();
                            setTimeout(function () {
                                $("#myElemNo").hide();
                            }, 3000);
                        }

                    });
        });

        var table = $('#example23').DataTable({
            "order": [[ 0, "ASC" ]]
        });
     
        // var buttons = new $.fn.dataTable.Buttons(table, {
        //      buttons: [
        //        'excelHtml5',
        //        'pdfHtml5'
        //     ]
        // }).container().appendTo($('#buttons'));
    });

</script>
