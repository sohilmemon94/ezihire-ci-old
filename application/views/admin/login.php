<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/ico" sizes="64x64" href="<?php echo base_url(); ?>img/favicon.png">
    <title><?php echo PROJECT_NAME; ?> | Admin Login</title>
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}
</style>

<?php 
$returnpath = $this->config->item('customers_images_uploaded_path');
$local_image = "background.jpg";
if(isset($adminmaster->background_image) && !empty($adminmaster->background_image)) { 
    $background_image = $adminmaster->background_image;
    $imagefullpath = base_url().$background_image;
    if(!empty($background_image)) {
        if(!file_exists($imagefullpath)){
            $imagefullpath = $returnpath.$background_image;
        } else {
            $imagefullpath = $returnpath.$local_image;
        }
    }
} else {
    $imagefullpath = $returnpath.$local_image;
} ?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        <div class="login-register" style="background-image:url('<?php echo $imagefullpath; ?>');">      
            <div class="login-box card">
            <div class="card-body">
                    
                <form class="form-horizontal form-material" name="loginform" id="loginform" method="post"  enctype="multipart/form-data">
                    <h3 class="box-title m-b-20"> <img src="<?php echo base_url(); ?>img/logo.png" style="align: center;margin: 0 auto;display: block;"></h3>

					
                    <?php if(isset($error)){ echo $error; }
                                   echo $this->session->flashdata("message"); ?>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="email" placeholder="Email"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" name="password" placeholder="Password"> </div>
                    </div>
                    
                    <?php if ($_SERVER['HTTP_HOST'] != "localhost") { ?> 
                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('google_sitekey') ?>"></div>
                        </div>
                    </div>
                    <?php } ?>
                
                    <div class="form-group">
                       <div class="col-md-12">
                            <span class="pull-left"><a href="<?php echo base_url(); ?>admin/index"></a></span>
                            <span class="pull-right"><a href="<?php echo base_url(); ?>admin/forgot_password"><i class="fa fa-lock m-r-5"></i> Forgot password?</a></span>
                        </div>
                    </div>

				    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block waves-effect waves-light" type="submit" id="btnContactUs">Log In</button>
                        </div>
                    </div>
                  
                </form>
                
				
            </div>
          </div>
        </div>
        
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
   
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
<script>
    $(document).ready( function() {
        $('#error').delay(3000).fadeOut();
    });
</script>
<script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>js/additional-methods.min.js"></script>
<script>
$(function() {
    $("#loginform").validate({
        rules: {
            email: {
                required:true,
                email:true,
            },
            password: {
                required:true,
            },
         
        },
        messages: {
            email: {
                required:"Please enter email",
                email:"Please enter valid email",
            },
            password: {
                required:"Please enter password",
            },
        },
        submitHandler: function(form) {
          form.submit();
        }
    });

    $('#btnContactUs').click(function() {
        if($('#loginform').valid()){
            $(this).attr('disabled', 'disabled');
            var html ="<span class='spinner-border' role='status' aria-hidden='true'></span> Connecting...";
            $(this).html('');
            $(this).html(html);
            $(this).parents('form').submit();
        }
    });
});
</script>   