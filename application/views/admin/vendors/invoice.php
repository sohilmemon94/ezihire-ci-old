
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>images/favicon.ico">
    <title>Infiraise - Vendor Aggrement</title>
    <style>
        section{margin-left:15px !important;margin-right:15px !important;}
        body{
            border:5px solid black;padding-left:10px !important;padding-right:10px !important;margin-left:100px !important;margin-right:100px !important;background: #ffffff;font-family: 'Open Sans', sans-serif;font-size: 14px;font-weight: normal;margin: 15px;
        } 
        section.edu-info {margin: 70px 0;}
        section.experience-info {padding: 30px 0;}
        section.projects {padding: 0 0 30px 0;}
        section.projects .message-item {margin: 40px 0 0 0;}
        .experience h2{margin:10px 0 0 0;}
        .top-head img{text-align:center !important;}
        .top-head p{text-align:center !important;line-height: 24px;margin: 0;color: #000;}
        /* p{margin: 0!important;} */
        h2{font-size: 26px;color: #232323;font-weight: normal;margin: 0px 0 10px 0;}
        h6{font-size: 16px;color: #000;font-family: 'Open Sans', sans-serif;font-weight: 400;margin: 0;line-height: 28px;}
        .employee-info{margin: 40px 0 0 0;}
        .employee-info h2{margin: 0 0 0 0;}
        .employee-info ul {padding: 0;}
        .employee-info ul li {margin: 10px 0;}
        .message-item:before {content: ""; background: #8a8a8a;border-radius: 2px;bottom: -30px;height: 100%;left: -30px;position: absolute;width: 1px;}
        .message-item:after {content: "";background: #000000;border: 2px solid #000000;border-radius: 50%;box-shadow: 0 0 5px rgb(0 0 0 / 10%);height: 15px;left: -36px;position: absolute;top: 10px;width: 15px;}
        .message-item {margin: 24px 0 0 0;position: relative;}
        .message-item .message-inner:before {border-right: 10px solid rgb(0, 0, 0);border-style: solid;border-width: 10px;color: rgba(0,0,0,0);content: "";display: block;height: 0;position: absolute;left: -21px;top: 6px;width: 0;}
        .message-inner{border: 1px solid #797979;border-radius: 3px;padding: 10px;position: relative;}
        .message-item:after {content: "";color: black;border: 2px solid #000000;border-radius: 50%;box-shadow: 0 0 5px rgb(0 0 0 / 10%);height: 15px;left: -39px;position: absolute;top: 10px;width: 15px;}
        .message-item .message-head {border-bottom: 1px solid #656565;margin-bottom: 8px;padding-bottom: 8px;color: #fff;}
        .clearfix:before, .clearfix:after {content: " ";display: table;}
        .clearfix:before, .clearfix:after {content: '\0020';display: block;overflow: hidden;visibility: hidden;width: 0;height: 0;}
        .message-item .message-head .user-detail h5 {font-size: 16px;font-weight: bold;margin: 0;color: #000;}
        .message-item .message-head .post-meta {float: left;padding: 0 15px 0 0;}
        .message-item .message-head .post-meta >div {color: black;font-weight: bold;text-align: right;}
        .post-meta > div {color: black;font-size: 12px;line-height: 22px;}
        .qa-message-content p {line-height: 24px;margin: 0;color: #000;}
        .user-detail{overflow: hidden;}
        .edu-info table {
border-collapse: collapse;
width: 100%;
border: 1px solid black;
}

.edu-info th, td {
  text-align: center;
  padding: 8px;
  border-left: 1px solid black;
  border-top: 1px solid black;
}
/* .table-form tbody tr:nth-child(1),tr:nth-child(2) {background-color: #f2f2f2;} */

.main-content p {text-align: justify;margin:10px 0;line-height: 18px;}
.logo {text-align: center;}
.page-content h6 {font-weight: bold;margin-top: 30px;}
.page-content ol li p {margin: 10px 0;text-align:justify;}
ol.k {padding: 0 0 0 17px;margin:0;}
.details {padding: 20px 0;}
span {font-weight: bold;}
/* .table-form{margin:20px 0;} */

.table-form table{border-collapse: collapse;
width: 100%;
}

.edu-info th, td {
  text-align: center;
  padding: 8px;
  border-left: 1px solid black;
  border-top: 1px solid black;
}  

.text-center {
    text-align:center !important;
}

.table-form th{text-align:left;padding: 8px;text-transform: uppercase;bwidth:50%;}
.table-form th, td{text-align:left;border: none;}
.page-content span {font-weight: normal;font-size: 13px;}
@media screen and (max-width:767px)
{
    .employee-info{margin: 40px 0 0 0;}
}

@media screen and (max-width:576px)
{
    p{font-size: 8px;}
}
</style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<section>        
    <div class="container" >
        
         <div class="row">
            <div class="col-md-12 logo">

                <p><img src="<?php echo base_url(); ?>uploads/logo.png" alt="logo"></p>
               
                <p><b><u>RESOURCE AUGMENTATION AGREEMENT</u></b></p>
            </div>
        </div>

        <br><hr>
        <div class="row">
            <div class="col-md-12">
                <div class="main-content">
                <p>This Resource Augmentation Agreement (“Agreement”) is hereby made and executed at Gandhinagar on this <u><b><?php echo date('l',strtotime($vendors->agreement_date)); ?></b></u> day of <u><b><?php echo date('d,F, Y',strtotime($vendors->agreement_date)); ?></b></u></p>
                    <p class="text-center"><span>BETWEEN</span></p> 
                    <p><span>VIVAANSH EDUTECH PVT LTD (“Vivaansh”)</span>, a Company incorporated under the Companies Act, 2013 bearing (CIN:<b>U80903GJ2018PTC102874</b>)and having its registered office at A-10, Marvel Society, Division 4, Marutinandan Vihar, SP Ring Road, Bopal, Ahmedabad, Gujarat-380058, hereinafter referred to as “<b>Infiraise</b>”, which term shall unless repugnant to the context mean and include its successors-in-interest, assigns and legal representatives of the <b>FIRST PART;</b></p>
                    <p class="text-center"><span>AND</span></p> 
                    <p><u><b><?php echo $vendors->username; ?></b></u> a Vendor incorporated under the provisions of Companies Act, 1956 bearing Client Identification Number (CIN) <u><b><?php echo $vendors->cin; ?></b></u> & GSTIN <u><b><?php echo $vendors->gstin; ?></b></u> and having its registered office at <u><b><?php echo $vendors->registered_office; ?></b></u>, hereinafter referred to as “<span>Vendor</span>”, which term shall unless repugnant to the context mean and include its successors-in-interest, assigns and legal representatives of the <span>SECOND PART;</span></p>

                    <p>“<span>Infiraise</span>” and “<span>Vendor</span>” shall hereinafter individually be referred to as “<span>Party</span>” and collectively as “<span>Parties</span>”.</p>

                    <p><span>WHEREAS:</span></p>

                    <p>A.  Infiraise has experience, expertise and skill in providing total solutions in area of IT.</p>
                    <p>B.  Vendor is engaged in <span><?php echo $industry->iname; ?></span>. </p>
                    <p>C.  Vendor has offered its services to Infiraise whenever called upon by Infiraise.</p>

                    <p><span>NOW THIS AGREEMENT WITNESSES AS FOLLOWS:</span></p>

                <p><span>1.  DEFINITIONS:</span></p>
                <p>1.1 “<span>Affiliate</span>” means any entity that controls, is controlled by, or is under common control with either Party for purposes of this Agreement, </p>
                <p>1.2 “<span>Control</span>” means possessing, directly or indirectly, the power to direct or cause the direction of the management, policies or operations of an entity, whether through ownership of voting securities, by Agreement or otherwise.</p>

                <p>1.3 “<span>Agreement</span>” means this Agreement, the Appendices hereto, and any duly executed amendments thereto.</p><p>
                1.4 “<span>Deliverables</span>” means the services, which will be developed and delivered to the Infiraise as per the needs of the Infiraise;</p><p>
                1.5 “<span>Disclosing Party</span>” means the Infirasie that discloses any Confidential Information with reference to this agreement to Vendor and its Resources; </p><p>
                1.6 “<span>Client</span>” means and includes with whom Infirasie has entered into the business transaction.</p><p>
                1.7  “<span>Representatives</span>” means Either Party’s officers, directors, employees, agents and subcontractors (and their employees).</p><p>
                1.8 “<span>Receiving Party</span>” shall mean the Vendor and its Resources which receives any Confidential Information with reference to this Agreement. </p><p>
                1.9 “<span>Services</span>" shall mean any and all services with reference to this Agreement to be provided by Vendor as and when required by the Infiraise.</p><p>
                1.10 "<span>Task Order</span>" means any statement of work expressly referring to be issued under this Agreement and executed by authorized representatives of both parties and shall at a minimum specify the following(a) the term of the Task Order (b) a detailed description of the Services covered by the Task Order; (c)the Charges for the Services covered by the Task Order</p><p>
                1.11     “<span>Working business days</span>” means every working day of the week excluding Saturday, Sunday, public holidays and national holidays, which are deemed as non-business working days."</p><p>
                1.12    “<span>Resources</span>” shall means and include the competent personnel provided the Vendor to Infiraise from time to time.</p> 

                <p><span>2.  SERVICES:</span></p>
                <p>2.1. It is agreed between the Parties that the Vendor shall assign Resources to perform the Services as per the requirements of the Infiraise. Such deployed Resources shall perform Services as directed by the Infiraise as per the terms and Conditions of this Agreement. </p>
                <p>2.2. Perform and deliver Services under Task Orders, in accordance with the milestones, delivery dates, specifications and requirements as set forth in such Task Orders. Infiraise shall mention and discuss as per its work, requirements for resources and expertise person for the required work to the Vendor</p>


                <p><span>3. PAYMENT:</span></p>
                <p>3.1. Infiraise shall pay Vendor for Services utilized by it in accordance with the stipulations in Annexure-A to this Agreement.</p>
                <p>3.2. Parties shall obtain all permits and licenses and shall pay all fees and taxes required by federal, state, local or foreign laws or regulations in connection with the performance of Services pursuant to this Agreement. The fees for Services exclude any and all GST. Parties shall be solely responsible for their respective taxes.</p>
                <p>3.3. Vendor shall provide Infiraise with an itemized invoice for all charges/ fees and approved expenses that become due hereunder. Each valid and undisputed invoice shall be due and payable within 30 (thirty) working business days after Infiraise receipt of such invoice. In case of disputed invoice, if dispute is not resolved within 15 (fifteen) working business days, invoice shall have deemed to be accepted. </p>

                 <p><span>4. CONFIDENTIAL INFORMATION:</span></p>


                <p>4.1.   For the purposes of this Agreement issued hereunder, “Confidential Information” shall mean:</p>

                <p>(i)    Information disclosed to Receiving Party by or obtained by Receiving Party from Disclosing Party in connection with, this Agreement.</p>
                <p>(ii)    All Deliverables, deployed resources, which may be delivered under the Agreement.</p>
                <p>(iii)   All notes, analyses and studies that may be prepared in connection with this Agreement.</p>
                <p>(iv)    Any information that under the circumstances of the disclosure needs to be treated as confidential.</p>
                <p>(v) Any data or personal information, which the Receiving Party might come into possession during the subsistence of this Agreement.</p>
                <p>4.2. The term ‘Confidential Information’ shall not, however, mean any information, which is </p>
                <p>(i)  is in or enters the public domain, other than by breach by the Receiving Party;</p>
                <p>
(ii)    is known to the Receiving Party on a non-confidential basis prior to disclosure under this Agreement, at the time of first receipt, or thereafter becomes known to the Receiving Party without similar restrictions from a source other than the Disclosing Party;</p>
                <p>
(iii)   is rightfully received by the Receiving Party from a third-party who did not acquire or disclose such information by a wrongful or tortuous act; </p>
                <p>
(iv)    is furnished to others by the Disclosing Party without restrictions similar to those herein on the right of such others to use or disclose;</p>
                <p>
(v) is or has been developed independently by the Receiving Party without reference to or reliance on the Disclosing Party's Confidential Information; </p>
                <p>
(vi)    is approved in writing by the Disclosing Party for disclosure; or is required to be disclosed by the Receiving Party under applicable laws, court orders or any order from a regulatory, statutory or governmental authority of any jurisdiction. 
</p>
                <p>

4.3.    Vendor shall, at all times, maintain adequate procedures to prevent improper and unauthorized disclosure of any Confidential Information and to prevent loss of any such information. In the event of any loss or disclosure of Confidential Information, intentionally or otherwise, Receiving Party shall notify Disclosing Party immediately and initiate at its cost all remedial measures necessary to minimize adverse consequences thereof.
</p>
                <p>
4.4.    Receiving Party shall ensure safety and security of documents, items of work in process and products that embody Confidential Information by storing them in areas with restricted access to prevent its unauthorized disclosure.</p>
                <p>
4.5.    Receiving Party shall not disclose Confidential Information to subcontractors or other team members nor subcontract any part of the work/project/task/assignment entrusted to it under any Agreement by Receiving Party without first obtaining written consent from Disclosing Party.</p>
                <p>
4.6.    The Receiving Party shall not use Confidential Information except for those purposes expressly contemplated under this Agreement or as authorized by Disclosing Party.</p>
                <p>
4.7.    In case of breach of any confidentiality obligations, Disclosing Party shall be entitled, without waiving any other rights or remedies, to seek such injunctive or equitable relief as may be deemed proper by a court of competent jurisdiction.</p>
                <p>
4.8.    All Confidential Information, including the inherent intellectual properties, remains the sole and exclusive property of the Disclosing Party.</p>
                <p>
4.9.    Any data or personal information of Disclosing Party, its employees or Disclosing Party’s Customers and its employees shall remain strictly confidential and the Receiving Party shall comply with all applicable privacy and data protection laws.</p>
                <p>
4.10.   Upon termination or expiration of this Agreement or upon Disclosing Party’s written request, Receiving Party shall return to Disclosing Party or if specifically instructed by Disclosing Party destroy all copies of the Confidential Information already in the Receiving Party’s possession or within its control. The obligation of the Receiving Party to protect the Confidential Information shall be subsistence in perpetuity notwithstanding the expiry or termination of any other agreements/writings that may be executed between the Parties.
</p>

 <p><span>4.  OWNERSHIP OF DELIVERABLES:</span></p>

                <p>
4.1.    “Intellectual Property” means all (i) patents, patent applications, patent disclosures and inventions (whether patentable or not), (ii) trademarks, service marks, trade dress, trade names, logos, corporate names, Internet domain names, and registrations and applications for the registration thereof together with all of the goodwill associated therewith, (iii) copyrights and copyrightable works (including computer programs and mask works) and registrations and applications thereof, (iv) trade secrets, know-how and other confidential information, (v) waivable or assignable rights of publicity, waivable or assignable moral rights and (vi) unregistered and registered design rights and any applications for registration thereof; and (vii) database rights and all other forms of intellectual property, such as data.</p>
                <p>
4.2.    The Vendor and Resources shall be bound by the following obligations with respect to ownership of Deliverables: 

Infiraise shall have exclusive title and ownership rights, including all Intellectual Property rights, throughout the world in all Deliverables. To the extent that exclusive title and/or ownership rights may not originally vest in Infiraise as contemplated herein, Vendor and Resources hereby irrevocably assigns all right, title and interest, including Intellectual Property and ownership rights, in the Deliverables to Infiraise, and shall cause its Representatives to irrevocably assign to Infiraise all such rights in the Deliverables.</p>
                <p>
4.3.    This Agreement does not grant or otherwise give either Party ownership in or other proprietary rights or license to use the other Party’s Intellectual Property Rights except as expressly provided for herein. Each party’s rights and obligations under this Section shall remain in effect and survive any termination or expiration of this Agreement. </p>
                <p>
4.4.    Except for rights expressly granted under this agreement,</p>
                <p>
4.4.1.  nothing in this agreement will function to transfer any of either Party's Intellectual Property Rights to the other party, and</p>
                <p>
4.4.2.  each party will retain exclusive interest in and ownership of its Intellectual Property developed before this agreement or developed outside the scope of this agreement.</p>
                <p>
4.5.    Vendor and Resources shall not, in any manner whatsoever, commercially exploit or claim any rights over any Intellectual Property Rights of the Infirasie including its trademark, copyright, patent rights and others which it may utilize for the Service. </p>

<p><span>5.  INDEPENDENT CONTRACTOR</span></p>
<p>Vendor acknowledges that it is acting as an independent contractor, that Vendor is solely responsible for its actions or inactions, and that nothing in this Agreement shall be construed to create an agency or employment relationship between Vendor and Infiraise or its Representatives. Vendor is not authorized to enter into contracts or agreements on behalf of Infiraise or Representatives or to otherwise create obligations of Infiraise or Representatives to third Parties.</p>
<p><span>6. WARRANTIES:</span></p>

<p>
6.1.    Parties warrants that in the execution of this Agreement they shall comply with all applicable laws and regulations and do all things necessary for respective Party to comply with all applicable laws, regulations and ordinances. Parties also agrees to obtain the required Government documents and approvals, if any, prior to export of any technical data disclosed to Vendors and its Resources assigned under this Agreement.
</p>
<p>
6.2.    Vendor warrants that it’s entering into this Agreement and provision of the Services does not violate any other obligations it may have.
</p>
<p><span>7.  LIMITATION OF LIABILITY:</span></p>
<p>Infiraise shall not be liable to the Vendor for any indirect, special, consequential, incidental or punitive damages, losses (including loss of profit, loss of business, loss of goodwill and loss of opportunity) arising out of or relating to this Agreement or Services, however caused, and whether the Vendor has been advised of the possibility of such damages. In case of any direct damages arising out of or relating to this Agreement or the Services, the liability of the Infiraise shall be limited to last one month’s payment paid to Vendor.</p>

<p><span>8.  INDEMNIFICATION</span></p>

<p>Subject to Applicable Laws, Vendor (the “<span>Indemnifying Party</span>”) agrees to indemnify Infiraise (the “<span>Indemnified Party</span>”) and its directors, officers, employees, agents and representatives against any losses, damages, liabilities, cost or expenses (including reasonable attorney’s fees), claims, suits, actions, proceedings, demands, penalties, fines, judgments, awards, or damages (hereinafter referred to as “<span>Demand</span>”) arising out of (a) the proven gross negligence or willful misconduct of the indemnifying Party or its representatives or affiliates vis-à-vis its obligations under this agreement; (b)Misrepresentation by the Indemnifying Party to the other Party or third Parties; (c) non-compliance by the Indemnifying Party with relevant obligations under all applicable laws in relation to the subject matter of this Agreement; or (d) any material breach of this agreement by the Indemnifying Party or any of its affiliates of any representation, warranty, covenant, agreement or other obligation contained in this Agreement. (e) breach of Confidentiality obligations and Intellectual Property Rights</p>


<p><span>9.  TERM AND TERMINATION</span></p>
<p>9.1.   This Agreement shall commence on the Effective Date <?php echo date('d,F,Y',strtotime($vendors->agreement_date)); ?> for <?php echo date('d,F, Y',strtotime('+1 year', strtotime($vendors->agreement_date))); ?> (1) year/s, thereafter, this Agreement shall be renewed unless Infiraise terminates this Agreement by giving the Vendor any advance notice in writing prior Thirty (30) days.   </p>



<p><span>10. TERMINATION:</span></p>
<p>
10.1.   Termination by Will- Infiraise may terminate this Agreement at any time during the term of this agreement by giving (30 days) Thirty days’ prior written notice to Vendor without assigning any reason whatsoever for such termination. </p><p>
10.2.   Termination for Cause- Infiraise may terminate this Agreement in case of breach of any of the provisions of this Agreement by the Vendor, if the Vendor does not cure the breach within (Thirty) 30 days after receipt of a written Notice of the breach.   </p><p>
10.3.   Termination for Bankruptcy, Insolvency, Winding Up, etc- Either party may terminate this Agreement with immediate effect upon written notice in the event that the other party abandons its responsibilities under this Agreement, becomes bankrupt or insolvent or files any proposal or makes any assignment for the benefit of creditors, or an order is made for its winding up or a receiver is appointed for substantial part of its property. </p><p>
10.4.   Any provision or clause in this Agreement that, by its language or context, implies its survival shall survive any termination or expiration of this Agreement.</p>

<p><span>11. NON-COMPETE</span></p>

<p>Vendor agrees that during the term of this Agreement and for a period of three (3) year following its expiration or termination, Vendor, Resources and/or its Affiliates, shall not, either on their own account or for any Person, enter into the employment of, act as a consultant to, or perform any Services for (i) any entity which competes to a material extent with the business activities in which the Infirasie is engaged at the time of such termination or in which business activities the Infirasie has documented plans to become engaged in and as to which Vendor and Resources has knowledge at the time of execution or termination of this Agreement, or (ii) any entity in which any such relationship with the Vendor and Resources would result in the inevitable use or disclosure of Confidential Information.</p>

<p><span>12. APPLICABLE LAW</span></p>
<p>This Agreement shall be construed, and the legal relations between the Parties hereto shall be determined, in accordance with the Laws of Ahmedabad, India.</p>

<p><span>13.    COUNTERPARTS</span></p>
<p>This Agreement shall be executed in two counterparts, each of which shall be deemed as original but all of this together shall constitute one and the same instrument.</p>


<p><span>14. SEVERABILITY</span></p>
<p>If any provision of this Agreement is held by a court of competent jurisdiction to be contrary to law such provisions shall be severed from this Agreement, and the other remaining provisions of this Agreement shall remain in full force and effect.</p>


<p><span>15. AMENDMENT:</span></p>
<p>Any amendment to this Agreement shall be in writing and signed by the Parties.</p>

<p><span>16. FORCE MAJEURE:</span></p>

<p>Where there is an event of force majeure, the Party prevented from or delayed in performing its obligations under this Agreement must immediately notify the other Party giving full particulars of the event of force majeure and the reasons for the event of force majeure preventing that Party from, or delaying that Party in performing its obligations under this Agreement and that Party must use its reasonable efforts to mitigate the effect of the event of force majeure upon its or their performance of the Agreement and to fulfil its or their obligations under the Agreement. </p>

<p>The “Force majeure” as employed herein shall mean acts of God, strikes, lockouts, terrorist attack, industrial disturbances, war, blockades, insurrections, riots, epidemics, civil disturbances, explosions, fire, floods, earthquake, storms, lightning and any other causes similar to the kind herein which are beyond the control and contemplation of any Party and which by the exercise of due care and diligence any Party is unable to overcome.</p>

<p>If the situation of Force Majeure lasts for a continuous period of more than forty five (45) days, this Agreement can be terminated by mutual consent and in such event, the Party prevented from performing its obligations shall be absolved from performing the balance portion of its obligations under this Agreement.</p>


<p><span>17. WAIVER</span></p>
<p>No waiver of any terms of this Agreement shall be valid unless in writing and designated as such. Any forbearance or delay on the part of either Party in enforcing any of its rights under this Agreement shall not be construed as a waiver of such right to enforce the same for such occurrence or any other occurrence.</p>

<p><span>18.    ENTIRE AGREEMENT</span></p>

<p>This Agreement, including the Appendices attached hereto, sets forth the entire agreement and understanding of the Parties with respect to the subject matter hereof, and supersedes all prior oral and written agreements, understandings, representations, conditions and all other communications relating thereto.</p>

<p><span>IN WITNESS HEREOF the hands of the Parties hereto or their duly authorized representatives on the day and year first above written.</span></p>




             

            </div>
        </div>
        </div>

        <div class="row details">
        <div class="table-form">
            <div class="col">

            <table>
                <tbody>
                <tr>
                    <th colspan="2"><b>Sign and Seal</b></th>
                    <th colspan="2"><b>Sign and Seal</b></th>
                </tr>   
                <tr>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                </tr>
                <tr>
                    <th colspan="2"><b>On Behalf of (“Infiraise”)</b></th>
                    <th colspan="2"><b>On Behalf of (“Vendor”)</b></th>
                </tr>
               

                <tr>
                <td colspan="2">Authorized Signatory</td>
                <td colspan="2">Authorized Signatory</td>
                
                </tr>

                <tr>
                <td colspan="2">Name: </td>
                <td colspan="2">Name: </td>
                </tr>

                <tr>
                <td colspan="2">Title:</td>
                <td colspan="2">Title:</td>
                
                </tr>
                </tbody>
            </table>
            </div>  
        </div>
        </div>
</section>
</body>
</html>
