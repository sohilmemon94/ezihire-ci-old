
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <link rel="icon" type="image/png" sizes="16x16" href="http://localhost/ezihire/images/favicon.ico">
    <title>Infiraise - Vendor Aggrement</title>
    <style>
        section{margin-left:15px !important;margin-right:15px !important;}
        body{
            border:5px solid black;padding-left:10px !important;padding-right:10px !important;margin-left:100px !important;margin-right:100px !important;background: #ffffff;font-family: 'Open Sans', sans-serif;font-size: 14px;font-weight: normal;margin: 15px;
        } 
        section.edu-info {margin: 70px 0;}
        section.experience-info {padding: 30px 0;}
        section.projects {padding: 0 0 30px 0;}
        section.projects .message-item {margin: 40px 0 0 0;}
        .experience h2{margin:10px 0 0 0;}
        .top-head img{text-align:center !important;}
        .top-head p{text-align:center !important;line-height: 24px;margin: 0;color: #000;}
        /* p{margin: 0!important;} */
        h2{font-size: 26px;color: #232323;font-weight: normal;margin: 0px 0 10px 0;}
        h6{font-size: 16px;color: #000;font-family: 'Open Sans', sans-serif;font-weight: 400;margin: 0;line-height: 28px;}
        .employee-info{margin: 40px 0 0 0;}
        .employee-info h2{margin: 0 0 0 0;}
        .employee-info ul {padding: 0;}
        .employee-info ul li {margin: 10px 0;}
        .message-item:before {content: ""; background: #8a8a8a;border-radius: 2px;bottom: -30px;height: 100%;left: -30px;position: absolute;width: 1px;}
        .message-item:after {content: "";background: #000000;border: 2px solid #000000;border-radius: 50%;box-shadow: 0 0 5px rgb(0 0 0 / 10%);height: 15px;left: -36px;position: absolute;top: 10px;width: 15px;}
        .message-item {margin: 24px 0 0 0;position: relative;}
        .message-item .message-inner:before {border-right: 10px solid rgb(0, 0, 0);border-style: solid;border-width: 10px;color: rgba(0,0,0,0);content: "";display: block;height: 0;position: absolute;left: -21px;top: 6px;width: 0;}
        .message-inner{border: 1px solid #797979;border-radius: 3px;padding: 10px;position: relative;}
        .message-item:after {content: "";color: black;border: 2px solid #000000;border-radius: 50%;box-shadow: 0 0 5px rgb(0 0 0 / 10%);height: 15px;left: -39px;position: absolute;top: 10px;width: 15px;}
        .message-item .message-head {border-bottom: 1px solid #656565;margin-bottom: 8px;padding-bottom: 8px;color: #fff;}
        .clearfix:before, .clearfix:after {content: " ";display: table;}
        .clearfix:before, .clearfix:after {content: '\0020';display: block;overflow: hidden;visibility: hidden;width: 0;height: 0;}
        .message-item .message-head .user-detail h5 {font-size: 16px;font-weight: bold;margin: 0;color: #000;}
        .message-item .message-head .post-meta {float: left;padding: 0 15px 0 0;}
        .message-item .message-head .post-meta >div {color: black;font-weight: bold;text-align: right;}
        .post-meta > div {color: black;font-size: 12px;line-height: 22px;}
        .qa-message-content p {line-height: 24px;margin: 0;color: #000;}
        .user-detail{overflow: hidden;}
        .edu-info table {
border-collapse: collapse;
width: 100%;
border: 1px solid black;
}

.edu-info th, td {
  text-align: center;
  padding: 8px;
  border-left: 1px solid black;
  border-top: 1px solid black;
}
/* .table-form tbody tr:nth-child(1),tr:nth-child(2) {background-color: #f2f2f2;} */

.main-content p {text-align: justify;margin:10px 0;line-height: 18px;}
.logo {text-align: center;}
.page-content h6 {font-weight: bold;margin-top: 30px;}
.page-content ol li p {margin: 10px 0;text-align:justify;}
ol.k {padding: 0 0 0 17px;margin:0;}
.details {padding: 20px 0;}
span {font-weight: bold;}
/* .table-form{margin:20px 0;} */

.table-form table{border-collapse: collapse;
width: 100%;
}

.edu-info th, td {
  text-align: center;
  padding: 8px;
  border-left: 1px solid black;
  border-top: 1px solid black;
}   

.table-form th{text-align:left;padding: 8px;text-transform: uppercase;bwidth:50%;}
.table-form th, td{text-align:left;border: none;}
.page-content span {font-weight: normal;font-size: 13px;}
@media screen and (max-width:767px)
{
    .employee-info{margin: 40px 0 0 0;}
}

@media screen and (max-width:576px)
{
    p{font-size: 8px;}
}
.border {
    border: 1px solid black;
}
td {
    text-align: center !important;;
    border-right: 1px solid black !important;
}
</style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<section>        
    <div class="container" >
        
         <div class="row">
            <div class="col-md-12 logo">

                <p><img src="<?php echo base_url(); ?>uploads/logo.png" alt="logo"></p>
               
                <p><b><u>Skills Matrix</u></b></p>
            </div>
        </div>
        <hr>
        <div class="row">
        <div class="table-form">
            <div class="col">
            <table>
                <tbody>
                <tr class="border">
                    <td style="background-color:black;color:#fff;font-size:18px"><b>Technology</b></td>
                    <td style="background-color:black;color:#fff;font-size:18px"><b>Exp 3+</b></td>
                    <td style="background-color:black;color:#fff;font-size:18px"><b>Cost 3+</b></td>
                    <td style="background-color:black;color:#fff;font-size:18px"><b>Exp 5+</b></td>
                    <td style="background-color:black;color:#fff;font-size:18px"><b>Cost +5</b></td>
                    <td style="background-color:black;color:#fff;font-size:18px"><b>Exp 7+</b></td>
                    <td style="background-color:black;color:#fff;font-size:18px"><b>Cost +7</b></td>
                    <td style="background-color:black;color:#fff;font-size:18px"><b>Total</b></td>
                </tr>
                <?php 
                if(count($technologies)>0) {
                    foreach ($technologies as $key => $v) {
                        if($v->total_emp>0) { 

                     ?>
                    <tr>
                    <td class="border" style="text-align:left !important;"><?php echo $v->tname; ?></td>
                    <td class="border"><?php if(isset($v->exp_3_year) && !empty($v->exp_3_year)) { echo $v->exp_3_year; } else { echo "-"; }  ?></td>
                    <td class="border"><?php if(isset($v->exp_3_year_cost) && !empty($v->exp_3_year_cost)) {  echo $v->exp_3_year_cost; } else { echo "-"; }  ?></td>
                    <td class="border"><?php if(isset($v->exp_5_year) && !empty($v->exp_5_year)) {  echo $v->exp_5_year; } else { echo "-"; }  ?></td>
                    <td class="border"><?php if(isset($v->exp_5_year_cost) && !empty($v->exp_5_year_cost)) {  echo $v->exp_5_year_cost; } else { echo "-"; }  ?></td>
                    <td class="border"><?php if(isset($v->exp_7_year) && !empty($v->exp_7_year)) {  echo $v->exp_7_year; } else { echo "-"; }  ?></td>
                    <td class="border"><?php if(isset($v->exp_7_year_cost) && !empty($v->exp_7_year_cost)) {  echo $v->exp_7_year_cost; } else { echo "-"; } ?></td>
                    <td class="border"><?php echo $v->total_emp; ?></td>
                    </tr> 
                <?php }  }
                } ?>

                  
                    
                </tr>
                </tbody>
            </table>
            </div>  
        </div>
        </div>

<hr>
        <div class="row">
            <div class="col-md-12">

                <p style="text-align:center;">
                    <b>Email : <a href="mailto:sales@infiraise.com" target="_blank">sales@infiraise.com</a></b>
                    <b>Contact Us : <a href="tel:917096146146" target="_blank">+91-709-614-6146</a></b>
                    <b>Website : <a href="http://infiraise.com" target="_blank">www.infiraise.com</a></b>
                </p>
            </div>
        </div>
        
</section>
</body>
</html>
