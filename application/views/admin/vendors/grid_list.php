<!DOCTYPE html>
<html lang="en">
    <?php 
        $my_permission = get_my_permission();
    ?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo $title; ?> | Admin - <?php echo $page; ?> <?php echo $action; ?></title>
        <?php $this->load->view("admin/common/common_css"); ?>
       

    </head>

    <body class="fix-header fix-sidebar card-no-border">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    <div class="row page-titles">
                        <div class="col-md-6 col-12 align-self-center">
                            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>reports/vendors_skills_list"><?php echo $page; ?></a></li>
                                <li class="breadcrumb-item active"><?php echo $action; ?></li>
                            </ol>
                        </div>
                       
                    </div>

                      <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">

                                <form role="form" action="<?php echo base_url(); ?>reports/vendors_skills_list" name="form" id="searchRecords"  method="post" class="form-horizontal form-label-left" >
                                    <div class="row">
                                        <div class="col-md-3 margin_left_10 margin_right_responsive_10">
                                                <select class="form-control select2" name="skills" id="skills" required="required">
                                                    <option name="0" id="0" value="0">--- Select Your skills ---</option>
                                                    <?php foreach($technologies as $r=>$v) { ?>
                                                        <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" <?php if($skills == $v->id) { echo "selected=selected"; } ?> id="<?php echo $v->id; ?>"><?php echo $v->tname; ?></option>
                                                    <?php } ?>
                                                </select> 
                                        </div>

                                         <div class="col-md-3 margin_left_10 margin_right_responsive_10" id="exp_year_div">
                                            
                                                <select class="form-control select2" name="exp_year" id="exp_year">
                                                    <option name="0" id="0" value="0">--- Select Your Exp ---</option>
                                                    <option name="3" id="3" <?php if($exp_year == 3) { echo "selected=selected"; } ?> value="3">3+ Year</option>
                                                    <option name="5" id="5" <?php if($exp_year == 5) { echo "selected=selected"; } ?> value="5">5+ Year</option>
                                                    <option name="7" id="7" <?php if($exp_year == 7) { echo "selected=selected"; } ?> value="7">7+ Year</option>
                                                </select> 
                                        </div>


                                        <div class="col-md-3">
                                            <a href="<?php echo base_url(); ?>reports/vendors_skills_list" class="btn btn-danger">Reset</a>
                                            <input type="submit" class="btn btn-info" name="search" value="Search" id="search_customer_list" >

                                        </div>
                                    </div>
                                </form>  
                                    <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                    <h4 class="card-title"></h4>
                                    <h6 class="card-subtitle"></h6>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- .row -->
                    <div class="row" id='postsList'>
                        <!-- .col -->
                        

                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div id='pagination'></div>
                        </div>
                    </div>

                  
                  
                </div>

                <?php $this->load->view("admin/common/common_footer"); ?>

                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
<?php $this->load->view("admin/common/common_js"); ?>
    </body>

</html>

 <script>
    $(document).ready( function() {
        $('#error').delay(7000).fadeOut();
         $("#exp_year_div").hide();

        $('#skills').on('change', function () {
            var skillsss = $(this).val();
            
            if (skillsss != 0) {
                $("#exp_year_div").show();
            } else {
                $("#exp_year_div").hide();
            }
        });
 
    $('#pagination').on('click','a',function(e){
        e.preventDefault(); 
        var pageno = $(this).attr('data-ci-pagination-page');
        loadPagination(pageno);
    });
 
    loadPagination(0);
 
    function loadPagination(pagno){
       $.ajax({
         url: '<?php echo base_url(); ?>reports/fetch_vendors_listview/'+pagno,
         type: 'get',
         data: {skillsid:<?php echo $skills; ?>,exp_id:<?php echo $exp_year; ?>},
         dataType: 'json',
         success: function(response){
            $('#pagination').html(response.pagination);
            console.log(response.result);

            createTable(response.result,response.row);
         }
       });
     }
 
    function createTable(result,sno){
       sno = Number(sno);
       $('#postsList').empty();

       var base_url = '<?php echo base_url(); ?>';
        if(result.length > 0) {
            for(index in result){

                var id = result[index].customer_id;
                var name = result[index].username;
                var ownername = result[index].ownername;
                var address = result[index].address;
                var image = "";
                image = result[index].image;

                var designation = result[index].code;
                var email = result[index].email;
                var mobileno = result[index].mobileno;
                var exp_3_year = result[index].exp_3_year;
                var exp_3_year_cost = result[index].exp_3_year_cost;
                var exp_5_year = result[index].exp_5_year;
                var exp_5_year_cost = result[index].exp_5_year_cost;
                var exp_7_year = result[index].exp_7_year;
                var exp_7_year_cost = result[index].exp_7_year_cost;
                var total_emp = result[index].total_emp;
                var tname = result[index].tname;
     
                 
                  sno+=1;
                  var tr = "";
                  tr += '<div class="col-md-6 col-lg-6 col-xlg-4"><div class="card card-body"><div class="row"><div class="col-md-4 col-lg-3 text-center">'+image+'</div><div class="col-md-8 col-lg-9"><h4 class="mb-0">'+ name +'</h4><b>'+ownername+'</b>-<small>'+designation+'</small><p>'+email+'<br/>'+mobileno+'</p></div></div>';

                if(total_emp) {
                        tr +='<div class="row"><div class="col-md-12 col-lg-12"><table class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%"><tr><th>3+ Exp</th><th>5+ Exp</th><th>7+ Exp</th><th>Total</th><th>Action</th></tr><tr>';

                        // tr +='<td>';
                        // if(tname) {
                        //     tr += tname;
                        // } else {
                        //     tr +='-';
                        // }
                        // tr +='</td>';


                        tr +='<td>';
                      if(exp_3_year) {
                        tr += exp_3_year;
                        if(exp_3_year_cost) {
                            tr += "("+exp_3_year_cost+")";
                        } 
                      } else {
                        tr +='-';
                      }
                      tr +='</td>';

                      tr +='<td>';
                      if(exp_5_year) {
                        tr += exp_5_year;
                        if(exp_5_year_cost) {
                            tr += "("+exp_5_year_cost+")";
                        } 
                      } else {
                        tr +='-';
                      }
                      tr +='</td>';

                      tr +='<td>';
                      if(exp_7_year) {
                        tr += exp_7_year;
                        if(exp_7_year_cost) {
                            tr += "("+exp_7_year_cost+")";
                        } 
                      } else {
                        tr +='-';
                      }
                      tr +='</td>';

                    tr +='<td>';
                      if(total_emp) {
                        tr += total_emp;
                      } else {
                        tr +='-';
                      }
                      tr +='</td>';


                      tr += '<td> <a href="'+base_url+'vendors/vendors_employees_details/'+id+'" target="_blank"';
                       tr += 'data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-eye"></i></a></td>';

                  tr +='</tr></table></div></div></div></div>';
                }

                $('#postsList').append(tr);
            }
        } else {
            var tr = "";
            tr += '<div class="card card-body" id="myElems"><div class="row  page-titles"><div class="col-md-12 col-lg-12">';
                tr +='<div class="alert alert-danger alert-dismissible " role="alert" id="myElem"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Sorry !</strong> Vendors not found </div>';
            tr +='</div></div></div>';
            $('#postsList').append(tr);
        }
    }


        var dataTable = $('#example23').DataTable({  
           "processing":true,  
           "serverSide":true,  
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 0,
                "pagingType": "full_numbers",
                "order":[],  
                "ajax":{  
                    url:"<?php echo base_url() . 'employees/fetch_employees_list'; ?>",  
                    type:"POST",
                    
                },  
           "columnDefs":[  
                {  
                     "order": [[ 1, "desc" ]],
                     "orderable":false,  
                },  
           ],  
        });  
        
        setTimeout(function () {
            $("#myElem").hide();
            $("#myElems").hide();
        }, 3000);

        $("body").on("change",".tgl_checkbox",function(){
            var table = $(this).data("table");
            var status = $(this).data("status");
            var id = $(this).data("id");
            var id_field = $(this).data("idfield");
            var bin=0;

            if($(this).is(':checked')){
                bin = 1;
            }
            $.ajax({
              method: "POST",
              url: "<?php echo site_url("employees/change_status"); ?>",
              data: { table: table, status: status, id : id, id_field : id_field, on_off : bin }
            })
              .done(function( msg ) {
              //  alert(msg);
               if(msg == '1') {
                    $("#myElem").show();
               setTimeout(function() { $("#myElem").hide(); }, 3000);

                } else if(msg == '0') {
                  $("#myElemNo").show();
               setTimeout(function() { $("#myElemNo").hide(); }, 3000);
                }

              }); 
        });

    });

</script>
