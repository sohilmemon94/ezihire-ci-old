<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->

    <title><?php echo PROJECT_NAME; ?> - <?php echo $page; ?> <?php echo $action; ?></title>
    <?php  $this->load->view("admin/common/common_css"); ?>
</head>

  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}

input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}

</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                           <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>vendors/approve_vendors_list"><?php echo $page; ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $action; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                <h4 class="card-title">New <?php echo $page; ?></h4>
                                <h6 class="card-subtitle"></h6>
                                <?php if (validation_errors())
                                {   
                                echo '<div class="alert alert-warning alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning!</strong> ';

                                echo validation_errors();
                                echo '</div>';

                                }
                                ?>
                                 <form class="form" name="form" id="form_edit" role="form" method="post" enctype="multipart/form-data">

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Company Name <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="text" id="username" name="username" value="" placeholder="Company Name">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Owner Name <span class="text-danger">*</span></label>
                                                     <input class="form-control" type="text" id="ownername" name="ownername" value="" placeholder="Owner Name">
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Industry <span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="industry_id" id="industry_id">
                                                    <option>--- Select Industry ---</option>
                                                    <?php foreach($industrys as $r=>$v) { ?>
                                                        <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" id="<?php echo $v->id; ?>"><?php echo $v->iname; ?></option>
                                                    <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Address <span class="text-danger">*</span></label>
                                                    <textarea class="form-control" placeholder="Address" id="address" name="address"></textarea>
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Mobile No <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="number"  id="mobileno" name="mobileno" value="" placeholder="Mobile No">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Altername Number <span class="text-danger">*</span></label>
                                                     <input class="form-control" type="number"  id="alternateno" name="alternateno" value="" placeholder="Altername No">
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Linked In <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="url"  id="linkedin" name="linkedin" value="" placeholder="Linked In">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Skype ID <span class="text-danger">*</span></label>
                                                     <input class="form-control" type="text" id="skypeid" name="skypeid" value="" placeholder="Skype ID">
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Email ID <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="text" id="email" name="email" value="" placeholder="Email ID " >
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Password <span class="text-danger">*</span></label>
                                                    <input class="form-control" type="password" name="password" value="" id="password">
                                                </div>
                                            </div>                                      
                                        </div>

                                         <div class="form-group row">

                                            <div class="col-md-3">
                                                <div class="mb-3">
                                                    <label class="control-label">Agreement Date<span class="text-danger">*</span></label>
                                                    <input class="form-control" type="date" id="agreement_date" name="agreement_date" min="<?php echo date('Y-m-d'); ?>">
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="mb-3">
                                                    <label class="control-label">CIN No <span class="text-danger"></span></label>
                                                    <input class="form-control" type="text" id="cin" name="cin" placeholder="CIN No" >
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="mb-3">
                                                    <label class="control-label">GSTIN No<span class="text-danger"></span></label>
                                                    <input class="form-control" type="text" name="gstin" id="gstin" >
                                                </div>
                                            </div>    

                                            <div class="col-md-3">
                                                <div class="mb-3">
                                                    <label class="control-label">GSTIN Registered Office ? <span class="text-danger"></span></label>
                                                    <input class="form-control" type="text" name="registered_office" id="registered_office" >
                                                </div>
                                            </div>                                  
                                        </div>




                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Profile Pic</label>
                                                   <input type="file" name="vendor_image" id="vendor_image" class="dropify" />
                                                </div>
                                            </div> 
                                            <div class="col-md-6">
                                            </div>
                                        </div>

                                 
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label for="example-text-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Status  <span class="text-danger"></span></label>
                                        
                                                    <input class='tgl tgl-ios tgl_checkbox js-switch' name="status" type='checkbox' />
                                                </div>
                                            </div> 
                                        </div>



                                    <div class="form-group row">
                                     
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                         </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <button type="submit" class="btn btn-info" id="save_button"  name="save_button" value="Insert">Insert</button>
                                            <input type="hidden" name="save_button" id="save_button" value="Insert">
                                            <a href="<?php echo site_url("vendors/approve_vendors_list"); ?>" class="btn btn-inverse">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Validation wizard -->
   
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- footer -->
 <!-- ============================================================== -->

 <?php  $this->load->view("admin/common/common_footer"); ?>

 <!-- ============================================================== -->
 <!-- End footer -->
 <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php  $this->load->view("admin/common/common_js"); ?>
</body>

</html>

<script>
 $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script>
$(function() {
    $("#form_edit").validate({
        // Specify validation rules
        rules: {
            username:{
                required:true,
                remote: {
                    url: "<?php echo base_url(); ?>check_company_name_exist",
                    type: "POST",
                    data: {
                        username: function(){ return $("#username").val(); },
                    }
                },
            },
            ownername:{
                required:true,
            },
            industry_id:{
                required:true,
                min:1,
            },
            address:{
                required:true,
            },
            mobileno:{
                required:true,
                number:true,
            },
            alternateno:{
                required:true,
                number:true,
            },
            linkedin:{
                required:true,
                url:true,
            },
            skypeid:{
                required:true,
            },
            email: {
                required:true,
                email:true,
                remote: {
                    url: "<?php echo base_url(); ?>check_email_exist",
                    type: "POST",
                    data: {
                        email: function(){ return $("#email").val(); },
                    }
                },
            },
            password: {
                required:true,
            },
            agreement_date:{
                required:true,
            },
        },
        messages: {
            username:{
                required:"Please enter company name",
                remote: "Company already exist !",
            },
            ownername:{
                required:"Please enter Owner Name",
            },
            industry_id:{
                required:"Please select type of industry",
                min:"Please select type of industry",
            },
            address:{
                required:"Please enter addresss",
            },
            mobileno:{
                required:"Please enter mobile no",
            },
            alternateno:{
                required:"Please enter mobile no",
            },
            linkedin:{
                required:"Please enter linkedin URL",
            },
            skypeid:{
                required:"Please enter skype ID",
            },
            email: {
                required:"Please enter email",
                email:"Please enter valid email",
                remote: "Email Id already exist !",
            },
            password: {
                required:"Please enter password",
            },
            agreement_date:{
                required:"Please select Agreement Date",
            }
        },
        submitHandler: function(form) {
           form.submit();
        }
    });

    $('#save_button').click(function() {
        if($('#form_edit').valid()){
            $(this).attr('disabled', 'disabled');
            $(this).html('Saving...');
            $(this).parents('form').submit();
        }
    });
});
</script>
