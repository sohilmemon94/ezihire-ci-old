<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->

    <title><?php echo PROJECT_NAME; ?> - <?php echo $page; ?> <?php echo $action; ?></title>
    <?php  $this->load->view("admin/common/common_css"); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}
</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    <div class="row page-titles">
                        <div class="col-md-6 col-12 align-self-center">
                            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                            <ol class="breadcrumb">
                               <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>clients/clients_list"><?php echo $page; ?></a></li>
                               <li class="breadcrumb-item active"><?php echo $action; ?></li>
                           </ol>
                       </div>

                   </div>
                   <!-- ============================================================== -->
                   <!-- End Bread crumb and right sidebar toggle -->
                   <!-- ============================================================== -->
                   <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-outline-info">
                            <div class="card-body">
                               <h4 class="card-title">Edit <?php echo $page; ?></h4>
                               <h6 class="card-subtitle"></h6>
                               <?php if (validation_errors())
                               {   
                                echo '<div class="alert alert-warning alert-dismissible" id="error" role="alert">
                                <i class="fa fa-check"></i>
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <strong>Warning!</strong> ';

                                echo validation_errors();
                                echo '</div>';
                                }
                                ?>
                           
                                    <form class="form" name="form" id="form_edit" role="form" method="post" enctype="multipart/form-data">

                                         <div class="form-group m-t-40 row">
                                            <label for="example-text-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">clients Name <span class="text-danger">*</span></label>
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <input type="text" class="form-control" name="bname" id="bname" placeholder="clients Name" value="<?php echo $clients->bname; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group m-t-40 row">
                                            <label for="example-password-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">clients Image <span class="text-danger"></span></label>
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <input type="file" name="clients_images" id="clients_images"  class="dropify" accept="image/jpeg, image/png, image/jpg,image/svg,image/webp" />
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <img src="<?php echo $this->config->item('clients_images_uploaded_path').$clients->bimage; ?>" style="height:50px; width: 50px;">
                                            </div>
                                        </div>   
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Status <span class="text-danger">*</span></label>
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                               <input class='tgl tgl-ios tgl_checkbox js-switch' name="status" type='checkbox' <?php echo ($clients->status=='1')? "checked" : ""; ?> />
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-lg-2 col-md-2 col-sm-2">
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4">
                                                <button type="submit" class="btn btn-info" id="save_button"  name="save_button" value="Update">Update</button>
                                                 <a href="<?php echo site_url("clients/clients_list"); ?>" class="btn btn-inverse">Cancel</a>
                                            </div>
                                        </div>
                                    </form>
                 </div>
             </div>
         </div>
     </div>
     <!-- /.row -->

 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- footer -->
 <!-- ============================================================== -->

 <?php  $this->load->view("admin/common/common_footer"); ?>

 <!-- ============================================================== -->
 <!-- End footer -->
 <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php  $this->load->view("admin/common/common_js"); ?>
<script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
<script>
CKEDITOR.replace('address', {
filebrowserUploadUrl: 'https://myreikeylife.com/admin/ck_upload.php',
filebrowserUploadMethod: 'form'
});
</script>
</body>

</html>

<script>
  // Wait for the DOM to be ready
$(function() {
    $("#form_edit").validate({
        // Specify validation rules
        rules: {
            bname:{
                required:true,
            },
        },
        messages: {
            bname:{ required:"Please enter clients name"},
        },
        submitHandler: function(form) {
          form.submit();
        }
    });
});

</script>

