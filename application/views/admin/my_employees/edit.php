<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->

    <title><?php echo PROJECT_NAME; ?> - <?php echo $page; ?> <?php echo $action; ?></title>
    <?php  $this->load->view("admin/common/common_css"); ?>
</head>

  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}

input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}

</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                           <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>employees/my_employees_list"><?php echo $page; ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $action; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                <h4 class="card-title">Edit <?php echo $page; ?></h4>
                                <h6 class="card-subtitle"></h6>
                                <?php if (validation_errors())
                                {   
                                echo '<div class="alert alert-warning alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning!</strong> ';

                                echo validation_errors();
                                echo '</div>';

                                }
                                
                                $exist_vendor_id = $admin_id;
                                $is_access = "No";
                                if($customer_role_type == "V") {
                                    if($admin_id == $exist_vendor_id) {
                                        $is_access = "Yes";
                                    }
                                } else if($customer_role_type == "A") {
                                    $is_access = "No";
                                } else {
                                    $is_access = "No";
                                }
                                
                                ?>
                                <form class="form" name="form" id="form_edit" role="form" method="post" enctype="multipart/form-data">

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="control-label">Vendor Name <span class="text-danger">*</span></label>
                                                <select class="form-control select2" name="vendor_id" id="vendor_id"> 
                                                    <option>--- Select Vendor ---</option>
                                                    <?php foreach($vendors as $r=>$v) { 
                                                        if($is_access == "Yes") {
                                                            if($v->customer_id == $admin_id) {
                                                        ?>
                                                            <option value="<?php echo $v->customer_id; ?>" name="<?php echo $v->customer_id; ?>" id="<?php echo $v->customer_id; ?>"  <?php if($v->customer_id == $employees->customers_id) { echo "selected=selected"; } ?> <?php if($v->customer_id == $admin_id) { ?> selected="selected" <?php } ?>><?php echo $v->username; ?></option>
                                                            <?php }
                                                        } else { ?>
                                                            <option value="<?php echo $v->customer_id; ?>" name="<?php echo $v->customer_id; ?>" id="<?php echo $v->customer_id; ?>" <?php if($v->customer_id == $employees->customers_id) { echo "selected=selected"; } ?> ><?php echo $v->username; ?></option> 
                                                    <?php } } ?>
                                            </select>
                                            </div>
                                            </div>
                                        
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="control-label">Employee Name <span class="text-danger">*</span></label>
                                                 <input class="form-control" type="text" id="name" name="name" value="<?php echo $employees->name; ?>" placeholder="Employee Name">
                                            </div>
                                        </div> 
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="control-label">Skills <span class="text-danger">*</span></label>
                                                <select class="select2 form-control" name="skill_id" id="skills" required="required"  style="height: 36px;width: 100%;">
                                                <option>--- Select Your skills ---</option>
                                                <?php foreach($technologies as $r=>$v) { ?>
                                                    <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" <?php if($v->id == $employees->primary_skills) { echo "selected=selected"; } ?> id="<?php echo $v->id; ?>"><?php echo $v->tname; ?></option>
                                                <?php } ?>
                                                </select> 
                                            </div>
                                        </div>    

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="control-label">Gender <span class="text-danger">*</span></label>
                                                    <select class="form-control" name="gender" id="gender">
                                                        <option name="Male" <?php if($employees->gender == "Male") { echo "selected=selected"; } ?> id="Male" value="Male" >Male</option>
                                                            <option name="Female"  <?php if($employees->gender == "Female") { echo "selected=selected"; } ?> id="Female" value="Female">Female</option>
                                                    </select>                                                     
                                            </div>
                                        </div>      
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="control-label">Budget<span class="text-danger">*</span></label>
                                                <select class="form-control select2" name="budget" id="budget">
                                                <option>--- Select Job Budget ---</option>
                                                <?php for($i=5000;$i<=200000;$i=$i+5000) { ?>
                                                    <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>" <?php if($i == $employees->budget) { echo "selected=selected"; } ?>><?php echo $i; ?></option>
                                                <?php } ?>
                                                </select> 
                                            </div>
                                        </div>       

                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="control-label">Candidate Availibity <span class="text-danger">*</span></label>
                                                <select class="form-control select2" name="candidate_availability" id="candidate_availability">
                                                    <option value="immediate" name="immediate" id="immediate" <?php if("immediate" == $employees->candidate_availability) { echo "selected=selected"; } ?> >Immediate</option>
                                                        <option value="7 days" name="7 days" id="7 days" <?php if("7 days" == $employees->candidate_availability) { echo "selected=selected"; } ?>>7 Days</option>
                                                        <option value="15 days" name="15 days" id="15 days" <?php if("15 days" == $employees->candidate_availability) { echo "selected=selected"; } ?>>15 Days</option>
                                                        <option value="30 days" name="30 days" id="30 days" <?php if("30 days" == $employees->candidate_availability) { echo "selected=selected"; } ?>>30 Days</option>
                                                </select>
                                            </div>
                                        </div>                                       
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label class="control-label">Resume<span class="text-danger"></span>( only PDF,Doc Files )</label>
                                               <input type="file" name="resume" id="resume" class="dropify"  />
                                            </div>
                                        </div> 
                                        <div class="col-md-6">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                     
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <button type="submit" class="btn btn-info" id="save_button"  name="save_button" value="Update">Update</button>
                                            <input type="hidden" name="save_button" id="save_button" value="Update">
                                            <a href="<?php echo site_url("employees/my_employees_list"); ?>" class="btn btn-inverse">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Validation wizard -->
   
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- footer -->
 <!-- ============================================================== -->

 <?php  $this->load->view("admin/common/common_footer"); ?>

 <!-- ============================================================== -->
 <!-- End footer -->
 <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php  $this->load->view("admin/common/common_js"); ?>
</body>

</html>

<script>
 $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script>
$(function() {
    $("#form_edit").validate({
        // Specify validation rules
        rules: {
            vendor_id:{
                required:true,
                min:1,
            },
            skill_id:{
                required:true,
                min:1,
            },
            subskills:{
                required:true,
            },
            name:{
                required:true,
            },
            budget:{
                required:true,
                min:1,
            },
            candidate_availability: {
                required:true,
            },
        },
        messages: {
            vendor_id:{
                required:"Please select Vendor",
                min:"Please select Vendor",
            },
            skill_id:{
                required:"Please select job skill",
                min:"Please select job skill",
            },
            budget:{
                required:"Please select budget",
                min:"Please select budget",
            },

            name:{
                required:"Please enter employee name",
            },           
            candidate_availability: {
                required:"Please select candidate availability",
            },
        },
        submitHandler: function(form) {
           form.submit();
        }
    });

    $('#save_button').click(function() {
        if($('#form_edit').valid()){
            $(this).attr('disabled', 'disabled');
            $(this).html('Saving...');
            $(this).parents('form').submit();
        }
    });
});
</script>
