<!DOCTYPE html>
<html lang="en">
    <?php 
        $my_permission = get_my_permission();
    ?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo PROJECT_NAME; ?> - <?php echo $page; ?> <?php echo $action; ?></title>
        <?php $this->load->view("admin/common/common_css"); ?>
    </head>

    <body class="fix-header fix-sidebar card-no-border">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    <div class="row page-titles">
                        <div class="col-md-6 col-12 align-self-center">
                            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>education/education_list"><?php echo $page; ?></a></li>
                                <li class="breadcrumb-item active"><?php echo $action; ?></li>
                            </ol>
                        </div>
                       
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">



                                    <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                     <div class="alert alert-success alert-dismissible " role="alert" id="myElem" style="display:none">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <strong>Success !</strong> Education activated successfully. </div>
                                    <div class="alert alert-danger alert-dismissible " role="alert" id="myElemNo"  style="display:none">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <strong>Success !</strong> Education deactivated successfully. </div>

                                   
                                    <h4 class="card-title">
                                        <?php if(in_array('add_education', $my_permission)) { ?>

                                            <div class="pull-right">
                                                <a href="<?php echo site_url("education/add_education"); ?>" data-toggle="tooltip"  data-placement="left"  title="Add" class="btn btn-info" style="color: rgb(255, 255, 255); padding: 5px 13px;"><i class="fa fa-plus"></i></a>
                                            </div>
                                        <?php } ?>  

                                    </h4>
                                    <h6 class="card-subtitle"></h6>
                                    <div class="table-responsive m-t-40">
                                        <div id="buttons"></div>
                                        <br>
                                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Name</th>
                                                    <th>Status</th>
                                                    <th>Created Date</th>
                                                    <th>Updated Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
 
                                                
                                            <?php
                                            if(in_array('education_list', $my_permission)) {

                                                $no = 1;
                                                if (count($educations) > 0) {
                                                    foreach ($educations as $education) {
                                                    $edit_url =  base_url() . 'education/edit_education/' . $education->id;
                                                    $delete_url =  base_url() . 'education/education_delete/' . $education->id;
                                                    
                                                   
                                                        ?>  
                                                        <tr>
                                                           
                                                            <td><?php echo $no; $no = $no+1; ?></td>
                                                            <td><?php echo $education->ename; ?></td>
                                                            
                                                            <td>
                                                                 <?php 
                                                                $readonly = "";
                                                                $disabled = "";
                                                                  if(!in_array('edit_education', $my_permission)) { 
                                                                        $readonly .="readonly ";
                                                                        $disabled .="disabled ";
                                                                    ?>
                                                                  <?php } ?>
                                                                <input type="checkbox" class="js-switch tgl_checkbox" data-color="#55ce63" data-table="tbl_education" data-status="status" data-idfield="id"  data-id="<?php echo $education->id; ?>" id='cb_<?php echo $education->id; ?>'  <?php echo $readonly; echo $disabled;  echo ($education->status == '1') ? "checked" : ""; ?> />
                                                            </td>
                                                            <td>
                                                                <?php
                                                                $date = date("d-M-Y", strtotime($education->created_at));
                                                                $time = date("H:i:s", strtotime($education->created_at));
                                                                echo $date . " " . $time;
                                                                ?>
                                                            </td>
                                                             <td>
                                                                <?php
                                                                $date = date("d-M-Y", strtotime($education->updated_at));
                                                                $time = date("H:i:s", strtotime($education->updated_at));
                                                                echo $date . " " . $time;
                                                                ?>
                                                            </td>
                                                            <td>  
                                                                <?php  if(in_array('edit_education', $my_permission)) { ?>
                                                                <a href="<?php echo $edit_url; ?>" data-toggle="tooltip"  data-placement="left" title="Edit" class="btn btn-success"><i class="fa fa-edit"></i></a>
                                                                 <?php } ?>

                                                                <?php  if(in_array('education_delete', $my_permission)) { ?>
                                                                    <a href="javascript:void(0);" onclick="confirm_delete('<?php echo $delete_url; ?>')" data-toggle="tooltip"  data-placement="left" title="Delete" class="btn btn-danger"><i class="fa fa-trash"></i></a> 
                                                                <?php } ?>
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    }
                                                } 

                                            } else { ?> 
                                            <tr><td colspan="6">
                                                You have not access to module.
                                            </td></tr>
                                            <?php } ?>
                                            
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php $this->load->view("admin/common/common_footer"); ?>

                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
<?php $this->load->view("admin/common/common_js"); ?>
    </body>

</html>


<script>
    $(document).ready(function () {
        $('#error').delay(3000).fadeOut();
    });

    $(function () {

        $("body").on("change", ".tgl_checkbox", function () {
            var table = $(this).data("table");
            var status = $(this).data("status");
            var id = $(this).data("id");
            var id_field = $(this).data("idfield");
            var bin = 0;

            if ($(this).is(':checked')) {
                bin = 1;
            }

            /*               alert(table+'---'+status+'---'+id+'---'+id_field+'---'+bin);
             return false; */
            $.ajax({
                method: "POST",
                url: "<?php echo site_url("admin/change_status"); ?>",
                data: {table: table, status: status, id: id, id_field: id_field, on_off: bin}
            })
                    .done(function (msg) {
                        //  alert(msg);
                        if (msg == '1') {
                            $("#myElem").show();
                            setTimeout(function () {
                                $("#myElem").hide();
                            }, 3000);

                        } else if (msg == '0') {
                            $("#myElemNo").show();
                            setTimeout(function () {
                                $("#myElemNo").hide();
                            }, 3000);
                        }

                    });
        });

        var table = $('#example23').DataTable({
            "order": [[ 0, "ASC" ]]
        });
     
        // var buttons = new $.fn.dataTable.Buttons(table, {
        //      buttons: [
        //        'excelHtml5',
        //        'pdfHtml5'
        //     ]
        // }).container().appendTo($('#buttons'));
    });

</script>
