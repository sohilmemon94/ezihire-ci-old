<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo $title; ?> | Admin - <?php echo $page; ?> <?php echo $action; ?></title>
        <?php $this->load->view("admin/common/common_css"); ?>
      
    </head>

  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}

input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}

</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
       <?php $this->load->view("admin/common/common_header"); ?>

                    
                            <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
         <?php $this->load->view("admin/common/common_sidebar"); ?>




</div>        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>customers/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>customers/customers_view"><?php echo $page; ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $action; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <!-- Validation wizard -->
                <div class="row" id="validation">
                    <div class="col-12">
                        <div class="card wizard-content">
                            <div class="card-body">
                                <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                 <h4 class="card-title">View <?php echo $page; ?></h4>
                                <h6 class="card-subtitle"></h6>
                                 <?php if (validation_errors())
                                {   
                                echo '<div class="alert alert-warning alert-dismissible" id="error" role="alert">
                                                                        <i class="fa fa-check"></i>
                                                                      <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                      <strong>Warning!</strong> ';

                                echo validation_errors();
                                echo '</div>';
 
                                }
                                ?>
                                 
                                  
                        <form class="form" name="form" id="form_edit" role="form" method="post" enctype="multipart/form-data">

                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label>Name<span class="text-danger">*</span></label>
                                     <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name"value="<?php echo $household->name; ?>" disabled="disabled">
                                
                                </div>
                                <!-- /.form-group -->
                              </div>
                             <div class="col-md-6">
                                <div class="form-group">
                                  <label>Address<span class="text-danger">*</span></label>
                                <textarea class="form-control" rows="2" placeholder="Enter ..." id="address" name="address"  disabled="disabled"><?php echo $household->address; ?></textarea>
                                     
                                      </div>

                                <!-- /.form-group -->
                              </div>
                           
                            </div><!-- row -->

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                      <label>GEO Code<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="geo_code" name="geo_code" placeholder="Enter Geo Code" value="<?php echo $household->geo_code; ?>"   disabled="disabled">

                                        

                                      <br><br>
                                      <?php 

                                        $geo_code = $household->geo_code;
                                        $latlong = explode(',',$geo_code);
                                        $lat = $long = "0.00";
                                        $lat = $latlong[0];
                                        $long = $latlong[1];


                                      ?>
                                    <div class="map" id="map" style="width: auto; height: 250px;"></div>
                                    <input type="hidden" name="vCurrentLatitude" id="lat" value="<?php echo $lat; ?>">
                                    <input type="hidden" name="vCurrentLongitude" id="lng" value="<?php echo $long; ?>">


                                    </div>
                                    <!-- /.form-group -->
                                </div>

                                <div class="col-md-6">
                <div class="form-group">
                  <label>Phone</label>
                     <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="fa fa-phone"></i></span>
                    </div>
                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $household->phone; ?>" disabled="disabled" >
                  </div>
                  <!-- /.input group -->
                
                </div>
                <!-- /.form-group -->
                </div>

                            </div><!-- row -->



                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group">
                               <label>Location Type<span class="text-danger">*</span></label>
                                 <select class="form-control select2" id="location_type" name="location_type" style="width: 100%;" disabled="disabled">
                                  <option>--- Select Location --- </option>
                                  <?php if(count($locations)>0) { 
                                      foreach ($locations as $key => $value) { ?>
                                          <option id="<?php echo $value->id; ?>" name="<?php echo $value->id; ?>" value="<?php echo $value->id; ?>" <?php if($household->location_id == $value->id) { echo "selected=selected"; } ?>><?php echo $value->name; ?></option>
                                      <?php }
                                      } ?>
                                  
                                </select>
                                 

                                </div>
                                </div>
                                <div class="col-md-6">
                  <div class="form-group">
                  <label>No. Of Member</label>
                     <input type="text" class="form-control" id="no_of_member" name="no_of_member" placeholder="" disabled="disabled" value="<?php echo $household->no_of_member; ?>">
                
                </div>
                                </div>
                            </div><!-- row -->




                          


                            <div class="form-group row">
                                <div class="col-lg-4 col-md-4 col-sm-4">

                                    <a href="<?php echo site_url("household/household_list"); ?>" class="btn btn-inverse">Back</a>
                                </div>
                            </div>
                        </form>



                                  
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <?php  $this->load->view("admin/common/common_footer"); ?>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

   <?php $this->load->view("admin/common/common_js"); ?>
</body>
</html>


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=<?php echo API_KEY; ?>&libraries=places"></script>

<script>
    $(document).ready(function () {
        $('#error').delay(3000).fadeOut();
    });

    $(function () {      

        var lat = document.getElementById('lat').value;
        var lng = document.getElementById('lng').value;

        var latlng = new google.maps.LatLng(lat,lng);
        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });

        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -14)
        });
        var infowindow = new google.maps.InfoWindow();   
        google.maps.event.addListener(marker, 'click', function() {
          infowindow.open(map, marker);
        });

    });
</script>

