<!DOCTYPE html>
<html lang="en">
    <?php 
        $my_permission = get_my_permission();
    ?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo $title; ?> | Admin - <?php echo $page; ?> <?php echo $action; ?></title>
        <?php $this->load->view("admin/common/common_css"); ?>
       

    </head>

    <body class="fix-header fix-sidebar card-no-border">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    <div class="row page-titles">
                        <div class="col-md-6 col-12 align-self-center">
                            <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>interviewschedule/interviewschedule_report_list"><?php echo $page; ?></a></li>
                                <li class="breadcrumb-item active"><?php echo $action; ?></li>
                            </ol>
                        </div>
                       
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">

                                    <?php 
                                    $params_start_date = $params_end_date = NULL;
                                    if(isset($start_date) && !empty($start_date)) { 
                                        $params_start_date = date('Y-m-d H:i',strtotime($start_date)); 
                                    }

                                    if(isset($end_date) && !empty($end_date)) { 
                                        $params_end_date = date('Y-m-d H:i',strtotime($end_date));
                                    }
                                    ?>

                                    <form role="form" action="" name="form" id="searchRecords"  method="post" class="form-horizontal form-label-left" >
                                        <div class="row">
                                            <div class="col-md-2 margin_left_10 margin_right_responsive_10">
                                                    <select class="form-control select2" name="skills" id="skills" required="required">
                                                        <option name="0" id="0" value="0">--- Select skills ---</option>
                                                        <?php foreach($technologies as $r=>$v) { ?>
                                                            <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" id="<?php echo $v->id; ?>"><?php echo $v->tname; ?></option>
                                                        <?php } ?>
                                                    </select> 
                                            </div>
                                            <?php if($customer_role_type != "V") { ?>
                                            <div class="col-md-2 margin_left_10 margin_right_responsive_10">
                                                    <select class="form-control select2" name="created_by" id="created_by" required="required">
                                                        <option name="0" id="0" value="0">--- Select User ---</option>
                                                        <?php foreach($vendors as $r=>$v) { ?>
                                                            <option value="<?php echo $v->customer_id; ?>" name="<?php echo $v->customer_id; ?>" id="<?php echo $v->customer_id; ?>"><?php echo $v->username; ?></option>
                                                        <?php } ?>
                                                    </select> 
                                            </div>  
                                             <?php } ?>

                                             <div class="col-md-2 margin_left_10 margin_right_responsive_10">

                                                    <select class="form-control select2" name="status" id="status" required="required">
                                                        <option name="0" id="0" value="0">--- Interview Status ---</option>

                                                        <option name="Proposed" id="Proposed" value="Proposed" >Proposed</option>

                                                        <option name="Round 1" id="Round 1" value="Round 1" >Round 1</option>

                                                        <option name="Round 2" id="Round 2" value="Round 2" >Round 2</option>

                                                        <option name="Round 1 Awaiting" id="Round 1 Waiting Awaiting" value="Round 1 Awaiting" >Round 1 Waiting</option>

                                                        <option name="Round 2 Awaiting" id="Round 2 Awaiting" value="Round 2 Awaiting" >Round 2 Awaiting</option>

                                                        <option name="Round 1 Reject" id="Round 1 Reject" value="Round 1 Reject" >Round 1 Reject</option>

                                                        <option name="Round 2 Reject" id="Round 1 Reject" value="Round 2 Reject" >Round 2 Reject</option>

                                                        <option name="Selected" id="Selected" value="Selected" >Selected</option>

                                                        <option name="Hold" id="Hold" value="Hold" >Hold</option>

                                                        <option name="Close" id="Close" value="Close">Close</option>

                                                    </select>

                                             </div>   
                                            <div class="col-md-2 input-group date margin_left_10 margin_right_responsive_10" id="myDatepicker3">
                                                <input type="date" class="form-control required" id="start_date" name="start_date" placeholder="start date" value="<?php echo $params_start_date; ?>">
                                            </div>

                                            <div class="col-md-2 input-group date margin_left_10 margin_right_responsive_10" id="myDatepicker4">
                                                <input type="date" class="form-control required" id="end_date" name="end_date" placeholder="end date" value="<?php echo $params_end_date; ?>">
                                            </div>

                                            <div class="col-md-2">
                                                <a href="<?php echo base_url(); ?>interviewschedule/interviewschedule_report_list" class="btn btn-danger">Reset</a>
                                                <input type="submit" class="btn btn-info" name="search" value="Search" id="search_customer_list" >

                                            </div>
                                        </div>
                                    </form>  

                                    <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                  
                                    
                                    <h4 class="card-title">
                                        
                                       
                                    </h4>

                                    <div class="alert alert-success alert-dismissible " role="alert" id="myElem" style="display:none">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <strong>Success !</strong> Post Job activated successfully. </div>
                                    <div class="alert alert-danger alert-dismissible " role="alert" id="myElemNo"  style="display:none">
                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                        <strong>Success !</strong> Post Job deactivated successfully. </div>

                                    <h6 class="card-subtitle"></h6>
                                    <div class="table-responsive">
                                        <div id="buttons"></div>
                                        <br>
                                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date&nbsp;1</th>
                                                    <th>Date&nbsp;2</th>
                                                    <th>Post&nbsp;By</th>
                                                    <th>Vendor</th>
                                                    <th>Candidate&nbsp;Name</th>
                                                    <th>Skills</th>
                                                    <th width="5%">Status</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>
                                      

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                  
                </div>

                <?php $this->load->view("admin/common/common_footer"); ?>

                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
<?php $this->load->view("admin/common/common_js"); ?>
    </body>

</html>

 <script>
    $(document).ready( function() {

         $("body").on("change","#start_date",function(){
            var start_date =  $("#start_date").val();
            var min_date = start_date;
            $("#end_date").attr({"min" : min_date});
        });

        $('#search_customer_list').click(function(e){
            e.preventDefault();
            $('#example23').DataTable().ajax.reload();
        });

        $('#error').delay(7000).fadeOut();

       
        var dataTable = $('#example23').DataTable({  
           "processing":true,  
           "serverSide":true,  
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 0,
                "pagingType": "full_numbers",
                "order":[],  
                "ajax":{  
                    url:"<?php echo base_url() . 'interviewschedule/fetch_interviewschedule_report_list'; ?>",  
                    type:"POST",
                    data: function (e) {
                        e.extra_search = $("#searchRecords").serializeArray()
                    },
                    
                },  
           "columnDefs":[  
                {  
                     "order": [[ 1, "desc" ]],
                     "orderable":false,  
                },  
           ],  
           
        });  

        
        var buttons = new $.fn.dataTable.Buttons(dataTable, {
             buttons: [
               'excelHtml5',
               'pdfHtml5',
               'csvHtml5',
            ]
        }).container().appendTo($('#buttons'));

        $("body").on("change",".tgl_checkbox",function(){
            var table = $(this).data("table");
            var status = $(this).data("status");
            var id = $(this).data("id");
            var id_field = $(this).data("idfield");
            var bin=0;

            if($(this).is(':checked')){
                bin = 1;
            }
            $.ajax({
              method: "POST",
              url: "<?php echo site_url("interviewschedule/change_status"); ?>",
              data: { table: table, status: status, id : id, id_field : id_field, on_off : bin }
            })
              .done(function( msg ) {
              //  alert(msg);
               if(msg == '1') {
                    $("#myElem").show();
               setTimeout(function() { $("#myElem").hide(); }, 3000);

                } else if(msg == '0') {
                  $("#myElemNo").show();
               setTimeout(function() { $("#myElemNo").hide(); }, 3000);
                }

              }); 
        });

    });

</script>
