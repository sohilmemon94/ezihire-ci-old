<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->

    <title><?php echo PROJECT_NAME; ?> - <?php echo $page; ?> <?php echo $action; ?></title>
    <?php  $this->load->view("admin/common/common_css"); ?>
</head>

  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}

input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}

</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    
                    
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>interviewschedule/interviewschedule_list"><?php echo $page; ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $action; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <!-- Validation wizard -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                <h4 class="card-title">New <?php echo $page; ?></h4>
                                <h6 class="card-subtitle"></h6>
                                <?php if (validation_errors())
                                {   
                                echo '<div class="alert alert-warning alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning!</strong> ';

                                echo validation_errors();
                                echo '</div>';

                                }
                                
                                $exist_vendor_id = $admin_id;
                                $is_access = "No";
                                if($customer_role_type == "V") {
                                    if($admin_id == $exist_vendor_id) {
                                        $is_access = "Yes";
                                    }
                                } else if($customer_role_type == "A") {
                                    $is_access = "No";
                                } else {
                                    $is_access = "No";
                                }
                                
                                ?>
                                 <form class="form" name="form" id="form_edit" role="form" method="post" enctype="multipart/form-data">

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Post Job <span class="text-danger">*</span></label>
                                                    <select class="form-control select2" name="postjob_id" id="postjob_id" readonly="readonly" disabled="disabled"> 
                                                    <option>--- Select Post Job ---</option>
                                                    <?php foreach($postjobs as $r=>$v) { 
                                                        
                                                        ?>
                                                            <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" id="<?php echo $v->id; ?>" <?php if($v->id == $interviewschedule->postjob_id) { echo "selected=selected"; } ?> ><?php echo $v->username." - ".$v->tname." - ".date('d-M-Y',strtotime($v->post_date)); ?></option>
                                                        <?php } ?>
                                                    </select> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Candidate Name <span class="text-danger">*</span></label>
                                                    <?php echo $interviewschedule->candidate_name; ?>

                                                </div>
                                            </div> 
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">First Interview Date<span class="text-danger">*</span></label>
                                                    <?php echo date('d-M-Y',strtotime($interviewschedule->interview_one_date)); ?>
                                                </div>
                                            </div>                                      
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">

                                                    <?php 
                                                    if(isset($interviewschedule->interview_two_date) && !empty($interviewschedule->interview_two_date)) {
                                                        $interview_two_date =  date('Y-m-d',strtotime($interviewschedule->interview_two_date)); 
                                                    } else {
                                                        $interview_two_date = "";
                                                    } ?>
                                                    <label class="control-label">Second Interview Date<span class="text-danger"></span></label>
                                                    <input type="date" name="interview_date" id="interview_date" class="form-control" value="<?php echo $interview_two_date; ?>">
                                                </div>
                                            </div>                                      
                                        </div>


                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Interview Comment<span class="text-danger">*</span></label>
                                                    <textarea cols="5" class="form-control" name="comment" id="comment"><?php echo $interviewschedule->comment; ?></textarea>
                                                </div>
                                            </div> 
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="control-label">Interview Status<span class="text-danger">*</span></label>
                                                    <select class="form-control" name="status" id="status" required="required">
                                                        <option>--- Select Interview Status</option>

                                                        <option name="Round 1" id="Round 1" value="Round 1" <?php if($interviewschedule->status == "Round 1") { echo "selected=selected"; } ?>>Round 1</option>

                                                        <option name="Round 2" id="Round 2" value="Round 2" <?php if($interviewschedule->status == "Round 2") { echo "selected=selected"; } ?>>Round 2</option>

                                                        <option name="Round 1 Awaiting" id="Round 1 Waiting Awaiting" value="Round 1 Awaiting" <?php if($interviewschedule->status == "Round 1 Waiting") { echo "selected=selected"; } ?>>Round 1 Waiting</option>

                                                        <option name="Round 2 Awaiting" id="Round 2 Awaiting" value="Round 2 Awaiting" <?php if($interviewschedule->status == "Round 2 Awaiting") { echo "selected=selected"; } ?>>Round 2 Awaiting</option>

                                                        <option name="Round 1 Reject" id="Round 1 Reject" value="Round 1 Reject" <?php if($interviewschedule->status == "Round 1 Reject") { echo "selected=selected"; } ?>>Round 1 Reject</option>

                                                        <option name="Round 2 Reject" id="Round 1 Reject" value="Round 2 Reject" <?php if($interviewschedule->status == "Round 2 Reject") { echo "selected=selected"; } ?>>Round 2 Reject</option>

                                                        <option name="Selected" id="Selected" value="Selected" <?php if($interviewschedule->status == "Selected") { echo "selected=selected"; } ?>>Selected</option>

                                                        <option name="Hold" id="Hold" value="Hold" <?php if($interviewschedule->status == "Hold") { echo "selected=selected"; } ?>>Hold</option>

                                                        <option name="Close" id="Close" value="Close" <?php if($interviewschedule->status == "Close") { echo "selected=selected"; } ?>>Close</option>

                                                    </select>
                                                </div>
                                            </div> 
                                        </div>


                                    <div class="form-group row">
                                     
                                       
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <button type="submit" class="btn btn-info" id="save_button"  name="save_button" value="Update">Update</button>
                                            <input type="hidden" name="save_button" id="save_button" value="Update">
                                            <a href="<?php echo site_url("interviewschedule/interviewschedule_list"); ?>" class="btn btn-inverse">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
  
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- footer -->
 <!-- ============================================================== -->

 <?php  $this->load->view("admin/common/common_footer"); ?>

 <!-- ============================================================== -->
 <!-- End footer -->
 <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php  $this->load->view("admin/common/common_js"); ?>
</body>

</html>


<script>
 $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script>
    $(function() {

        $(".imagedelete").on("click",function(e){
            e.preventDefault();
            var id =$(this).attr('id');
            if(id!="" && id!=0){
                $.ajax({
                    url: "<?php echo site_url("interviewschedule/deleteimages");?>",
                    type: "post",
                    data: {
                    'id': id
                    },
                    success: function (response) {
                        if(response==1){
                            $("#myElem").show();
                            setTimeout(function() { $("#myElem").hide(); }, 3000);
                            location.reload();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        });

        $("#form_edit").validate({
        // Specify validation rules
        rules: {
            comment:{
                required:true,
            },
            status:{
                required:true,
            },
        },
        messages: {
            comment:{
                required:"Please enter comment",
            },
            status:{
                required:"Please select status",
            },
            
        },
        submitHandler: function(form) {
           form.submit();
        }
    });

    $('#save_button').click(function() {
        if($('#form_edit').valid()){
            $(this).attr('disabled', 'disabled');
            $(this).html('Saving...');
            $(this).parents('form').submit();
        }
    });
    
});
</script>

