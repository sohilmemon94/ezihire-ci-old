  <!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo $title; ?> | Admin - <?php echo $page; ?> <?php echo $action; ?></title>
        <?php $this->load->view("admin/common/common_css"); ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    </head>

    <body class="fix-header fix-sidebar card-no-border">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
              <!-- ============================================================== -->
              <!-- Container fluid  -->
              <!-- ============================================================== -->
              <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->

                <div class="row page-titles">
                  <div class="col-md-6 col-12 align-self-center">
                    <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>category/category_list"><?php echo $page; ?></a></li>
                      <li class="breadcrumb-item active"><?php echo $action; ?></li>
                    </ol>
                  </div>

                </div>

                <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <div class="pull-right">
                                    <a href="<?php echo base_url(); ?>category/category_list" class="btn btn-info" style="color: rgb(255, 255, 255); padding: 5px 13px;"><i class="fa fa-arrow-circle-left"></i></a>
                                    </div>

                                    </h4>
                                    <h6 class="card-subtitle"></h6>
                                    <div class="table-responsive m-t-40">

                                        <h3>User Details</h3>
                                         <form role="form" name="form" id="demo-form2" action="" method="post" data-parsley-validate="" class="form-horizontal form-label-left" enctype="multipart/form-data">
                                        <table class="table table-hover table-bordered detail-view">
                                            <tbody>
                                                <tr>
                                                    <td width="25%">Name</td>
                                                    <td width="75%"><?php echo $customers->name; ?></td>
                                                </tr>

                                                <tr>
                                                    <td width="25%">customers Email</td>
                                                    <td width="75%"><?php echo $customers->email; ?></td>
                                                </tr>

                                               
                                                <tr>
                                                    <td width="25%">Mobile</td>
                                                    <td width="75%"><?php echo $customers->country_code."&nbsp;". $customers->mobile; ?></td>
                                                </tr>

                                                <tr>
                                                    <td width="25%">Mobile OTP</td>
                                                    <td width="75%"><?php echo $customers->mobile_otp; ?></td>
                                                </tr>

                                                <tr>
                                                    <td>OTP Verify</td>
                                                    <td>
                                                      <input class='tgl tgl-ios tgl_checkbox js-switch' name="status" type='checkbox' <?php echo ($customers->mobile_otp_vefity=='1')? "checked" : ""; ?> disabled="disabled" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Customers Location</td>
                                                    <td>
                                                      
                                                       <div class="map" id="map" style="width: 650px; height: 250px;"></div>
                                                  <input type="hidden" name="vCurrentLatitude" id="lat" value="<?php echo $customers->c_latitude; ?>">
                                                  <input type="hidden" name="vCurrentLongitude" id="lng" value="<?php echo $customers->c_longitude; ?>">

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Status</td>
                                                    <td>
                                                      <input class='tgl tgl-ios tgl_checkbox js-switch' name="status" type='checkbox' <?php echo ($customers->status=='1')? "checked" : ""; ?> disabled="disabled" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>Is Block</td>
                                                    <td>
                                                      <input class='tgl tgl-ios tgl_checkbox js-switch' name="status" type='checkbox' <?php echo ($customers->is_block=='0')? "checked" : ""; ?> disabled="disabled"/>
                                                    </td>
                                                </tr>

                                               

                                                <tr>
                                                    <td>Registerd Date</th>
                                                    <td>  <?php
                                                      $date = date("d-M-Y H:i:s"), strtotime($customers->created_at));
                                                      echo $date; ?></td>
                                                </tr>

                                            </tbody>
                                        </table>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



              </div>

              <?php $this->load->view("admin/common/common_footer"); ?>

              <!-- ============================================================== -->
              <!-- End footer -->
              <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
<?php $this->load->view("admin/common/common_js"); ?>
    </body>

</html>

<script type="text/javascript">
$(function (){
 $("a.single_image").fancybox();
});
</script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC711vkhHG424lDbLWy3ZH0CIgPVDHb8Dc&libraries=places"></script>


    <script type="text/javascript">

      var lat = document.getElementById('lat').value;
      var lng = document.getElementById('lng').value;

    function initialize() {
       var latlng = new google.maps.LatLng(lat,lng);
        var map = new google.maps.Map(document.getElementById('map'), {
          center: latlng,
          zoom: 13
        });
        var marker = new google.maps.Marker({
          map: map,
          position: latlng,
          draggable: false,
          anchorPoint: new google.maps.Point(0, -29)
       });
        var infowindow = new google.maps.InfoWindow();   
        google.maps.event.addListener(marker, 'click', function() {
          var iwContent = '<div id="iw_container">' +
          '<div class="iw_title"><h5><?php echo $customers->name; ?> </div></div>';
          // including content to the infowindow
          infowindow.setContent(iwContent);
          // opening the infowindow in the current map and at the current marker location
          infowindow.open(map, marker);
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>