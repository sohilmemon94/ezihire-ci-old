<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->

    <title><?php echo PROJECT_NAME; ?> - <?php echo $page; ?> <?php echo $action; ?></title>
    <?php  $this->load->view("admin/common/common_css"); ?>
</head>
<style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}
</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>customers/approve_customers_list"><?php echo $page; ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $action; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <!-- Validation wizard -->
                <div class="row">
                    <div class="col-12">
                        <div class="card wizard-content">
                            <div class="card-body">
                                <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                 <h4 class="card-title">Edit <?php echo $page; ?></h4>
                                <h6 class="card-subtitle"></h6>
                                 <?php if (validation_errors())
                                {   
                                echo '<div class="alert alert-warning alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning!</strong> ';

                                echo validation_errors();
                                echo '</div>';
 
                                }
                                ?>

                                <div class="alert alert-success alert-dismissible " role="alert" id="myElem" style="display:none">
                                     <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <strong>Success !</strong> Image deleted successfully. </div>
                                  
                                <form role="form" name="form"  id="form_edit" action="" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                    <div class="form-group m-t-40 row">
                                        <label for="example-text-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Username <span class="text-danger">*</span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <input class="form-control" type="text" id="username" name="username" value="<?php echo $customers->username; ?>" placeholder="Username" id="example-text-input">
                                            
                                        </div>
                                    </div>

                                    <div class="form-group m-t-40 row">
                                        <label for="example-text-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Email ID <span class="text-danger"></span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <input class="form-control" type="text" id="email" name="email" value="<?php echo $customers->email; ?>" placeholder="Email" id="example-text-input" readonly="readonly" disabled="disabled" >
                                           
                                        </div>
                                    </div>
                                    

                                    <div class="form-group m-t-40 row">
                                        <label for="example-password-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Profile Pic <span class="text-danger"></span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <input type="file" name="image" id="image" class="dropify" />
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">

                                             <?php 
                                        $image = $customers->image;
                                        $imagefullpath = base_url().$image;
                                        if(!empty($image)) {
                                        $returnpath = $this->config->item('customers_images_uploaded_path');

                                        if(!file_exists($imagefullpath)){
                                        $imagePath = "";
                                        $imagefullpath = $returnpath.$image;
                                        $imagePath .= '<span class="round"><a class="fresco" data-fresco-group="example" href="'.$imagefullpath.'"><img src="';
                                        $imagePath .= $imagefullpath;
                                        $imagePath .= '"style="height: 50px;width: 50px;border-radius: 50%;" border="1" class="img-responsive"></a>
                                        </span>';
                                        echo $imagePath;

                                        ?>
                                        <br><span><a href="javascript:void(0);"  data-toggle="tooltip" class="imagedelete" data-placement="left" id="<?php echo $customers->customer_id; ?>" title="Delete" ><i class="fa fa-trash"></i></a></span>
                                        <?php
                                        }
                                        }
                                        ?>

                                        </div>
                                    </div>


                                     <div class="form-group row">
                                        <label for="example-password-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Password <span class="text-danger">*</span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <input class="form-control" type="password" name="password" value="<?php echo $customers->org_password; ?>" id="password">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-password-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Role<span class="text-danger"></span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <select class="form-control" name="role_id" id="role_id">
                                            <option>--- Select Role ---</option>
                                            <?php foreach($role as $r=>$v) { ?>
                                                <option  value="<?php echo $v->id; ?>"  name="<?php echo $v->id; ?>" id="<?php echo $v->id; ?>" <?php if($v->id == $customers->role_id) { echo "selected=selected"; } ?>> <?php echo $v->name; ?></option>
                                            <?php } ?>
                                            </select> 
                                        </div>
                                    </div>

                                 
                                    <div class="form-group m-t-40 row">
                                        <label for="example-text-input" class="col-lg-2 col-md-2 col-sm-2 col-form-label">Status  <span class="text-danger"></span></label>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                   <input class='tgl tgl-ios tgl_checkbox js-switch' name="status" type='checkbox' <?php echo ($customers->status =='1')? "checked" : ""; ?> />
                                    </div>
                                    </div>


                                    <div class="form-group row">
                                     
                                        <div class="col-lg-2 col-md-2 col-sm-2">
                                         </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <button type="submit" class="btn btn-info" id="save_button"  id="save_button" name="save_button" value="Update">Edit</button>
                                            <a href="<?php echo site_url("customers/approve_customers_list"); ?>" class="btn btn-inverse">Cancel</a>
                                        </div>
                                    </div>
                                </form>



                                  
                            </div>
                        </div>
                    </div>
                </div>
    <!-- /.row -->

 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- footer -->
 <!-- ============================================================== -->

 <?php  $this->load->view("admin/common/common_footer"); ?>

 <!-- ============================================================== -->
 <!-- End footer -->
 <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php  $this->load->view("admin/common/common_js"); ?>
</body>

</html>
<script>
 $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script>
    $(function() {

        $(".imagedelete").on("click",function(e){
            e.preventDefault();
            var id =$(this).attr('id');
            if(id!="" && id!=0){
                $.ajax({
                    url: "<?php echo site_url("customers/deleteimages");?>",
                    type: "post",
                    data: {
                    'id': id
                    },
                    success: function (response) {
                        if(response==1){
                            $("#myElem").show();
                            setTimeout(function() { $("#myElem").hide(); }, 3000);
                            location.reload();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(textStatus, errorThrown);
                    }
                });
            }
        });

        $("#form_edit").validate({
        // Specify validation rules
        rules: {
            email: {
                required:true,
                email:true,
                remote: {
                    url: "<?php echo base_url(); ?>check_email_exist",
                    type: "POST",
                    data: {
                        email: function(){ return $("#email").val(); },
                    }
                },
            },
            username:{
                required:true,
            },
            password: {
                required:true,
            },
            role_type:{
                required:true,
                min:1,
            }
        },
        messages: {
            email: {
                required:"Please enter email",
                email:"Please enter valid email",
                remote: "Email Id already exist !",
            },
            username:{
                required:"Please enter username",
            },
            password: {
                required:"Please enter password",
            },
            role_type:{
                required:"Please select role",
                min:"Please select role",
            }
        },
        submitHandler: function(form) {
           form.submit();
        }
    });

    // $('#save_button').click(function() {
    //     if($('#form_edit').valid()){
    //         $(this).attr('disabled', 'disabled');
    //         $(this).html('Saving...');
    //         $(this).parents('form').submit();
    //     }
    // });
    
});
</script>
