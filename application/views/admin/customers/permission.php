<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo $title; ?> | <?php echo $page; ?> <?php echo $action; ?></title>
        <?php $this->load->view("admin/common/common_css"); ?>
       
    </head>

  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}

input[type="file"] {
  display: block;
}
.imageThumb {
  max-height: 75px;
  border: 2px solid;
  padding: 1px;
  cursor: pointer;
}
.pip {
  display: inline-block;
  margin: 10px 10px 0 0;
}
.remove {
  display: block;
  background: #444;
  border: 1px solid black;
  color: white;
  text-align: center;
  cursor: pointer;
}
.remove:hover {
  background: white;
  color: black;
}

</style>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    
                <div class="row page-titles">
                    <div class="col-md-6 col-12 align-self-center">
                        <h3 class="text-themecolor m-b-0 m-t-0"><?php echo $page; ?></h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/dashboard">Dashboard</a></li>
                           <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>customers/approve_customers_list"><?php echo $page; ?></a></li>
                            <li class="breadcrumb-item active"><?php echo $action; ?></li>
                        </ol>
                    </div>
                    
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
               
                                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <?php
                                    if (isset($error)) {
                                        echo $error;
                                    }
                                    echo $this->session->flashdata("message");
                                    ?>
                                <h4 class="card-title">New <?php echo $page; ?></h4>
                                <h6 class="card-subtitle"></h6>
                                <div class="text-center">
                                    <input type="checkbox" name="checkAll" id="checkAll"> Select All
                                </div>
                                <br>
                                <?php if (validation_errors())
                                {   
                                echo '<div class="alert alert-warning alert-dismissible" id="error" role="alert"><i class="fa fa-check"></i><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><strong>Warning!</strong> ';

                                echo validation_errors();
                                echo '</div>';

                                }
                                ?>
                                <form role="form" name="form"  id="form_edit" action="" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">
                                   
                                   <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr class="text-center">
                                                <td><b>Module Name</b></td>
                                                <td><b>List</b></td>
                                                <td><b>Add</b></td>
                                                <td><b>Edit</b></td>
                                                <td><b>Delete</b></td>
                                            </tr>
                                        </thead>
                                    <tbody>

                                        <?php

                                        if(count($permission)>0) {
                                            foreach($permission as $p=>$pv) {
                                            ?>
                                            <tr>
                                                <td><?php echo $pv[0]->module; ?></td>

                                                <?php if(count($pv)>0) {
                                                    foreach ($pv as $key => $value) { ?>

                                                        <?php 
                                                       
                                                        $disabled = $checkbox = $editcheckbox = "";
                                                        if(in_array($value->name, $match_permission_arr)) {
                                                           
                                                            $editcheckbox = "checked='checked'";
                                                        } 

                                                        // if($key == 0) {
                                                        //     $disabled = "";
                                                        //     $checkbox = "checked='checked'";
                                                        // }

                                                         if(isset($value->name) && !empty($value->name)) { 

                                                        ?>

                                                        <td class="text-center">
                                                            <input type="checkbox" value="1" class="checkbox" name="<?php echo $value->name; ?>" <?php echo $editcheckbox; ?> />
                                                        </td>

                                                        <?php } else { ?> 
                                                                <td class="text-center">
                                                                </td>
                                                            <?php } ?>
                                                    <?php }
                                                } ?>
                                               
                                            </tr>

                                            <?php }
                                        }

                                        ?>

                                      
                                    </tbody>
                                    </table>
                                </div>

                                    <div class="form-group row">
                                     
                                        
                                        <div class="col-lg-4 col-md-4 col-sm-4">
                                            <button class="btn btn-info" type="submit" id="btnContactUs">Save Permission</button>

                                            <a href="<?php echo site_url("customers/approve_customers_list"); ?>" class="btn btn-inverse">Cancel</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Validation wizard -->
            
                   
                <!-- ============================================================== -->
                <!-- End PAge Content -->
  
 </div>
 <!-- ============================================================== -->
 <!-- End Container fluid  -->
 <!-- ============================================================== -->
 <!-- ============================================================== -->
 <!-- footer -->
 <!-- ============================================================== -->

 <?php  $this->load->view("admin/common/common_footer"); ?>

 <!-- ============================================================== -->
 <!-- End footer -->
 <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<?php  $this->load->view("admin/common/common_js"); ?>
</body>

</html>


<script>
 $(document).ready( function() {

    $("#checkAll").click(function () {
        if(this.checked){
            $(".checkbox").prop('checked', true);
        } else {
            $(".checkbox").prop('checked', false);
        }
    });
    
    $('#error').delay(3000).fadeOut();
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script>
    $(function() {

        $('#btnContactUs').click(function() {
            if($('#form_edit').valid()){
                $(this).attr('disabled', 'disabled');
                var html ="<span class='spinner-boll' role='status' aria-hidden='true'></span> Saving...";
                $(this).html('');
                $(this).html(html);
                $(this).parents('form').submit();
            }
        });
    });
</script>
