<!DOCTYPE html>
<html lang="en">
    <?php 
        $my_permission = get_my_permission();
    ?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->

        <title><?php echo PROJECT_NAME; ?> Admin - <?php echo $page; ?></title>
        <?php $this->load->view("admin/common/common_css"); ?>
        <style>
            .canvasjs-chart-credit {
                display: none;
            }
        </style>
    </head>

 <body class="fix-header fix-sidebar card-no-border">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- ============================================================== -->
        <!-- Main wrapper - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <div id="main-wrapper">
            <!-- ============================================================== -->
            <!-- Topbar header - style you can find in pages.scss -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_header"); ?>
            <!-- ============================================================== -->
            <!-- End Topbar header -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <?php $this->load->view("admin/common/common_sidebar"); ?>
            <!-- ============================================================== -->
            <!-- End Left Sidebar - style you can find in sidebar.scss  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Page wrapper  -->
            <!-- ============================================================== -->
            <div class="page-wrapper">
                <!-- ============================================================== -->
                <!-- Container fluid  -->
                <!-- ============================================================== -->
                <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->

                    <div class="row page-titles">
                        <div class="col-md-6 col-12 align-self-center">
                            <h3 class="text-themecolor m-b-0 m-t-0">Dashboard</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Dashboard</li>
                            </ol>
                        </div>
                      
                    </div>

                    <?php if(isset($error)){ echo $error; }
                                   echo $this->session->flashdata("message"); ?>

                    <?php if(isset($error)){ echo $error; }
                                   echo $this->session->flashdata("error"); ?>
                    <?php if(isset($error)){ echo $error; }
                                   echo $this->session->flashdata("message"); ?>
                     <?php 
                     
                         $random_array = array("bg-info","bg-success","bg-danger","bg-warning","bg-primary","bg-megna","bg-themess","bg-pink","bg-teal","bg-inverse","bg-theme","bg-red");
                        shuffle($random_array);
                        ?>
                    <div class="row">
                        <!-- Column -->
                        <?php 
                        if(in_array('approve_vendors_list',$my_permission) || in_array('pending_vendors_list',$my_permission) || in_array('rejected_vendors_list',$my_permission) ) { ?>

                        <div class="col-md-6 col-lg-3 col-xlg-3">
                            <div class="card card-inverse card-info">
                                <div class="box <?php echo $random_array[0]; ?> text-center">
                                   <h2 class="font-light text-white"><a href="<?php echo base_url(); ?>vendors/approve_vendors_list" style="color:#fff;">Vendors</a></h2>
                                    <h2 class="text-white">
                                        <?php echo count($vendors); ?>
                                    </h2>
                                </div>
                            </div>
                        </div>

                         <?php } ?>
                        <?php if(in_array('technology_list',$my_permission)) { ?>
                         <!-- Column -->
                        <div class="col-md-6 col-lg-3 col-xlg-3">
                            <div class="card card-inverse card-info">
                                <div class="box <?php echo $random_array[2]; ?> text-center">
                                   <h2 class="font-light text-white"><a href="<?php echo base_url(); ?>technology/technology_list" style="color:#fff;">Technologies</a></h2>
                                    <h2 class="text-white">
                                        <?php echo count($technologys); ?>
                                    </h2>
                                </div>
                            </div>
                        </div>

                        <?php } ?>
                        <!-- Column -->
                        <?php if(in_array('employees_list',$my_permission)) { ?>

                            <div class="col-md-6 col-lg-3 col-xlg-3">
                                <div class="card card-inverse card-info">
                                    <div class="box <?php echo $random_array[1]; ?> text-center">
                                       <h2 class="font-light text-white"><a href="<?php echo base_url(); ?>employees/employees_list" style="color:#fff;">Employees</a></h2>
                                        <h2 class="text-white">
                                            <?php echo count($employees); ?>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                           
                        <?php if(in_array('postjob_list',$my_permission)) { ?>

                            <div class="col-md-6 col-lg-3 col-xlg-3">
                                <div class="card card-inverse card-info">
                                    <div class="box <?php echo $random_array[3]; ?> text-center">
                                       <h2 class="font-light text-white"><a href="<?php echo base_url(); ?>postjob/postjob_list" style="color:#fff;">Post Job</a></h2>
                                        <h2 class="text-white">
                                            <?php echo count($postjob); if($no_of_position>0) { echo " (".$no_of_position." Position )"; } ?>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>


                        <div class="col-md-6 col-lg-3 col-xlg-3">
                            <div class="card card-inverse card-warning">
                                <div class="box <?php echo $random_array[5]; ?> text-center">
                                    <h2 class="font-light text-white">Skill Matrix</h2>
                                    <h2 class="text-white">
                                        <?php echo $skills; ?>
                                    </h2>   
                                </div>
                            </div>
                        </div>

                        <!-- Column -->
                        <div class="col-md-6 col-lg-3 col-xlg-3">
                            <div class="card card-inverse card-warning">
                                <div class="box <?php echo $random_array[4]; ?> text-center">
                                    <h2 class="font-light text-white"><a href="<?php echo base_url(); ?>admin/settings" style="color:#fff;">Settings</a></h2>
                                    <h2 class="text-white">
                                        &nbsp;
                                    </h2>   
                                </div>
                            </div>
                        </div>
                    </div>


                    <?php 
                    if(in_array('interviewschedule_list',$my_permission) || in_array('interviewschedule_add',$my_permission) || in_array('interviewschedule_edit',$my_permission) || in_array('interviewschedule_delete',$my_permission) ) { ?>

                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex no-block">
                                        <h4 class="card-title">Interview Schedules Reports</h4>
                                    </div>
                                    <div class="table-responsive mt-4">
                                        <table class="table stylish-table no-wrap">
                                            <thead>
                                                <tr>
                                                    <th>Icon</th>
                                                    <th>User Name</th>
                                                    <th>No of Interview</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(count($interview_by_users)>0) { 
                                                    foreach($interview_by_users as $k=>$v) { 

                                                    $random_array = array("round-success","round-primary","round-danger","round-warning");
                                                    $random_keys=array_rand($random_array,1);
                                                    $backColor =  $random_array[$random_keys];
                                               
                                                    $imagePath= '';
                                                
                                                    $imagePath .= '<span class="round ';
                                                    $imagePath .= $backColor;
                                                    $imagePath .='">';
                                                    $firstCharacter = substr($v->username , 0, 1);
                                                    $imagePath .= trim(strtoupper($firstCharacter));
                                                    $imagePath .='</span>';
                                               
                                                ?>
                                                <tr>
                                                    <td><?php echo $imagePath; ?></td>
                                                    <td><?php echo $v->username; ?></td>
                                                    <td><?php echo $v->interviewschedule; ?></td>
                                                </tr>
                                            <?php } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex no-block">
                                        <h4 class="card-title">Interview Report by Status</h4>
                                    </div>
                                    <div class="table-responsive mt-4">
                                        <table class="table stylish-table no-wrap">
                                            <thead>
                                                <tr>
                                                    <th>Icon</th>
                                                    <th>Status</th>
                                                    <th>No of Interview</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php if(count($interview_by_status)>0) { 
                                                    foreach($interview_by_status as $k=>$v) { 

                                                    $random_array = array("round-success","round-primary","round-danger","round-warning");
                                                    $random_keys=array_rand($random_array,1);
                                                    $backColor =  $random_array[$random_keys];
                                               
                                                    $imagePath= '';
                                                
                                                    $imagePath .= '<span class="round ';
                                                    $imagePath .= $backColor;
                                                    $imagePath .='">';
                                                    $firstCharacter = substr($v['status'] , 0, 1);
                                                    $imagePath .= trim(strtoupper($firstCharacter));
                                                    $imagePath .='</span>';
                                               
                                                ?>
                                                <tr>
                                                    <td><?php echo $imagePath; ?></td>
                                                    <td><?php echo $v['status']; ?></td>
                                                    <td><?php echo $v['interviewschedule']; ?></td>
                                                </tr>
                                            <?php } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex no-block">
                                        <h4 class="card-title">Skills Reports</h4>
                                    </div>
                                    <div class="table-responsive mt-4">
                                         <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Skill</th>
                                                    <th>Technology</th>
                                                    <th>Total Skills</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                if(count($technologys)>0) { 
                                                    foreach($technologys as $k=>$v) { 
                                                     $total = 0;
                                                     if(isset($v->total) && !empty($v->total)) {
                                                         $total = $v->total;
                                                     }
                                                    $random_array = array("round-success","round-primary","round-danger","round-warning");
                                                    $random_keys=array_rand($random_array,1);
                                                    $backColor =  $random_array[$random_keys];
                                               
                                                    $imagePath= '';
                                                
                                                    $imagePath .= '<span class="round ';
                                                    $imagePath .= $backColor;
                                                    $imagePath .='">';
                                                    $firstCharacter = substr($v->tname , 0, 1);
                                                    $imagePath .= trim(strtoupper($firstCharacter));
                                                    $imagePath .='</span>';
                                               
                                                ?>
                                                <tr>
                                                    <td><?php echo $imagePath; ?></td>
                                                    <td><?php echo $v->tname; ?></td>
                                                    <td><?php echo $total; ?></td>
                                                </tr>
                                            <?php } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                       
                    </div>
                </div>
                <!--============================================================== -->
                <!--End Container fluid -->
                <!--============================================================== -->
                <!--============================================================== -->
                <!--footer -->
                <!--============================================================== -->

                <?php $this->load->view("admin/common/common_footer");
                ?>

                <!-- ============================================================== -->
                <!-- End footer -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Page wrapper  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Wrapper -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- All Jquery -->
        <!-- ============================================================== -->
        <?php $this->load->view("admin/common/common_js"); ?>
    </body>
	
    <script> $.reel.def.indicator = 5;</script>
    <script>
         var table = $('#example23').DataTable({
            "order": [[ 0, "ASC" ]]
        });
     
    </script>
</html>