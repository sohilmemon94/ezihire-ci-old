    
<?php 
$my_permission = get_my_permission();
?>

<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            
        </div>
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- <li class="nav-small-cap">PERSONAL</li> -->
                <li>
                    
                    <a class="has-arrow" href="<?php echo base_url(); ?>admin/dashboard" aria-expanded="false"><i class="fa fa-tachometer" aria-hidden="true"></i><span class="hide-menu">Dashboard </a>
                    
                </li>

                <?php 
                if(in_array('technology_list',$my_permission) || in_array('industry_list',$my_permission) || in_array('education_list',$my_permission)  || in_array('job_list',$my_permission) || in_array('agreements_list',$my_permission)  ) { ?>

                <li>
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-wrench" aria-hidden="true"></i><span class="hide-menu">Tools</span></a>
                    <ul aria-expanded="false" class="collapse">

                        <?php if(isset($_SESSION['customer_role_type']) && !empty($_SESSION['customer_role_type']) && $_SESSION['customer_role_type'] == "A") { ?>
                            <li><a href="<?php echo base_url(); ?>backup/backup_list">Backup List</a></li>
                        <?php } ?>

                        <?php 
                        if(in_array('technology_list',$my_permission) || in_array('add_technology',$my_permission) || in_array('edit_technology',$my_permission) || in_array('technology_delete',$my_permission) ) { ?>
                            <li><a href="<?php echo base_url(); ?>technology/technology_list">Technology List</a></li>
                        <?php } ?>

                        <?php 
                        if(in_array('industry_list',$my_permission) || in_array('add_industry',$my_permission) || in_array('edit_industry',$my_permission) || in_array('industry_delete',$my_permission) ) { ?>
                            <li><a href="<?php echo base_url(); ?>industry/industry_list">Industry List</a></li>
                        <?php } ?>
                          <?php 
                        if(in_array('education_list',$my_permission) || in_array('add_education',$my_permission) || in_array('edit_education',$my_permission) || in_array('education_delete',$my_permission) ) { ?>
                            <li><a href="<?php echo base_url(); ?>education/education_list">Education List</a></li>
                        <?php } ?>

                         <?php 
                        if(in_array('job_list',$my_permission) || in_array('add_job',$my_permission) || in_array('edit_job',$my_permission) || in_array('job_delete',$my_permission) ) { ?>
                            <li><a href="<?php echo base_url(); ?>job/job_list">Job List</a></li>
                        <?php } ?>

                        <?php 
                        if(in_array('approve_customers_list',$my_permission) || in_array('customers_add',$my_permission) || in_array('customers_edit',$my_permission) || in_array('customers_delete',$my_permission) ) { ?>
                            <li><a href="<?php echo base_url(); ?>customers/approve_customers_list">Staff List</a></li>
                        <?php } ?>

                          <?php 
                        if(in_array('agreements_list',$my_permission) || in_array('add_agreements',$my_permission) || in_array('edit_agreements',$my_permission) || in_array('agreements_delete',$my_permission) ) { ?>
                            <li><a href="<?php echo base_url(); ?>agreements/agreements_list">Agreements List</a></li>
                        <?php } ?>

                    
                    </ul>
                </li>

                <?php } ?>
                <?php 
                if(in_array('approve_vendors_list',$my_permission) || in_array('pending_vendors_list',$my_permission) || in_array('rejected_vendors_list',$my_permission) ) { ?>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-user-secret" aria-hidden="true"></i><span class="hide-menu">Vendors </span></a>
                        <ul aria-expanded="false" class="collapse">

                            <?php 
                            if(in_array('approve_vendors_list',$my_permission) || in_array('vendors_add',$my_permission) || in_array('vendors_edit',$my_permission) || in_array('vendors_delete',$my_permission) ) { ?>
                                <li><a href="<?php echo base_url(); ?>vendors/approve_vendors_list">Vendors List</a></li>
                            <?php } ?>

                            <?php if(isset($_SESSION['customer_role_type']) && !empty($_SESSION['customer_role_type']) && $_SESSION['customer_role_type'] == "A") { ?>
                            <li><a href="<?php echo base_url(); ?>vendors/unapprove_vendors_list">New Vendor List</a></li>
                        <?php } ?>

                        </ul>
                    </li>

                <?php } ?>

                <?php 
                if(in_array('employees_list',$my_permission) || in_array('my_employees_list',$my_permission) ) { ?>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-users" aria-hidden="true"></i><span class="hide-menu">Employees </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <?php 
                            if(in_array('employees_list',$my_permission) || in_array('employees_add',$my_permission) || in_array('employees_edit',$my_permission) || in_array('employees_delete',$my_permission) ) { ?>

                                <li><a href="<?php echo base_url(); ?>employees/employees_list">All Profiles List</a></li>
                                <?php }
                            if(in_array('my_employees_list',$my_permission) || in_array('my_employees_add',$my_permission) || in_array('my_employees_edit',$my_permission) || in_array('my_employees_delete',$my_permission)) { ?>

                                <li><a href="<?php echo base_url(); ?>employees/my_employees_list">Upload Employees List</a></li>
                            <?php } 

                            if($_SESSION['customer_role_type'] == "V") { 

                                $customer_id = $_SESSION['customer_id']; ?>

                            <?php 
                                if(in_array('vendors_employees_details_view',$my_permission) || in_array('vendors_employees_details',$my_permission) ) { ?>

                                    <li><a href="<?php echo base_url(); ?>vendors/vendors_employees_details/<?php echo $customer_id; ?>">Skills Matrix</a></li>
                            <?php } ?>
                            <?php } ?>
                        </ul>
                    </li>

                <?php } ?>

                 <?php if(isset($_SESSION['customer_role_type']) && !empty($_SESSION['customer_role_type']) && $_SESSION['customer_role_type'] == "A") { ?>
                     <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-tasks" aria-hidden="true"></i><span class="hide-menu">Post Inquiry </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href="<?php echo base_url(); ?>postinquiry/postinquiry_list">Inquiry List</a></li>
                        </ul>
                    </li>

                <?php } ?>
                <?php 
                if(in_array('postjob_list',$my_permission)) { ?>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-tasks" aria-hidden="true"></i><span class="hide-menu">Post Job </span></a>
                        <ul aria-expanded="false" class="collapse">

                            <?php 
                            if(in_array('postjob_list',$my_permission) || in_array('add_postjob',$my_permission) || in_array('edit_postjob',$my_permission) || in_array('postjob_delete',$my_permission) ) { ?>
                                <li><a href="<?php echo base_url(); ?>postjob/postjob_list">My Post Job List</a></li>
                            <?php } ?>
                        </ul>
                    </li>

                <?php } ?>

                <?php 
                if(in_array('interviewschedule_list',$my_permission)) { ?>

                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-info" aria-hidden="true"></i><span class="hide-menu">Interview Schedule </span></a>
                        <ul aria-expanded="false" class="collapse">

                            <?php 
                            if(in_array('interviewschedule_list',$my_permission) || in_array('interviewschedule_add',$my_permission) || in_array('interviewschedule_edit',$my_permission) || in_array('interviewschedule_delete',$my_permission) ) { ?>
                                <li><a href="<?php echo base_url(); ?>interviewschedule/interviewschedule_list">Interview Schedule List</a></li>
                            <?php } ?>
                        </ul>
                    </li>

                <?php } ?>


                <?php 
                    if(in_array('approve_vendors_list',$my_permission)) { ?>
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-file-text" aria-hidden="true"></i><span class="hide-menu">Reports </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <?php 
                            if(in_array('approve_vendors_list',$my_permission) ) { ?>
                                <li><a href="<?php echo base_url(); ?>reports/vendors_skills_list">Vendors Skills</a></li>
                            <?php } ?>

                             <?php 
                            if(in_array('interviewschedule_list',$my_permission) || in_array('interviewschedule_add',$my_permission) || in_array('interviewschedule_edit',$my_permission) || in_array('interviewschedule_delete',$my_permission) ) { ?>
                                <li><a href="<?php echo base_url(); ?>interviewschedule/interviewschedule_report_list">Interview Schedule Rports</a></li>
                            <?php } ?>

                        </ul>
                    </li>
                <?php } ?>


                <li>
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="ti-settings"></i><span class="hide-menu">Settings</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>admin/settings">My Profile</a></li>
                       
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    <div class="sidebar-footer">
        <!-- item-->
        <a href="<?php echo base_url(); ?>admin/settings" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
        <a href="<?php echo base_url(); ?>customers/signout" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
    </div>
    <!-- End Bottom points-->
</aside>
