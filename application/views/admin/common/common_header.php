
<header class="topbar" style="position: relative; top: 0px; width: 100%;">
      <nav class="navbar top-navbar navbar-expand-md navbar-light" id="top_header">
          <!-- ============================================================== -->
          <!-- Logo -->
          <!-- ============================================================== -->
          <div class="navbar-header">

             <a class="navbar-brand" href="<?php echo base_url(); ?>admin/dashboard">
                        <!-- Logo icon -->
                       
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="<?php echo base_url(); ?>img/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo base_url(); ?>img/logo-icon.png" alt="homepage" class="light-logo" />
                        
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span>
                         <!-- dark Logo text -->
                         <img src="<?php echo base_url(); ?>img/logo-text.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo base_url(); ?>img/logo-text.png" class="light-logo" alt="homepage" /></span> </a>
          </div>
          <!-- ============================================================== -->
          <!-- End Logo -->
          <!-- ============================================================== -->
          <div class="navbar-collapse collapse" id="navbarSupportedContent">
              <!-- ============================================================== -->
              <!-- toggle and nav items -->
              <!-- ============================================================== -->
              <ul class="navbar-nav mr-auto mt-md-0 ">
                  <!-- This is  -->
                  <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muteds waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                  <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muteds waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                  <!-- ============================================================== -->
                  <!-- Comment -->
                  <!-- ============================================================== -->
                    <li class="nav-item dropdown">
             
            
          </li>
                  <!-- ============================================================== -->
                  <!-- End Messages -->
                  <!-- ============================================================== -->
              </ul>
              <!-- ============================================================== -->
              <!-- User profile and search -->
              <!-- ============================================================== -->
              <ul class="navbar-nav my-lg-0">
                  <li class="nav-item hidden-sm-down">
                     
                  </li>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="JavaScript:Void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                          <?php 
                          $adminimage = $this->session->userdata('customer_image');

                          $returnpath = $this->config->item('customers_images_uploaded_path');

                          $imagefullpath = $returnpath.$adminimage;
                          
                          $user_name = $this->session->userdata('customer_user_name');
                          if($adminimage == NULL || empty($adminimage)) {
                           //   $image = "uploads/No_image.jpg";

                               $random_array = array("round-success","round-primary","round-danger","round-warning");
                                          $random_keys=array_rand($random_array,1);
                                          $backColor =  $random_array[$random_keys];
                                                      ?>
                                  <span class="round <?php echo $backColor; ?>">
                                      <?php  
                                      $firstCharacter = substr($user_name, 0, 1);
                                      echo trim(strtoupper($firstCharacter));
                                      ?>
                                      </span>
                       <?php  } else { ?>
                              <img src="<?php echo $imagefullpath; ?>" alt="user" class="profile-pic" />
                         <?php  } ?>
                      <div class="dropdown-menu dropdown-menu-right animated flipInY">
                          <ul class="dropdown-user">
                              <li>
                                  <div class="dw-user-box">
                                      <div class="u-img">

                                         <?php 
                                          $adminimage = $this->session->userdata('customer_image');

                                          $returnpath = $this->config->item('customers_images_uploaded_path');

                                          $imagefullpath = $returnpath.$adminimage;
                                                                                   
                                          $user_name = $this->session->userdata('customer_user_name');
                                          if($adminimage == NULL || empty($adminimage)) {
                                           //   $image = "uploads/No_image.jpg";

                                               $random_array = array("round-success","round-primary","round-danger","round-warning");
                                                          $random_keys=array_rand($random_array,1);
                                                          $backColor =  $random_array[$random_keys];
                                                                      ?>
                                                  <span class="round <?php echo $backColor; ?>">
                                                      <?php  
                                                      $firstCharacter = substr($user_name, 0, 1);
                                                      echo trim(strtoupper($firstCharacter));
                                                      ?>
                                                      </span>


                                       <?php  } else { ?>
                                              <img src="<?php echo $imagefullpath; ?>" alt="user" class="profile-pic" />
                                         <?php  } ?>

                                      </div>
                                      <div class="u-text">
                                          <h4><?php echo $image = $this->session->userdata('customer_user_name'); ?></h4>
                                          <p class="text-muted"><?php echo $image = $this->session->userdata('customer_email'); ?></p></div>
                                  </div>
                              </li>
                              <li><a href="<?php echo base_url(); ?>admin/settings"><i class="ti-settings"></i> Settings</a></li>
                              
                              <li role="separator" class="divider"></li>
                              <li><a href="<?php echo base_url(); ?>customers/signout"><i class="fa fa-power-off"></i> Logout</a></li>
                          </ul>
                      </div>
                  </li>
                  <li class="nav-item dropdown">
                      <?php
                          echo form_open($this->uri->uri_string());
                      ?>
                      
                      <?php
                          echo form_close();
                      ?>
                  </li>
              </ul>
          </div>
      </nav>
  </header>