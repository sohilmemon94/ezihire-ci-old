<?php
defined('BASEPATH') OR exit('No direct script access allowed');  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/ico" sizes="16x16" href="<?php echo base_url(); ?>img/favicon.png">
    <title><?php echo PROJECT_NAME; ?> | Inquiry</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url(); ?>css/colors/default-dark.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}
</style>

<?php 
$returnpath = $this->config->item('customers_images_uploaded_path');
$local_image = "background.jpg";
if(isset($adminmaster->background_image) && !empty($adminmaster->background_image)) { 
    $background_image = $adminmaster->background_image;
    $imagefullpath = base_url().$background_image;
    if(!empty($background_image)) {
        if(!file_exists($imagefullpath)){
            $imagefullpath = $returnpath.$background_image;
        } else {
            $imagefullpath = $returnpath.$local_image;
        }
    }
} else {
    $imagefullpath = $returnpath.$local_image;
} ?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        
        <div class="login-register" style="background-image:url('<?php echo $imagefullpath; ?>');position:initial !important;">
          
            <div class="login-box card" style="width:500px;">
                <div class="card-body">
                           <form class="form-horizontal form-material" name="signupform" id="signupform" method="post"  enctype="multipart/form-data">
                    <h3 class="box-title m-b-20"> <img src="<?php echo base_url(); ?>img/logo.png" style="align: center;margin: 0 auto;display: block;"></h3>
                    <h3>Post job Inquiry</h3>
                    
                    <?php if(isset($error)){ echo $error; }
                                   echo $this->session->flashdata("message"); ?>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <!-- <label class="control-label">Company Name <span class="text-danger">*</span></label> -->
                            <input class="form-control" type="text" name="username" id="username" placeholder="Company Name"></div>
                    </div>


                    <div class="form-group ">
                        <div class="col-xs-12">
                           <!--  <label class="control-label">Mobile No <span class="text-danger">*</span></label> -->
                            <input class="form-control" type="text" name="mobileno" id="mobileno"  placeholder="Mobile No"></div>
                    </div>


                    <div class="form-group ">
                        <div class="col-xs-12">
                           <!--  <label class="control-label">Email <span class="text-danger">*</span></label> -->
                            <input class="form-control" type="text" name="email" placeholder="Email" id="email"></div>
                    </div>

                     <div class="form-group">
                        <div class="col-xs-12">
                           <!--  <label class="control-label">Job Description <span class="text-danger">*</span></label> -->
                             <textarea class="form-control" placeholder="Job Description" id="description" cols="5" name="description"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                       <div class="col-xs-12">
                            <!--  <label class="control-label">Skills <span class="text-danger">*</span></label> -->
                            <select class="select2 form-control" name="skill_id" id="skills" required="required"  style="height: 36px;width: 100%;">
                            <option>--- Select Your skills ---</option>
                            <?php foreach($technologies as $r=>$v) { ?>
                            <option value="<?php echo $v->id; ?>" name="<?php echo $v->id; ?>" id="<?php echo $v->id; ?>"><?php echo $v->tname; ?></option>
                            <?php } ?>
                            </select> 

                           </div>
                    </div>

                    <div class="form-group">
                         <div class="col-xs-12">
                                <!-- <label class="control-label">Job Duration<span class="text-danger">*</span></label> -->
                                <select class="form-control select2" name="duration" id="duration">
                                <option>--- Select Job Duration ---</option>
                                <?php for($i=1;$i<=12;$i++) { ?>
                                    <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i; ?> Month</option>
                                <?php } ?>
                                </select> 

                           </div>
                    </div>
         
                   

                     <div class="form-group">
                         <div class="col-xs-12">
                             <!-- <label class="control-label">Experience<span class="text-danger">*</span></label> -->
                            <select class="form-control select2" name="experience" id="experience">
                            <option>--- Select Job Experience ---</option>
                            <?php for($i=1;$i<=10;$i++) { ?>
                            <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i; ?> + Exp</option>
                            <?php } ?>
                            </select> 
                        </div>
                    </div>

                     <div class="form-group">
                        <div class="col-xs-12">
                            <!--  <label class="control-label">Budget<span class="text-danger">*</span></label> -->
                            <select class="form-control select2" name="budget" id="budget">
                            <option>--- Select Job Budget ---</option>
                            <?php for($i=100000;$i<=250000;$i=$i+10000) { ?>
                                <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                            </select> 
                        </div>
                    </div>


                     <div class="form-group">
                         <div class="col-xs-12">
                            <!-- <label class="control-label">No of Position<span class="text-danger">*</span></label> -->
                            <select class="form-control select2" name="no_of_position" id="no_of_position">
                                <option>--- Select No Of Position ---</option>
                                <?php for($i=1;$i<=50;$i++) { ?>
                                    <option value="<?php echo $i; ?>" name="<?php echo $i; ?>" id="<?php echo $i; ?>"><?php echo $i; ?> </option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>

                     <div class="form-group">
                        <div class="col-xs-12">
                           <!--  <label class="control-label">Candidate Availibity <span class="text-danger">*</span></label> -->
                            <select class="form-control select2" name="candidate_availability" id="candidate_availability">
                                <option value="immediate" name="immediate" id="immediate">Immediate</option>
                                <option value="7 days" name="7 days" id="7 days">7 Days</option>
                                <option value="15 days" name="15 days" id="15 days">15 Days</option>
                                <option value="30 days" name="30 days" id="30 days">30 Days</option>
                            </select>
                        </div>
                    </div>

                    <?php if ($_SERVER['HTTP_HOST'] != "localhost") { ?> 
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('google_sitekey') ?>"></div>
                            </div>
                        </div>
                    <?php } ?>

                        <div class="form-group">
                           <div class="col-md-12">
                                <span class="pull-left"><a href="<?php echo base_url(); ?>login"><i class="fa fa-lock m-r-5"></i>Log In ?</a></span>
                                <span class="pull-right"><a href="<?php echo base_url(); ?>signup"><i class="fa fa-lock m-r-5"></i>Signup ?</a></span>
                            </div>
                        </div>


                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block waves-effect waves-light" type="submit" id="btnContactUs">Post Inquiry</button>
                        </div>
                    </div>
                  
                </form>
                
                </div>
            </div>
        </div>

    </section>

     <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
<script>
   $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
  });
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>js/additional-methods.min.js"></script>
<script>

$(function() {
  $("#signupform").validate({
    rules: {
        username:{
            required:true,
        },           
        mobileno:{
            required:true,
            number:true,
        },            
        email: {
            required:true,
            email:true,
        },
        skill_id:{
            required:true,
            min:1,
        },
        duration:{
            required:true,
             min:1,
        },
        description:{
            required:true,
        },
        experience:{
            required:true,
            min:1,
        },
        budget:{
            required:true,
            min:1,
        },
        no_of_position:{
            required:true,
            min:1,
        },
        candidate_availability: {
            required:true,
        },
    },
    messages: {
        username:{
            required:"Please enter company name",
            remote: "Company already exist !",
        },            
        mobileno:{
            required:"Please enter mobile no",
        },            
        email: {
            required:"Please enter email",
            email:"Please enter valid email",
            remote: "Email Id already registered with us !",
        },
        skill_id:{
                required:"Please select job skill",
                min:"Please select job skill",
            },
            subskills:{
                required:"Please enter sub skills",
            },
            duration:{
                required:"Please select job duration",
            },
            description:{
                required:"Please enter job description",
            },
            experience:{
                required:"Please select job experience",
                min:"Please select job experience",
            },
            budget:{
                required:"Please select budget",
                min:"Please select budget",
            },
            no_of_position:{
                required:"Please select no of position",
                min:"Please select no of position",
            },
            no_of_interview:{
                required:"Please select Interview round",
                min:"Please select Interview round",
            },
            candidate_availability: {
                required:"Please select candidate availability",
            },
    },
    submitHandler: function(form) {
      form.submit();
    }
  });

    $('#btnContactUs').click(function() {
        if($('#signupform').valid()){
            $(this).attr('disabled', 'disabled');
            var html ="<span class='spinner-border' role='status' aria-hidden='true'></span> Connecting...";
            $(this).html('');
            $(this).html(html);
            $(this).parents('form').submit();
        }
    });
});

</script>   
