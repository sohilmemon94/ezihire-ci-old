<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/ico" sizes="64x64" href="<?php echo base_url(); ?>img/favicon.png">
    <title><?php echo PROJECT_NAME; ?> - HIRING - SOLUTIONS - SIMPLIFIED </title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@700&display=swap" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
</head>
  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}

.m_main h1 {
    font-size: 70px;
    line-height: 90px;
    font-family: 'Rubik', sans-serif;
    font-weight: 700;
    color: #FFFFFF;
    margin: 0;
}
.m_main {
    border-bottom: 5px solid white;
    padding:0 0 20px 0;
}
.main-text{
    padding:10px 20px;
}
.m_text p {
    font-size: 40px;
    color: #FFFFFF;
    line-height: 47px;
    padding: 20px 0 0 0;
   
    font-weight: 400;
    font-family: 'Rubik';
    margin: 0;

}
#wrapper .container{width:100%;max-width:1240px;margin:0 auto;}

.login-box {
  width: 71% !important;
  margin-bottom: 0;
  border-radius: 10px;
  margin-right: 0px;
}

  button,a i {
    margin-right: 22px;
}
.card-body {
    padding: 50px 30px;
}
.btn{font-size:20px;line-height:25px;font-weight: 400;font-family: 'Rubik';}
.btn.query{color:#7243EA;font-weight:400;background:transparent;}

.powerby {
    padding: 75px 0 0 0;
    text-align: center;
}
.powerby span {
    font-size: 20px;
    color: #ffffff;
    line-height: 23px;
    font-weight: 400;
    vertical-align: bottom;
    margin-right: 10px;
}

.login-register {
    background-size: cover !important;
    background-repeat: no-repeat !important;
    background-position: center center !important;
    height: 100% !important;
    width: 100% !important;
    padding: auto 0 !important;
    position: fixed !important;
}

@media only screen and (max-width:991px)
{
    .m_main h1{font-size:60px;line-height:60px;}
    .m_text p{font-size:30px;}
    .btn{font-size:15px;}
    button,a i {
    margin-right: 10px !important;
}
}
@media only screen and (max-width:767px){
    .login-box{margin:30px 0;width:100% !important;}
  
}
@media only screen and (max-width:576px)
{
    .login-box{width:100% !important}
    .m_text p{font-size:20px;}
    .m_main h1{font-size:50px;line-height:50px;}
}
</style>

<?php 
$returnpath = $this->config->item('customers_images_uploaded_path');
$local_image = "background.jpg";
if(isset($adminmaster->background_image) && !empty($adminmaster->background_image)) { 
    $background_image = $adminmaster->background_image;
    $imagefullpath = base_url().$background_image;
    if(!empty($background_image)) {
        if(!file_exists($imagefullpath)){
            $imagefullpath = $returnpath.$background_image;
        } else {
            $imagefullpath = $returnpath.$local_image;
        }
    }
} else {
    $imagefullpath = $returnpath.$local_image;
} ?>

<body>
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <section id="wrapper">
        <div class="login-register" style="background-image:url('<?php echo $imagefullpath; ?>');">      
            <div class="container">
                <div class="row">
                    <div class="col-md-6 main-text">
                        <div class="m_main">
                            <h1>HIRING </h1> 
                        <h1>SOLUTIONS</h1>  
                        <h1>SIMPLIFIED</h1> 
                        </div>
                        <div class="m_text">
                            <p>Review. Interview. Onboarding.</p>
                        </div>
                    </div>  

                    <div class="col-md-6">
                        <div class="login-box card">
                            <div class="card-body">
                                <h3 class="box-title m-b-20"> <img src="<?php echo base_url(); ?>img/logo.png" style="align: center;margin: 0 auto;display: block;"></h3>
                                <div class="form-group ">
                                    <div class="col-xs-12">
                                    <a class="btn btn-primary btn-lg btn-block waves-effect waves-light" href="<?php echo base_url(); ?>signup"><img src="<?php echo base_url(); ?>img/register.png">&nbsp; &nbsp; Register as a Vendor</a>
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <a class="btn btn-primary btn-lg btn-block waves-effect waves-light" href="<?php echo base_url(); ?>login"><img src="<?php echo base_url(); ?>img/sign-in.png">&nbsp; &nbsp; Log In</a>
                                    </div>
                                </div>

                                <div class="form-group text-center">
                                    <div class="col-xs-12">
                                    <a class="btn query btn-lg btn-block waves-effect waves-light" href="<?php echo base_url(); ?>inquiry"> <img src="<?php echo base_url(); ?>img/inquiry.png">&nbsp; &nbsp; Inquiry?</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>  
                <div class="row powerby">
                    <div class="col-md-12">
                        <span>Powered By</span>  <img src="<?php echo base_url(); ?>img/powerby.png">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>

</html>

