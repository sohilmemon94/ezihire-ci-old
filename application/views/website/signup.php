<?php
defined('BASEPATH') OR exit('No direct script access allowed');  
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/ico" sizes="16x16" href="<?php echo base_url(); ?>img/favicon.png">
    <title><?php echo PROJECT_NAME; ?> | Signup</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url(); ?>css/colors/default-dark.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
  <style>
  .userform label {
    width: 120px;
    color: #333;
    float: left;
}
input.error {
    border: 1px solid red;
}
label.error{
    width: 100%;
    color: red;
    font-style: normal !important;
    margin-left: 0px !important;
    margin-bottom: 5px;
}
</style>

<?php 
$returnpath = $this->config->item('customers_images_uploaded_path');
$local_image = "background.jpg";
if(isset($adminmaster->background_image) && !empty($adminmaster->background_image)) { 
    $background_image = $adminmaster->background_image;
    $imagefullpath = base_url().$background_image;
    if(!empty($background_image)) {
        if(!file_exists($imagefullpath)){
            $imagefullpath = $returnpath.$background_image;
        } else {
            $imagefullpath = $returnpath.$local_image;
        }
    }
} else {
    $imagefullpath = $returnpath.$local_image;
} ?>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper">
        
        <div class="login-register" style="background-image:url('<?php echo $imagefullpath; ?>');position:initial !important;">
          
            <div class="login-box card" style="width:500px;">
                <div class="card-body">
                           <form class="form-horizontal form-material" name="signupform" id="signupform" method="post"  enctype="multipart/form-data">
                    <h3 class="box-title m-b-20"> <img src="<?php echo base_url(); ?>img/logo.png" style="align: center;margin: 0 auto;display: block;"></h3>

                    
                    <?php if(isset($error)){ echo $error; }
                                   echo $this->session->flashdata("message"); ?>

                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="username" id="username" placeholder="Company Name"></div>
                    </div>


                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="mobileno" id="mobileno"  placeholder="Mobile No"></div>
                    </div>


                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" name="email" placeholder="Email" id="email"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" id="password" name="password" placeholder="Password"> </div>
                    </div>
                        
                    <?php if ($_SERVER['HTTP_HOST'] != "localhost") { ?> 
                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <div class="g-recaptcha" data-sitekey="<?php echo $this->config->item('google_sitekey') ?>"></div>
                            </div>
                        </div>
                    <?php } ?>

                        <div class="form-group">
                           <div class="col-md-12">
                                <span class="pull-left"><a href="<?php echo base_url(); ?>inquiry"><i class="fa fa-question-circle m-r-5"></i> Inquiry ?</a></span>
                                <span class="pull-right"><a href="<?php echo base_url(); ?>login"><i class="fa fa-lock m-r-5"></i>Log In ?</a></span>
                            </div>
                        </div>


                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block waves-effect waves-light" type="submit" id="btnContactUs">Signup</button>
                        </div>
                    </div>
                  
                </form>
                
                </div>
            </div>
        </div>

    </section>

     <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url(); ?>assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>
</body>


</html>
<script>
   $(document).ready( function() {
    $('#error').delay(3000).fadeOut();
  });
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?php echo base_url(); ?>js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>js/additional-methods.min.js"></script>
<script>

$(function() {
  $("#signupform").validate({
    rules: {
        username:{
            required:true,
            remote: {
                url: "<?php echo base_url(); ?>check_company_name_exist",
                type: "POST",
                data: {
                    username: function(){ return $("#username").val(); },
                }
            },
        },           
        mobileno:{
            required:true,
            number:true,
        },            
        email: {
            required:true,
            email:true,
            remote: {
                url: "<?php echo base_url(); ?>check_email_exist",
                type: "POST",
                data: {
                    email: function(){ return $("#email").val(); },
                }
            },
        },
        password: {
            required:true,
        },
    },
    messages: {
        username:{
            required:"Please enter company name",
            remote: "Company already exist !",
        },            
        mobileno:{
            required:"Please enter mobile no",
        },            
        email: {
            required:"Please enter email",
            email:"Please enter valid email",
            remote: "Email Id already registered with us !",
        },
        password: {
            required:"Please enter password",
        },
    },
    submitHandler: function(form) {
      form.submit();
    }
  });

    $('#btnContactUs').click(function() {
        if($('#signupform').valid()){
            $(this).attr('disabled', 'disabled');
            var html ="<span class='spinner-border' role='status' aria-hidden='true'></span> Connecting...";
            $(this).html('');
            $(this).html(html);
            $(this).parents('form').submit();
        }
    });
});
</script>   