<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// echo $_SERVER['DOCUMENT_ROOT'];
// echo "<Br>";
// echo FCPATH;

include_once FCPATH.'vendor/autoload.php';

class Pdf {

    public $param;
    public $pdf;

    public function __construct($param = array(''))
    {
        $this->param =$param;
        $mpdfConfig = array(
            'mode' => 'utf-8',
            'format' => 'A4',    // format - A4, for example, default ''
            'margin_left' => 5, 
            'margin_right' => 5,
            'margin_top' => 10,
            'margin_bottom' => 10,
            'orientation' => 'P'  	// L - landscape, P - portrait
        );
        
        $this->pdf = new \Mpdf\Mpdf($mpdfConfig);
        // $this->pdf->SetFont('ocrb');
        $this->pdf->SetMargins(10, 10, 10, 10 );

    }
}