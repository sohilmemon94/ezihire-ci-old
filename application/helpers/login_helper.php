<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('_is_user_login'))
{
     function _is_user_login($thi)
    {
        $userid = _get_current_user_id($thi);
        $usertype = _get_current_user_type_id($thi);
       //echo  $is_access = _get_user_type_access($thi,$usertype);
        $is_access = true;
        if(isset($userid) && $userid!="" && isset($usertype))
        {
            if($is_access == true){
                 return $userid;
            }else{
                $thi->load->view("admin/common/not_access");
                return false;    
            }
            
        }else
        {
           
            return false;
        }

    }   
}
    
    function dd($array){
        echo "<pre>";
        print_r($array);
        echo "</pre>";
        exit;        
    }

   
    function get_user_status() {
            $customer_status = 0;
            if(isset($_SESSION['customer_id']) && !empty($_SESSION['customer_id'])){
                $customer_id = $_SESSION['customer_id'];
                $CI = get_instance();
                $CI->load->model("customers_model");
                $customer_details = $CI->customers_model->get_detail_by_id($customer_id);
                if(isset($customer_details) && !empty($customer_details)) {
                    $customer_status = $customer_details->status;
                } 
            } 
            return $customer_status;
    }

    function auto_signout() {
        if(isset($_SESSION['customer_id']) && !empty($_SESSION['customer_id'])){

            $customer_id = $_SESSION['customer_id'];
            if(isset($customer_id)) {
                if($customer_id >= 1) {
                    session_destroy();
                    foreach($_SESSION as $k=>$v){
                        unset($_SESSION[$v]);
                    }
                    if (isset($_COOKIE['customer_login'])) {
                        unset($_COOKIE['customer_login']); 
                        setcookie('customer_login', null, -1, '/'); 
                    } 
                    redirect("admin/login");
                    exit;
                } else {
                    redirect("admin/login");
                    exit;
                }
            } else {
                redirect("admin/login");
                exit;
            }
        }
    }

    function get_my_permission(){
            $customer_permission_arr = array();
            if(isset($_SESSION['customer_id']) && !empty($_SESSION['customer_id'])){
                $customer_id = $_SESSION['customer_id'];
                $CI = get_instance();
                $CI->load->model("customers_model");
                $match_permission = $CI->customers_model->get_customers_permission_list_by_id($customer_id);
                $match_permission_arr = array();
                if(count($match_permission)>0){
                    foreach($match_permission as $k=>$v) {
                        $match_permission_arr[] = $v->name;
                    }
                }
                $customer_permission_arr = $match_permission_arr;
            } 
            return $customer_permission_arr;
    }
    

if ( ! function_exists('_is_business_login'))
{
    function _is_business_login($thi)
    {
        $business_id = _get_current_business_id($thi);
       //echo  $is_access = _get_user_type_access($thi,$usertype);
        $is_access = true;
        if(isset($business_id) && $business_id!="")
        {
            if($is_access == true){
                 return $business_id;
            }else{
                $thi->load->view("admin/common/not_access");
                return false;    
            }
            
        }else
        {
           
            return false;
        }
    }   
}


if ( ! function_exists('_is_customer_login'))
{
     function _is_customer_login($thi)
    {
        $userid = _get_current_customer_id($thi);
        $usertype = _get_current_customer_type_id($thi);
       //echo  $is_access = _get_user_type_access($thi,$usertype);
        $is_access = true;
        if(isset($userid) && $userid!="" && isset($usertype)){
            if($is_access == true){
                 return true;
            } else {
                $thi->load->view("admin/common/not_access");
                return false;    
            }
        } else {
            return false;
        }
    }   
}




if ( ! function_exists('_is_store_login'))
{
     function _is_store_login($thi)
    {
        $userid = _get_current_store_id($thi);
        
        $usertype = _get_current_store_type_id($thi);
       //echo  $is_access = _get_user_type_access($thi,$usertype);
        $is_access = true;
        if(isset($userid) && $userid!="" && isset($usertype))
        {
            if($is_access == true){
                 return true;
            }else{
                $thi->load->view("admin/common/not_access");
                return false;    
            }
            
        }else
        {
           
            return false;
        }

    }   
}


if ( ! function_exists('_is_frontend_user_login'))
{
     function _is_frontend_user_login($thi)
    {
        $userid = _get_current_user_id($thi);
        $usertype = _get_current_user_type_id($thi);
         
        if(isset($userid) && $userid!="" && isset($usertype))
        {
                 return true;
        }else
        {
           
            return false;
        }

    }   
}
if(! function_exists('_get_post_back')){
    function _get_post_back($thi,$post){
        return ($thi->input->post($post)!="")? $thi->input->post($post) : ""; ;
    }
}
if(! function_exists('_get_current_user_id')){
    function _get_current_user_id($thi){
        return $thi->session->userdata("admin_id");
    }
}

if(! function_exists('_get_current_business_id')){
    function _get_current_business_id($thi){
        return $thi->session->userdata("business_id");
    }
}

if(! function_exists('_get_current_customer_id')){
    function _get_current_customer_id($thi){
        return $thi->session->userdata("customer_id");
    }
}

if(! function_exists('_get_current_rto_customer_id')){
    function _get_current_rto_customer_id($thi){
        return $thi->session->userdata("rto_customer_id");
    }
}



if(! function_exists('_get_current_store_id')){
    function _get_current_store_id($thi){
        return $thi->session->userdata("store_user_id");
    }
}


if(! function_exists('_get_current_user_name')){
    function _get_current_user_name($thi){
        return $thi->session->userdata("user_name");
    }
}

if(! function_exists('_get_current_customer_type_id')){
    function _get_current_customer_type_id($thi){
        return $thi->session->userdata("customer_type_id");
    }
}


if(! function_exists('_get_current_rto_customer_type_id')){
    function _get_current_rto_customer_type_id($thi){
        return $thi->session->userdata("rto_customer_type_id");
    }
}


if(! function_exists('_get_current_user_type_id')){
    function _get_current_user_type_id($thi){
        return $thi->session->userdata("user_type_id");
    }
}


if(! function_exists('_get_current_store_type_id')){
    function _get_current_store_type_id($thi){
        return $thi->session->userdata("store_type_id");
    }
}


/*
if(! function_exists('_get_user_type_access')){
    function _get_user_type_access($thi,$user_type_id){
            $cur_class = $thi->router->fetch_class();
            $cur_method = $thi->router->fetch_method();
            $result = $thi->db->query("select * from user_type_access where user_type_id = '".$user_type_id."' and class = '".$cur_class."' and (method = '".$cur_method."' or method='*')");
            
            $row = $result->row();
            
            if($result->num_rows() > 0){
                return true;
            }else{
                return false;
            }
            return false;
    }
} */
if(! function_exists('_get_user_redirect')){
    function _get_user_redirect($thi){
                            if(_get_current_user_type_id($thi)==0)
                            {
                                return "admin/dashboard";
                            }
                            else if(_get_current_user_type_id($thi)==3)
                            {
                                return "admin/dashboard";
                            }else if(_get_current_user_type_id($thi)==1)
                            {
                                return "admin/dashboard";
                            }
    }
}


if(! function_exists('_get_store_redirect')){
    function _get_store_redirect($thi){
                            if(_get_current_store_type_id($thi)==0)
                            {
                                return "restaurant/dashboard";
                            }
                            else if(_get_current_store_type_id($thi)==3)
                            {
                                return "restaurant/dashboard";
                            }else if(_get_current_store_type_id($thi)==1)
                            {
                                return "restaurant/dashboard";
                            }
    }
}


if(! function_exists('_get_customer_redirect')){
    function _get_customer_redirect($thi){
        if(_get_current_customer_type_id($thi)==0)
        {
            return "admin/dashboard";
        }
        else if(_get_current_customer_type_id($thi)==3)
        {
            return "admin/dashboard";
        }else if(_get_current_customer_type_id($thi)==1)
        {
            return "admin/dashboard";
        }
    }
}







if(! function_exists('_is_active_menu')){
    function _is_active_menu($thi,$class,$method){
        $c_class = $thi->router->fetch_class();
        $c_method = $thi->router->fetch_method();
        if(in_array($c_class,$class)){
            return "active";
        }
        if(in_array($c_method,$method)){
            return "active";
        }
    }
}