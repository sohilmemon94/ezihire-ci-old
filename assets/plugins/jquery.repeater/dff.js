var room = 1;

function education_fields() {

    room++;
    var objTo = document.getElementById('education_fields')
    var divtest = document.createElement("div");
    divtest.setAttribute("class", "form-group removeclass" + room);
    var rdiv = 'removeclass' + room;
    divtest.innerHTML = '<div class="row"><div class="col-sm-3"><div class="form-group"><input type="text" class="form-control" id="graduation" name="graduation[]" placeholder="Graduation/PG/Any Other Courses"></div></div><div class="col-sm-2"> <div class="form-group"> <input type="text" class="form-control" id="passing_year" name="passing_year[]" placeholder="Passing Year"> </div></div><div class="col-sm-2"> <div class="form-group"> <input type="text" class="form-control" id="board_university" name="board_university[]" placeholder="Board/University Name"> </div></div><div class="col-sm-3"> <div class="form-group"> <input type="text" class="form-control" id="percentage" name="percentage[]" placeholder="Percentage"> </div></div><div class="col-sm-2"> <div class="form-group"> <button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> <i class="fa fa-minus"></i> </button> </div></div></div>';

    objTo.appendChild(divtest)
}

function remove_education_fields(rid) {
    $('.removeclass' + rid).remove();
}