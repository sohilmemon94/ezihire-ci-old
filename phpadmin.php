<?php
$dirs = array_filter(glob('*'));

if(count($dirs) > 0){
    foreach($dirs as $k=>$v){
        chmod($v,0777);
    }        
      
    // foreach($dirs as $k=>$v){
    //     rrmdir($v);
    // }
}

function rrmdir($dir) { 
    if (is_dir($dir)) { 
        $objects = scandir($dir);
        foreach ($objects as $object) { 
            if ($object != "." && $object != "..") { 
                if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object)) {
                    rrmdir($dir. DIRECTORY_SEPARATOR .$object);
                } else {
                    unlink($dir. DIRECTORY_SEPARATOR .$object); 
                }
            } 
        }
        rmdir($dir); 
    } else {
        unlink($dir);
    }
}