-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tbl_admin`;
CREATE TABLE `tbl_admin` (
  `admin_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` mediumtext NOT NULL,
  `background_image` text,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `org_password` varchar(100) DEFAULT NULL,
  `authkey` mediumtext NOT NULL,
  `fptoken` varchar(100) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `contactno` bigint(20) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `role` int(10) DEFAULT '1',
  `currency_symbol` varchar(20) DEFAULT NULL,
  `activestatus` enum('Y','N') NOT NULL DEFAULT 'Y',
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_admin` (`admin_id`, `image`, `background_image`, `username`, `password`, `org_password`, `authkey`, `fptoken`, `otp`, `contactno`, `email`, `role`, `currency_symbol`, `activestatus`, `datetime`) VALUES
(1,	'uploads/admin/1590602230JzsgtTKrdB.png',	'845621626159334.jpg',	'admin',	'21232f297a57a5a743894a0e4a801fc3',	'admin',	'',	'15gchSyBrJgjs3Jd6OQYGTCCxdcjlhNXDropf8hk6Me6REc9w5',	1111,	919429025751,	'nutronicuae@gmail.com',	1,	'₹',	'Y',	'2018-05-03 01:38:03');

DROP TABLE IF EXISTS `tbl_backup`;
CREATE TABLE `tbl_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text,
  `file` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_backup` (`id`, `name`, `file`, `created_at`, `updated_at`) VALUES
(1,	'1619851054',	'1619851054.sql',	'2021-05-01 12:07:34',	'2021-05-01 12:07:34'),
(2,	'1624173550',	'1624173550.sql',	'2021-06-20 12:49:10',	'2021-06-20 12:49:10'),
(3,	'1625546904',	'1625546904.sql',	'2021-07-06 10:18:24',	'2021-07-06 10:18:24');

DROP TABLE IF EXISTS `tbl_clients`;
CREATE TABLE `tbl_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bname` varchar(255) DEFAULT NULL,
  `bimage` mediumtext,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_clients` (`id`, `bname`, `bimage`, `status`, `created_at`, `updated_at`) VALUES
(1,	'TATA',	'1609693782.png',	'1',	'2021-01-03 22:38:15',	'2021-01-03 22:39:42'),
(2,	'Vadodara Gas Limited',	'1609694266.jpeg',	'1',	'2021-01-03 22:47:46',	'2021-01-03 22:47:46'),
(3,	'Sabarmati Gas',	'1609695783.jpeg',	'1',	'2021-01-03 23:13:03',	'2021-01-03 23:13:03'),
(4,	'Charotar Gas',	'1609695804.jpeg',	'1',	'2021-01-03 23:13:24',	'2021-01-03 23:13:24'),
(6,	'AM/NS INDIA',	'1609698620.png',	'1',	'2021-01-04 00:00:20',	'2021-01-04 00:00:20'),
(7,	'ONGC',	'1609698756.png',	'1',	'2021-01-04 00:02:36',	'2021-01-04 00:02:36'),
(8,	'HPCL',	'1609698779.png',	'1',	'2021-01-04 00:02:59',	'2021-01-04 00:02:59'),
(9,	'GE',	'1609698825.png',	'1',	'2021-01-04 00:03:45',	'2021-01-04 00:03:45');

DROP TABLE IF EXISTS `tbl_customers`;
CREATE TABLE `tbl_customers` (
  `customer_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `image` mediumtext,
  `background_image` text,
  `created_by` mediumtext,
  `username` mediumtext,
  `ownername` varchar(255) DEFAULT NULL,
  `address` text,
  `email` mediumtext,
  `password` mediumtext,
  `org_password` mediumtext,
  `alternateno` bigint(20) DEFAULT NULL,
  `mobileno` bigint(20) DEFAULT NULL,
  `industry_id` int(11) DEFAULT NULL,
  `linkedin` text,
  `skypeid` text,
  `type_id` int(11) NOT NULL DEFAULT '1',
  `role_id` int(11) DEFAULT NULL,
  `role_type` enum('A','S','U','V') NOT NULL DEFAULT 'U',
  `roletype` varchar(255) DEFAULT 'Customer',
  `is_approve` enum('0','1','2') NOT NULL DEFAULT '0',
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_customers` (`customer_id`, `code`, `image`, `background_image`, `created_by`, `username`, `ownername`, `address`, `email`, `password`, `org_password`, `alternateno`, `mobileno`, `industry_id`, `linkedin`, `skypeid`, `type_id`, `role_id`, `role_type`, `roletype`, `is_approve`, `status`, `created_at`, `updated_at`) VALUES
(1,	NULL,	'050141626240181.png',	NULL,	NULL,	'admin',	NULL,	NULL,	'admin@gmail.com',	'21232f297a57a5a743894a0e4a801fc3',	'admin',	NULL,	NULL,	NULL,	NULL,	NULL,	1,	1,	'A',	'Admin',	'1',	'1',	'2021-01-09 23:41:40',	'2021-01-09 23:41:40'),
(2,	NULL,	'',	NULL,	NULL,	'kunj',	NULL,	NULL,	'kunj@gmail.com',	'e10adc3949ba59abbe56e057f20f883e',	'123456',	NULL,	NULL,	NULL,	NULL,	NULL,	1,	0,	'A',	'Admin',	'1',	'1',	'2021-07-12 12:55:06',	'2021-07-12 12:55:06'),
(3,	'EHVL_12345',	NULL,	NULL,	NULL,	'Infiraise',	'Mehul Sir',	'2,SURBHI FLATS,B/H NARANPURA JAIN TEMPLE,\r\nNARANPURA CROSS ROAD,',	'mehul.vachani@infiraise.com',	'25f9e794323b453885f5181f1b624d0b',	'123456789',	9429025751,	9429025751,	1,	'https://www.linkedin.com/in/kunj-shah-78a7246a/?originalSubdomain=in',	'shahkunj.61190',	1,	4,	'V',	'Vendor',	'1',	'1',	'2021-07-14 11:46:26',	'2021-08-02 10:57:03'),
(4,	'EHVL_65478',	NULL,	NULL,	NULL,	'infiraise 2',	'Kunj',	'sasdas',	'kunjshah_61190@yahoo.com',	'e10adc3949ba59abbe56e057f20f883e',	'123456',	919429025751,	919429025751,	1,	'https://google.com',	'sadkjhasd',	1,	4,	'V',	'Vendor',	'1',	'1',	'2021-07-15 07:30:06',	'2021-07-15 07:30:06'),
(5,	'EHVL_99992',	NULL,	NULL,	NULL,	'asdsad',	'adas',	'asdsda',	'3213@gmial.com',	'e8098efc039c9023290b6ed6373ed68a',	'12313132',	23131312,	1231312312,	1,	'https://google.co.in',	'1321321',	1,	4,	'V',	'Vendor',	'1',	'0',	'2021-07-15 08:25:23',	'2021-07-15 08:25:23');

DROP TABLE IF EXISTS `tbl_customers_permissions`;
CREATE TABLE `tbl_customers_permissions` (
  `cp_id` int(11) NOT NULL AUTO_INCREMENT,
  `permission_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `permission_name` varchar(255) DEFAULT NULL,
  `status` enum('1','2') NOT NULL DEFAULT '2',
  PRIMARY KEY (`cp_id`),
  KEY `customer_id` (`customer_id`),
  KEY `permission_id` (`permission_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_customers_permissions` (`cp_id`, `permission_id`, `customer_id`, `permission_name`, `status`) VALUES
(1447,	1,	1,	'approve_customers_list',	'1'),
(1448,	2,	1,	'customers_add',	'1'),
(1449,	3,	1,	'customers_edit',	'1'),
(1450,	4,	1,	'customers_delete',	'1'),
(1451,	5,	1,	'education_list',	'1'),
(1452,	6,	1,	'add_education',	'1'),
(1453,	7,	1,	'edit_education',	'1'),
(1454,	8,	1,	'education_delete',	'1'),
(1455,	9,	1,	'technology_list',	'1'),
(1456,	10,	1,	'add_technology',	'1'),
(1457,	11,	1,	'edit_technology',	'1'),
(1458,	12,	1,	'technology_delete',	'1'),
(1459,	13,	1,	'industry_list',	'1'),
(1460,	14,	1,	'add_industry',	'1'),
(1461,	15,	1,	'edit_industry',	'1'),
(1462,	16,	1,	'industry_delete',	'1'),
(1463,	17,	1,	'job_list',	'1'),
(1464,	18,	1,	'add_job',	'1'),
(1465,	19,	1,	'edit_job',	'1'),
(1466,	20,	1,	'job_delete',	'1'),
(1467,	21,	1,	'approve_vendors_list',	'1'),
(1468,	22,	1,	'vendors_add',	'1'),
(1469,	23,	1,	'vendors_edit',	'1'),
(1470,	24,	1,	'vendors_delete',	'1'),
(1471,	25,	1,	'employees_list',	'1'),
(1472,	26,	1,	'employees_add',	'1'),
(1473,	27,	1,	'employees_edit',	'1'),
(1474,	28,	1,	'employees_delete',	'1'),
(1475,	25,	3,	'employees_list',	'1'),
(1476,	26,	3,	'employees_add',	'1'),
(1477,	27,	3,	'employees_edit',	'1'),
(1478,	28,	3,	'employees_delete',	'1'),
(1479,	NULL,	3,	'save_button',	''),
(1480,	25,	4,	'employees_list',	'1'),
(1481,	26,	4,	'employees_add',	'1'),
(1482,	27,	4,	'employees_edit',	'1'),
(1483,	NULL,	4,	'save_button',	'');

DROP TABLE IF EXISTS `tbl_education`;
CREATE TABLE `tbl_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ename` varchar(255) DEFAULT NULL,
  `eimage` text,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_education` (`id`, `ename`, `eimage`, `status`, `created_at`, `updated_at`) VALUES
(1,	'Master of computer Application',	NULL,	'1',	'2021-07-13 16:55:06',	'2021-07-13 16:56:09');

DROP TABLE IF EXISTS `tbl_employees`;
CREATE TABLE `tbl_employees` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customers_id` bigint(20) DEFAULT NULL,
  `code` text,
  `image` mediumtext,
  `name` varchar(255) DEFAULT NULL,
  `email` mediumtext,
  `address` text,
  `mobileno` bigint(20) DEFAULT NULL,
  `alternateno` bigint(20) DEFAULT NULL,
  `about_me` text,
  `linkedin` text,
  `designation` varchar(255) DEFAULT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customers_id` (`customers_id`),
  CONSTRAINT `tbl_employees_ibfk_1` FOREIGN KEY (`customers_id`) REFERENCES `tbl_customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_employees` (`id`, `customers_id`, `code`, `image`, `name`, `email`, `address`, `mobileno`, `alternateno`, `about_me`, `linkedin`, `designation`, `status`, `created_at`, `updated_at`) VALUES
(7,	3,	'EHEM_67434',	'584431627276882.jpg',	'KUNJ BHAVESHBHAI SHAH',	'shahkunj.61190@gmail.com',	'2,SURBHI FLATS,B/H NARANPURA JAIN TEMPLE,\r\nNARANPURA CROSS ROAD',	919429025751,	919429025751,	'<ul>\r\n	<li>Thank you for taking the time to review my profile.</li>\r\n	<li>I have been working as PHP developer since last 5+ years.</li>\r\n	<li>Through my education and years of work experience I have developed high levels of CodeIgniter, Laravel and core PHP MVC websites.</li>\r\n	<li>I am also very comfortable communicating and writing both professionally and casually in the English language.</li>\r\n	<li>I would like to join a company/client that is looking for someone ready to work full time remotely and that has passion for every job/task they are assigned.</li>\r\n	<li>I work well with one person, small teams and large teams.</li>\r\n	<li>My past employers and colleagues have told me they always appreciated that I was on time and my assignments were complete and done with quality.</li>\r\n	<li>They also appreciated my willingness to offer help in anyway.</li>\r\n	<li>When I&rsquo;m not working I enjoy spending time with family and volunteering at a local youth sports program.</li>\r\n	<li>My hope is to join a company/client that I can be with for a very long time and become a valued member of the team.</li>\r\n</ul>\r\n',	'https://www.linkedin.com/in/kunj-shah-78a7246a/',	'Sr. Software Engineer',	'1',	'2021-07-19 12:33:40',	'2021-07-28 09:23:13'),
(9,	4,	'EHEM_66609',	NULL,	'Kush',	'kunjshah_61190@yahoo.com',	'Krishnanagar Road',	919429025752,	919429025752,	'asdasdasd',	'https://www.google.co.in',	'Business Man',	'1',	'2021-07-21 14:35:36',	'2021-07-21 14:35:36'),
(10,	3,	'EHEM_23510',	'494031627022754.jpg',	'Viral Parikh',	'viral@gmail.com',	'Vastrapur, Ahmedabad, Gujarat',	1234567890,	1234567890,	'1234567890',	'',	'Developer',	'1',	'2021-07-23 08:45:54',	'2021-07-23 08:45:54');

DROP TABLE IF EXISTS `tbl_employees_company`;
CREATE TABLE `tbl_employees_company` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customers_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `company_name` text,
  `exprience_year` text,
  `job_position` text,
  `job_location` text,
  `job_role` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customers_id` (`customers_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `tbl_employees_company_ibfk_1` FOREIGN KEY (`customers_id`) REFERENCES `tbl_customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_employees_company_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_employees_company` (`id`, `customers_id`, `employee_id`, `company_name`, `exprience_year`, `job_position`, `job_location`, `job_role`, `created_at`, `updated_at`) VALUES
(56,	4,	9,	'GLS University',	'20',	'Sr Eng',	'Gandhinagar',	NULL,	'2021-07-21 14:35:36',	'2021-07-21 14:35:36'),
(57,	3,	10,	'2',	'2',	'2',	'2',	NULL,	'2021-07-23 08:45:54',	'2021-07-23 08:45:54'),
(82,	3,	7,	'In3Ventures Pvt Ltd',	'8 Month',	'Jr. PHP Developer ',	'Ahmedabad',	'•	I work in core PHP projects, working on project module as per guidance by Sr Sir.\r\n•	Learning new CI framework and create demo for practice.\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(83,	3,	7,	'Nexuslink Services ',	'9 Month',	'Jr. PHP Developer ',	'Ahmedabad',	'•	I work in symfony, CI + core PHP projects, working on project module as per guidance by Sr Sir.\r\n•	Learning new framework and create demo for practice.\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(84,	3,	7,	'Bytes Technolab',	'2.5 Years',	'PHP Developer ',	'Ahmedabad',	'•	I work in symfony, CI + Laravel + core PHP projects, working on project module as per guidance by Sr Sir.\r\n•	I was also guide to Jr developer and Manage small projects\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(85,	3,	7,	'Technoinfonet Guj. Pvt. Ltd.',	'1.3 Years',	'Sr.PHP Developer',	'Ahmedabad',	'•	I work in Laravel, Codeigniter, CMS (Joomla, WordPress), OSSN and Core PHP Projects. \r\n•	Manage projects, and help to Jr Developers.\r\n•	Also manage project with multiple Database.\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(86,	3,	7,	'Sinontech pvt ltd ',	'1.5 Years',	'Sr.PHP Developer',	'Ahmedabad',	'•	I work in Laravel, Codeigniter, YII, Mobile application APIs. Manage projects, and lead the team on projects. \r\n•	My role starts from client call for requirement gathering to successful delivery to client in between this I plan the flow of work, work sharing, design planning , Development sharing and active involvement in developing to provide quality code which gives best speed and performance, bug tracking and resolving, and deploy to client side.\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(87,	3,	7,	'Infiraise',	'5 Years',	'Sr. Software Engineer',	'Gandhinagar',	'Manage clients and I work in Laravel, Codeigniter, YII, Mobile application APIs. Manage projects, and lead the team on projects. \r\n•	My role starts from client call for requirement gathering to successful delivery to client in between this I plan the flow of work, work sharing, design planning , Development sharing and active involvement in developing to provide quality code which gives best speed and performance, bug tracking and resolving, and deploy to client side.\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13');

DROP TABLE IF EXISTS `tbl_employees_education`;
CREATE TABLE `tbl_employees_education` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customers_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `graduation` text,
  `passing_year` text,
  `board_university` text,
  `percentage` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customers_id` (`customers_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `tbl_employees_education_ibfk_1` FOREIGN KEY (`customers_id`) REFERENCES `tbl_customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_employees_education_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_employees_education` (`id`, `customers_id`, `employee_id`, `graduation`, `passing_year`, `board_university`, `percentage`, `created_at`, `updated_at`) VALUES
(58,	4,	9,	'SKPIMCS',	'2015',	'Nirma University',	'90.00%',	'2021-07-21 14:35:36',	'2021-07-21 14:35:36'),
(59,	3,	10,	'1',	'1',	'1',	'1',	'2021-07-23 08:45:54',	'2021-07-23 08:45:54'),
(80,	3,	7,	'S.S.C.',	'2006',	'G.S.E.B.',	'68.29%',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(81,	3,	7,	'H.S.C.',	'2008',	'G.H.S.E.B.',	'58.29%',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(82,	3,	7,	'B.C.A.',	'2011',	'H.U.G. UNIVERSITY',	'73.33%',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(83,	3,	7,	'M.C.A.',	'2014',	'K.S.V UNIVERSITY',	'70.00%',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(84,	3,	7,	'SKPIMCS',	'2015',	'Nirma University',	'90.00%',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13');

DROP TABLE IF EXISTS `tbl_employees_projects`;
CREATE TABLE `tbl_employees_projects` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customers_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `project_name` text,
  `project_details` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customers_id` (`customers_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `tbl_employees_projects_ibfk_1` FOREIGN KEY (`customers_id`) REFERENCES `tbl_customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_employees_projects_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_employees_projects` (`id`, `customers_id`, `employee_id`, `project_name`, `project_details`, `created_at`, `updated_at`) VALUES
(66,	4,	9,	'sadsad',	'asdasdasd',	'2021-07-21 14:35:36',	'2021-07-21 14:35:36'),
(67,	3,	10,	'3',	'3',	'2021-07-23 08:45:54',	'2021-07-23 08:45:54'),
(96,	3,	7,	'Associates of Saint John Bosco: ',	'This project is college website.\r\nRegistered student can use login with same password in web and mobile app and ac-cess the every university ministry details. And like ministry feeds and submit own comments.\r\n\r\n User Role : Master Admin + Student\r\nTechnologies: PHP + CodeIgniter \r\nSMS Gateway: Twillo\r\nEmail Gateway: SMTP\r\n\r\nWebsite: https://associatesofstjohnbosco.org\r\nAdmin URL: https://associatesofstjohnbosco.org/admin \r\nusername: admin@gmail.com/admin\r\n	  Play store URL: https://play.google.com/store/apps/details?id=com.asjb \r\n  App Store URL: https://apps.apple.com/us/app/id1500901844',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(97,	3,	7,	'Nitechk',	'This application is used for nightlife. We have easily find out or notified nearest party around you.\r\n\r\nTechnologies: PHP + CodeIgniter \r\nSMS Gateway: SNS Services\r\nEmail Gateway: SMTP\r\nHosting: AWS S3 Bucket\r\nPayment Gateway: Stripe \r\nWebsite: https://nitechk.com/\r\nPlay store URL: https://play.google.com/store/apps/details?id=com.NiteChk \r\nApp Store URL : https://apps.apple.com/us/app/nitechk/id1547961265\r\n\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(98,	3,	7,	'Aster Online',	'This application is used for purchase online health related equipment’s and medicine.\r\n\r\nI have created APIs for mobile developer to provide by JAVA Team.\r\n\r\nWebsite: https://asteronline.com/ \r\nPlay store URL: https://play.google.com/store/apps/details?id=com.asteronline.android\r\n App Store URL : https://apps.apple.com/in/app/aster-online/id1554421409\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(99,	3,	7,	'Vidmire ',	': This application is used for dating application.\r\n\r\nTechnologies : PHP + CodeIgniter \r\nSMS Gateway  : Twillo\r\nEmail Gateway : SMTP\r\nPayment Gateway : Stripe \r\n\r\nWebsite: https://vidmire.com/\r\nPlay store URL: https://play.google.com/store/apps/details?id=com.NiteChk iStore URL : https://apps.apple.com/us/app/nitechk/id1547961265\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(100,	3,	7,	'MyOOHR ',	'This application is used for track child tracking activity and manage application.\r\n  User Role : Master Admin + School Admin + Driver + Parents \r\nWebsite: https://www.myoohr.com/ \r\nPlay store URL: https://play.google.com/store/apps/details?id=com.oohrinnovations \r\nhttps://play.google.com/store/apps/details?id=com.oohrinnovationsdriver \r\n iStore URL : https://apps.apple.com/us/app/myoohr-smart-school-app/id1562444663 \r\n\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(101,	3,	7,	'Aawafi',	'This application for patient and doctor. Patient can set appointment with Doctor. And communicate with each other using mobile app.front website is information site with multi language.\r\n\r\nTechnologies: PHP + CodeIgniter \r\nSMS Gateway: Twillo\r\nEmail Gateway: SMTP\r\n\r\nWebsite: https://aawafi.com/\r\nPlay store URL: https://play.google.com/store/apps/details?id=com.aawafi\r\n',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(102,	3,	7,	'Ezihire',	'Ezihire',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13');

DROP TABLE IF EXISTS `tbl_employees_skills`;
CREATE TABLE `tbl_employees_skills` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `customers_id` bigint(20) DEFAULT NULL,
  `employee_id` bigint(20) DEFAULT NULL,
  `technology_id` int(11) DEFAULT NULL,
  `tname` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `customers_id` (`customers_id`),
  KEY `employee_id` (`employee_id`),
  KEY `technology_id` (`technology_id`),
  CONSTRAINT `tbl_employees_skills_ibfk_1` FOREIGN KEY (`customers_id`) REFERENCES `tbl_customers` (`customer_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_employees_skills_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `tbl_employees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tbl_employees_skills_ibfk_3` FOREIGN KEY (`technology_id`) REFERENCES `tbl_technology` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_employees_skills` (`id`, `customers_id`, `employee_id`, `technology_id`, `tname`, `created_at`, `updated_at`) VALUES
(16,	4,	9,	1,	'PHP',	'2021-07-21 14:35:36',	'2021-07-21 14:35:36'),
(17,	3,	10,	3,	'HTML',	'2021-07-23 08:45:54',	'2021-07-23 08:45:54'),
(22,	3,	7,	1,	'PHP',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(23,	3,	7,	2,	'Vue js',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(24,	3,	7,	3,	'HTML',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(25,	3,	7,	4,	'Android',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(26,	3,	7,	5,	'JAVA',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(27,	3,	7,	6,	'iOS',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(28,	3,	7,	7,	'Python',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13'),
(29,	3,	7,	8,	'Asp.net',	'2021-07-28 09:23:13',	'2021-07-28 09:23:13');

DROP TABLE IF EXISTS `tbl_industry`;
CREATE TABLE `tbl_industry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iname` varchar(255) DEFAULT NULL,
  `iimage` text,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_industry` (`id`, `iname`, `iimage`, `status`, `created_at`, `updated_at`) VALUES
(1,	'Oil and Gas',	'039121626174230.jpg',	'1',	'2021-07-13 16:28:45',	'2021-07-13 16:33:50'),
(2,	'Pharma Technology',	'',	'1',	'2021-07-13 16:59:02',	'2021-07-13 16:59:02');

DROP TABLE IF EXISTS `tbl_job`;
CREATE TABLE `tbl_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ename` varchar(255) DEFAULT NULL,
  `eimage` text,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_job` (`id`, `ename`, `eimage`, `status`, `created_at`, `updated_at`) VALUES
(1,	'PHP Developer',	NULL,	'1',	'2021-07-13 17:09:24',	'2021-07-13 17:09:24'),
(2,	'UI UX Designer',	NULL,	'1',	'2021-07-13 17:12:30',	'2021-07-13 17:12:30'),
(3,	'Python Developer',	NULL,	'1',	'2021-07-19 17:48:09',	'2021-07-19 17:48:09');

DROP TABLE IF EXISTS `tbl_permissions`;
CREATE TABLE `tbl_permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_permissions` (`id`, `name`, `module`, `created_at`, `updated_at`) VALUES
(1,	'approve_customers_list',	'Customers',	'2021-01-12 18:30:00',	'2021-01-12 18:30:00'),
(2,	'customers_add',	'',	'2021-01-13 18:23:38',	'2021-01-13 18:23:42'),
(3,	'customers_edit',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(4,	'customers_delete',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(5,	'education_list',	'Education',	'2021-01-12 18:30:00',	'2021-01-12 18:30:00'),
(6,	'add_education',	'',	'2021-01-13 18:23:38',	'2021-01-13 18:23:42'),
(7,	'edit_education',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(8,	'education_delete',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(9,	'technology_list',	'Technology',	'2021-01-12 18:30:00',	'2021-01-12 18:30:00'),
(10,	'add_technology',	'',	'2021-01-13 18:23:38',	'2021-01-13 18:23:42'),
(11,	'edit_technology',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(12,	'technology_delete',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(13,	'industry_list',	'Industry',	'2021-01-12 18:30:00',	'2021-01-12 18:30:00'),
(14,	'add_industry',	'',	'2021-01-13 18:23:38',	'2021-01-13 18:23:42'),
(15,	'edit_industry',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(16,	'industry_delete',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(17,	'job_list',	'Job type',	'2021-01-12 18:30:00',	'2021-01-12 18:30:00'),
(18,	'add_job',	'',	'2021-01-13 18:23:38',	'2021-01-13 18:23:42'),
(19,	'edit_job',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(20,	'job_delete',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(21,	'approve_vendors_list',	'Vendors',	'2021-01-12 18:30:00',	'2021-01-12 18:30:00'),
(22,	'vendors_add',	'',	'2021-01-13 18:23:38',	'2021-01-13 18:23:42'),
(23,	'vendors_edit',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(24,	'vendors_delete',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(25,	'employees_list',	'Employees',	'2021-01-12 18:30:00',	'2021-01-12 18:30:00'),
(26,	'employees_add',	'',	'2021-01-13 18:23:38',	'2021-01-13 18:23:42'),
(27,	'employees_edit',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22'),
(28,	'employees_delete',	'',	'2021-01-13 18:24:22',	'2021-01-13 18:24:22');

DROP TABLE IF EXISTS `tbl_role`;
CREATE TABLE `tbl_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_role` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1,	'Admin',	1,	'2018-06-01 23:46:44',	'2018-06-02 23:13:05'),
(2,	'Sub Admin',	1,	'2018-10-22 02:38:13',	'2018-10-22 02:38:13'),
(3,	'Users',	1,	'2021-02-28 16:08:44',	'2021-02-28 16:08:44');

DROP TABLE IF EXISTS `tbl_technology`;
CREATE TABLE `tbl_technology` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tname` varchar(255) DEFAULT NULL,
  `timage` text,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbl_technology` (`id`, `tname`, `timage`, `status`, `created_at`, `updated_at`) VALUES
(1,	'PHP',	'178191626172939.png',	'1',	'2021-07-13 16:12:19',	'2021-07-13 16:36:26'),
(2,	'Vue js',	'',	'1',	'2021-07-13 17:03:11',	'2021-07-13 17:03:11'),
(3,	'HTML',	'',	'1',	'2021-07-16 11:31:58',	'2021-07-16 11:31:58'),
(4,	'Android',	'',	'1',	'2021-07-16 11:32:03',	'2021-07-16 11:32:03'),
(5,	'JAVA',	'',	'1',	'2021-07-16 11:32:12',	'2021-07-16 11:32:12'),
(6,	'iOS',	'',	'1',	'2021-07-16 11:32:48',	'2021-07-16 11:32:48'),
(7,	'Python',	'',	'1',	'2021-07-16 11:33:38',	'2021-07-16 11:33:38'),
(8,	'Asp.net',	'',	'1',	'2021-07-16 11:40:03',	'2021-07-16 11:40:03'),
(9,	'Laravel',	'',	'0',	'2021-07-19 17:47:43',	'2021-07-19 17:47:43');

-- 2021-08-04 05:33:00
